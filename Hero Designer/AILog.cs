﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public class AILog
    {
        public static Dictionary<string, List<string>> Entries = new Dictionary<string, List<string>>();
        public static void Add(string key, string value)
        {
            if (!Entries.ContainsKey(key))
                Entries.Add(key, new List<string>());
            List<string> values;
            if (Entries.TryGetValue(key, out values))
            {
                string entry = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff");
                entry += " " + value;
                values.Add(entry);
            }
        }
        public static void RemoveAll()
        {
            Entries = new Dictionary<string, List<string>>();
        }
    }
}

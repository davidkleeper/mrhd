﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Hero_Designer
{
    public class AIStrategyEvolveSlots
    {

        public static AIStrategyEvolveSlotsData Do(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, AIConfig config, int configBonusIndex)
        {
            // Get power entry and a second random power entry.
            PowerEntry powerEntry = powerEntries[powerEntryIndex];
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            float originalSum = bonusInfoWithWeight.Sum(powerEntry, toon.Archetype);
            AIStrategyEvolveSlotsData slotStrategyData = NewAIStrategyEvolveSlotsData(powerEntry, powerEntryIndex, originalSum, powerEntry, powerEntryIndex, originalSum);

            int randomPowerEntryIndex = GetRandomPowerEntryIndex(powerEntryIndex, powerEntries.ToArray(), powerEntryIndex, config);
            if (-1 == randomPowerEntryIndex) return slotStrategyData;
            PowerEntry randomPowerEntry = powerEntries[randomPowerEntryIndex].Clone() as PowerEntry;
            float originalRandomSum = bonusInfoWithWeight.Sum(randomPowerEntry, toon.Archetype);
            slotStrategyData = NewAIStrategyEvolveSlotsData(powerEntry, powerEntryIndex, originalSum, randomPowerEntry, randomPowerEntryIndex, originalRandomSum);

            // if we cant't enhance this power, return.
            if (!bonusInfoWithWeight.CanEnhance(powerEntry))
                return slotStrategyData;

            // Clone the two original power entries. 
            PowerEntry powerEntryClone = powerEntry.Clone() as PowerEntry;
            PowerEntry randomPowerEntryClone = randomPowerEntry.Clone() as PowerEntry;

            // Move slots between the two clones.
            if (!PowerEntry.CanMoveSlots(randomPowerEntryClone, powerEntryClone, config.SlotFlags[powerEntryIndex]))
                return slotStrategyData;
            int slotsToMove = PowerEntry.GetMoveSlotCount(randomPowerEntryClone, powerEntryClone, config.SlotFlags[powerEntryIndex]);
            if (-1 == slotsToMove)
                return slotStrategyData;
            if (!PowerEntry.MoveSlots(randomPowerEntryClone, powerEntryClone, slotsToMove, config.SlotFlags[powerEntryIndex]))
                return slotStrategyData;

            // Set the toon to use the two clones.
            List<PowerEntry> powerEntriesClone = toon.GetPowerEntries();
            powerEntriesClone[powerEntryIndex] = powerEntryClone;
            powerEntriesClone[randomPowerEntryIndex] = randomPowerEntryClone;
            toon.SetPowerEntries(powerEntriesClone);

            // Slot the two clones and record their sums.
            powerEntryClone.ClearEnhancements();
            powerEntryClone = AISlotter.SlotPowerEntry(toon, powerEntriesClone, powerEntryIndex, config);
            powerEntriesClone[slotStrategyData.PowerEntryIndex1] = powerEntryClone;
            float cloneSum = bonusInfoWithWeight.Sum(powerEntryClone, toon.Archetype);
            randomPowerEntryClone.ClearEnhancements();
            randomPowerEntryClone = AISlotter.SlotPowerEntry(toon, powerEntriesClone, randomPowerEntryIndex, config);
            powerEntriesClone[slotStrategyData.PowerEntryIndex2] = randomPowerEntryClone;
            float cloneRandomSum = bonusInfoWithWeight.Sum(randomPowerEntryClone, toon.Archetype);
            toon.SetPowerEntries(powerEntriesClone);

            // If the resulting build is not valid, use the original.
            toon.SetDefaultSlotLevels();
            string message;
            if (!toon.IsValid(out message))
            {
                AILog.Add(message, "Class: AIStrategyEvolveSlots, Power: " + powerEntry.Power.DisplayName + ", Bonus: " + bonusInfoWithWeight.DisplayName + " REJECTED. Invalid Build.");
                toon.SetPowerEntries(powerEntries);
                return slotStrategyData;
            }
            if (!AIStrategyProtectPreviousBonuses.ProtectsPreviousBonuses(toon, powerEntries, powerEntryClone, powerEntryIndex, randomPowerEntryClone, randomPowerEntryIndex, config, configBonusIndex))
            {
                AILog.Add(message, "Class: AIStrategyEvolveSlots, Power: " + powerEntry.Power.DisplayName + ", Bonus: " + bonusInfoWithWeight.DisplayName + " REJECTED. Lowers Previous Bonus.");
                toon.SetPowerEntries(powerEntries);
                return slotStrategyData;
            }

            // Reset the toon.
            toon.SetPowerEntries(powerEntries);

            // If the two clones produced a better result, use them.
            if (cloneSum + cloneRandomSum > originalSum + originalRandomSum)
            {
                slotStrategyData.PowerEntry1 = powerEntryClone;
                slotStrategyData.PowerEntry2 = randomPowerEntryClone;
                slotStrategyData.Sum = cloneSum + cloneRandomSum;
            }
            return slotStrategyData;
        }

        private static AIStrategyEvolveSlotsData NewAIStrategyEvolveSlotsData(PowerEntry powerEntry1, int powerEntryIndex1, float sum1, PowerEntry powerEntry2, int powerEntryIndex2, float sum2)
        {
            AIStrategyEvolveSlotsData slotStrategyData = new AIStrategyEvolveSlotsData();
            slotStrategyData.PowerEntry1 = powerEntry1;
            slotStrategyData.PowerEntryIndex1 = powerEntryIndex1;
            slotStrategyData.PowerEntry2 = powerEntry2;
            slotStrategyData.PowerEntryIndex2 = powerEntryIndex2;
            slotStrategyData.Sum = sum1 + sum2;
            return slotStrategyData;
        }

        private static int GetRandomPowerEntryIndex(int startingIndex, PowerEntry[] powerEntries, int powerEntryIndex, AIConfig config)
        {
            int randomPowerEntryIndex = startingIndex;
            PowerEntry blockPowerEntry = powerEntries[randomPowerEntryIndex];
            bool blockPowerEntryFlag = config.PowerFlags[randomPowerEntryIndex];
            bool[] blockSlotFlags = config.SlotFlags[randomPowerEntryIndex];
            Random rnd = new Random();
            int attemps = 0;

            while (randomPowerEntryIndex == powerEntryIndex
            || blockPowerEntry.Slots.Length - PowerEntry.GetLockedSlotsCount(blockSlotFlags) <= 1
            || !blockPowerEntryFlag)
            {
                randomPowerEntryIndex = rnd.Next(1, powerEntries.Length) - 1;
                blockPowerEntry = powerEntries[randomPowerEntryIndex];
                blockPowerEntryFlag = config.PowerFlags[randomPowerEntryIndex];
                blockSlotFlags = config.SlotFlags[randomPowerEntryIndex];
                attemps++;
                if (1000 < attemps) return -1;
            }
            return randomPowerEntryIndex;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public class AIConfig : ICloneable
    {
        public static bool[] GetDefaultPowerFlags(AIToon toon)
        {
            List<bool> defaultFlags = new List<bool>();
            if (null == toon) return defaultFlags.ToArray();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();

            foreach (PowerEntry powerEntry in powerEntries)
            {
                defaultFlags.Add("Brawl" != powerEntry.Power.DisplayName);
            }
            return defaultFlags.ToArray();
        }

        public static bool[][] GetDefaultSlotFlags(AIToon toon)
        {
            List<bool[]> defaultFlags = new List<bool[]>();
            if (null == toon) return defaultFlags.ToArray();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();

            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                defaultFlags.Add(new bool[powerEntry.SlotCount]);
                for (int slotIndex = 0; slotIndex < powerEntry.SlotCount; slotIndex++)
                {
                    defaultFlags[powerEntryIndex][slotIndex] = true;
                }
            }
            return defaultFlags.ToArray();
        }

        public AIConfig()
        {
            Version = CurrentVersion;
            Generations = 100;
            Iterations = 10;
            ImportanceOption = 0;
        }
        public AIConfig(AIToon toon)
        {
            Version = CurrentVersion;
            Generations = 100;
            Iterations = 10;
            ImportanceOption = 0;
            PowerFlags = GetDefaultPowerFlags(toon);
            SlotFlags = GetDefaultSlotFlags(toon);
            BonusPowerFlags = GetDefaultPowerFlags(toon);
        }
        public AIConfig(AIToon toon, int generations, int iterations)
        {
            Version = CurrentVersion;
            Generations = generations;
            Iterations = iterations;
            ImportanceOption = 0;
            PowerFlags = GetDefaultPowerFlags(toon);
            SlotFlags = GetDefaultSlotFlags(toon);
            BonusPowerFlags = GetDefaultPowerFlags(toon);
        }
        public AIConfig(AIToon toon, int generations, int iterations, BonusInfoWithWeight[] bonusInfoWithWeightArray)
        {
            Version = CurrentVersion;
            BonusInfoWithWeightArray = bonusInfoWithWeightArray;
            Generations = generations;
            Iterations = iterations;
            ImportanceOption = 0;
            PowerFlags = GetDefaultPowerFlags(toon);
            SlotFlags = GetDefaultSlotFlags(toon);
            BonusPowerFlags = GetDefaultPowerFlags(toon);
        }
        public AIConfig(bool[] powerFlags, bool[] bonusPowerFlags, int generations, int iterations, BonusInfoWithWeight[] bonusInfoWithWeightArray)
        {
            Version = CurrentVersion;
            BonusInfoWithWeightArray = bonusInfoWithWeightArray;
            Generations = generations;
            Iterations = iterations;
            ImportanceOption = 0;
            PowerFlags = powerFlags;
            SlotFlags = new List<bool[]>().ToArray();
            BonusPowerFlags = bonusPowerFlags;
        }
        public AIConfig(AIConfig config)
        {
            Version = CurrentVersion;
            BonusInfoWithWeightArray = config.BonusInfoWithWeightArray;
            Generations = config.Generations;
            Iterations = config.Iterations;
            ImportanceOption = config.ImportanceOption;
            PowerFlags = config.PowerFlags;
            SlotFlags = config.SlotFlags;
            BonusPowerFlags = config.BonusPowerFlags;
        }

        public object Clone()
        {
            AIConfig clone = new AIConfig();
            clone.Version = this.Version;
            clone.BonusInfoWithWeightArray = (null != this.BonusInfoWithWeightArray)? this.BonusInfoWithWeightArray.Clone() as BonusInfoWithWeight[] : null;
            clone.Generations = this.Generations;
            clone.Iterations = this.Iterations;
            clone.ImportanceOption = this.ImportanceOption;
            clone.PowerFlags = this.PowerFlags.Clone() as bool[];
            clone.SlotFlags = this.SlotFlags.Clone() as bool[][];
            clone.BonusPowerFlags = this.BonusPowerFlags.Clone() as bool[];
            return clone;
        }

        public string Version { get; set; }
        public BonusInfoWithWeight[] BonusInfoWithWeightArray { get; set; }
        public int Generations { get; set; }
        public int Iterations { get; set; }
        public int ImportanceOption { get; set; }
        public bool[] PowerFlags { get; set; }
        public bool[][] SlotFlags { get; set; }
        public bool[] BonusPowerFlags { get; set; }
        private static string CurrentVersion = "0.0.3";

    }
}

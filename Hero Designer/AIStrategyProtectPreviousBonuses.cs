﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public static class AIStrategyProtectPreviousBonuses
    {
        public static bool ProtectsPreviousBonuses(AIToon toon, List<PowerEntry> originalPowerEntries, PowerEntry newPowerEntry, int newPowerEntryIndex, AIConfig config, int configBonusIndex)
        {
            if (configBonusIndex == 0) return true;
            PowerEntry originalPower = originalPowerEntries[newPowerEntryIndex];
            for (int bonusIndex = configBonusIndex - 1; bonusIndex >= 0; bonusIndex--)
            {
                BonusInfoWithWeight previousBonusInfo = config.BonusInfoWithWeightArray[bonusIndex];
                float originalSum = previousBonusInfo.Sum(originalPower, toon.Archetype);
                float newSum = previousBonusInfo.Sum(newPowerEntry, toon.Archetype);
                if (originalSum > newSum) 
                    return false;
            }
            return true;
        }

        public static bool ProtectsPreviousBonuses(AIToon toon, List<PowerEntry> originalPowerEntries, PowerEntry newPowerEntry1, int newPowerEntryIndex1, PowerEntry newPowerEntry2, int newPowerEntryIndex2, AIConfig config, int configBonusIndex)
        {
            return ProtectsPreviousBonuses(toon, originalPowerEntries, newPowerEntry1, newPowerEntryIndex1, config, configBonusIndex)
                && ProtectsPreviousBonuses(toon, originalPowerEntries, newPowerEntry2, newPowerEntryIndex2, config, configBonusIndex);
        }
    }
}

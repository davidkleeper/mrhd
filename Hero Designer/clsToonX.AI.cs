﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Data_Classes;

namespace Hero_Designer
{
    public partial class clsToonX : Character
    {
        public PowerEntry GetPowerEntryByPowerName(string powerName)
        {
            for (int index = 0; index < CurrentBuild.Powers.Count; index++)
            {
                PowerEntry powerEntry = CurrentBuild.Powers[index];
                if (null == powerEntry.Power)
                    continue;
                if (!string.Equals(powerEntry.Power.DisplayName, powerName, StringComparison.OrdinalIgnoreCase))
                    continue;
                return powerEntry;
            }
            return null;
        }

        public void SetPowerEntryByPowerName(PowerEntry powerEntry, string powerName)
        {
            for (int index = 0; index < CurrentBuild.Powers.Count; index++)
            {
                PowerEntry p = CurrentBuild.Powers[index];
                if (null == p.Power)
                    continue;
                if (!string.Equals(p.Power.DisplayName, powerName, StringComparison.OrdinalIgnoreCase))
                    continue;
                CurrentBuild.Powers[index] = powerEntry;
                return;
            }
            CurrentBuild.AddPower(powerEntry.Power);
        }

        public int GetPowerEntryIndexByPowerName(string powerName)
        {
            for (int index = 0; index < CurrentBuild.Powers.Count; index++)
            {
                PowerEntry powerEntry = CurrentBuild.Powers[index];
                if (!string.Equals(powerEntry.Power.DisplayName, powerName, StringComparison.OrdinalIgnoreCase))
                    continue;
                return index;
            }
            return -1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public class AIEventGeneration : EventArgs
    {
        public AIEventGeneration(bool begin, bool end, int generation, AIToon toon)
        {
            Begin = begin;
            End = end;
            Generation = generation;
            Toon = toon;
        }
        public bool Begin;
        public bool End;
        public int Generation;
        public AIToon Toon;
    }
}

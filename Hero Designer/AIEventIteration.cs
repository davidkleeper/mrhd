﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public class AIEventIteration : EventArgs
    {
        public AIEventIteration(int iteration)
        {
            Iteration = iteration;
        }
        public int Iteration;
    }
}

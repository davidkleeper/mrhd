﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Base.Data_Classes;
using System.Linq;
using System.Windows.Forms;

namespace Hero_Designer
{
    public class AIToon : ICloneable
    {
        public string Name;
        public Archetype Archetype;

        public IPowerset Fitness;
        public IPowerset Primary;
        public IPowerset Secondary;
        public IPowerset Pool1;
        public IPowerset Pool2;
        public IPowerset Pool3;
        public IPowerset Pool4;
        public IPowerset Epic;

        public PowerEntry Brawl;
        public PowerEntry Health;
        public PowerEntry Stamina;
        public PowerEntry Level1Primary;
        public PowerEntry Level1Secondary;
        public PowerEntry Level2;
        public PowerEntry Level4;
        public PowerEntry Level6;
        public PowerEntry Level8;
        public PowerEntry Level10;
        public PowerEntry Level12;
        public PowerEntry Level14;
        public PowerEntry Level16;
        public PowerEntry Level18;
        public PowerEntry Level20;
        public PowerEntry Level22;
        public PowerEntry Level24;
        public PowerEntry Level26;
        public PowerEntry Level28;
        public PowerEntry Level30;
        public PowerEntry Level32;
        public PowerEntry Level35;
        public PowerEntry Level38;
        public PowerEntry Level41;
        public PowerEntry Level44;
        public PowerEntry Level47;
        public PowerEntry Level49;

        public PowerEntry Level1Kheldian;
        public PowerEntry Level10Kheldian;
        public PowerEntry Nova1;
        public PowerEntry Nova2;
        public PowerEntry Nova3;
        public PowerEntry Nova4;
        public PowerEntry Dwarf1;
        public PowerEntry Dwarf2;
        public PowerEntry Dwarf3;
        public PowerEntry Dwarf4;
        public PowerEntry Dwarf5;
        public PowerEntry Dwarf6;

        public AIToon()
        {
            Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            Health = new PowerEntry(0, Fitness.Powers[1]);                    // Health
            Stamina = new PowerEntry(0, Fitness.Powers[3]);                   // Stamina
            Brawl = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Brawl"));
        }

        public object Clone()
        {
            AIToon toon = new AIToon();
            if (null != Archetype) toon.Archetype = new Archetype(Archetype);
            if (null != Fitness) toon.Fitness = new Powerset(Fitness);
            if (null != Primary) toon.Primary = new Powerset(Primary);
            if (null != Secondary) toon.Secondary = new Powerset(Secondary);
            if (null != Pool1) toon.Pool1 = new Powerset(Pool1);
            if (null != Pool2) toon.Pool2 = new Powerset(Pool2);
            if (null != Pool3) toon.Pool3 = new Powerset(Pool3);
            if (null != Pool4) toon.Pool4 = new Powerset(Pool4);
            if (null != Epic) toon.Epic = new Powerset(Epic);
            if (null != Brawl) toon.Brawl = Brawl.Clone() as PowerEntry;
            if (null != Health) toon.Health = Health.Clone() as PowerEntry;
            if (null != Stamina) toon.Stamina = Stamina.Clone() as PowerEntry;
            if (null != Level1Primary) toon.Level1Primary = Level1Primary.Clone() as PowerEntry;
            if (null != Level1Secondary) toon.Level1Secondary = Level1Secondary.Clone() as PowerEntry;
            if (null != Level2) toon.Level2 = Level2.Clone() as PowerEntry;
            if (null != Level4) toon.Level4 = Level4.Clone() as PowerEntry;
            if (null != Level6) toon.Level6 = Level6.Clone() as PowerEntry;
            if (null != Level8) toon.Level8 = Level8.Clone() as PowerEntry;
            if (null != Level10) toon.Level10 = Level10.Clone() as PowerEntry;
            if (null != Level12) toon.Level12 = Level12.Clone() as PowerEntry;
            if (null != Level14) toon.Level14 = Level14.Clone() as PowerEntry;
            if (null != Level16) toon.Level16 = Level16.Clone() as PowerEntry;
            if (null != Level18) toon.Level18 = Level18.Clone() as PowerEntry;
            if (null != Level20) toon.Level20 = Level20.Clone() as PowerEntry;
            if (null != Level22) toon.Level22 = Level22.Clone() as PowerEntry;
            if (null != Level24) toon.Level24 = Level24.Clone() as PowerEntry;
            if (null != Level26) toon.Level26 = Level26.Clone() as PowerEntry;
            if (null != Level28) toon.Level28 = Level28.Clone() as PowerEntry;
            if (null != Level30) toon.Level30 = Level30.Clone() as PowerEntry;
            if (null != Level32) toon.Level32 = Level32.Clone() as PowerEntry;
            if (null != Level35) toon.Level35 = Level35.Clone() as PowerEntry;
            if (null != Level38) toon.Level38 = Level38.Clone() as PowerEntry;
            if (null != Level41) toon.Level41 = Level41.Clone() as PowerEntry;
            if (null != Level44) toon.Level44 = Level44.Clone() as PowerEntry;
            if (null != Level47) toon.Level47 = Level47.Clone() as PowerEntry;
            if (null != Level49) toon.Level49 = Level49.Clone() as PowerEntry;

            if (null != Level1Kheldian) toon.Level1Kheldian = Level1Kheldian.Clone() as PowerEntry;
            if (null != Level10Kheldian) toon.Level1Kheldian = Level10Kheldian.Clone() as PowerEntry;
            if (null != Nova1) toon.Nova1 = Nova1.Clone() as PowerEntry;
            if (null != Nova2) toon.Nova2 = Nova2.Clone() as PowerEntry;
            if (null != Nova3) toon.Nova3 = Nova3.Clone() as PowerEntry;
            if (null != Nova4) toon.Nova4 = Nova4.Clone() as PowerEntry;
            if (null != Dwarf1) toon.Dwarf1 = Dwarf1.Clone() as PowerEntry;
            if (null != Dwarf2) toon.Dwarf2 = Dwarf2.Clone() as PowerEntry;
            if (null != Dwarf3) toon.Dwarf3 = Dwarf3.Clone() as PowerEntry;
            if (null != Dwarf4) toon.Dwarf4 = Dwarf4.Clone() as PowerEntry;
            if (null != Dwarf5) toon.Dwarf5 = Dwarf5.Clone() as PowerEntry;
            if (null != Dwarf6) toon.Dwarf6 = Dwarf6.Clone() as PowerEntry;

            return toon;
        }

        public List<PowerEntry> GetPowerEntries()
        {
            List<PowerEntry> powerEntries = new List<PowerEntry>();
            powerEntries.Add(Health);
            powerEntries.Add(Stamina);
            powerEntries.Add(Level1Primary);
            powerEntries.Add(Level1Secondary);
            powerEntries.Add(Level2);
            powerEntries.Add(Level4);
            powerEntries.Add(Level6);
            powerEntries.Add(Level8);
            powerEntries.Add(Level10);
            powerEntries.Add(Level12);
            powerEntries.Add(Level14);
            powerEntries.Add(Level16);
            powerEntries.Add(Level18);
            powerEntries.Add(Level20);
            powerEntries.Add(Level22);
            powerEntries.Add(Level24);
            powerEntries.Add(Level26);
            powerEntries.Add(Level28);
            powerEntries.Add(Level30);
            powerEntries.Add(Level32);
            powerEntries.Add(Level35);
            powerEntries.Add(Level38);
            powerEntries.Add(Level41);
            powerEntries.Add(Level44);
            powerEntries.Add(Level47);
            powerEntries.Add(Level49);
            powerEntries.Add(Brawl);

            if (null != Level1Kheldian) powerEntries.Add(Level1Kheldian);
            if (null != Level10Kheldian) powerEntries.Add(Level10Kheldian);
            if (null != Nova1) powerEntries.Add(Nova1);
            if (null != Nova2) powerEntries.Add(Nova2);
            if (null != Nova3) powerEntries.Add(Nova3);
            if (null != Nova4) powerEntries.Add(Nova4);
            if (null != Dwarf1) powerEntries.Add(Dwarf1);
            if (null != Dwarf2) powerEntries.Add(Dwarf2);
            if (null != Dwarf3) powerEntries.Add(Dwarf3);
            if (null != Dwarf4) powerEntries.Add(Dwarf4);
            if (null != Dwarf5) powerEntries.Add(Dwarf5);
            if (null != Dwarf6) powerEntries.Add(Dwarf6);

            return powerEntries;
        }

        public bool SetPowerEntries(List<PowerEntry> powerEntries)
        {
            List<PowerEntry> originalPowerEntries = GetPowerEntries();
            int index = 0;
            Health = powerEntries[index++];
            Stamina = powerEntries[index++];
            Level1Primary = powerEntries[index++];
            Level1Secondary = powerEntries[index++];
            Level2 = powerEntries[index++];
            Level4 = powerEntries[index++];
            Level6 = powerEntries[index++];
            Level8 = powerEntries[index++];
            Level10 = powerEntries[index++];
            Level12 = powerEntries[index++];
            Level14 = powerEntries[index++];
            Level16 = powerEntries[index++];
            Level18 = powerEntries[index++];
            Level20 = powerEntries[index++];
            Level22 = powerEntries[index++];
            Level24 = powerEntries[index++];
            Level26 = powerEntries[index++];
            Level28 = powerEntries[index++];
            Level30 = powerEntries[index++];
            Level32 = powerEntries[index++];
            Level35 = powerEntries[index++];
            Level38 = powerEntries[index++];
            Level41 = powerEntries[index++];
            Level44 = powerEntries[index++];
            Level47 = powerEntries[index++];
            Level49 = powerEntries[index++];
            Brawl = powerEntries[index++];

            if (27 < powerEntries.Count)
            {
                Level1Kheldian = powerEntries[27];
                Level10Kheldian = powerEntries[28];
            }
            if (332 == powerEntries.Count)
            {
                Nova1 = powerEntries[29];
                Nova2 = powerEntries[30];
                Nova3 = powerEntries[31];
                Nova4 = powerEntries[32];
            }
            else if (35 == powerEntries.Count)
            {
                Dwarf1 = powerEntries[29];
                Dwarf2 = powerEntries[30];
                Dwarf3 = powerEntries[31];
                Dwarf4 = powerEntries[32];
                Dwarf5 = powerEntries[33];
                Dwarf6 = powerEntries[34];
            }
            else if (39 == powerEntries.Count)
            {
                Nova1 = powerEntries[29];
                Nova2 = powerEntries[30];
                Nova3 = powerEntries[31];
                Nova4 = powerEntries[32];
                Dwarf1 = powerEntries[33];
                Dwarf2 = powerEntries[34];
                Dwarf3 = powerEntries[35];
                Dwarf4 = powerEntries[36];
                Dwarf5 = powerEntries[37];
                Dwarf6 = powerEntries[38];
            }
            SetPowerLevels();
            return true;
        }

        public void SetPowerLevels()
        {
            if (null != Health) Health.Level = 1;
            if (null != Stamina) Stamina.Level = 1;
            if (null != Level1Primary) Level1Primary.Level = 0;
            if (null != Level1Secondary) Level1Secondary.Level = 0;
            if (null != Level2) Level2.Level = 1;
            if (null != Level4) Level4.Level = 3;
            if (null != Level6) Level6.Level = 5;
            if (null != Level8) Level8.Level = 7;
            if (null != Level10) Level10.Level = 9;
            if (null != Level12) Level12.Level = 11;
            if (null != Level14) Level14.Level = 13;
            if (null != Level16) Level16.Level = 15;
            if (null != Level18) Level18.Level = 17;
            if (null != Level20) Level20.Level = 19;
            if (null != Level22) Level22.Level = 21;
            if (null != Level24) Level24.Level = 23;
            if (null != Level26) Level26.Level = 25;
            if (null != Level28) Level28.Level = 27;
            if (null != Level30) Level30.Level = 29;
            if (null != Level32) Level32.Level = 31;
            if (null != Level35) Level35.Level = 34;
            if (null != Level38) Level38.Level = 37;
            if (null != Level41) Level41.Level = 40;
            if (null != Level44) Level44.Level = 43;
            if (null != Level47) Level47.Level = 46;
            if (null != Level49) Level49.Level = 48;
            if (null != Brawl) Brawl.Level = 1;

            if (null != Level1Kheldian) Level1Kheldian.Level = 0;
            if (null != Level10Kheldian) Level10Kheldian.Level = 9;
            if (null != Nova1)
            {
                PowerEntry nova = GetPowerEntryByPowerName("Bright Nova");
                if (null == nova) nova = GetPowerEntryByPowerName("Dark Nova");
                if (null != nova)
                {
                    Nova1.Level = nova.Level;
                    Nova2.Level = nova.Level;
                    Nova3.Level = nova.Level;
                    Nova4.Level = nova.Level;
                }
            }
            if (null != Dwarf1)
            {
                PowerEntry dwarf = GetPowerEntryByPowerName("White Dwarf");
                if (null == dwarf) dwarf = GetPowerEntryByPowerName("Black Dwarf");
                if (null != dwarf)
                {
                    Dwarf1.Level = dwarf.Level;
                    Dwarf2.Level = dwarf.Level;
                    Dwarf3.Level = dwarf.Level;
                    Dwarf4.Level = dwarf.Level;
                    Dwarf5.Level = dwarf.Level;
                    Dwarf6.Level = dwarf.Level;
                }
            }
        }

        public void SetDefaultSlotLevels()
        {
            SetPowerLevels();
            if (0 < Health.SlotCount) Health.Slots[0].Level = 1;
            if (0 < Stamina.SlotCount) Stamina.Slots[0].Level = 1;
            if (0 < Level1Primary.SlotCount) Level1Primary.Slots[0].Level = 1;
            if (0 < Level1Secondary.SlotCount) Level1Secondary.Slots[0].Level = 1;
            if (0 < Level2.SlotCount) Level2.Slots[0].Level = 1;
            if (0 < Level4.SlotCount) Level4.Slots[0].Level = 3;
            if (0 < Level6.SlotCount) Level6.Slots[0].Level = 5;
            if (0 < Level8.SlotCount) Level8.Slots[0].Level = 7;
            if (0 < Level10.SlotCount) Level10.Slots[0].Level = 9;
            if (0 < Level12.SlotCount) Level12.Slots[0].Level = 11;
            if (0 < Level14.SlotCount) Level14.Slots[0].Level = 13;
            if (0 < Level16.SlotCount) Level16.Slots[0].Level = 15;
            if (0 < Level18.SlotCount) Level18.Slots[0].Level = 17;
            if (0 < Level20.SlotCount) Level20.Slots[0].Level = 19;
            if (0 < Level22.SlotCount) Level22.Slots[0].Level = 21;
            if (0 < Level24.SlotCount) Level24.Slots[0].Level = 23;
            if (0 < Level26.SlotCount) Level26.Slots[0].Level = 25;
            if (0 < Level28.SlotCount) Level28.Slots[0].Level = 27;
            if (0 < Level30.SlotCount) Level30.Slots[0].Level = 29;
            if (0 < Level32.SlotCount) Level32.Slots[0].Level = 31;
            if (0 < Level35.SlotCount) Level35.Slots[0].Level = 34;
            if (0 < Level38.SlotCount) Level38.Slots[0].Level = 37;
            if (0 < Level41.SlotCount) Level41.Slots[0].Level = 40;
            if (0 < Level44.SlotCount) Level44.Slots[0].Level = 43;
            if (0 < Level47.SlotCount) Level47.Slots[0].Level = 46;
            if (0 < Level49.SlotCount) Level49.Slots[0].Level = 48;
            if (0 < Brawl.SlotCount) Brawl.Slots[0].Level = 1;

            if (null != Level1Kheldian) Level1Kheldian.Slots[0].Level = 0;
            if (null != Level10Kheldian) Level10Kheldian.Slots[0].Level = 9;
            if (null != Nova1)
            {
                PowerEntry nova = GetPowerEntryByPowerName("Bright Nova");
                if (null == nova) nova = GetPowerEntryByPowerName("Dark Nova");
                if (null != nova)
                {
                    Nova1.Slots[0].Level = nova.Slots[0].Level;
                    Nova2.Slots[0].Level = nova.Slots[0].Level;
                    Nova3.Slots[0].Level = nova.Slots[0].Level;
                    Nova4.Slots[0].Level = nova.Slots[0].Level;
                }
            }
            if (null != Dwarf1)
            {
                PowerEntry dwarf = GetPowerEntryByPowerName("White Dwarf");
                if (null == dwarf) dwarf = GetPowerEntryByPowerName("Black Dwarf");
                if (null != dwarf)
                {
                    Dwarf1.Slots[0].Level = dwarf.Slots[0].Level;
                    Dwarf2.Slots[0].Level = dwarf.Slots[0].Level;
                    Dwarf3.Slots[0].Level = dwarf.Slots[0].Level;
                    Dwarf4.Slots[0].Level = dwarf.Slots[0].Level;
                    Dwarf5.Slots[0].Level = dwarf.Slots[0].Level;
                    Dwarf6.Slots[0].Level = dwarf.Slots[0].Level;
                }
            }

            int slotsPerLevel = 2;
            int levelJump = 2;
            int currentLevel = 3;
            int totalSlotsAdded = 0;
            int slotsAdded = 0;
            int maxSlots = DatabaseAPI.Database.Levels.Sum(level => level.Slots);
            List<PowerEntry> powerEntries = PowerEntry.SortByLevel(GetPowerEntries());
            for (int powerIndex = 0; powerIndex < powerEntries.Count; powerIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerIndex];
                if (null == powerEntry.Power)
                    continue;
                if (1 >= powerEntry.Slots.Length)
                    continue;
                if (null != Level1Kheldian && Level1Kheldian == powerEntry)
                    continue;
                if (null != Level10Kheldian && Level10Kheldian == powerEntry)
                    continue;

                for (int slotIndex = 1; slotIndex < powerEntry.Slots.Length; slotIndex++)
                {
                    powerEntry.Slots[slotIndex].Level = (currentLevel - 1);
                    totalSlotsAdded++;
                    if (totalSlotsAdded == maxSlots)
                        return;
                    slotsAdded++;
                    if (slotsAdded == slotsPerLevel)
                    {
                        currentLevel += levelJump;
                        slotsAdded = 0;
                        if (31 <= currentLevel)
                            slotsPerLevel = 3;
                        if (33 == currentLevel || 36 == currentLevel || 39 == currentLevel || 42 == currentLevel || 45 == currentLevel)
                            levelJump = 1;
                        if (34 == currentLevel || 37 == currentLevel || 40 == currentLevel || 43 == currentLevel || 46 <= currentLevel || 48 == currentLevel)
                            levelJump = 2;
                    }
                }
            }
        }

        public void SetEnhancementsToMaxLevel()
        {
            List<PowerEntry> powerEntries = GetPowerEntries();
            int newVal = 52;
            int levelMax = 52;
            foreach (PowerEntry powerEntry in powerEntries)
            {
                foreach (SlotEntry slot in powerEntry.Slots)
                {
                    newVal = 52;
                    if (slot.Enhancement.Enh <= -1)
                        continue;
                    switch (DatabaseAPI.Database.Enhancements[slot.Enhancement.Enh].TypeID)
                    {
                        case Enums.eType.InventO:
                            levelMax = DatabaseAPI.Database.Enhancements[slot.Enhancement.Enh].LevelMax;
                            if (newVal > levelMax) newVal = levelMax;
                            slot.Enhancement.IOLevel = Enhancement.GranularLevelZb(newVal, levelMax, levelMax);
                            break;
                        case Enums.eType.SetO:
                            levelMax = DatabaseAPI.Database.EnhancementSets[DatabaseAPI.Database.Enhancements[slot.Enhancement.Enh].nIDSet].LevelMax;
                            if (levelMax > 49) levelMax = 49;
                            if (newVal > levelMax) newVal = levelMax;
                            slot.Enhancement.IOLevel = newVal;
                            break;
                        case Enums.eType.None:
                        case Enums.eType.Normal:
                        case Enums.eType.SpecialO:
                        default:
                            break;
                    }
                }
            }
            SetPowerEntries(powerEntries);
        }

        public bool IsValid(out string message)
        {
            if (!SlotLevelsCorrect(out message))
                return false;
            if (!FollowsRuleOfFive(out message))
                return false;
            if (!FollowsED(out message))
                return false;
            message = "";
            return true;
        }

        public PowerEntry GetPowerEntryByPowerName(string name)
        {
            List<PowerEntry> powerEntries = GetPowerEntries();
            foreach (PowerEntry powerEntry in powerEntries)
            {
                if (powerEntry.Power.DisplayName == name)
                    return powerEntry;
                if (powerEntry.Power.FullName == name)
                    return powerEntry;
                if (powerEntry.Power.PowerName == name)
                    return powerEntry;
            }
            return null;
        }

        public float SumBonus(BonusInfo bonusInfo, AIConfig config)
        {
            float sum = 0.0f;
            List<PowerEntry> powerEntries = GetPowerEntries();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                if (!config.BonusPowerFlags[powerEntryIndex])
                    continue;
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                sum += bonusInfo.SumBonuses(powerEntry);
            }
            return sum;
        }

        public float SumSpecialEnhancements(BonusInfo bonusInfo, AIConfig config)
        {
            float sum = 0.0f;
            List<PowerEntry> powerEntries = GetPowerEntries();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                if (!config.BonusPowerFlags[powerEntryIndex])
                    continue;
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                sum += bonusInfo.SumSpecialEnhancements(powerEntry);
            }
            return sum;
        }

        public float SumEnhancementEffects(BonusInfo bonusInfo, AIConfig config)
        {
            float sum = 0.0f;
            List<PowerEntry> powerEntries = GetPowerEntries();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                if (!config.BonusPowerFlags[powerEntryIndex])
                    continue;
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                sum += bonusInfo.SumEnhancementEffects(powerEntry);
            }
            return sum;
        }

        public float Sum(BonusInfo bonusInfo, AIConfig config)
        {
            float sum = 0.0f;
            List<PowerEntry> powerEntries = GetPowerEntries();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                if (!config.BonusPowerFlags[powerEntryIndex])
                    continue;
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                sum += bonusInfo.Sum(powerEntry, Archetype);
            }
            return sum;
        }

        public bool HasEnhancement(IEnhancement enhancement)
        {
            List<PowerEntry> powerEntries = GetPowerEntries();
            foreach (PowerEntry powerEntry in powerEntries)
            {
                if (powerEntry.HasEnhancement(enhancement))
                    return true;
            }
            return false;
        }

        public bool HasEnhancement(IEnhancement enhancement, PowerEntry powerEntry)
        {
            return powerEntry.HasEnhancement(enhancement);
        }

        public int GetEnhancementCount(IEnhancement enhancement, PowerEntry powerEntry)
        {
            if (null == powerEntry.Power)
                return 0;
            int count = 0;
            foreach (SlotEntry slot in powerEntry.Slots)
            {
                if (-1 == slot.Enhancement.Enh)
                    continue;
                IEnhancement e = DatabaseAPI.GetEnhancementByIndex(slot.Enhancement.Enh);
                if (null == e)
                    continue;
                if (enhancement.UIDSet == e.UIDSet && enhancement.Name == e.Name)
                    count++;
            }
            return count;
        }

        public bool CanAddEnhancement(IEnhancement enhancement, PowerEntry powerEntry)
        {
            if (!powerEntry.CanAddEnhancement(enhancement))
                return false;
            if (enhancement.Unique && HasEnhancement(enhancement))
                return false;
            if ("" != enhancement.UIDSet && powerEntry.HasEnhancement(enhancement))
                return false;
            if ("" == enhancement.UIDSet && 3 <= GetEnhancementCount(enhancement, powerEntry))
                return false;
            EnhancementType[] enhancementTypes = enhancement.GetEnhancementTypes();
            IEnhancement superiorVersion = enhancement.GetSuperiorVersion();
            if (null != superiorVersion && HasEnhancement(superiorVersion))
                return false;
            if (null != superiorVersion && HasEnhancement(enhancement))
                return false;
            IEnhancement nonSuperiorVersion = enhancement.GetNonSuperiorVersion();
            if (null != nonSuperiorVersion && HasEnhancement(nonSuperiorVersion))
                return false;
            if (null != nonSuperiorVersion && HasEnhancement(enhancement))
                return false;
            return true;
        }

        public bool SlotLevelsCorrect(out string message)
        {
            List<PowerEntry> powerEntries = GetPowerEntries();

            foreach (PowerEntry powerEntry in powerEntries)
            {
                if (null == powerEntry.Power)
                    continue;
                foreach (SlotEntry slotEntry in powerEntry.Slots)
                {
                    if (powerEntry.Level > slotEntry.Level)
                    {
                        message = powerEntry.Power.DisplayName + " has incorrect slot levels.";
                        return false;
                    }
                }
            }
            message = "";
            return true;
        }

        public bool FollowsED(out string message)
        {
            List<PowerEntry> powerEntries = GetPowerEntries();
            foreach (PowerEntry powerEntry in powerEntries)
            {
                if (!powerEntry.FollowsED())
                {
                    message = powerEntry.Power.DisplayName + " does not follow ED.";
                    return false;
                }
            }
            message = "";
            return true;
        }

        public bool FollowsRuleOfFive(out string message)
        {
            Dictionary<string, int> counts = GetBonusCounts();
            foreach (KeyValuePair<string, int> kvp in counts)
            {
                if (5 < kvp.Value)
                {
                    message = kvp.Key + " does not follow Rule of Five.";
                    return false;
                }
            }
            message = "";
            return true;
        }

        public bool HasFourOrMoreBonuses(EnhancementSet enhancementSet, int slots)
        {
            Dictionary<string, int> counts = GetBonusCounts(enhancementSet, slots);

            foreach (KeyValuePair<string, int> kvp in counts)
            {
                if (4 <= kvp.Value)
                    return true;
            }
            return false;
        }

        public Dictionary<string, int> GetBonusCounts(EnhancementSet enhancementSet, int slots)
        {
            Dictionary<string, int> counts = new Dictionary<string, int>();
            Dictionary<string, int> enhancementSetBonusCounts = GetBonusCounts();

            for (int bonusIndex = 0; bonusIndex < slots - 1; bonusIndex++)
            {
                if (bonusIndex >= enhancementSet.Bonus.Length)
                    break;
                EnhancementSet.BonusItem bonusItem = enhancementSet.Bonus[bonusIndex];
                for (int bonusNameIndex = 0; bonusNameIndex < bonusItem.Name.Length; bonusNameIndex++)
                {
                    string bonusName = bonusItem.Name[bonusNameIndex];
                    if (enhancementSetBonusCounts.ContainsKey(bonusName))
                    {
                        if (!counts.ContainsKey(bonusName))
                            counts.Add(bonusName, enhancementSetBonusCounts[bonusName]);
                    }
                    else
                    {
                        counts.Add(bonusName, 0);
                    }
                }
            }
            return counts;
        }

        public Dictionary<string, int> GetBonusCounts()
        {
            List<PowerEntry> powerEntries = GetPowerEntries();
            Dictionary<string, int> characterBonusCounts = new Dictionary<string, int>();

            foreach (PowerEntry powerEntry in powerEntries)
            {
                Dictionary<string, List<IEnhancement>> enhancementGroups = powerEntry.GetEnhancementsGroupedBySet();
                foreach (KeyValuePair<string, List<IEnhancement>> kvp in enhancementGroups)
                {
                    if ("None" == kvp.Key)
                        continue;
                    int enhancementSetIndex = DatabaseAPI.GetEnhancementSetIndexByName(kvp.Key);
                    if (-1 == enhancementSetIndex)
                        continue;
                    EnhancementSet set = DatabaseAPI.GetEnhancementSetByIndex(enhancementSetIndex);
                    if (null == set)
                        continue;
                    for (int bonusIndex = 0; bonusIndex < kvp.Value.Count - 1; bonusIndex++)
                    {
                        if (bonusIndex >= set.Bonus.Length)
                            break;
                        EnhancementSet.BonusItem bonusItem = set.Bonus[bonusIndex];
                        for (int bonusNameIndex = 0; bonusNameIndex < bonusItem.Name.Length; bonusNameIndex++)
                        {
                            string bonusName = bonusItem.Name[bonusNameIndex];
                            if (characterBonusCounts.ContainsKey(bonusName))
                                characterBonusCounts[bonusName] = characterBonusCounts[bonusName] + 1;
                            else
                                characterBonusCounts.Add(bonusName, 1);
                        }
                    }
                }
            }
            return characterBonusCounts;
        }

        public EnhancementSet GetHighestValidSet(EnhancementSet[] sortedEnhancementSets, PowerEntry powerEntry, int slots)
        {
            foreach (EnhancementSet enhancementSet in sortedEnhancementSets)
            {
                int count = 1;
                if (HasFourOrMoreBonuses(enhancementSet, slots))
                    continue;
                foreach (int enhancementIndex in enhancementSet.Enhancements)
                {
                    IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementIndex);
                    if (!CanAddEnhancement(enhancement, powerEntry))
                        continue;
                    count++;
                    if (count == slots)
                        return enhancementSet;
                }
            }
            return null;
        }

        public static void TestNewCode()
        {
            TestSetDefaultSlotLevels();
            TestGetPowerEntries();
            TestSetPowerEntries();
            TestClone();
            TestGetHighestValidSet();
            TestGetEnhancementCount();
            TestHasEnhancement();
            TestHasFourOrMoreBonuses();
            TestCanAddEnhancement();
            TestGetBonusCounts(); 
            TestMultiSlotPowerEntry();
            AIToonUtils.TestNewCode();
            AIStrategyHighestSet.TestNewCode();
            AIStrategyFrankenslot.TestNewCode();
            AIStrategyEvolveSpecialIOs.TestNewCode();
            HeroDesignerAIData.AIToonPOCOTest.TestNewCode();
        }

        public static void TestSetDefaultSlotLevels()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            toon.SetDefaultSlotLevels();
            Debug.Assert(toon.Health.Slots[0].Level == 1, "toon.Health.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Health.Slots[1].Level == 12, "toon.Health.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Health.Slots[2].Level == 12, "toon.Health.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Stamina.Slots[0].Level == 1, "toon.Stamina.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Stamina.Slots[1].Level == 14, "toon.Stamina.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Stamina.Slots[2].Level == 14, "toon.Stamina.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[0].Level == 1, "toon.Level1Primary.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[1].Level == 2, "toon.Level1Primary.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[2].Level == 2, "toon.Level1Primary.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[3].Level == 4, "toon.Level1Primary.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[4].Level == 4, "toon.Level1Primary.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[5].Level == 6, "toon.Level1Primary.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[0].Level == 1, "toon.Level1Secondary.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[1].Level == 6, "toon.Level1Secondary.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[2].Level == 8, "toon.Level1Secondary.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[3].Level == 8, "toon.Level1Secondary.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[4].Level == 10, "toon.Level1Secondary.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[5].Level == 10, "toon.Level1Secondary.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[0].Level == 1, "toon.Level2.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[1].Level == 16, "toon.Level2.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[2].Level == 16, "toon.Level2.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[3].Level == 18, "toon.Level2.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[4].Level == 18, "toon.Level2.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level2.Slots[5].Level == 20, "toon.Level2.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[0].Level == 3, "toon.Level4.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[1].Level == 20, "toon.Level4.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[2].Level == 22, "toon.Level4.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[3].Level == 22, "toon.Level4.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[4].Level == 24, "toon.Level4.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level4.Slots[5].Level == 24, "toon.Level4.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[0].Level == 5, "toon.Level6.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[1].Level == 26, "toon.Level6.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[2].Level == 26, "toon.Level6.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[3].Level == 28, "toon.Level6.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[4].Level == 28, "toon.Level6.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level6.Slots[5].Level == 30, "toon.Level6.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[0].Level == 7, "toon.Level8.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[1].Level == 30, "toon.Level8.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[2].Level == 30, "toon.Level8.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[3].Level == 32, "toon.Level8.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[4].Level == 32, "toon.Level8.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level8.Slots[5].Level == 32, "toon.Level8.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[0].Level == 9, "toon.Level10.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[1].Level == 33, "toon.Level10.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[2].Level == 33, "toon.Level10.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[3].Level == 33, "toon.Level10.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[4].Level == 35, "toon.Level10.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level10.Slots[5].Level == 35, "toon.Level10.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[0].Level == 11, "toon.Level12.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[1].Level == 35, "toon.Level12.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[2].Level == 36, "toon.Level12.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[3].Level == 36, "toon.Level12.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[4].Level == 36, "toon.Level12.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level12.Slots[5].Level == 38, "toon.Level12.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[0].Level == 13, "toon.Level14.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[1].Level == 38, "toon.Level14.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[2].Level == 38, "toon.Level14.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[3].Level == 39, "toon.Level14.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[4].Level == 39, "toon.Level14.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level14.Slots[5].Level == 39, "toon.Level14.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[0].Level == 15, "toon.Level16.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[1].Level == 41, "toon.Level16.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[2].Level == 41, "toon.Level16.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[3].Level == 41, "toon.Level16.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[4].Level == 42, "toon.Level16.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level16.Slots[5].Level == 42, "toon.Level16.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[0].Level == 17, "toon.Level18.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[1].Level == 42, "toon.Level18.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[2].Level == 44, "toon.Level18.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[3].Level == 44, "toon.Level18.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[4].Level == 44, "toon.Level18.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level18.Slots[5].Level == 45, "toon.Level18.Slots[5].Level is incorrect!");
            Debug.Assert(toon.Level20.Slots[0].Level == 19, "toon.Level20.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level20.Slots[1].Level == 45, "toon.Level20.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level20.Slots[2].Level == 45, "toon.Level20.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level20.Slots[3].Level == 47, "toon.Level20.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level20.Slots[4].Level == 47, "toon.Level20.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level22.Slots[0].Level == 21, "toon.Level22.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level22.Slots[1].Level == 47, "toon.Level22.Slots[1].Level is incorrect!");
            Debug.Assert(toon.Level22.Slots[2].Level == 49, "toon.Level22.Slots[2].Level is incorrect!");
            Debug.Assert(toon.Level22.Slots[3].Level == 49, "toon.Level22.Slots[3].Level is incorrect!");
            Debug.Assert(toon.Level22.Slots[4].Level == 49, "toon.Level22.Slots[4].Level is incorrect!");
            Debug.Assert(toon.Level24.Slots[0].Level == 23, "toon.Level24.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level26.Slots[0].Level == 25, "toon.Level26.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level28.Slots[0].Level == 27, "toon.Level28.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level30.Slots[0].Level == 29, "toon.Level30.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level32.Slots[0].Level == 31, "toon.Level32.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level35.Slots[0].Level == 34, "toon.Level35.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level38.Slots[0].Level == 37, "toon.Level38.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level41.Slots[0].Level == 40, "toon.Level41.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level44.Slots[0].Level == 43, "toon.Level44.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level47.Slots[0].Level == 46, "toon.Level47.Slots[0].Level is incorrect!");
            Debug.Assert(toon.Level49.Slots[0].Level == 48, "toon.Level49.Slots[0].Level is incorrect!");
        }

        public static void TestGetPowerEntries()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            int index = 0;
            Debug.Assert(powerEntries[index++] == toon.Health, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Stamina, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level1Primary, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level1Secondary, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level2, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level4, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level6, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level8, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level10, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level12, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level14, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level16, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level18, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level20, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level22, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level24, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level26, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level28, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level30, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level32, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level35, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level38, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level41, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level44, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level47, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Level49, "powerEntry is incorrect!");
            Debug.Assert(powerEntries[index++] == toon.Brawl, "powerEntry is incorrect!");
        }

        public static void TestSetPowerEntries()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            PowerEntry temp = powerEntries[25];
            powerEntries[25] = powerEntries[24];
            powerEntries[24] = temp;
            toon.SetPowerEntries(powerEntries);
            powerEntries = toon.GetPowerEntries();
            Debug.Assert(powerEntries[24] == temp, "powerEntries[24] is incorrect!");
        }

        public static void TestClone()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            AIToon clone = toon.Clone() as AIToon;
            Debug.Assert(0 == clone.Archetype.CompareTo(toon.Archetype), "clone.Archetype is incorrect!");
            clone.Archetype.Playable = !toon.Archetype.Playable;
            Debug.Assert(0 != clone.Archetype.CompareTo(toon.Archetype), "clone.Archetype is incorrect!");

            Debug.Assert(0 == clone.Fitness.CompareTo(toon.Fitness), "clone.Fitness is incorrect!");
            Debug.Assert(0 == clone.Primary.CompareTo(toon.Primary), "clone.Primary is incorrect!");
            Debug.Assert(0 == clone.Secondary.CompareTo(toon.Secondary), "clone.Secondary is incorrect!");
            Debug.Assert(0 == clone.Pool1.CompareTo(toon.Pool1), "clone.Pool1 is incorrect!");
            Debug.Assert(0 == clone.Pool2.CompareTo(toon.Pool2), "clone.Pool2 is incorrect!");
            Debug.Assert(0 == clone.Pool3.CompareTo(toon.Pool3), "clone.Pool3 is incorrect!");
            Debug.Assert(0 == clone.Pool4.CompareTo(toon.Pool4), "clone.Pool4 is incorrect!");
            Debug.Assert(0 == clone.Epic.CompareTo(toon.Epic), "clone.Epic is incorrect!");
            toon.Fitness.GroupName = "Clone";
            Debug.Assert(0 != clone.Fitness.CompareTo(toon.Fitness), "clone.Fitness is incorrect!");

            Debug.Assert(0 == clone.Brawl.CompareTo(toon.Brawl), "clone.Brawl is incorrect!");
            Debug.Assert(0 == clone.Health.CompareTo(toon.Health), "clone.Health is incorrect!");
            Debug.Assert(0 == clone.Stamina.CompareTo(toon.Stamina), "clone.Stamina is incorrect!");
            Debug.Assert(0 == clone.Level1Primary.CompareTo(toon.Level1Primary), "clone.Level1Primary is incorrect!");
            Debug.Assert(0 == clone.Level1Secondary.CompareTo(toon.Level1Secondary), "clone.Level1Secondary is incorrect!");
            Debug.Assert(0 == clone.Level2.CompareTo(toon.Level2), "clone.Level2 is incorrect!");
            Debug.Assert(0 == clone.Level4.CompareTo(toon.Level4), "clone.Level4 is incorrect!");
            Debug.Assert(0 == clone.Level6.CompareTo(toon.Level6), "clone.Level6 is incorrect!");
            Debug.Assert(0 == clone.Level8.CompareTo(toon.Level8), "clone.Level8 is incorrect!");
            Debug.Assert(0 == clone.Level10.CompareTo(toon.Level10), "clone.Level10 is incorrect!");
            Debug.Assert(0 == clone.Level12.CompareTo(toon.Level12), "clone.Level12 is incorrect!");
            Debug.Assert(0 == clone.Level14.CompareTo(toon.Level14), "clone.Level14 is incorrect!");
            Debug.Assert(0 == clone.Level16.CompareTo(toon.Level16), "clone.Level16 is incorrect!");
            Debug.Assert(0 == clone.Level18.CompareTo(toon.Level18), "clone.Level18 is incorrect!");
            Debug.Assert(0 == clone.Level20.CompareTo(toon.Level20), "clone.Level20 is incorrect!");
            Debug.Assert(0 == clone.Level22.CompareTo(toon.Level22), "clone.Level22 is incorrect!");
            Debug.Assert(0 == clone.Level24.CompareTo(toon.Level24), "clone.Level24 is incorrect!");
            Debug.Assert(0 == clone.Level26.CompareTo(toon.Level26), "clone.Level26 is incorrect!");
            Debug.Assert(0 == clone.Level28.CompareTo(toon.Level28), "clone.Level28 is incorrect!");
            Debug.Assert(0 == clone.Level30.CompareTo(toon.Level30), "clone.Level30 is incorrect!");
            Debug.Assert(0 == clone.Level32.CompareTo(toon.Level32), "clone.Level32 is incorrect!");
            Debug.Assert(0 == clone.Level35.CompareTo(toon.Level35), "clone.Level35 is incorrect!");
            Debug.Assert(0 == clone.Level38.CompareTo(toon.Level38), "clone.Level38 is incorrect!");
            Debug.Assert(0 == clone.Level41.CompareTo(toon.Level41), "clone.Level41 is incorrect!");
            Debug.Assert(0 == clone.Level44.CompareTo(toon.Level44), "clone.Level44 is incorrect!");
            Debug.Assert(0 == clone.Level47.CompareTo(toon.Level47), "clone.Level47 is incorrect!");
            Debug.Assert(0 == clone.Level49.CompareTo(toon.Level49), "clone.Level49 is incorrect!");
            toon.Health.Level = clone.Health.Level + 1;
            Debug.Assert(0 != clone.Health.CompareTo(toon.Health), "clone.Health is incorrect!");
        }

        public static void TestToonSumPowerBonuses()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            float sum = toon.SumBonus(BonusInfo.DefenseEnergy, new AIConfig());
            Debug.Assert(BaseUtils.IsKindaSortaEqual(sum, 0.0f, 0.001f), "sum is incorrect!");

            EnhancementSet thunderstrike = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Thunderstrike"));
            toon.Level1Primary.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(thunderstrike.Enhancements[0]).GetLoadString(), ":");
            toon.Level1Primary.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(thunderstrike.Enhancements[1]).GetLoadString(), ":");
            toon.Level1Primary.Slots[2].LoadFromString(DatabaseAPI.GetEnhancementByIndex(thunderstrike.Enhancements[2]).GetLoadString(), ":");
            sum = toon.SumBonus(BonusInfo.DefenseEnergy, new AIConfig());
            Debug.Assert(BaseUtils.IsKindaSortaEqual(sum, 0.025f, 0.001f), "sum is incorrect!");
        }

        public static void TestGetEnhancementCount()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            IEnhancement endRed = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
            IEnhancement damage = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            toon.Level1Primary.Slots[0].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[1].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[2].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[3].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[4].LoadFromString(endRed.GetLoadString(), ":");
            Debug.Assert(toon.GetEnhancementCount(damage, toon.Level1Primary) == 0, "GetEnhancementCount is incorrect!");
            toon.Level1Primary.Slots[0].LoadFromString(damage.GetLoadString(), ":");
            Debug.Assert(toon.GetEnhancementCount(damage, toon.Level1Primary) == 1, "GetEnhancementCount is incorrect!");
            toon.Level1Primary.Slots[1].LoadFromString(damage.GetLoadString(), ":");
            Debug.Assert(toon.GetEnhancementCount(damage, toon.Level1Primary) == 2, "GetEnhancementCount is incorrect!");
            toon.Level1Primary.Slots[2].LoadFromString(damage.GetLoadString(), ":");
            Debug.Assert(toon.GetEnhancementCount(damage, toon.Level1Primary) == 3, "GetEnhancementCount is incorrect!");
        }

        public static void TestHasEnhancement()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            EnhancementSet thunderstrike = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Thunderstrike"));
            IEnhancement thunderstrike0 = DatabaseAPI.GetEnhancementByIndex(thunderstrike.Enhancements[0]);
            Debug.Assert(toon.HasEnhancement(thunderstrike0) == false, "HasEnhancement is incorrect!");
            toon.Level1Primary.Slots[0].LoadFromString(thunderstrike0.GetLoadString(), ":");
            Debug.Assert(toon.HasEnhancement(thunderstrike0) == true, "HasEnhancement is incorrect!");
            Debug.Assert(toon.HasEnhancement(thunderstrike0, toon.Level1Primary) == true, "HasEnhancement is incorrect!");
            Debug.Assert(toon.HasEnhancement(thunderstrike0, toon.Level2) == false, "HasEnhancement is incorrect!");
        }

        public static void TestHasFourOrMoreBonuses()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            EnhancementSet reactiveDefenses = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Reactive Defenses"));
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == false, "HasFourOrMoreBonuses is incorrect!");
            toon.Level1Secondary.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[0]).GetLoadString(), ":");
            toon.Level1Secondary.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[1]).GetLoadString(), ":");
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == false, "HasFourOrMoreBonuses is incorrect!");
            toon.Level2.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[0]).GetLoadString(), ":");
            toon.Level2.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[1]).GetLoadString(), ":");
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == false, "HasFourOrMoreBonuses is incorrect!");
            toon.Level10.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[0]).GetLoadString(), ":");
            toon.Level10.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[1]).GetLoadString(), ":");
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == false, "HasFourOrMoreBonuses is incorrect!");
            toon.Level12.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[0]).GetLoadString(), ":");
            toon.Level12.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[1]).GetLoadString(), ":");
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == true, "HasFourOrMoreBonuses is incorrect!");
            toon.Level14.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[0]).GetLoadString(), ":");
            toon.Level14.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(reactiveDefenses.Enhancements[1]).GetLoadString(), ":");
            Debug.Assert(toon.HasFourOrMoreBonuses(reactiveDefenses, 2) == true, "HasFourOrMoreBonuses is incorrect!");
        }

        public static void TestCanAddEnhancement()
        {
            AIToon toon = AIToonUtils.CreateBuildBlasterFireMental();
            EnhancementSet apocalypse = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Apocalypse"));
            IEnhancement apocalypse0 = DatabaseAPI.GetEnhancementByIndex(apocalypse.Enhancements[0]);
            IEnhancement apocalypse1 = DatabaseAPI.GetEnhancementByIndex(apocalypse.Enhancements[1]);
            toon.Level1Primary.Slots[0].LoadFromString(apocalypse0.GetLoadString(), ":");
            Debug.Assert(toon.CanAddEnhancement(apocalypse0, toon.Level1Primary) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(apocalypse1, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(apocalypse0, toon.Level18) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(apocalypse1, toon.Level18) == true, "CanAddEnhancement is incorrect!");

            IEnhancement damage = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            IEnhancement slow = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Slow);
            IEnhancement endRed = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
            toon.Level1Primary.Slots[0].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[1].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[2].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[3].LoadFromString(endRed.GetLoadString(), ":");
            toon.Level1Primary.Slots[4].LoadFromString(endRed.GetLoadString(), ":");
            Debug.Assert(toon.CanAddEnhancement(damage, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(slow, toon.Level1Primary) == false, "CanAddEnhancement is incorrect!");
            toon.Level1Primary.Slots[0].LoadFromString(damage.GetLoadString(), ":");
            toon.Level1Primary.Slots[1].LoadFromString(damage.GetLoadString(), ":");
            toon.Level1Primary.Slots[2].LoadFromString(damage.GetLoadString(), ":");
            Debug.Assert(toon.CanAddEnhancement(damage, toon.Level1Primary) == false, "CanAddEnhancement is incorrect!");

            EnhancementSet blastersWrath = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Blaster's Wrath"));
            IEnhancement blastersWrath0 = DatabaseAPI.GetEnhancementByIndex(blastersWrath.Enhancements[0]);
            IEnhancement blastersWrath1 = DatabaseAPI.GetEnhancementByIndex(blastersWrath.Enhancements[1]);
            IEnhancement superiorBlastersWrath0 = blastersWrath0.GetSuperiorVersion();
            IEnhancement superiorBlastersWrath1 = blastersWrath1.GetSuperiorVersion();
            Debug.Assert(toon.CanAddEnhancement(blastersWrath0, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath0, toon.Level2) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath0, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath0, toon.Level2) == true, "CanAddEnhancement is incorrect!");
            toon.Level1Primary.Slots[0].LoadFromString(superiorBlastersWrath0.GetLoadString(), ":");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath0, toon.Level1Primary) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath0, toon.Level2) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath0, toon.Level2) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath1, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath1, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            toon.Level1Primary.Slots[0].LoadFromString(blastersWrath0.GetLoadString(), ":");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath0, toon.Level1Primary) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath0, toon.Level2) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath0, toon.Level2) == false, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(superiorBlastersWrath1, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
            Debug.Assert(toon.CanAddEnhancement(blastersWrath1, toon.Level1Primary) == true, "CanAddEnhancement is incorrect!");
        }

        public static void TestGetHighestValidSet()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            HashSet<EnhancementSet> defenseBonusSets = toon.Level1Primary.Power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefenseAll);
            EnhancementSet[] sortedEnhancementSets = EnhancementSet.SortEnhancementSets(defenseBonusSets, 6, new BonusInfoWithWeight(BonusInfo.DefenseAll, 1.0f));
            EnhancementSet highestSet = toon.GetHighestValidSet(sortedEnhancementSets, toon.Level1Primary, 6);
            Debug.Assert(highestSet.DisplayName == "Superior Winter's Bite", "highestSet is incorrect!");
            toon.Level6.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[0]).GetLoadString(), ":");
            toon.Level6.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[1]).GetLoadString(), ":");
            toon.Level6.Slots[2].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[2]).GetLoadString(), ":");
            toon.Level6.Slots[3].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[3]).GetLoadString(), ":");
            toon.Level6.Slots[4].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[4]).GetLoadString(), ":");
            toon.Level6.Slots[5].LoadFromString(DatabaseAPI.GetEnhancementByIndex(highestSet.Enhancements[5]).GetLoadString(), ":");
            highestSet = toon.GetHighestValidSet(sortedEnhancementSets, toon.Level1Primary, 6);
            Debug.Assert(highestSet.DisplayName == "Lethargic Repose", "highestSet is incorrect!");
        }

        public static void TestGetBonusCounts()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceFieldWithIOs();
            Dictionary<string, int> counts = toon.GetBonusCounts();

            Debug.Assert(counts.Count == 25, "counts.Count is incorrect!");

            EnhancementSet shieldWall = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Shield Wall"));
            counts = toon.GetBonusCounts(shieldWall, 6);
            Debug.Assert(counts.Count == 5, "counts.Count is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.Set_Bonus.Improved_Regeneration_4"), "counts.ContainsKey(Set_Bonus.Set_Bonus.Improved_Regeneration_4) is incorrect!");
            Debug.Assert(counts["Set_Bonus.Set_Bonus.Improved_Regeneration_4"] == 3, "counts[Set_Bonus.Set_Bonus.Improved_Regeneration_4] is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4"), "counts.ContainsKey(Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4) is incorrect!");
            Debug.Assert(counts["Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4"] == 1, "counts[Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4] is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.Set_Bonus.Increased_Health_5"), "counts.ContainsKey(Set_Bonus.Set_Bonus.Increased_Health_5) is incorrect!");
            Debug.Assert(counts["Set_Bonus.Set_Bonus.Increased_Health_5"] == 1, "counts[Set_Bonus.Set_Bonus.Increased_Health_5] is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.PVP_Set_Bonus.Repel_Resist_3"), "counts.ContainsKey(Set_Bonus.PVP_Set_Bonus.Repel_Resist_3) is incorrect!");
            Debug.Assert(counts["Set_Bonus.PVP_Set_Bonus.Repel_Resist_3"] == 1, "counts[Set_Bonus.PVP_Set_Bonus.Repel_Resist_3] is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.Set_Bonus.Energy_Neg_Mez_Res_5"), "counts.ContainsKey(Set_Bonus.Set_Bonus.Energy_Neg_Mez_Res_5) is incorrect!");
            Debug.Assert(counts["Set_Bonus.Set_Bonus.Energy_Neg_Mez_Res_5"] == 1, "counts[Set_Bonus.Set_Bonus.Energy_Neg_Mez_Res_5] is incorrect!");

            counts = toon.GetBonusCounts(shieldWall, 3);
            Debug.Assert(counts.Count == 2, "counts.Count is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.Set_Bonus.Improved_Regeneration_4"), "counts.ContainsKey(Set_Bonus.Set_Bonus.Improved_Regeneration_4) is incorrect!");
            Debug.Assert(counts["Set_Bonus.Set_Bonus.Improved_Regeneration_4"] == 3, "counts[Set_Bonus.Set_Bonus.Improved_Regeneration_4] is incorrect!");
            Debug.Assert(counts.ContainsKey("Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4"), "counts.ContainsKey(Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4) is incorrect!");
            Debug.Assert(counts["Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4"] == 1, "counts[Set_Bonus.PVP_Set_Bonus.Improved_Recovery_4] is incorrect!");
        }

        public static void TestMultiSlotPowerEntry()
        {

            /*
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            BonusInfoWithWeight[] bonusInfoWithWeightArray = new BonusInfoWithWeight[1];
            bonusInfoWithWeightArray[0] = new BonusInfoWithWeight(BonusInfo.DefenseSmashing, 1.0f);
            PowerEntry result = AISlotter.MultiSlotPowerEntry(toon, powerEntries, 2, bonusInfoWithWeightArray);
            Debug.Assert(6 == result.Slots.Length, "result is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[0].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[0] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[1].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[1] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[2].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[2] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[3].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[3] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[4].Enhancement.Enh).UIDSet == "", "enhancement[4] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[5].Enhancement.Enh).UIDSet == "", "enhancement[5] is incorrect!");

            bonusInfoWithWeightArray[0] = new BonusInfoWithWeight(BonusInfo.Recovery.CloneWithPvP(), 1.0f);
            result = AISlotter.MultiSlotPowerEntry(toon, powerEntries, 3, bonusInfoWithWeightArray);
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[0].Enhancement.Enh).UIDSet == "Shield_Wall", "enhancement[0] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[1].Enhancement.Enh).UIDSet == "Shield_Wall", "enhancement[1] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[2].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[2] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[3].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[3] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[4].Enhancement.Enh).UIDSet == "Kismet", "enhancement[4] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[5].Enhancement.Enh).UIDSet == "Kismet", "enhancement[5] is incorrect!");
        */
        }
    }
}

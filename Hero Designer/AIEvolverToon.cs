﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hero_Designer
{
    public static class AIEvolverToon
    {
        public static event EventHandler<AIEventGeneration> RaiseGenerationEvent;

        public static void EvolveToon(AIToon toon, AIConfig config, CancellationToken cancellationToken)
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            OnRaiseGenerationEvent(new AIEventGeneration(true, false, 0, toon.Clone() as AIToon));

            for (int generationCount = 0; generationCount < config.Generations; generationCount++)
            {
                for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
                {
                    if (!config.PowerFlags[powerEntryIndex])
                        continue;
                    AIEvolverPowerEntry.EvolvePower(toon, powerEntries, powerEntryIndex, config, cancellationToken);
                    toon.SetPowerEntries(powerEntries);
                }
                OnRaiseGenerationEvent(new AIEventGeneration(false, false, generationCount + 1, toon.Clone() as AIToon));
            }
            toon.SetDefaultSlotLevels();
            toon.SetEnhancementsToMaxLevel();
            OnRaiseGenerationEvent(new AIEventGeneration(false, true, config.Generations, toon.Clone() as AIToon));
        }

        private static void OnRaiseGenerationEvent(AIEventGeneration e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<AIEventGeneration> handler = RaiseGenerationEvent;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                // Use the () operator to raise the event.
                handler(typeof(AIEvolverToon), e);
            }
        }
    }
}

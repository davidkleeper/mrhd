﻿namespace Hero_Designer.Forms
{
    partial class AIBonus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public System.Windows.Forms.Button BtnBack;
        public System.Windows.Forms.Button BtnNext;
        public System.Windows.Forms.Button BtnCancel;
        public System.Windows.Forms.Button BtnSave;
        public System.Windows.Forms.Button BtnSaveAIWizardConfig;
        public System.Windows.Forms.Button BtnLoadAIWizardConfig;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AIBonus));
            this._pnlNavigation = new System.Windows.Forms.Panel();
            this._lblChoicesLoaded = new System.Windows.Forms.Label();
            this.BtnLoadAIWizardConfig = new System.Windows.Forms.Button();
            this.BtnSaveAIWizardConfig = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnBack = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this._pnlLeft = new System.Windows.Forms.Panel();
            this._cboxSpeak = new System.Windows.Forms.CheckBox();
            this._lblRobotWoman = new System.Windows.Forms.Label();
            this._pictureBoxAgent = new System.Windows.Forms.PictureBox();
            this._pnlContent = new System.Windows.Forms.Panel();
            this._pnlNavigation.SuspendLayout();
            this._pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxAgent)).BeginInit();
            this.SuspendLayout();
            // 
            // _pnlNavigation
            // 
            this._pnlNavigation.Controls.Add(this._lblChoicesLoaded);
            this._pnlNavigation.Controls.Add(this.BtnLoadAIWizardConfig);
            this._pnlNavigation.Controls.Add(this.BtnSaveAIWizardConfig);
            this._pnlNavigation.Controls.Add(this.BtnSave);
            this._pnlNavigation.Controls.Add(this.BtnBack);
            this._pnlNavigation.Controls.Add(this.BtnNext);
            this._pnlNavigation.Controls.Add(this.BtnCancel);
            this._pnlNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._pnlNavigation.Location = new System.Drawing.Point(0, 406);
            this._pnlNavigation.Name = "_pnlNavigation";
            this._pnlNavigation.Size = new System.Drawing.Size(802, 47);
            this._pnlNavigation.TabIndex = 0;
            // 
            // _lblChoicesLoaded
            // 
            this._lblChoicesLoaded.AutoSize = true;
            this._lblChoicesLoaded.Location = new System.Drawing.Point(185, 18);
            this._lblChoicesLoaded.Name = "_lblChoicesLoaded";
            this._lblChoicesLoaded.Size = new System.Drawing.Size(254, 17);
            this._lblChoicesLoaded.TabIndex = 82;
            this._lblChoicesLoaded.Text = "Your saved choices have been loaded.";
            // 
            // BtnLoadAIWizardConfig
            // 
            this.BtnLoadAIWizardConfig.Location = new System.Drawing.Point(16, 9);
            this.BtnLoadAIWizardConfig.Name = "BtnLoadAIWizardConfig";
            this.BtnLoadAIWizardConfig.Size = new System.Drawing.Size(163, 28);
            this.BtnLoadAIWizardConfig.TabIndex = 81;
            this.BtnLoadAIWizardConfig.Text = "&Load Saved Choices";
            this.BtnLoadAIWizardConfig.UseVisualStyleBackColor = true;
            this.BtnLoadAIWizardConfig.Click += new System.EventHandler(this.btnLoadAIWizardConfig_Click);
            // 
            // BtnSaveAIWizardConfig
            // 
            this.BtnSaveAIWizardConfig.Location = new System.Drawing.Point(16, 9);
            this.BtnSaveAIWizardConfig.Name = "BtnSaveAIWizardConfig";
            this.BtnSaveAIWizardConfig.Size = new System.Drawing.Size(163, 28);
            this.BtnSaveAIWizardConfig.TabIndex = 80;
            this.BtnSaveAIWizardConfig.Text = "&Save Your Choices";
            this.BtnSaveAIWizardConfig.UseVisualStyleBackColor = true;
            this.BtnSaveAIWizardConfig.Click += new System.EventHandler(this.btnSaveAIWizardConfig_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(16, 9);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 28);
            this.BtnSave.TabIndex = 79;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BtnBack
            // 
            this.BtnBack.Location = new System.Drawing.Point(529, 9);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(75, 28);
            this.BtnBack.TabIndex = 7;
            this.BtnBack.Text = "< &Back";
            this.BtnBack.UseVisualStyleBackColor = true;
            this.BtnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.Location = new System.Drawing.Point(622, 9);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(75, 28);
            this.BtnNext.TabIndex = 6;
            this.BtnNext.Text = "&Next >";
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(713, 9);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 28);
            this.BtnCancel.TabIndex = 5;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // _pnlLeft
            // 
            this._pnlLeft.Controls.Add(this._cboxSpeak);
            this._pnlLeft.Controls.Add(this._lblRobotWoman);
            this._pnlLeft.Controls.Add(this._pictureBoxAgent);
            this._pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this._pnlLeft.Location = new System.Drawing.Point(0, 0);
            this._pnlLeft.Name = "_pnlLeft";
            this._pnlLeft.Size = new System.Drawing.Size(139, 406);
            this._pnlLeft.TabIndex = 1;
            // 
            // _cboxSpeak
            // 
            this._cboxSpeak.AutoSize = true;
            this._cboxSpeak.Location = new System.Drawing.Point(26, 177);
            this._cboxSpeak.Name = "_cboxSpeak";
            this._cboxSpeak.Size = new System.Drawing.Size(70, 21);
            this._cboxSpeak.TabIndex = 47;
            this._cboxSpeak.Text = "Speak";
            this._cboxSpeak.UseVisualStyleBackColor = true;
            this._cboxSpeak.CheckedChanged += new System.EventHandler(this.cboxSpeak_Change);
            // 
            // _lblRobotWoman
            // 
            this._lblRobotWoman.AutoSize = true;
            this._lblRobotWoman.Location = new System.Drawing.Point(23, 143);
            this._lblRobotWoman.Name = "_lblRobotWoman";
            this._lblRobotWoman.Size = new System.Drawing.Size(98, 17);
            this._lblRobotWoman.TabIndex = 3;
            this._lblRobotWoman.Text = "Robot Woman";
            // 
            // _pictureBoxAgent
            // 
            this._pictureBoxAgent.Image = ((System.Drawing.Image)(resources.GetObject("_pictureBoxAgent.Image")));
            this._pictureBoxAgent.Location = new System.Drawing.Point(5, 12);
            this._pictureBoxAgent.Name = "_pictureBoxAgent";
            this._pictureBoxAgent.Size = new System.Drawing.Size(130, 130);
            this._pictureBoxAgent.TabIndex = 3;
            this._pictureBoxAgent.TabStop = false;
            // 
            // _pnlContent
            // 
            this._pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pnlContent.Location = new System.Drawing.Point(139, 0);
            this._pnlContent.Name = "_pnlContent";
            this._pnlContent.Size = new System.Drawing.Size(663, 406);
            this._pnlContent.TabIndex = 2;
            // 
            // AIBonus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 453);
            this.Controls.Add(this._pnlContent);
            this.Controls.Add(this._pnlLeft);
            this.Controls.Add(this._pnlNavigation);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(820, 500);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(820, 500);
            this.Name = "AIBonus";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Bonus Wizard";
            this._pnlNavigation.ResumeLayout(false);
            this._pnlNavigation.PerformLayout();
            this._pnlLeft.ResumeLayout(false);
            this._pnlLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxAgent)).EndInit();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.ResumeLayout(false);
        }

        #endregion
    }
}
﻿namespace Hero_Designer.Forms
{
    partial class AIBonusViewAndSaveSnapshotsItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSnapshotName = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblSnapshotName
            // 
            this.lblSnapshotName.AutoSize = true;
            this.lblSnapshotName.Location = new System.Drawing.Point(3, 5);
            this.lblSnapshotName.Name = "lblSnapshotName";
            this.lblSnapshotName.Size = new System.Drawing.Size(109, 17);
            this.lblSnapshotName.TabIndex = 0;
            this.lblSnapshotName.Text = "Snapshot Name";
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(160, 3);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(126, 26);
            this.btnView.TabIndex = 1;
            this.btnView.Text = "&View Summary";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(293, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 26);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Save As Mids File";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // AIWizardViewAndSaveSnapshotsItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.lblSnapshotName);
            this.Name = "AIWizardViewAndSaveSnapshotsItem";
            this.Size = new System.Drawing.Size(432, 35);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSnapshotName;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnSave;
    }
}

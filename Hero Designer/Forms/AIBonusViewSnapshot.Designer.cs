﻿namespace Hero_Designer.Forms
{
    partial class AIBonusViewSnapshot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBuild = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxBuild
            // 
            this.textBoxBuild.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBuild.Location = new System.Drawing.Point(0, 0);
            this.textBoxBuild.Multiline = true;
            this.textBoxBuild.Name = "textBoxBuild";
            this.textBoxBuild.ReadOnly = true;
            this.textBoxBuild.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxBuild.Size = new System.Drawing.Size(800, 450);
            this.textBoxBuild.TabIndex = 70;
            // 
            // AIWizardViewSnapshot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxBuild);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIWizardViewSnapshot";
            this.ShowInTaskbar = false;
            this.Text = "Snapshot Bonus Summary";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxBuild;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIPermaIntro : Form
    {
        public Button btnBack;
        public Button btnNext;
        public Button btnCancel;
        public Button btnSave;
        public Button btnSaveAIWizardConfig;
        public Button btnLoadAIWizardConfig;
        public AIToon origialToon;

        public AIPermaIntro()
        {
            InitializeComponent();
        }

        public void Setup(Button btnBack, Button btnNext, Button btnCancel, bool speak)
        {
            this.btnBack = btnBack;
            this.btnNext = btnNext;
            this.btnCancel = btnCancel;
            if (speak) Speak();
        }

        public void Speak()
        {
            SpeechSynthesizer speech = new SpeechSynthesizer();
            speech.SelectVoiceByHints(VoiceGender.Female, VoiceAge.NotSet);
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
            speech.SpeakAsync(lblIntro3.Text);
        }
    }
}
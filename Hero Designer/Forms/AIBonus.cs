﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Speech.Synthesis;
using Base.Master_Classes;

namespace Hero_Designer.Forms
{
    public partial class AIBonus : Form
    {
        public AIBonus()
        {
            InitializeComponent();
            CreateForms(); 
        }

        public void Setup()
        {
            LoadToon();
            _lblChoicesLoaded.Visible = false;
            _choicesLoadedLabelTimer.Tick += new EventHandler(TimerEventProcessor);
            _choicesLoadedLabelTimer.Interval = 5000;
            _cboxSpeak.Checked = false;
            IsClosing = false;
            SetCurrentForm(IntroForm);
        }

        private void CreateForms()
        {
            IntroForm = new AIBonusIntro();
            SelectForm = new AIBonusSelect();
            GenerationsAndIterationsForm = new AIBonusGenerationsAndIterations();
            EvolveForm = new AIBonusEvolve();
            ResultsForm = new AIBonusResults();
            IntroForm.TopLevel = false;
            SelectForm.TopLevel = false;
            GenerationsAndIterationsForm.TopLevel = false;
            EvolveForm.TopLevel = false;
            ResultsForm.TopLevel = false;
        }

        private void LoadToon()
        {
            if (49 != MidsContext.Character.Level)
            {
                this.BtnBack.Enabled = false;
                this.BtnNext.Enabled = false;
                System.Windows.Forms.MessageBox.Show("Your character must be level 50 to use the Bonus Wizard.");
                OrigialToon = null;
                return;
            }
            OrigialToon = AIToonUtils.Convert(MidsContext.Character as clsToonX);
            Config = new AIConfig(OrigialToon);
            Config.PowerFlags = AIConfig.GetDefaultPowerFlags(OrigialToon);
        }

        public void Back()
        {
            if (SelectForm == _currentForm) SetCurrentForm(IntroForm);
            else if (GenerationsAndIterationsForm == _currentForm) SetCurrentForm(SelectForm);
            else if (EvolveForm == _currentForm) SetCurrentForm(GenerationsAndIterationsForm);
            else if (ResultsForm == _currentForm) SetCurrentForm(EvolveForm);
        }

        public void Next()
        {
            if (IntroForm == _currentForm) SetCurrentForm(SelectForm);
            else if (SelectForm == _currentForm) SetCurrentForm(GenerationsAndIterationsForm);
            else if (GenerationsAndIterationsForm == _currentForm) SetCurrentForm(EvolveForm);
            else if (EvolveForm == _currentForm) SetCurrentForm(ResultsForm);
        }

        public void SetCurrentForm(Form currentForm)
        {
            _currentForm = currentForm;
            _pnlContent.Controls.Clear();
            _pnlContent.Controls.Add(currentForm);
            StopSpeaking();
            if (IntroForm == _currentForm) IntroForm.Setup(this);
            else if (SelectForm == _currentForm) SelectForm.Setup(this);
            else if (GenerationsAndIterationsForm == _currentForm) GenerationsAndIterationsForm.Setup(this);
            else if (EvolveForm == _currentForm) EvolveForm.Setup(this);
            else if (ResultsForm == _currentForm) ResultsForm.Setup(this);
            currentForm.Show();
            Speak();
        }

        private void Save()
        {
            string filename = "";
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog.Title = "Save AI Character";
            saveFileDialog.FileName = "AI Character.mxd";
            saveFileDialog.DefaultExt = "mxd";
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = "Hero/Villian Builds (*.mxd)|*.mxd|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            filename = saveFileDialog.FileName;

            AIToonUtils.WriteAIToon(EvolveForm.FinalToon, filename);
        }

        private void SaveConfig()
        {
            Config.BonusInfoWithWeightArray = SelectForm.GetBonuses().ToArray();
            Config.Generations = GenerationsAndIterationsForm.Generations;
            Config.Iterations = GenerationsAndIterationsForm.Iterations;
            Config.ImportanceOption = SelectForm.ImportanceOption;

            string jsonString = JsonSerializer.Serialize(Config);
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();

            saveFileDialog.Title = "Save Bonus Information";
            saveFileDialog.FileName = "AI Character.bwc";
            saveFileDialog.DefaultExt = "bwc";
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = "Bonus Wizard Config (*.bwc)|*.bwc|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            string filename = saveFileDialog.FileName;
            System.IO.File.WriteAllText(@filename, jsonString);
        }

        private void LoadConfig()
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Bonus Wizard Config (*.bwc)|*.bwc|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filename = openFileDialog.FileName;
                    string jsonString = System.IO.File.ReadAllText(filename);
                    Config = JsonSerializer.Deserialize<AIConfig>(jsonString);

                    Config.PowerFlags = FixPowerFlags(OrigialToon, Config.PowerFlags);
                    Config.BonusPowerFlags = FixBonusPowerFlags(OrigialToon, Config.BonusPowerFlags);
                    Config.SlotFlags = FixSlotFlags(OrigialToon, Config.SlotFlags);
                    SelectForm.UseConfig(Config);
                    GenerationsAndIterationsForm.UseConfig(Config);
                    BtnLoadAIWizardConfig.Visible = true;
                    _lblChoicesLoaded.Visible = true;
                    _choicesLoadedLabelTimer.Start();
                }
            }
        }

        private bool[] FixPowerFlags(AIToon toon, bool[] powerFlags)
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            if (null == Config.PowerFlags) Config.PowerFlags = new bool[] { true };
            if (powerEntries.Count == Config.PowerFlags.Length) return powerFlags;
            if (powerEntries.Count < Config.PowerFlags.Length) return powerFlags.Take(powerEntries.Count).ToArray();
            List<bool> newPowerFlags = (null != powerFlags)? powerFlags.ToList() : Config.PowerFlags.ToList();
            while (powerEntries.Count > newPowerFlags.Count) newPowerFlags.Add(true);
            return newPowerFlags.ToArray();
        }

        private bool[] FixBonusPowerFlags(AIToon toon, bool[] bonusPowerFlags)
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            if (null == Config.BonusPowerFlags) Config.BonusPowerFlags = new bool[] { true };
            if (powerEntries.Count == Config.BonusPowerFlags.Length) return bonusPowerFlags;
            if (powerEntries.Count < Config.BonusPowerFlags.Length) return bonusPowerFlags.Take(powerEntries.Count).ToArray();
            List<bool> newPowerFlags = (null != bonusPowerFlags) ? bonusPowerFlags.ToList() : Config.BonusPowerFlags.ToList();
            while (powerEntries.Count > newPowerFlags.Count) newPowerFlags.Add(true);
            return newPowerFlags.ToArray();
        }

        public static bool[][] FixSlotFlags(AIToon toon, bool[][] slotFlags)
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            List<bool[]> newSlotFlags = new List<bool[]>();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                newSlotFlags.Add(Array.Empty<bool>());
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                bool[] flags = (null != slotFlags)? slotFlags[powerEntryIndex] : new bool[] { true };
                if (flags.Length == powerEntry.SlotCount)
                {
                    newSlotFlags[powerEntryIndex] = flags;
                }
                else if (flags.Length > powerEntry.SlotCount)
                {
                    newSlotFlags[powerEntryIndex] = flags.Take(powerEntry.SlotCount).ToArray();
                }
                else
                {
                    List<bool> newList = flags.ToList<bool>();
                    for (int newFlagCounter = flags.Length; newFlagCounter < powerEntry.SlotCount; newFlagCounter++)
                    {
                        newList.Add(true);
                    }
                    newSlotFlags[powerEntryIndex] = newList.ToArray();
                }
            }
            return newSlotFlags.ToArray();
        }

        private void Speak()
        {
            StopSpeaking();
            if (!ShouldSpeak())
                return;
            Speech = new SpeechSynthesizer();
            Speech.SelectVoiceByHints(VoiceGender.Female, VoiceAge.NotSet);
            if (IntroForm == _currentForm) IntroForm.Speak(Speech);
            else if (SelectForm == _currentForm) SelectForm.Speak(Speech);
            else if (GenerationsAndIterationsForm == _currentForm) GenerationsAndIterationsForm.Speak(Speech);
            else if (EvolveForm == _currentForm) EvolveForm.Speak(Speech);
            else if (ResultsForm == _currentForm) ResultsForm.Speak(Speech);
        }

        private void StopSpeaking()
        {
            if (null != Speech)
            {
                Speech.Dispose();
                Speech = null;
            }
        }

        public bool ShouldSpeak()
        {
            return _cboxSpeak.Checked;
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Next();
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            Back();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void btnSaveAIWizardConfig_Click(object sender, EventArgs e)
        {
            SaveConfig();
        }

        private void btnLoadAIWizardConfig_Click(object sender, EventArgs e)
        {
            LoadConfig();
        }

        private void cboxSpeak_Change(object sender, EventArgs e)
        {
            Speak();
        }

        private void Form_Closing(Object sender, FormClosingEventArgs e)
        {
            IsClosing = true;
            EvolveForm.CancelEvolve();
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            _choicesLoadedLabelTimer.Stop();
            _lblChoicesLoaded.Visible = false;
        }

        public AIToon OrigialToon { get; private set; }
        public AIConfig Config { get; private set; }
        public AIBonusIntro IntroForm { get; private set; }
        public AIBonusSelect SelectForm { get; private set; }
        public AIBonusGenerationsAndIterations GenerationsAndIterationsForm { get; private set; }
        public AIBonusEvolve EvolveForm { get; private set; }
        public AIBonusResults ResultsForm { get; private set; }
        public SpeechSynthesizer Speech;
        public bool IsClosing;

        private Form _currentForm;
        private Panel _pnlNavigation;
        private Panel _pnlLeft;
        private Panel _pnlContent;
        private PictureBox _pictureBoxAgent;
        private Label _lblRobotWoman;
        private Label _lblChoicesLoaded;
        private CheckBox _cboxSpeak;
        private Timer _choicesLoadedLabelTimer = new Timer();
    }
}
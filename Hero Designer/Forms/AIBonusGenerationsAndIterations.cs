﻿using System;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIBonusGenerationsAndIterations : Form
    {
        public AIBonusGenerationsAndIterations()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus bonusWizard)
        {
            _bonusWizard = bonusWizard;
            UpdateButtons(bonusWizard);
        }

        public void UpdateButtons(AIBonus bonusWizard)
        {
            bonusWizard.BtnBack.Enabled = true;
            bonusWizard.BtnNext.Enabled = true;
            bonusWizard.BtnSave.Visible = false;
            bonusWizard.BtnSaveAIWizardConfig.Visible = true;
            bonusWizard.BtnLoadAIWizardConfig.Visible = false;
        }

        public void UseConfig(AIConfig config)
        {
            trackBarGenerations.Value = config.Generations;
            trackBarIterations.Value = config.Iterations;
            lblGenerations.Text = "Generations: " + trackBarGenerations.Value.ToString();
            lblIterations.Text = "Iterations: " + trackBarIterations.Value.ToString();
        }

        private void trackBarGenerations_Scroll(object sender, EventArgs e)
        {
            lblGenerations.Text = "Generations: " + trackBarGenerations.Value.ToString();
        }

        private void trackBarIterations_Scroll(object sender, EventArgs e)
        {
            lblIterations.Text = "Iterations: " + trackBarIterations.Value.ToString();
        }

        public void Speak(SpeechSynthesizer speech)
        {
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
            speech.SpeakAsync(lblIntro3.Text);
        }

        private void linkControlPowerEvolution_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AIBonusPowerSelect powerSelectDialog = new AIBonusPowerSelect();
            powerSelectDialog.Setup(_bonusWizard.OrigialToon, _bonusWizard.Config.PowerFlags);
            powerSelectDialog.ShowDialog(this);
        }

        private void linkControlSlotEvolution_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AIBonusSlotSelect slotSelectDialog = new AIBonusSlotSelect();
            slotSelectDialog.Setup(_bonusWizard.OrigialToon, _bonusWizard.Config);
            slotSelectDialog.ShowDialog(this);
        }

        private void linkControlPowerBonuses_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AIBonusPowerSelect powerSelectDialog = new AIBonusPowerSelect();
            powerSelectDialog.Setup(_bonusWizard.OrigialToon, _bonusWizard.Config.BonusPowerFlags, "Bonus Power Select");
            powerSelectDialog.ShowDialog(this);
        }

        public int Generations { get { return trackBarGenerations.Value; } }
        public int Iterations { get { return trackBarIterations.Value; } }
        private AIBonus _bonusWizard;

    }
}


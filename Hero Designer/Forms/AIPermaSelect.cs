﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIPermaSelect : Form
    {
        public Button btnBack;
        public Button btnNext;
        public Button btnCancel;

        private string[] _selectedBonuses = new string[3];
        private Base.Data_Classes.Archetype _archetype;
        private IPowerset _powerset;
        private IPower _selectedPower;
        private Dictionary<string, IPowerset> _currentPowersets;
        private Dictionary<string, IPower> _currentPowers;
        private IPower power1;
        private IPower power2;
        private IPower power3;
        private IPower power4;
        private IPower power5;

        public AIPermaSelect()
        {
            InitializeComponent();
            lblPower1.Visible = false;
            lblPower1.Text = "";
            btnRemovePower1.Visible = false;
            lblPower2.Visible = false;
            btnRemovePower2.Visible = false;
            lblPower3.Visible = false;
            btnRemovePower3.Visible = false;
            lblPower4.Visible = false;
            btnRemovePower4.Visible = false;
            lblPower5.Visible = false;
            btnRemovePower5.Visible = false;
            btnAddPower.Enabled = false;
            PopulateArchetypes();
        }

        public void Setup(Button btnBack, Button btnNext, Button btnCancel, bool speak)
        {
            this.btnBack = btnBack;
            this.btnNext = btnNext;
            this.btnCancel = btnCancel;
            cboxPrimaryPowersets.Enabled = false;
            cboxSecondaryPowersets.Enabled = false;
            cboxEpicPowersets.Enabled = false;
            cboxPoolPowersets.Enabled = false;
            cboxPowers.Enabled = false;
            UpdateButtons();
            if (speak) Speak();
        }

        public void UpdateButtons()
        {
            if (null != btnNext) btnNext.Enabled = lblPower1.Text != "" && -1 != cboxArchetype.SelectedIndex;
        }

        public IPower[] GetPowers()
        {
            IPower[] powers = new IPower[GetPowerCount()];
            if (null != power1) powers[0] = power1;
            if (null != power2) powers[1] = power2;
            if (null != power3) powers[2] = power3;
            if (null != power4) powers[3] = power4;
            if (null != power5) powers[4] = power5;
            return powers;
        }

        private void AIWizard2_Load(object sender, EventArgs e)
        {

        }

        private void PopulateArchetypes()
        {
            Base.Data_Classes.Archetype[] playerArchetypes = DatabaseAPI.GetPlayerArchetypes();
            foreach (Base.Data_Classes.Archetype playerarchetype in playerArchetypes)
            {
                cboxArchetype.Items.Add(playerarchetype.DisplayName);
            }
        }

        private void PopulatePrimaryPowersets()
        {
            PopulatePowersets(_archetype.Primary, cboxPrimaryPowersets);
        }

        private void PopulateSecondaryPowersets()
        {
            PopulatePowersets(_archetype.Secondary, cboxSecondaryPowersets);
        }

        private void PopulateEpicPowersets()
        {
            PopulatePowersets(_archetype.Ancillary, cboxEpicPowersets);
        }

        private void PopulatePoolPowersets()
        {
            PopulatePowersets(DatabaseAPI.GetPowersetIndexes(_archetype, Enums.ePowerSetType.Pool), cboxPoolPowersets);
        }

        private void PopulatePowersets(int[] powersetIndexes, System.Windows.Forms.ComboBox cboxPowerset)
        {
            IPowerset[] powersets = new IPowerset[powersetIndexes.Length];
            for (int index = 0; index < powersetIndexes.Length; index++)
            {
                int powersetIndex = powersetIndexes[index];
                IPowerset powerset = DatabaseAPI.GetPowersetByIndex(powersetIndex);
                powersets[index] = powerset;
            }
            PopulatePowersets(powersets, cboxPowerset);
        }

        private void PopulatePowersets(IPowerset[] powersets, System.Windows.Forms.ComboBox cboxPowerset)
        {
            cboxPowerset.Items.Clear();
            cboxPowerset.Text = "";
            cboxPowerset.SelectedIndex = -1;
            foreach (IPowerset powerset in powersets)
            {
                if (null == powerset)
                    continue;
                cboxPowerset.Items.Add(powerset.DisplayName);
                _currentPowersets.Add(powerset.DisplayName, powerset);
            }
            cboxPowerset.Enabled = true;
        }

        private void PopulatePowers()
        {
            cboxPowers.Items.Clear();
            cboxPowers.Text = "";
            cboxPowers.SelectedIndex = -1;
            _currentPowers = new Dictionary<string, IPower>();
            if (_archetype.DisplayName == "Dominator")
            {
                IPowerset[] inherentPowersets = DatabaseAPI.GetPowersetIndexes(_archetype, Enums.ePowerSetType.Inherent);
                IPowerset inherent = null;
                foreach (IPowerset powerset in inherentPowersets)
                {
                    if (powerset.DisplayName != "Inherent") continue;
                    inherent = powerset;
                    break;
                }
                if (null != inherent)
                {
                    foreach (IPower power in inherent.Powers)
                    {
                        if (power.DisplayName != "Domination") continue;
                        if (power.Effects.Length < 24) continue;
                        cboxPowers.Items.Add(power.DisplayName);
                        _currentPowers.Add(power.DisplayName, power);
                    }
                }
            }
            foreach (IPower power in _powerset.Powers)
            {
                if (!power.CanBePerma())
                    continue;
                cboxPowers.Items.Add(power.DisplayName);
                _currentPowers.Add(power.DisplayName, power);
            }
            cboxPowers.Enabled = true;
        }

        private void ChangePowersetSelection(ComboBox cboxPowerset, ComboBox cboxOtherPowerset1, ComboBox cboxOtherPowerset2, ComboBox cboxOtherPowerset3)
        {
            int index = cboxPowerset.SelectedIndex;
            string powersetName = cboxPowerset.SelectedItem?.ToString();
            if (null == powersetName)
                return;
            cboxOtherPowerset1.SelectedIndex = -1;
            cboxOtherPowerset2.SelectedIndex = -1;
            cboxOtherPowerset3.SelectedIndex = -1;
            cboxPowerset.SelectedIndex = index;
            cboxPowerset.SelectedItem = powersetName;
            _powerset = _currentPowersets[powersetName];
            PopulatePowers();
        }

        private void SetPower(string powerName, Label powerLable, Button removeButton, IPower power, int powerNumber)
        {
            powerLable.Text = powerName;
            powerLable.Visible = true;
            removeButton.Visible = true;
            if (1 == powerNumber) power1 = power;
            else if (2 == powerNumber) power2 = power;
            else if (3 == powerNumber) power3 = power;
            else if (4 == powerNumber) power4 = power;
            else if (5 == powerNumber) power5 = power;
            UpdateButtons();
        }

        private void RemovePower(Label powerLable, Button removeButton, int removePowerNumber)
        {
            powerLable.Text = "";
            powerLable.Visible = false;
            removeButton.Visible = false;
            if (1 == removePowerNumber) power1 = null;
            else if (2 == removePowerNumber) power2 = null;
            else if (3 == removePowerNumber) power3 = null;
            else if (4 == removePowerNumber) power4 = null;
            else if (5 == removePowerNumber) power5 = null;
            btnAddPower.Enabled = true;
        }

        private void CompactPowers()
        {
            Label[] selectedPowerLabels = new Label[5];
            Button[] selectedRemoveButtons = new Button[5];
            IPower[] selectedPowers = new IPower[5];

            selectedPowerLabels[0] = lblPower1;
            selectedPowerLabels[1] = lblPower2;
            selectedPowerLabels[2] = lblPower3;
            selectedPowerLabels[3] = lblPower4;
            selectedPowerLabels[4] = lblPower5;
            selectedRemoveButtons[0] = btnRemovePower1;
            selectedRemoveButtons[1] = btnRemovePower2;
            selectedRemoveButtons[2] = btnRemovePower3;
            selectedRemoveButtons[3] = btnRemovePower4;
            selectedRemoveButtons[4] = btnRemovePower5;
            selectedPowers[0] = power1;
            selectedPowers[1] = power2;
            selectedPowers[2] = power3;
            selectedPowers[3] = power4;
            selectedPowers[4] = power5;

            for (int selectedIndex = 1; selectedIndex < selectedPowerLabels.Length; selectedIndex++)
            {
                if (!selectedPowerLabels[selectedIndex].Visible)
                    continue;
                for (int freeIndex = selectedIndex - 1; freeIndex >= 0; freeIndex--)
                {
                    if (selectedPowerLabels[freeIndex].Visible)
                        continue;

                    System.Windows.Forms.Label sourcePowerLabel = selectedPowerLabels[selectedIndex];
                    System.Windows.Forms.Button sourceRemoveButtons = selectedRemoveButtons[selectedIndex];
                    IPower sourcePower = selectedPowers[selectedIndex];
                    string sourcePowerText = sourcePowerLabel.Text;
                    System.Windows.Forms.Label destPowerLabel = selectedPowerLabels[freeIndex];
                    System.Windows.Forms.Button destRemoveButtons = selectedRemoveButtons[freeIndex];
                    IPower destPower = selectedPowers[freeIndex];

                    SetPower(sourcePowerText, destPowerLabel, destRemoveButtons, sourcePower, freeIndex);
                    RemovePower(sourcePowerLabel, sourceRemoveButtons, selectedIndex);
                    break;
                }
            }
            UpdateButtons();
        }

        private int GetPowerCount()
        {
            if (lblPower5.Visible) return 5;
            if (lblPower4.Visible) return 4;
            if (lblPower3.Visible) return 3;
            if (lblPower2.Visible) return 2;
            if (lblPower1.Visible) return 1;
            return 0;
        }

        public void Speak()
        {
            SpeechSynthesizer speech = new SpeechSynthesizer();
            speech.SelectVoiceByHints(VoiceGender.Female, VoiceAge.NotSet);
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
        }

        private void cboxArchetype_Changed(object sender, EventArgs e)
        {
            string archetypeName = cboxArchetype.SelectedItem.ToString();
            _archetype = DatabaseAPI.GetArchetypeByName(archetypeName);
            if (null == _archetype)
                return;
            _currentPowersets = new Dictionary<string, IPowerset>();
            PopulatePrimaryPowersets();
            PopulateSecondaryPowersets();
            PopulateEpicPowersets();
            PopulatePoolPowersets();
        }

        private void cboxPrimaryPowersets_Change(object sender, EventArgs e)
        {
            ChangePowersetSelection(cboxPrimaryPowersets, cboxSecondaryPowersets, cboxEpicPowersets, cboxPoolPowersets);
        }

        private void cboxSecondaryPowersets_Change(object sender, EventArgs e)
        {
            ChangePowersetSelection(cboxSecondaryPowersets, cboxPrimaryPowersets, cboxEpicPowersets, cboxPoolPowersets);
        }

        private void cboxEpicPowersets_Change(object sender, EventArgs e)
        {
            ChangePowersetSelection(cboxEpicPowersets, cboxPrimaryPowersets, cboxSecondaryPowersets, cboxPoolPowersets);
        }

        private void cboxPoolPowersets_Change(object sender, EventArgs e)
        {
            ChangePowersetSelection(cboxPoolPowersets, cboxPrimaryPowersets, cboxSecondaryPowersets, cboxEpicPowersets);
        }

        private void cboxPowers_Change(object sender, EventArgs e)
        {
            btnAddPower.Enabled = -1 != cboxPowers.SelectedIndex;
            if (!btnAddPower.Enabled)
            {
                _selectedPower = null;
                return;
            }
            string powerName = cboxPowers.Items[cboxPowers.SelectedIndex].ToString();
            _selectedPower = _currentPowers[powerName];
        }

        private void btnAddPower_Click(object sender, EventArgs e)
        {
            if (null == cboxPowers.SelectedItem || String.IsNullOrEmpty(cboxPowers.SelectedItem.ToString()))
            {
                Console.Beep();
                return;
            }
            string selectedPower = cboxPowers.SelectedItem.ToString();
            if (String.IsNullOrEmpty(selectedPower) || selectedPower == lblPower1.Text || selectedPower == lblPower2.Text || selectedPower == lblPower3.Text || selectedPower == lblPower4.Text || selectedPower == lblPower5.Text)
            {
                Console.Beep();
                return;
            }

            if (false == lblPower1.Visible)
                SetPower(selectedPower, lblPower1, btnRemovePower1, _selectedPower, 1);
            else if (false == lblPower2.Visible)
                SetPower(selectedPower, lblPower2, btnRemovePower2, _selectedPower, 2);
            else if (false == lblPower3.Visible)
                SetPower(selectedPower, lblPower3, btnRemovePower3, _selectedPower, 3);
            else if (false == lblPower4.Visible)
                SetPower(selectedPower, lblPower4, btnRemovePower4, _selectedPower, 4);
            else if (false == lblPower5.Visible)
            {
                SetPower(selectedPower, lblPower5, btnRemovePower5, _selectedPower, 5);
                btnAddPower.Enabled = false;
            }
        }

        private void btnRemovePower1_Click(object sender, EventArgs e)
        {
            RemovePower(lblPower1, btnRemovePower1, 1);
            CompactPowers();
        }

        private void btnRemovePower2_Click(object sender, EventArgs e)
        {
            RemovePower(lblPower2, btnRemovePower2, 2);
            CompactPowers();
        }

        private void btnRemovePower3_Click(object sender, EventArgs e)
        {
            RemovePower(lblPower3, btnRemovePower3, 3);
            CompactPowers();
        }

        private void btnRemovePower4_Click(object sender, EventArgs e)
        {
            RemovePower(lblPower4, btnRemovePower4, 4);
            CompactPowers();
        }

        private void btnRemovePower5_Click(object sender, EventArgs e)
        {
            RemovePower(lblPower5, btnRemovePower5, 5);
            CompactPowers();
        }
    }
}

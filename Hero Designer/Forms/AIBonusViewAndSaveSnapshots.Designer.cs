﻿namespace Hero_Designer.Forms
{
    partial class AIBonusViewAndSaveSnapshots
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelSnapshots = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // flowLayoutPanelSnapshots
            // 
            this.flowLayoutPanelSnapshots.AutoScroll = true;
            this.flowLayoutPanelSnapshots.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelSnapshots.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelSnapshots.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelSnapshots.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanelSnapshots.Name = "flowLayoutPanelSnapshots";
            this.flowLayoutPanelSnapshots.Size = new System.Drawing.Size(432, 450);
            this.flowLayoutPanelSnapshots.TabIndex = 0;
            // 
            // AIWizardViewSnapshots
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 450);
            this.Controls.Add(this.flowLayoutPanelSnapshots);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIWizardViewSnapshots";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Snapshots";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelSnapshots;
    }
}
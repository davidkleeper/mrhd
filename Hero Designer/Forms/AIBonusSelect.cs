﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIBonusSelect : Form
    {
        public AIBonusSelect()
        {
            InitializeComponent();
            lblCap.Visible = false;
            textBoxCap.Visible = false;
            lblSelectedCap.Visible = false;
            lblBonus1.Visible = false;
            lblBonus1.Text = "";
            lblPvP1.Visible = false;
            lblCap1.Visible = false;
            btnRemoveBonus1.Visible = false;
            lblBonus2.Visible = false;
            lblPvP2.Visible = false;
            lblCap2.Visible = false;
            btnRemoveBonus2.Visible = false;
            lblBonus3.Visible = false;
            lblPvP3.Visible = false;
            lblCap3.Visible = false;
            btnRemoveBonus3.Visible = false;
            lblBonus4.Visible = false;
            lblPvP4.Visible = false;
            lblCap4.Visible = false;
            btnRemoveBonus4.Visible = false;
            lblBonus5.Visible = false;
            lblPvP5.Visible = false;
            lblCap5.Visible = false;
            btnRemoveBonus5.Visible = false;
            AddBonusItems();
            AddImportanceItems();
        }

        public void Setup(AIBonus bonusWizard)
        {
            _bonusWizard = bonusWizard;
            UpdateButtons(bonusWizard);
            UpdateCaps(_bonusWizard);
        }

        public void UpdateButtons(AIBonus bonusWizard)
        {
            if (null == bonusWizard) return;
            bonusWizard.BtnBack.Enabled = true;
            bonusWizard.BtnNext.Enabled = lblBonus1.Text != "" && -1 != cboxImportance.SelectedIndex;
            bonusWizard.BtnSave.Visible = false;
            bonusWizard.BtnSaveAIWizardConfig.Visible = false;
            bonusWizard.BtnLoadAIWizardConfig.Visible = false;
            btnPermaWizard.Visible = cboxImportance.SelectedItem.ToString() == TopToBottomCappedImportance;
        }

        public void UpdateCaps(AIBonus bonusWizard)
        {
            if (null == bonusWizard) return;
            bool enableCaps = cboxImportance.SelectedIndex == TopToBottomCappedImportanceIndex;
            BonusInfoWithWeight[] bonuses = bonusWizard.Config.BonusInfoWithWeightArray;
            if (!enableCaps)
            {
                lblCap1.Text = "-1";
                lblCap2.Text = "-1";
                lblCap3.Text = "-1";
                lblCap4.Text = "-1";
                lblCap5.Text = "-1";
            }
            if (null != bonuses)
            {
                if (!enableCaps)
                {
                    if (1 <= bonuses.Length)
                        bonuses[0].Cap = -1;
                    if (2 <= bonuses.Length)
                        bonuses[1].Cap = -1;
                    if (3 <= bonuses.Length)
                        bonuses[2].Cap = -1;
                    if (4 <= bonuses.Length)
                        bonuses[3].Cap = -1;
                    if (5 <= bonuses.Length)
                        bonuses[4].Cap = -1;
                }
                if (1 <= bonuses.Length)
                    lblCap1.Text = bonuses[0].Cap.ToString();
                if (2 <= bonuses.Length)
                    lblCap2.Text = bonuses[1].Cap.ToString();
                if (3 <= bonuses.Length)
                    lblCap3.Text = bonuses[2].Cap.ToString();
                if (4 <= bonuses.Length)
                    lblCap4.Text = bonuses[3].Cap.ToString();
                if (5 <= bonuses.Length)
                    lblCap5.Text = bonuses[4].Cap.ToString();
            }
            lblCap.Visible = enableCaps;
            textBoxCap.Visible = enableCaps;
            lblSelectedCap.Visible = enableCaps;
            lblCap1.Visible = enableCaps && bonuses != null && bonuses.Length >= 1;
            lblCap2.Visible = enableCaps && bonuses != null && bonuses.Length >= 2;
            lblCap3.Visible = enableCaps && bonuses != null && bonuses.Length >= 3;
            lblCap4.Visible = enableCaps && bonuses != null && bonuses.Length >= 4;
            lblCap5.Visible = enableCaps && bonuses != null && bonuses.Length >= 5;
        }

        public void UseConfig(AIConfig config)
        {
            BonusInfoWithWeight[] bonuses = config.BonusInfoWithWeightArray;
            cboxImportance.SelectedIndex = config.ImportanceOption;
            if (1 <= bonuses.Length)
                SetBonus(bonuses[0].DisplayName, bonuses[0].IncludePvP, bonuses[0].Cap, lblBonus1, lblPvP1, lblCap1, btnRemoveBonus1);
            if (2 <= bonuses.Length)
                SetBonus(bonuses[1].DisplayName, bonuses[1].IncludePvP, bonuses[1].Cap, lblBonus2, lblPvP2, lblCap2, btnRemoveBonus2);
            if (3 <= bonuses.Length)
                SetBonus(bonuses[2].DisplayName, bonuses[2].IncludePvP, bonuses[2].Cap, lblBonus3, lblPvP3, lblCap3, btnRemoveBonus3);
            if (4 <= bonuses.Length)
                SetBonus(bonuses[3].DisplayName, bonuses[3].IncludePvP, bonuses[3].Cap, lblBonus4, lblPvP4, lblCap4, btnRemoveBonus4);
            if (5 <= bonuses.Length)
                SetBonus(bonuses[4].DisplayName, bonuses[4].IncludePvP, bonuses[4].Cap, lblBonus5, lblPvP5, lblCap5, btnRemoveBonus5);
        }

        public List<BonusInfoWithWeight> GetBonuses()
        {
            List<BonusInfoWithWeight> bonusInfoList = new List<BonusInfoWithWeight>();

            if ("" != lblBonus1.Text) bonusInfoList.AddRange(BonusInfoWithWeight.GetBonusInfoWithWeight(lblBonus1.Text, GetImportance(1), bool.Parse(lblPvP1.Text), float.Parse(lblCap1.Text)));
            if ("" != lblBonus2.Text) bonusInfoList.AddRange(BonusInfoWithWeight.GetBonusInfoWithWeight(lblBonus2.Text, GetImportance(2), bool.Parse(lblPvP2.Text), float.Parse(lblCap2.Text)));
            if ("" != lblBonus3.Text) bonusInfoList.AddRange(BonusInfoWithWeight.GetBonusInfoWithWeight(lblBonus3.Text, GetImportance(3), bool.Parse(lblPvP3.Text), float.Parse(lblCap3.Text)));
            if ("" != lblBonus4.Text) bonusInfoList.AddRange(BonusInfoWithWeight.GetBonusInfoWithWeight(lblBonus4.Text, GetImportance(4), bool.Parse(lblPvP4.Text), float.Parse(lblCap4.Text)));
            if ("" != lblBonus5.Text) bonusInfoList.AddRange(BonusInfoWithWeight.GetBonusInfoWithWeight(lblBonus5.Text, GetImportance(5), bool.Parse(lblPvP5.Text), float.Parse(lblCap5.Text)));

            return bonusInfoList;
        }

        private void AddBonus()
        {
            if (null == cboxBonusToAdd.SelectedItem || String.IsNullOrEmpty(cboxBonusToAdd.SelectedItem.ToString()))
            {
                Console.Beep();
                return;
            }
            string selectedBonus = cboxBonusToAdd.SelectedItem.ToString();
            if (String.IsNullOrEmpty(selectedBonus) || selectedBonus == lblBonus1.Text || selectedBonus == lblBonus2.Text || selectedBonus == lblBonus3.Text || selectedBonus == lblBonus4.Text || selectedBonus == lblBonus5.Text)
            {
                Console.Beep();
                return;
            }
            bool selectedPvP = checkBoxPvP.Checked;
            float selectedCap = float.Parse(textBoxCap.Text);

            if (false == lblBonus1.Visible)
                SetBonus(selectedBonus, selectedPvP, selectedCap, lblBonus1, lblPvP1, lblCap1, btnRemoveBonus1);
            else if (false == lblBonus2.Visible)
                SetBonus(selectedBonus, selectedPvP, selectedCap, lblBonus2, lblPvP2, lblCap2, btnRemoveBonus2);
            else if (false == lblBonus3.Visible)
                SetBonus(selectedBonus, selectedPvP, selectedCap, lblBonus3, lblPvP3, lblCap3, btnRemoveBonus3);
            else if (false == lblBonus4.Visible)
                SetBonus(selectedBonus, selectedPvP, selectedCap, lblBonus4, lblPvP4, lblCap4, btnRemoveBonus4);
            else if (false == lblBonus5.Visible)
            {
                SetBonus(selectedBonus, selectedPvP, selectedCap, lblBonus5, lblPvP5, lblCap5, btnRemoveBonus5);
                btnAddBonus.Enabled = false;
            }
        }

        private void SetBonus(string bonus, bool pvp, float cap, Label bonusLable, Label pvpLable, Label capLabel, Button removeButton)
        {
            bonusLable.Text = bonus;
            pvpLable.Text = pvp.ToString();
            capLabel.Text = cap.ToString();
            bonusLable.Visible = true;
            pvpLable.Visible = true;
            capLabel.Visible = cboxImportance.SelectedIndex == TopToBottomCappedImportanceIndex;
            removeButton.Visible = true;
            UpdateButtons(_bonusWizard);
        }

        private void RemoveBonus(Label bonusLable, Label pvpLable, Label capLabel, Button removeButton)
        {
            bonusLable.Text = "";
            pvpLable.Text = "";
            bonusLable.Visible = false;
            pvpLable.Visible = false;
            capLabel.Visible = false;
            removeButton.Visible = false;
            btnAddBonus.Enabled = true;
        }

        private void CompactBonuses()
        {
            Label[] selectedBonuses = new Label[5];
            Label[] selectedPvPs = new Label[5];
            Label[] selectedCaps = new Label[5];
            Button[] selectedRemoveButtons = new Button[5];

            selectedBonuses[0] = lblBonus1;
            selectedBonuses[1] = lblBonus2;
            selectedBonuses[2] = lblBonus3;
            selectedBonuses[3] = lblBonus4;
            selectedBonuses[4] = lblBonus5;
            selectedPvPs[0] = lblPvP1;
            selectedPvPs[1] = lblPvP2;
            selectedPvPs[2] = lblPvP3;
            selectedPvPs[3] = lblPvP4;
            selectedPvPs[4] = lblPvP5;
            selectedCaps[0] = lblCap1;
            selectedCaps[1] = lblCap2;
            selectedCaps[2] = lblCap3;
            selectedCaps[3] = lblCap4;
            selectedCaps[4] = lblCap5;
            selectedRemoveButtons[0] = btnRemoveBonus1;
            selectedRemoveButtons[1] = btnRemoveBonus2;
            selectedRemoveButtons[2] = btnRemoveBonus3;
            selectedRemoveButtons[3] = btnRemoveBonus4;
            selectedRemoveButtons[4] = btnRemoveBonus5;

            for (int selectedIndex = 1; selectedIndex < selectedBonuses.Length; selectedIndex++)
            {
                if (!selectedBonuses[selectedIndex].Visible)
                    continue;
                for (int freeIndex = selectedIndex - 1; freeIndex >= 0; freeIndex--)
                {
                    if (selectedBonuses[freeIndex].Visible)
                        continue;

                    System.Windows.Forms.Label sourceBonus = selectedBonuses[selectedIndex];
                    System.Windows.Forms.Label sourcePvP = selectedPvPs[selectedIndex];
                    System.Windows.Forms.Label sourceCap = selectedCaps[selectedIndex];
                    System.Windows.Forms.Button sourceRemoveButtons = selectedRemoveButtons[selectedIndex];
                    string sourceBonusText = sourceBonus.Text;
                    string sourcePvPText = sourcePvP.Text;
                    string sourceCapText = sourceCap.Text;
                    System.Windows.Forms.Label destBonus = selectedBonuses[freeIndex];
                    System.Windows.Forms.Label destPvP = selectedPvPs[freeIndex];
                    System.Windows.Forms.Label destCap = selectedCaps[freeIndex];
                    System.Windows.Forms.Button destRemoveButtons = selectedRemoveButtons[freeIndex];

                    SetBonus(sourceBonusText, bool.Parse(sourcePvPText), float.Parse(sourceCapText), destBonus, destPvP, destCap, destRemoveButtons);
                    RemoveBonus(sourceBonus, sourcePvP, sourceCap, sourceRemoveButtons);
                    break;
                }
            }
            UpdateButtons(_bonusWizard);
        }

        private void AddBonusItems()
        {
            cboxBonusToAdd.Items.Add(BonusInfo.Accuracy.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Confuse.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DamageBuff.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseAll.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseAoE.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseCold.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseEnergy.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseFire.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseLethal.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseMelee.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseNegative.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefensePsionic.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseRanged.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.DefenseSmashing.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.EnduranceDiscount.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Fly.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Heal.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.HitPoints.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Hold.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Immobilize.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Jump.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.KnockbackProtection.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Knockdown.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.MaxEndurance.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.MezEnhance.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.MezResistance.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.PetDefense.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.PetResistance.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Range.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.RechargeTime.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Recovery.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ReduceDamage.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Regeneration.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.RepelResistance.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceCold.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceEnergy.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceFire.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceLethal.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceNegative.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistancePsionic.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceSmashing.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceToxic.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceSlowMovement.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.ResistanceSlowRecharge.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Running.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Slow.DisplayName);
            cboxBonusToAdd.Items.Add(BonusInfo.Stun.DisplayName);
        }

        private void AddImportanceItems()
        {
            cboxImportance.Items.Add(TopToBottomImportance);
            cboxImportance.Items.Add(TopToBottomCappedImportance);
            cboxImportance.Items.Add(TwiceTheNextBonusImportance);
            cboxImportance.Items.Add(DistributeEvenlyImportance);
            cboxImportance.SelectedIndex = 0;
        }

        private float GetImportance(int bonusIndex)
        {
            if (TopToBottomImportance == _importance)
            {
                return 1.0f;
            }
            else if (TopToBottomCappedImportance == _importance)
            {
                return 1.0f;
            }
            else if (TwiceTheNextBonusImportance == _importance)
            {
                int bonusCount = GetBonusCount();
                if (bonusCount < bonusIndex)
                    return 0.0f; ;
                float[] importanceArray = new float[] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                if (5 == bonusCount) importanceArray = new float[] { 0.33f, 0.4f, 0.5f, 0.66f, 1.0f };
                else if (4 == bonusCount) importanceArray = new float[] { 0.4f, 0.5f, 0.66f, 1.0f };
                else if (3 == bonusCount) importanceArray = new float[] { 0.5f, 0.66f, 1.0f };
                else if (2 == bonusCount) importanceArray = new float[] { 0.66f, 1.0f };
                else if (1 == bonusCount) importanceArray = new float[] { 1.0f };
                return importanceArray[bonusIndex - 1];
            }
            else if (DistributeEvenlyImportance == _importance)
            {
                int bonusCount = GetBonusCount();
                if (bonusCount < bonusIndex)
                    return 0.0f; ;
                float[] importanceArray = new float[] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                if (5 == bonusCount) importanceArray = new float[] { 0.2f, 0.25f, 0.33f, 0.5f, 1.0f };
                else if (4 == bonusCount) importanceArray = new float[] { 0.25f, 0.33f, 0.5f, 1.0f };
                else if (3 == bonusCount) importanceArray = new float[] { 0.33f, 0.5f, 1.0f };
                else if (2 == bonusCount) importanceArray = new float[] { 0.5f, 1.0f };
                else if (1 == bonusCount) importanceArray = new float[] { 1.0f };
                return importanceArray[bonusIndex - 1];
            }
            return 0.0f;
        }

        private int GetBonusCount()
        {
            if (lblBonus5.Visible) return 5;
            if (lblBonus4.Visible) return 4;
            if (lblBonus3.Visible) return 3;
            if (lblBonus2.Visible) return 2;
            if (lblBonus1.Visible) return 1;
            return 0;
        }

        public void Speak(SpeechSynthesizer speech)
        {
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
        }

        private void btnAddBonus_Click(object sender, EventArgs e)
        {
            AddBonus();
        }

        private void btnRemoveBonus1_Click(object sender, EventArgs e)
        {
            RemoveBonus(lblBonus1, lblPvP1, lblCap1, btnRemoveBonus1);
            CompactBonuses();
        }

        private void btnRemoveBonus2_Click(object sender, EventArgs e)
        {
            RemoveBonus(lblBonus2, lblPvP2, lblCap2, btnRemoveBonus2);
            CompactBonuses();
        }

        private void btnRemoveBonus3_Click(object sender, EventArgs e)
        {
            RemoveBonus(lblBonus3, lblPvP3, lblCap3, btnRemoveBonus3);
            CompactBonuses();
        }

        private void btnRemoveBonus4_Click(object sender, EventArgs e)
        {
            RemoveBonus(lblBonus4, lblPvP4, lblCap4, btnRemoveBonus4);
            CompactBonuses();
        }

        private void btnRemoveBonus5_Click(object sender, EventArgs e)
        {
            RemoveBonus(lblBonus5, lblPvP5, lblCap5, btnRemoveBonus5);
            CompactBonuses();
        }

        private void cboxBonusToAdd_Change(object sender, EventArgs e)
        {
            string selectedItem = cboxBonusToAdd.SelectedItem.ToString();
            textBoxCap.Text = "-1";
        }

        private void cboxImportance_Changed(object sender, EventArgs e)
        {
            _importance = cboxImportance.SelectedItem.ToString();
            if (null == _bonusWizard) return;
            UpdateButtons(_bonusWizard);
            UpdateCaps(_bonusWizard);
        }

        private void btnPermaWizard_Click(object sender, EventArgs e)
        {
            AIPerma frmPerma = new AIPerma();
            if (frmPerma.ShowDialog(this) == DialogResult.OK)
            {
            }

            frmPerma.Dispose();
        }

        public int ImportanceOption { get { return cboxImportance.SelectedIndex; } }
        private string[] _selectedBonuses = new string[3];
        private string _importance = "";
        private string TopToBottomImportance = "Top to Bottom";
        private string TopToBottomCappedImportance = "Top to Bottom Capped";
        private string TwiceTheNextBonusImportance = "Twice the Next Bonus";
        private string DistributeEvenlyImportance = "Distribute Evenly";
        private int TopToBottomImportanceIndex = 0;
        private int TopToBottomCappedImportanceIndex = 1;
        private int TwiceTheNextBonusImportanceIndex = 2;
        private int DistributeEvenlyImportanceIndex = 3;
        private AIBonus _bonusWizard;
    }
}

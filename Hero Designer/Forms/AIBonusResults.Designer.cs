﻿namespace Hero_Designer.Forms
{
    partial class AIBonusResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntro3 = new System.Windows.Forms.Label();
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.textBoxBuild = new System.Windows.Forms.TextBox();
            this.buttonViewLog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIntro3
            // 
            this.lblIntro3.AutoSize = true;
            this.lblIntro3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro3.Location = new System.Drawing.Point(14, 82);
            this.lblIntro3.Name = "lblIntro3";
            this.lblIntro3.Size = new System.Drawing.Size(255, 17);
            this.lblIntro3.TabIndex = 67;
            this.lblIntro3.Text = "The optimized values are shown below.";
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(14, 47);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(324, 17);
            this.lblIntro2.TabIndex = 56;
            this.lblIntro2.Text = "Click Save to save the optimized character to disk.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(14, 14);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(178, 17);
            this.lblIntro1.TabIndex = 55;
            this.lblIntro1.Text = "Optimizations Complete";
            // 
            // textBoxBuild
            // 
            this.textBoxBuild.Location = new System.Drawing.Point(17, 120);
            this.textBoxBuild.Multiline = true;
            this.textBoxBuild.Name = "textBoxBuild";
            this.textBoxBuild.ReadOnly = true;
            this.textBoxBuild.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxBuild.Size = new System.Drawing.Size(603, 285);
            this.textBoxBuild.TabIndex = 68;
            // 
            // buttonViewLog
            // 
            this.buttonViewLog.Location = new System.Drawing.Point(515, 76);
            this.buttonViewLog.Name = "buttonViewLog";
            this.buttonViewLog.Size = new System.Drawing.Size(105, 28);
            this.buttonViewLog.TabIndex = 84;
            this.buttonViewLog.Text = "&View Log";
            this.buttonViewLog.UseVisualStyleBackColor = true;
            this.buttonViewLog.Click += new System.EventHandler(this.buttonViewLog_Click);
            // 
            // AIWizard5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonViewLog);
            this.Controls.Add(this.textBoxBuild);
            this.Controls.Add(this.lblIntro3);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIWizard5";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIntro3;
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.TextBox textBoxBuild;
        private System.Windows.Forms.Button buttonViewLog;
    }
}
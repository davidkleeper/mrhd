﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusViewAndSaveSnapshotsItem : UserControl
    {
        public AIToon ToonSnapshot;
        public int SnapshotNumber;
        public AIBonusSelect Wizard2;
        public AIBonus _Wizard;

        public AIBonusViewAndSaveSnapshotsItem()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus bonusWizard, AIBonusSelect wizard2, AIToon snapShot, int snapshotIndex)
        {
            ToonSnapshot = snapShot;
            SnapshotNumber = snapshotIndex;
            lblSnapshotName.Text = "Snapshot " + snapshotIndex.ToString();
            Wizard2 = wizard2;
            _Wizard = bonusWizard;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            View();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void View()
        {
            AIBonusViewSnapshot snapshotDialog = new AIBonusViewSnapshot();
            BonusInfoWithWeight[] desiredBonuses = Wizard2.GetBonuses().ToArray();
            string[] lines = AIToonUtils.GetLines(ToonSnapshot, desiredBonuses, _Wizard.Config);
            snapshotDialog.Setup(lines);
            snapshotDialog.ShowDialog(this);
        }

        private void Save()
        {
            string filename = "";
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog.Title = "Save AI Character";
            saveFileDialog.FileName = "AI Character Snapshot " + SnapshotNumber.ToString() + ".mxd";
            saveFileDialog.DefaultExt = "mxd";
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = "Hero/Villian Builds (*.mxd)|*.mxd|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            filename = saveFileDialog.FileName;

            AIToonUtils.WriteAIToon(ToonSnapshot, filename);
        }
    }
}

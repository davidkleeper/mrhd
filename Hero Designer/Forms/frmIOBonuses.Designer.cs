﻿
using System;

namespace Hero_Designer.Forms
{
    partial class formIOBonusesSpreadsheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewIOBonuses = new System.Windows.Forms.DataGridView();
            this.btnCloseIOBonusSpreadsheet = new System.Windows.Forms.Button();
            this.btnExportToCSV = new System.Windows.Forms.Button();
            this.IOSetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOSetType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOEffectType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOSubType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOScale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOSlotsRequired = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOMinLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOMaxLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IOPVPBonus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIOBonuses)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.dataGridViewIOBonuses);
            this.panel1.Location = new System.Drawing.Point(12, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1095, 384);
            this.panel1.TabIndex = 0;
            // 
            // dataGridViewIOBonuses
            // 
            this.dataGridViewIOBonuses.AllowUserToAddRows = false;
            this.dataGridViewIOBonuses.AllowUserToDeleteRows = false;
            this.dataGridViewIOBonuses.AllowUserToOrderColumns = true;
            this.dataGridViewIOBonuses.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridViewIOBonuses.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewIOBonuses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewIOBonuses.CausesValidation = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIOBonuses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewIOBonuses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIOBonuses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IOSetName,
            this.IOSetType,
            this.IOEffectType,
            this.IOSubType,
            this.IOScale,
            this.IOSlotsRequired,
            this.IOMinLevel,
            this.IOMaxLevel,
            this.IOPVPBonus});
            this.dataGridViewIOBonuses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIOBonuses.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewIOBonuses.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewIOBonuses.MultiSelect = false;
            this.dataGridViewIOBonuses.Name = "dataGridViewIOBonuses";
            this.dataGridViewIOBonuses.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIOBonuses.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewIOBonuses.RowHeadersWidth = 51;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            this.dataGridViewIOBonuses.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewIOBonuses.RowTemplate.Height = 24;
            this.dataGridViewIOBonuses.Size = new System.Drawing.Size(1095, 384);
            this.dataGridViewIOBonuses.TabIndex = 0;
            // 
            // btnCloseIOBonusSpreadsheet
            // 
            this.btnCloseIOBonusSpreadsheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseIOBonusSpreadsheet.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseIOBonusSpreadsheet.Location = new System.Drawing.Point(958, 398);
            this.btnCloseIOBonusSpreadsheet.Name = "btnCloseIOBonusSpreadsheet";
            this.btnCloseIOBonusSpreadsheet.Size = new System.Drawing.Size(152, 40);
            this.btnCloseIOBonusSpreadsheet.TabIndex = 1;
            this.btnCloseIOBonusSpreadsheet.Text = "&Close";
            this.btnCloseIOBonusSpreadsheet.UseVisualStyleBackColor = true;
            // 
            // btnExportToCSV
            // 
            this.btnExportToCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportToCSV.Location = new System.Drawing.Point(12, 398);
            this.btnExportToCSV.Name = "btnExportToCSV";
            this.btnExportToCSV.Size = new System.Drawing.Size(152, 40);
            this.btnExportToCSV.TabIndex = 2;
            this.btnExportToCSV.Text = "&Export To CSV";
            this.btnExportToCSV.UseVisualStyleBackColor = true;
            this.btnExportToCSV.Click += new System.EventHandler(this.btnExportToCSV_Click);
            // 
            // IOSetName
            // 
            this.IOSetName.HeaderText = "Name";
            this.IOSetName.MinimumWidth = 6;
            this.IOSetName.Name = "IOSetName";
            this.IOSetName.ReadOnly = true;
            // 
            // IOSetType
            // 
            this.IOSetType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IOSetType.HeaderText = "Set Type";
            this.IOSetType.MinimumWidth = 6;
            this.IOSetType.Name = "IOSetType";
            this.IOSetType.ReadOnly = true;
            this.IOSetType.Width = 87;
            // 
            // IOEffectType
            // 
            this.IOEffectType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IOEffectType.HeaderText = "Effect Type";
            this.IOEffectType.MinimumWidth = 6;
            this.IOEffectType.Name = "IOEffectType";
            this.IOEffectType.ReadOnly = true;
            // 
            // IOSubType
            // 
            this.IOSubType.HeaderText = "Damage/Enhancement/ Mez Type";
            this.IOSubType.MinimumWidth = 6;
            this.IOSubType.Name = "IOSubType";
            this.IOSubType.ReadOnly = true;
            // 
            // IOScale
            // 
            this.IOScale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IOScale.HeaderText = "Scale";
            this.IOScale.MinimumWidth = 6;
            this.IOScale.Name = "IOScale";
            this.IOScale.ReadOnly = true;
            this.IOScale.Width = 72;
            // 
            // IOSlotsRequired
            // 
            this.IOSlotsRequired.HeaderText = "Slots Required";
            this.IOSlotsRequired.MinimumWidth = 6;
            this.IOSlotsRequired.Name = "IOSlotsRequired";
            this.IOSlotsRequired.ReadOnly = true;
            // 
            // IOMinLevel
            // 
            this.IOMinLevel.HeaderText = "Min Level";
            this.IOMinLevel.MinimumWidth = 6;
            this.IOMinLevel.Name = "IOMinLevel";
            this.IOMinLevel.ReadOnly = true;
            // 
            // IOMaxLevel
            // 
            this.IOMaxLevel.HeaderText = "Max Level";
            this.IOMaxLevel.MinimumWidth = 6;
            this.IOMaxLevel.Name = "IOMaxLevel";
            this.IOMaxLevel.ReadOnly = true;
            // 
            // IOPVPBonus
            // 
            this.IOPVPBonus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IOPVPBonus.HeaderText = "PVP Bonus?";
            this.IOPVPBonus.MinimumWidth = 6;
            this.IOPVPBonus.Name = "IOPVPBonus";
            this.IOPVPBonus.ReadOnly = true;
            this.IOPVPBonus.Width = 107;
            // 
            // formIOBonusesSpreadsheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCloseIOBonusSpreadsheet;
            this.ClientSize = new System.Drawing.Size(1122, 450);
            this.Controls.Add(this.btnExportToCSV);
            this.Controls.Add(this.btnCloseIOBonusSpreadsheet);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formIOBonusesSpreadsheet";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "IO Bonuses Spreadsheet";
            this.TopMost = true;
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIOBonuses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public void LoadIOBonuses()
        {
            for (int bonusIndex = 1; bonusIndex < bonuses.Length; bonusIndex++)
            {
                string bonus = bonuses[bonusIndex];
                System.Console.WriteLine(bonus);
                string[] bonusColumns = bonus.Split(',');
                int rowIndex = dataGridViewIOBonuses.Rows.Add();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOSetName"].Value = bonusColumns[0].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOSetType"].Value = bonusColumns[1].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOEffectType"].Value = bonusColumns[2].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOSubType"].Value = bonusColumns[3].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOScale"].Value = bonusColumns[4].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOSlotsRequired"].Value = bonusColumns[5].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOMinLevel"].Value = bonusColumns[6].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOMaxLevel"].Value = bonusColumns[7].Trim();
                dataGridViewIOBonuses.Rows[rowIndex].Cells["IOPVPBonus"].Value = bonusColumns[8].Trim();
            }
        }

        public void SaveIOBonuses()
        {
            string filename = "";
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog.Title = "Save IO Bonuses To .csv File";
            saveFileDialog.FileName = "IOSetBonuses.csv";
            saveFileDialog.DefaultExt = "csv";
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*"; 
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            filename = saveFileDialog.FileName;
            System.IO.File.WriteAllLines(filename, bonuses);
        }

        private string[] bonuses = EnhancementSet.GetList();
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewIOBonuses;
        private System.Windows.Forms.Button btnCloseIOBonusSpreadsheet;
        private System.Windows.Forms.Button btnExportToCSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOSetName;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOSetType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOEffectType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOSubType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOScale;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOSlotsRequired;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOMinLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOMaxLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IOPVPBonus;
    }
}
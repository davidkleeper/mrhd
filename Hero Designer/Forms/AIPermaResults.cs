﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIPermaResults : Form
    {
        public Button btnBack;
        public Button btnNext;
        public Button btnCancel;
        public AIPermaSelect wizard2;

        public AIPermaResults()
        {
            InitializeComponent();
        }
        public void Setup(Button btnBack, Button btnNext, Button btnCancel, bool speak)
        {
            this.btnBack = btnBack;
            this.btnNext = btnNext;
            this.btnCancel = btnCancel;
            UpdateButtons();
            DisplayPermaValues();
            if (speak) Speak();
        }

        public void UpdateButtons()
        {
            if (null != btnNext) btnNext.Enabled = false;
        }

        public void DisplayPermaValues()
        {
            if (null == wizard2) return;
            IPower[] powers = wizard2.GetPowers();
            string[] permaValuesDisplay = new string[powers.Length];
            for (int index = 0; index < powers.Length; index++)
            {
                double permaValue = Math.Round(powers[index].GetPermaValue(), 2);
                permaValuesDisplay[index] = powers[index].DisplayName +": " + permaValue.ToString() + "% recharge (Bonus Wizard Cap value equals " + (permaValue / 100).ToString() + ")";
            }
            textBoxPermaValues.Lines = permaValuesDisplay;
        }

        public void Speak()
        {
            SpeechSynthesizer speech = new SpeechSynthesizer();
            speech.SelectVoiceByHints(VoiceGender.Female, VoiceAge.NotSet);
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
            speech.SpeakAsync(lblIntro3.Text);
        }
    }
}

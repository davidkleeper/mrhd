﻿namespace Hero_Designer.Forms
{
    partial class AIBonusGenerationsAndIterations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.lblGenerations = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblIterations = new System.Windows.Forms.Label();
            this.lblIntro3 = new System.Windows.Forms.Label();
            this.trackBarGenerations = new System.Windows.Forms.TrackBar();
            this.trackBarIterations = new System.Windows.Forms.TrackBar();
            this.lblOptions = new System.Windows.Forms.Label();
            this.linkControlPowerEvolution = new System.Windows.Forms.LinkLabel();
            this.linkControlSlotEvolution = new System.Windows.Forms.LinkLabel();
            this.linkControlPowerBonus = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGenerations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIterations)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(12, 45);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(507, 17);
            this.lblIntro2.TabIndex = 38;
            this.lblIntro2.Text = "More generations and iterations give better results, but take longer to compute.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(12, 12);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(439, 17);
            this.lblIntro1.TabIndex = 37;
            this.lblIntro1.Text = "Select the Number Of Generations And Iterations You Want";
            // 
            // lblGenerations
            // 
            this.lblGenerations.AutoSize = true;
            this.lblGenerations.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenerations.Location = new System.Drawing.Point(12, 134);
            this.lblGenerations.Name = "lblGenerations";
            this.lblGenerations.Size = new System.Drawing.Size(125, 17);
            this.lblGenerations.TabIndex = 41;
            this.lblGenerations.Text = "Generations: 25";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 42;
            this.label2.Text = "Faster";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(520, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 43;
            this.label3.Text = "Better";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(520, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 48;
            this.label4.Text = "Better";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 47;
            this.label5.Text = "Faster";
            // 
            // lblIterations
            // 
            this.lblIterations.AutoSize = true;
            this.lblIterations.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIterations.Location = new System.Drawing.Point(12, 216);
            this.lblIterations.Name = "lblIterations";
            this.lblIterations.Size = new System.Drawing.Size(104, 17);
            this.lblIterations.TabIndex = 46;
            this.lblIterations.Text = "Iterations: 10";
            // 
            // lblIntro3
            // 
            this.lblIntro3.AutoSize = true;
            this.lblIntro3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro3.Location = new System.Drawing.Point(12, 79);
            this.lblIntro3.Name = "lblIntro3";
            this.lblIntro3.Size = new System.Drawing.Size(640, 17);
            this.lblIntro3.TabIndex = 49;
            this.lblIntro3.Text = "Once you\'ve done this, we\'re ready to optimize your character. This will not over" +
    "write your character.";
            // 
            // trackBarGenerations
            // 
            this.trackBarGenerations.Location = new System.Drawing.Point(15, 157);
            this.trackBarGenerations.Maximum = 2500;
            this.trackBarGenerations.Minimum = 25;
            this.trackBarGenerations.Name = "trackBarGenerations";
            this.trackBarGenerations.Size = new System.Drawing.Size(553, 56);
            this.trackBarGenerations.TabIndex = 50;
            this.trackBarGenerations.TickFrequency = 0;
            this.trackBarGenerations.Value = 25;
            this.trackBarGenerations.Scroll += new System.EventHandler(this.trackBarGenerations_Scroll);
            // 
            // trackBarIterations
            // 
            this.trackBarIterations.Location = new System.Drawing.Point(15, 236);
            this.trackBarIterations.Maximum = 250;
            this.trackBarIterations.Minimum = 10;
            this.trackBarIterations.Name = "trackBarIterations";
            this.trackBarIterations.Size = new System.Drawing.Size(553, 56);
            this.trackBarIterations.TabIndex = 51;
            this.trackBarIterations.TickFrequency = 0;
            this.trackBarIterations.Value = 10;
            this.trackBarIterations.Scroll += new System.EventHandler(this.trackBarIterations_Scroll);
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptions.Location = new System.Drawing.Point(12, 308);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(64, 17);
            this.lblOptions.TabIndex = 52;
            this.lblOptions.Text = "Options";
            // 
            // linkControlPowerEvolution
            // 
            this.linkControlPowerEvolution.AutoSize = true;
            this.linkControlPowerEvolution.Location = new System.Drawing.Point(12, 334);
            this.linkControlPowerEvolution.Name = "linkControlPowerEvolution";
            this.linkControlPowerEvolution.Size = new System.Drawing.Size(330, 17);
            this.linkControlPowerEvolution.TabIndex = 53;
            this.linkControlPowerEvolution.TabStop = true;
            this.linkControlPowerEvolution.Text = "Control which powers are changed during evolution";
            this.linkControlPowerEvolution.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkControlPowerEvolution_LinkClicked);
            // 
            // linkControlSlotEvolution
            // 
            this.linkControlSlotEvolution.AutoSize = true;
            this.linkControlSlotEvolution.Location = new System.Drawing.Point(12, 360);
            this.linkControlSlotEvolution.Name = "linkControlSlotEvolution";
            this.linkControlSlotEvolution.Size = new System.Drawing.Size(330, 17);
            this.linkControlSlotEvolution.TabIndex = 53;
            this.linkControlSlotEvolution.TabStop = true;
            this.linkControlSlotEvolution.Text = "Control which slots are changed during evolution";
            this.linkControlSlotEvolution.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkControlSlotEvolution_LinkClicked);
            // 
            // linkControlPowerBonus
            // 
            this.linkControlPowerBonus.AutoSize = true;
            this.linkControlPowerBonus.Location = new System.Drawing.Point(12, 386);
            this.linkControlPowerBonus.Name = "linkControlPowerBonus";
            this.linkControlPowerBonus.Size = new System.Drawing.Size(330, 17);
            this.linkControlPowerBonus.TabIndex = 54;
            this.linkControlPowerBonus.TabStop = true;
            this.linkControlPowerBonus.Text = "Control which powers affect bonuses";
            this.linkControlPowerBonus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkControlPowerBonuses_LinkClicked);
            // 
            // AIBonus3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.linkControlPowerEvolution);
            this.Controls.Add(this.linkControlSlotEvolution);
            this.Controls.Add(this.linkControlPowerBonus);
            this.Controls.Add(this.lblOptions);
            this.Controls.Add(this.lblIntro3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblIterations);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblGenerations);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.Controls.Add(this.trackBarIterations);
            this.Controls.Add(this.trackBarGenerations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIBonus3";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGenerations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIterations)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.Label lblGenerations;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblIterations;
        private System.Windows.Forms.Label lblIntro3;
        private System.Windows.Forms.TrackBar trackBarGenerations;
        private System.Windows.Forms.TrackBar trackBarIterations;
        private System.Windows.Forms.Label lblOptions;
        private System.Windows.Forms.LinkLabel linkControlPowerEvolution;
        private System.Windows.Forms.LinkLabel linkControlSlotEvolution;
        private System.Windows.Forms.LinkLabel linkControlPowerBonus;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIPerma : Form
    {

        public AIPerma()
        {
            InitializeComponent();
            checkBoxSpeak.Checked = false;
            Next();
        }

        private void btnNext_Click_1(object sender, EventArgs e)
        {
            Next();
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            Back();
        }

        private void checkBoxSpeak_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSpeak.Checked)
                Speak();
        }

        private void Speak()
        {
            AIPermaIntro wizard1 = frm[top] as AIPermaIntro;
            AIPermaSelect wizard2 = frm[top] as AIPermaSelect;
            AIPermaResults wizard3 = frm[top] as AIPermaResults;
            if (wizard1 != null) wizard1.Speak();
            else if (wizard2 != null) wizard2.Speak();
            else if (wizard3 != null) wizard3.Speak();
        }
    }
}
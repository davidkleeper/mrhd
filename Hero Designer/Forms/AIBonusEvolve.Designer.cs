﻿namespace Hero_Designer.Forms
{
    partial class AIBonusEvolve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                if (null != _tokenSource)
                {
                    _tokenSource.Dispose();
                    _tokenSource = null;
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.lblGenerations = new System.Windows.Forms.Label();
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.lblIterations = new System.Windows.Forms.Label();
            this.lblCurrentIteration = new System.Windows.Forms.Label();
            this.lblCurrentGeneration = new System.Windows.Forms.Label();
            this.progressBarEvolve = new System.Windows.Forms.ProgressBar();
            this.chartBonusProgress = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnViewSnapshots = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartBonusProgress)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGenerations
            // 
            this.lblGenerations.AutoSize = true;
            this.lblGenerations.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenerations.Location = new System.Drawing.Point(11, 90);
            this.lblGenerations.Name = "lblGenerations";
            this.lblGenerations.Size = new System.Drawing.Size(107, 17);
            this.lblGenerations.TabIndex = 59;
            this.lblGenerations.Text = "Generations: ";
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(11, 48);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(465, 17);
            this.lblIntro2.TabIndex = 56;
            this.lblIntro2.Text = "This may take a while. My owner is too cheap to upgrade my processors.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(11, 15);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(215, 17);
            this.lblIntro1.TabIndex = 55;
            this.lblIntro1.Text = "I\'m Computing Your Bonuses";
            // 
            // lblIterations
            // 
            this.lblIterations.AutoSize = true;
            this.lblIterations.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIterations.Location = new System.Drawing.Point(11, 116);
            this.lblIterations.Name = "lblIterations";
            this.lblIterations.Size = new System.Drawing.Size(86, 17);
            this.lblIterations.TabIndex = 64;
            this.lblIterations.Text = "Iterations: ";
            // 
            // lblCurrentIteration
            // 
            this.lblCurrentIteration.AutoSize = true;
            this.lblCurrentIteration.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentIteration.Location = new System.Drawing.Point(216, 116);
            this.lblCurrentIteration.Name = "lblCurrentIteration";
            this.lblCurrentIteration.Size = new System.Drawing.Size(146, 17);
            this.lblCurrentIteration.TabIndex = 66;
            this.lblCurrentIteration.Text = "Current Iteration: 0";
            // 
            // lblCurrentGeneration
            // 
            this.lblCurrentGeneration.AutoSize = true;
            this.lblCurrentGeneration.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentGeneration.Location = new System.Drawing.Point(216, 90);
            this.lblCurrentGeneration.Name = "lblCurrentGeneration";
            this.lblCurrentGeneration.Size = new System.Drawing.Size(167, 17);
            this.lblCurrentGeneration.TabIndex = 65;
            this.lblCurrentGeneration.Text = "Current Generation: 0";
            // 
            // progressBarEvolve
            // 
            this.progressBarEvolve.Location = new System.Drawing.Point(14, 150);
            this.progressBarEvolve.Name = "progressBarEvolve";
            this.progressBarEvolve.Size = new System.Drawing.Size(369, 23);
            this.progressBarEvolve.TabIndex = 67;
            // 
            // chartBonusProgress
            // 
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.AxisX.MajorGrid.LineWidth = 0;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.Title = "Snapshots";
            chartArea1.AxisY.Title = "Bonus";
            chartArea1.BackColor = System.Drawing.SystemColors.ButtonFace;
            chartArea1.Name = "ChartArea1";
            this.chartBonusProgress.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartBonusProgress.Legends.Add(legend1);
            this.chartBonusProgress.Location = new System.Drawing.Point(14, 196);
            this.chartBonusProgress.Name = "chartBonusProgress";
            this.chartBonusProgress.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            this.chartBonusProgress.Size = new System.Drawing.Size(585, 203);
            this.chartBonusProgress.TabIndex = 68;
            title1.Name = "Title1";
            title1.Text = "Snapshots Update Every 10 Generations";
            this.chartBonusProgress.Titles.Add(title1);
            // 
            // btnViewSnapshots
            // 
            this.btnViewSnapshots.Location = new System.Drawing.Point(454, 119);
            this.btnViewSnapshots.Name = "btnViewSnapshots";
            this.btnViewSnapshots.Size = new System.Drawing.Size(145, 57);
            this.btnViewSnapshots.TabIndex = 69;
            this.btnViewSnapshots.Text = "&View And Save Snapshots";
            this.btnViewSnapshots.UseVisualStyleBackColor = true;
            this.btnViewSnapshots.Click += new System.EventHandler(this.btnViewSnapshots_Click);
            // 
            // AIWizard4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnViewSnapshots);
            this.Controls.Add(this.chartBonusProgress);
            this.Controls.Add(this.progressBarEvolve);
            this.Controls.Add(this.lblCurrentIteration);
            this.Controls.Add(this.lblCurrentGeneration);
            this.Controls.Add(this.lblIterations);
            this.Controls.Add(this.lblGenerations);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIWizard4";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            ((System.ComponentModel.ISupportInitialize)(this.chartBonusProgress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblGenerations;
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.Label lblIterations;
        private System.Windows.Forms.Label lblCurrentIteration;
        private System.Windows.Forms.Label lblCurrentGeneration;
        private System.Windows.Forms.ProgressBar progressBarEvolve;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBonusProgress;
        private System.Windows.Forms.Button btnViewSnapshots;
    }
}
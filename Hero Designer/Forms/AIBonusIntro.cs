﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIBonusIntro : Form
    {
        public AIBonusIntro()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus bonusWizard)
        {
            UpdateButtons(bonusWizard);
        }

        public void UpdateButtons(AIBonus bonusWizard)
        {
            bonusWizard.BtnBack.Enabled = false;
            bonusWizard.BtnNext.Enabled = bonusWizard.OrigialToon != null;
            bonusWizard.BtnSave.Visible = false;
            bonusWizard.BtnSaveAIWizardConfig.Visible = false;
            bonusWizard.BtnLoadAIWizardConfig.Visible = bonusWizard.OrigialToon != null;
        }

        public void Speak(SpeechSynthesizer speech)
        {
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
            speech.SpeakAsync(lblIntro3.Text);
        }
    }
}
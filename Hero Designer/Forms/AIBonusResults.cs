﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace Hero_Designer.Forms
{
    public partial class AIBonusResults : Form
    {
        public AIBonusResults()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus bonusWizard)
        {
            _bonusWizard = bonusWizard;
            UpdateButtons(bonusWizard);
        }

        public void UpdateButtons(AIBonus bonusWizard)
        {
            bonusWizard.BtnBack.Enabled = false;
            bonusWizard.BtnNext.Enabled = false;
            bonusWizard.BtnSave.Enabled = true;
            bonusWizard.BtnSave.Visible = true;
            bonusWizard.BtnSaveAIWizardConfig.Visible = false;
            bonusWizard.BtnLoadAIWizardConfig.Visible = false;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            ShowResults();
        }

        public void ShowResults()
        {
            AIToon toon = _bonusWizard.EvolveForm.FinalToon;
            BonusInfoWithWeight[] desiredBonuses = _bonusWizard.SelectForm.GetBonuses().ToArray();
            string[] lines = AIToonUtils.GetLines(toon, desiredBonuses, _bonusWizard.Config);
            textBoxBuild.Lines = lines;
        }

        public void Speak(SpeechSynthesizer speech)
        {
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
            speech.SpeakAsync(lblIntro3.Text);
        }

        private void buttonViewLog_Click(object sender, EventArgs e)
        {
            AIBonusViewLog logDialog = new AIBonusViewLog();
            logDialog.ShowDialog(this);
        }
        private AIBonus _bonusWizard;
    }
}

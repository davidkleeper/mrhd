﻿namespace Hero_Designer.Forms
{
    partial class AIBonusSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.cboxBonusToAdd = new System.Windows.Forms.ComboBox();
            this.lblBonusToAdd = new System.Windows.Forms.Label();
            this.lblImportance = new System.Windows.Forms.Label();
            this.btnAddBonus = new System.Windows.Forms.Button();
            this.lblSelectedBonuses = new System.Windows.Forms.Label();
            this.lblBonus1 = new System.Windows.Forms.Label();
            this.btnRemoveBonus1 = new System.Windows.Forms.Button();
            this.btnRemoveBonus2 = new System.Windows.Forms.Button();
            this.lblBonus2 = new System.Windows.Forms.Label();
            this.btnRemoveBonus3 = new System.Windows.Forms.Button();
            this.lblBonus3 = new System.Windows.Forms.Label();
            this.btnRemoveBonus5 = new System.Windows.Forms.Button();
            this.lblBonus5 = new System.Windows.Forms.Label();
            this.btnRemoveBonus4 = new System.Windows.Forms.Button();
            this.lblBonus4 = new System.Windows.Forms.Label();
            this.v = new System.Windows.Forms.Label();
            this.checkBoxPvP = new System.Windows.Forms.CheckBox();
            this.lblSelectedPvP = new System.Windows.Forms.Label();
            this.lblPvP1 = new System.Windows.Forms.Label();
            this.lblPvP2 = new System.Windows.Forms.Label();
            this.lblPvP4 = new System.Windows.Forms.Label();
            this.lblPvP3 = new System.Windows.Forms.Label();
            this.lblPvP5 = new System.Windows.Forms.Label();
            this.cboxImportance = new System.Windows.Forms.ComboBox();
            this.lblCap = new System.Windows.Forms.Label();
            this.textBoxCap = new System.Windows.Forms.TextBox();
            this.lblSelectedCap = new System.Windows.Forms.Label();
            this.lblCap5 = new System.Windows.Forms.Label();
            this.lblCap4 = new System.Windows.Forms.Label();
            this.lblCap3 = new System.Windows.Forms.Label();
            this.lblCap2 = new System.Windows.Forms.Label();
            this.lblCap1 = new System.Windows.Forms.Label();
            this.btnPermaWizard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(12, 45);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(357, 17);
            this.lblIntro2.TabIndex = 17;
            this.lblIntro2.Text = "Choose up to five bonuses and the importance method.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(12, 12);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(317, 17);
            this.lblIntro1.TabIndex = 16;
            this.lblIntro1.Text = "Select the Bonuses You Want To Maximize";
            // 
            // cboxBonusToAdd
            // 
            this.cboxBonusToAdd.FormattingEnabled = true;
            this.cboxBonusToAdd.Location = new System.Drawing.Point(15, 169);
            this.cboxBonusToAdd.Name = "cboxBonusToAdd";
            this.cboxBonusToAdd.Size = new System.Drawing.Size(264, 24);
            this.cboxBonusToAdd.TabIndex = 20;
            this.cboxBonusToAdd.SelectedIndexChanged += new System.EventHandler(this.cboxBonusToAdd_Change);
            // 
            // lblBonusToAdd
            // 
            this.lblBonusToAdd.AutoSize = true;
            this.lblBonusToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonusToAdd.Location = new System.Drawing.Point(13, 146);
            this.lblBonusToAdd.Name = "lblBonusToAdd";
            this.lblBonusToAdd.Size = new System.Drawing.Size(110, 17);
            this.lblBonusToAdd.TabIndex = 21;
            this.lblBonusToAdd.Text = "Bonus To Add";
            // 
            // lblImportance
            // 
            this.lblImportance.AutoSize = true;
            this.lblImportance.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImportance.Location = new System.Drawing.Point(9, 78);
            this.lblImportance.Name = "lblImportance";
            this.lblImportance.Size = new System.Drawing.Size(88, 17);
            this.lblImportance.TabIndex = 22;
            this.lblImportance.Text = "Importance";
            // 
            // btnAddBonus
            // 
            this.btnAddBonus.Location = new System.Drawing.Point(435, 173);
            this.btnAddBonus.Name = "btnAddBonus";
            this.btnAddBonus.Size = new System.Drawing.Size(75, 23);
            this.btnAddBonus.TabIndex = 24;
            this.btnAddBonus.Text = "&Add";
            this.btnAddBonus.UseVisualStyleBackColor = true;
            this.btnAddBonus.Click += new System.EventHandler(this.btnAddBonus_Click);
            // 
            // lblSelectedBonuses
            // 
            this.lblSelectedBonuses.AutoSize = true;
            this.lblSelectedBonuses.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedBonuses.Location = new System.Drawing.Point(13, 225);
            this.lblSelectedBonuses.Name = "lblSelectedBonuses";
            this.lblSelectedBonuses.Size = new System.Drawing.Size(138, 17);
            this.lblSelectedBonuses.TabIndex = 25;
            this.lblSelectedBonuses.Text = "Selected Bonuses";
            // 
            // lblBonus1
            // 
            this.lblBonus1.AutoSize = true;
            this.lblBonus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonus1.Location = new System.Drawing.Point(15, 248);
            this.lblBonus1.Name = "lblBonus1";
            this.lblBonus1.Size = new System.Drawing.Size(83, 17);
            this.lblBonus1.TabIndex = 27;
            this.lblBonus1.Text = "Placeholder";
            // 
            // btnRemoveBonus1
            // 
            this.btnRemoveBonus1.Location = new System.Drawing.Point(435, 246);
            this.btnRemoveBonus1.Name = "btnRemoveBonus1";
            this.btnRemoveBonus1.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBonus1.TabIndex = 32;
            this.btnRemoveBonus1.Text = "Remove";
            this.btnRemoveBonus1.UseVisualStyleBackColor = true;
            this.btnRemoveBonus1.Click += new System.EventHandler(this.btnRemoveBonus1_Click);
            // 
            // btnRemoveBonus2
            // 
            this.btnRemoveBonus2.Location = new System.Drawing.Point(435, 275);
            this.btnRemoveBonus2.Name = "btnRemoveBonus2";
            this.btnRemoveBonus2.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBonus2.TabIndex = 35;
            this.btnRemoveBonus2.Text = "Remove";
            this.btnRemoveBonus2.UseVisualStyleBackColor = true;
            this.btnRemoveBonus2.Click += new System.EventHandler(this.btnRemoveBonus2_Click);
            // 
            // lblBonus2
            // 
            this.lblBonus2.AutoSize = true;
            this.lblBonus2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonus2.Location = new System.Drawing.Point(15, 277);
            this.lblBonus2.Name = "lblBonus2";
            this.lblBonus2.Size = new System.Drawing.Size(83, 17);
            this.lblBonus2.TabIndex = 33;
            this.lblBonus2.Text = "Placeholder";
            // 
            // btnRemoveBonus3
            // 
            this.btnRemoveBonus3.Location = new System.Drawing.Point(435, 304);
            this.btnRemoveBonus3.Name = "btnRemoveBonus3";
            this.btnRemoveBonus3.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBonus3.TabIndex = 38;
            this.btnRemoveBonus3.Text = "Remove";
            this.btnRemoveBonus3.UseVisualStyleBackColor = true;
            this.btnRemoveBonus3.Click += new System.EventHandler(this.btnRemoveBonus3_Click);
            // 
            // lblBonus3
            // 
            this.lblBonus3.AutoSize = true;
            this.lblBonus3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonus3.Location = new System.Drawing.Point(15, 306);
            this.lblBonus3.Name = "lblBonus3";
            this.lblBonus3.Size = new System.Drawing.Size(83, 17);
            this.lblBonus3.TabIndex = 36;
            this.lblBonus3.Text = "Placeholder";
            // 
            // btnRemoveBonus5
            // 
            this.btnRemoveBonus5.Location = new System.Drawing.Point(435, 363);
            this.btnRemoveBonus5.Name = "btnRemoveBonus5";
            this.btnRemoveBonus5.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBonus5.TabIndex = 44;
            this.btnRemoveBonus5.Text = "Remove";
            this.btnRemoveBonus5.UseVisualStyleBackColor = true;
            this.btnRemoveBonus5.Click += new System.EventHandler(this.btnRemoveBonus5_Click);
            // 
            // lblBonus5
            // 
            this.lblBonus5.AutoSize = true;
            this.lblBonus5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonus5.Location = new System.Drawing.Point(15, 365);
            this.lblBonus5.Name = "lblBonus5";
            this.lblBonus5.Size = new System.Drawing.Size(83, 17);
            this.lblBonus5.TabIndex = 42;
            this.lblBonus5.Text = "Placeholder";
            // 
            // btnRemoveBonus4
            // 
            this.btnRemoveBonus4.Location = new System.Drawing.Point(435, 334);
            this.btnRemoveBonus4.Name = "btnRemoveBonus4";
            this.btnRemoveBonus4.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBonus4.TabIndex = 41;
            this.btnRemoveBonus4.Text = "Remove";
            this.btnRemoveBonus4.UseVisualStyleBackColor = true;
            this.btnRemoveBonus4.Click += new System.EventHandler(this.btnRemoveBonus4_Click);
            // 
            // lblBonus4
            // 
            this.lblBonus4.AutoSize = true;
            this.lblBonus4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBonus4.Location = new System.Drawing.Point(15, 336);
            this.lblBonus4.Name = "lblBonus4";
            this.lblBonus4.Size = new System.Drawing.Size(83, 17);
            this.lblBonus4.TabIndex = 39;
            this.lblBonus4.Text = "Placeholder";
            // 
            // v
            // 
            this.v.AutoSize = true;
            this.v.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.v.Location = new System.Drawing.Point(298, 148);
            this.v.Name = "v";
            this.v.Size = new System.Drawing.Size(36, 17);
            this.v.TabIndex = 45;
            this.v.Text = "PvP";
            // 
            // checkBoxPvP
            // 
            this.checkBoxPvP.AutoSize = true;
            this.checkBoxPvP.Location = new System.Drawing.Point(306, 177);
            this.checkBoxPvP.Name = "checkBoxPvP";
            this.checkBoxPvP.Size = new System.Drawing.Size(18, 17);
            this.checkBoxPvP.TabIndex = 46;
            this.checkBoxPvP.UseVisualStyleBackColor = true;
            // 
            // lblSelectedPvP
            // 
            this.lblSelectedPvP.AutoSize = true;
            this.lblSelectedPvP.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedPvP.Location = new System.Drawing.Point(298, 226);
            this.lblSelectedPvP.Name = "lblSelectedPvP";
            this.lblSelectedPvP.Size = new System.Drawing.Size(36, 17);
            this.lblSelectedPvP.TabIndex = 47;
            this.lblSelectedPvP.Text = "PvP";
            // 
            // lblPvP1
            // 
            this.lblPvP1.AutoSize = true;
            this.lblPvP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPvP1.Location = new System.Drawing.Point(297, 252);
            this.lblPvP1.Name = "lblPvP1";
            this.lblPvP1.Size = new System.Drawing.Size(42, 17);
            this.lblPvP1.TabIndex = 48;
            this.lblPvP1.Text = "False";
            // 
            // lblPvP2
            // 
            this.lblPvP2.AutoSize = true;
            this.lblPvP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPvP2.Location = new System.Drawing.Point(297, 278);
            this.lblPvP2.Name = "lblPvP2";
            this.lblPvP2.Size = new System.Drawing.Size(42, 17);
            this.lblPvP2.TabIndex = 49;
            this.lblPvP2.Text = "False";
            // 
            // lblPvP4
            // 
            this.lblPvP4.AutoSize = true;
            this.lblPvP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPvP4.Location = new System.Drawing.Point(297, 337);
            this.lblPvP4.Name = "lblPvP4";
            this.lblPvP4.Size = new System.Drawing.Size(42, 17);
            this.lblPvP4.TabIndex = 51;
            this.lblPvP4.Text = "False";
            // 
            // lblPvP3
            // 
            this.lblPvP3.AutoSize = true;
            this.lblPvP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPvP3.Location = new System.Drawing.Point(297, 307);
            this.lblPvP3.Name = "lblPvP3";
            this.lblPvP3.Size = new System.Drawing.Size(42, 17);
            this.lblPvP3.TabIndex = 50;
            this.lblPvP3.Text = "False";
            // 
            // lblPvP5
            // 
            this.lblPvP5.AutoSize = true;
            this.lblPvP5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPvP5.Location = new System.Drawing.Point(297, 366);
            this.lblPvP5.Name = "lblPvP5";
            this.lblPvP5.Size = new System.Drawing.Size(42, 17);
            this.lblPvP5.TabIndex = 52;
            this.lblPvP5.Text = "False";
            // 
            // cboxImportance
            // 
            this.cboxImportance.FormattingEnabled = true;
            this.cboxImportance.Location = new System.Drawing.Point(12, 103);
            this.cboxImportance.Name = "cboxImportance";
            this.cboxImportance.Size = new System.Drawing.Size(267, 24);
            this.cboxImportance.TabIndex = 53;
            this.cboxImportance.SelectedIndexChanged += new System.EventHandler(this.cboxImportance_Changed);
            // 
            // lblCap
            // 
            this.lblCap.AutoSize = true;
            this.lblCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap.Location = new System.Drawing.Point(368, 148);
            this.lblCap.Name = "lblCap";
            this.lblCap.Size = new System.Drawing.Size(36, 17);
            this.lblCap.TabIndex = 54;
            this.lblCap.Text = "Cap";
            // 
            // textBoxCap
            // 
            this.textBoxCap.Location = new System.Drawing.Point(365, 172);
            this.textBoxCap.Name = "textBoxCap";
            this.textBoxCap.Size = new System.Drawing.Size(42, 22);
            this.textBoxCap.TabIndex = 55;
            // 
            // lblSelectedCap
            // 
            this.lblSelectedCap.AutoSize = true;
            this.lblSelectedCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedCap.Location = new System.Drawing.Point(368, 226);
            this.lblSelectedCap.Name = "lblSelectedCap";
            this.lblSelectedCap.Size = new System.Drawing.Size(36, 17);
            this.lblSelectedCap.TabIndex = 56;
            this.lblSelectedCap.Text = "Cap";
            // 
            // lblCap5
            // 
            this.lblCap5.AutoSize = true;
            this.lblCap5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap5.Location = new System.Drawing.Point(362, 367);
            this.lblCap5.Name = "lblCap5";
            this.lblCap5.Size = new System.Drawing.Size(21, 17);
            this.lblCap5.TabIndex = 61;
            this.lblCap5.Text = "-1";
            // 
            // lblCap4
            // 
            this.lblCap4.AutoSize = true;
            this.lblCap4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap4.Location = new System.Drawing.Point(362, 338);
            this.lblCap4.Name = "lblCap4";
            this.lblCap4.Size = new System.Drawing.Size(21, 17);
            this.lblCap4.TabIndex = 60;
            this.lblCap4.Text = "-1";
            // 
            // lblCap3
            // 
            this.lblCap3.AutoSize = true;
            this.lblCap3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap3.Location = new System.Drawing.Point(362, 308);
            this.lblCap3.Name = "lblCap3";
            this.lblCap3.Size = new System.Drawing.Size(21, 17);
            this.lblCap3.TabIndex = 59;
            this.lblCap3.Text = "-1";
            // 
            // lblCap2
            // 
            this.lblCap2.AutoSize = true;
            this.lblCap2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap2.Location = new System.Drawing.Point(362, 279);
            this.lblCap2.Name = "lblCap2";
            this.lblCap2.Size = new System.Drawing.Size(21, 17);
            this.lblCap2.TabIndex = 58;
            this.lblCap2.Text = "-1";
            // 
            // lblCap1
            // 
            this.lblCap1.AutoSize = true;
            this.lblCap1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCap1.Location = new System.Drawing.Point(362, 253);
            this.lblCap1.Name = "lblCap1";
            this.lblCap1.Size = new System.Drawing.Size(21, 17);
            this.lblCap1.TabIndex = 57;
            this.lblCap1.Text = "-1";
            // 
            // btnPermaWizard
            // 
            this.btnPermaWizard.Location = new System.Drawing.Point(388, 103);
            this.btnPermaWizard.Name = "btnPermaWizard";
            this.btnPermaWizard.Size = new System.Drawing.Size(122, 23);
            this.btnPermaWizard.TabIndex = 62;
            this.btnPermaWizard.Text = "Perma Wizard";
            this.btnPermaWizard.UseVisualStyleBackColor = true;
            this.btnPermaWizard.Click += new System.EventHandler(this.btnPermaWizard_Click);
            // 
            // AIBonusSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnPermaWizard);
            this.Controls.Add(this.lblCap5);
            this.Controls.Add(this.lblCap4);
            this.Controls.Add(this.lblCap3);
            this.Controls.Add(this.lblCap2);
            this.Controls.Add(this.lblCap1);
            this.Controls.Add(this.lblSelectedCap);
            this.Controls.Add(this.textBoxCap);
            this.Controls.Add(this.lblCap);
            this.Controls.Add(this.cboxImportance);
            this.Controls.Add(this.lblPvP5);
            this.Controls.Add(this.lblPvP4);
            this.Controls.Add(this.lblPvP3);
            this.Controls.Add(this.lblPvP2);
            this.Controls.Add(this.lblPvP1);
            this.Controls.Add(this.lblSelectedPvP);
            this.Controls.Add(this.checkBoxPvP);
            this.Controls.Add(this.v);
            this.Controls.Add(this.btnRemoveBonus5);
            this.Controls.Add(this.lblBonus5);
            this.Controls.Add(this.btnRemoveBonus4);
            this.Controls.Add(this.lblBonus4);
            this.Controls.Add(this.btnRemoveBonus3);
            this.Controls.Add(this.lblBonus3);
            this.Controls.Add(this.btnRemoveBonus2);
            this.Controls.Add(this.lblBonus2);
            this.Controls.Add(this.btnRemoveBonus1);
            this.Controls.Add(this.lblBonus1);
            this.Controls.Add(this.lblSelectedBonuses);
            this.Controls.Add(this.btnAddBonus);
            this.Controls.Add(this.lblImportance);
            this.Controls.Add(this.lblBonusToAdd);
            this.Controls.Add(this.cboxBonusToAdd);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIBonusSelect";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.ComboBox cboxBonusToAdd;
        private System.Windows.Forms.Label lblBonusToAdd;
        private System.Windows.Forms.Label lblImportance;
        private System.Windows.Forms.Button btnAddBonus;
        private System.Windows.Forms.Label lblSelectedBonuses;
        private System.Windows.Forms.Label lblBonus1;
        private System.Windows.Forms.Button btnRemoveBonus1;
        private System.Windows.Forms.Button btnRemoveBonus2;
        private System.Windows.Forms.Label lblBonus2;
        private System.Windows.Forms.Button btnRemoveBonus3;
        private System.Windows.Forms.Label lblBonus3;
        private System.Windows.Forms.Button btnRemoveBonus5;
        private System.Windows.Forms.Label lblBonus5;
        private System.Windows.Forms.Button btnRemoveBonus4;
        private System.Windows.Forms.Label lblBonus4;
        private System.Windows.Forms.Label v;
        private System.Windows.Forms.CheckBox checkBoxPvP;
        private System.Windows.Forms.Label lblSelectedPvP;
        private System.Windows.Forms.Label lblPvP1;
        private System.Windows.Forms.Label lblPvP2;
        private System.Windows.Forms.Label lblPvP4;
        private System.Windows.Forms.Label lblPvP3;
        private System.Windows.Forms.Label lblPvP5;
        private System.Windows.Forms.ComboBox cboxImportance;
        private System.Windows.Forms.Label lblCap;
        private System.Windows.Forms.TextBox textBoxCap;
        private System.Windows.Forms.Label lblSelectedCap;
        private System.Windows.Forms.Label lblCap5;
        private System.Windows.Forms.Label lblCap4;
        private System.Windows.Forms.Label lblCap3;
        private System.Windows.Forms.Label lblCap2;
        private System.Windows.Forms.Label lblCap1;
        private System.Windows.Forms.Button btnPermaWizard;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusViewLog : Form
    {
        private BindingSource bindingSource = new BindingSource();

        public AIBonusViewLog()
        {
            InitializeComponent();
            InitializeDataGridView();
        }

        private void InitializeDataGridView()
        {
            List<string> entries = new List<string>();

            foreach (KeyValuePair<string, List<string>> kvp in AILog.Entries)
            {
                foreach (string entry in kvp.Value)
                {
                    string s = entry + " " + kvp.Key;
                    entries.Add(s);
                }
            }

            dataGridViewLog.AutoGenerateColumns = true;
            dataGridViewLog.DataSource = entries.Select(x => new { Entry = x }).ToList();

            // dataGridViewLog.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewLog.EditMode = DataGridViewEditMode.EditProgrammatically;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class formIOBonusesSpreadsheet : Form
    {
        public formIOBonusesSpreadsheet()
        {
            InitializeComponent();
            LoadIOBonuses();
        }

        private void btnExportToCSV_Click(object sender, EventArgs e)
        {
            SaveIOBonuses();
        }
    }
}

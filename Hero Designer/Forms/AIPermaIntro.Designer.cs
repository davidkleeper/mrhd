﻿namespace Hero_Designer.Forms
{
    partial class AIPermaIntro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(12, 13);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(231, 17);
            this.lblIntro1.TabIndex = 8;
            this.lblIntro1.Text = "Welcome to the Perma Wizard.";
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(12, 46);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(469, 17);
            this.lblIntro2.TabIndex = 9;
            this.lblIntro2.Text = "My name is Robot Woman. I\'ll show you how to make a power permanent.";
            // 
            // lblIntro3
            // 
            this.lblIntro3.AutoSize = true;
            this.lblIntro3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro3.Location = new System.Drawing.Point(12, 83);
            this.lblIntro3.Name = "lblIntro3";
            this.lblIntro3.Size = new System.Drawing.Size(247, 17);
            this.lblIntro3.TabIndex = 10;
            this.lblIntro3.Text = "When you\'re ready to start, click Next.";
            // 
            // AIPerma1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.lblIntro3);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIPerma1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro3;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Speech.Synthesis;
using System.Runtime.InteropServices;

namespace Hero_Designer.Forms
{
    public partial class AIBonusEvolve : Form
    {
        public AIBonusEvolve()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus bonusWizard)
        {
            _bonusWizard = bonusWizard;
            UpdateButtons(bonusWizard);
        }

        public void UpdateButtons(AIBonus bonusWizard)
        {
            bonusWizard.BtnBack.Enabled = false;
            bonusWizard.BtnNext.Enabled = false;
            bonusWizard.BtnSave.Visible = false;
            bonusWizard.BtnSaveAIWizardConfig.Visible = false;
            bonusWizard.BtnLoadAIWizardConfig.Visible = false;
            lblGenerations.Text = "Generations: " + bonusWizard.GenerationsAndIterationsForm.Generations;
            lblIterations.Text = "Iterations: " + bonusWizard.GenerationsAndIterationsForm.Iterations;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            RunAI();
        }

        public void CancelEvolve()
        {
            if (null != _tokenSource)
            {
                _tokenSource.Cancel();
            }
        }

        private async void RunAI()
        {
            int generations = _bonusWizard.GenerationsAndIterationsForm.Generations;
            int iterations = _bonusWizard.GenerationsAndIterationsForm.Iterations;
            bool[] powerFlags = _bonusWizard.Config.PowerFlags;
            BonusInfoWithWeight[] desiredBonuses = _bonusWizard.SelectForm.GetBonuses().ToArray();

            _bonusWizard.BtnNext.Enabled = false;
            _bonusWizard.BtnBack.Enabled = false;
            progressBarEvolve.Value = 0;
            progressBarEvolve.Maximum = generations;
            foreach (BonusInfoWithWeight desiredBonus in desiredBonuses)
            {
                Series series = new Series(desiredBonus.DisplayName);
                series.Color = GetColor(desiredBonus.DisplayName);
                chartBonusProgress.Series.Add(series);
            }
            AILog.RemoveAll();
            AIEvolverToon.RaiseGenerationEvent += HandleGenerationEvent;
            AIEvolverPowerEntry.RaiseIterationEvent += HandleIterationEvent;
            _tokenSource = new CancellationTokenSource();
            _cancellationToken = _tokenSource.Token; 
            try 
            {
                await Task.Run(() =>
                {
                    _bonusWizard.Config.Generations = generations;
                    _bonusWizard.Config.Iterations = iterations;
                    _bonusWizard.Config.BonusInfoWithWeightArray = desiredBonuses;
                    AISlotter.SlotToon(_bonusWizard.OrigialToon, _bonusWizard.Config);
                    AIEvolverToon.EvolveToon(_bonusWizard.OrigialToon, _bonusWizard.Config, _cancellationToken);
                }, _tokenSource.Token).ConfigureAwait(false);
            }
            catch (OperationCanceledException e)
            {
                AILog.Add("Evolve Canceled.", "Class: AIBonusEvolve Toon evolution canceled.");
            }
            finally
            {
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }

        void AddChartData(AIEventGeneration e)
        {
            BonusInfoWithWeight[] desiredBonuses = _bonusWizard.SelectForm.GetBonuses().ToArray();

            ToonSnapshots.Add(e.Toon.Clone() as AIToon);
            for (int desiredBonusIndex = 0; desiredBonusIndex < desiredBonuses.Length; desiredBonusIndex++)
            {
                BonusInfoWithWeight desiredBonus = desiredBonuses[desiredBonusIndex];
                System.Windows.Forms.DataVisualization.Charting.Series series = chartBonusProgress.Series[desiredBonus.DisplayName];
                float value = Math.Abs(e.Toon.SumBonus(desiredBonus, _bonusWizard.Config) + e.Toon.SumSpecialEnhancements(desiredBonus, _bonusWizard.Config));    
                object[] setSeriesArray = new object[2];

                setSeriesArray[0] = series;
                setSeriesArray[1] = value;
                chartBonusProgress.BeginInvoke(new SetSeriesValueDelegate(SetSeriesValueDelegateMethod), setSeriesArray);
            }
        }

        Color GetColor(string displayName)
        {
            int alpha = 175;
            if (displayName == BonusInfo.Accuracy.DisplayName)
                return Color.FromArgb(alpha, Color.Yellow);
            if (displayName == BonusInfo.Confuse.DisplayName)
                return Color.FromArgb(alpha, Color.Pink);
            if (displayName == BonusInfo.DamageBuff.DisplayName)
                return Color.FromArgb(alpha, Color.Red);
            if (displayName == BonusInfo.DefenseAll.DisplayName)
                return Color.FromArgb(alpha, Color.Purple);
            if (displayName == BonusInfo.DefenseAoE.DisplayName)
                return Color.FromArgb(alpha, Color.DarkViolet);
            if (displayName == BonusInfo.DefenseCold.DisplayName)
                return Color.FromArgb(alpha, Color.BlueViolet);
            if (displayName == BonusInfo.DefenseEnergy.DisplayName)
                return Color.FromArgb(alpha, Color.DarkMagenta);
            if (displayName == BonusInfo.DefenseFire.DisplayName)
                return Color.FromArgb(alpha, Color.DarkSlateBlue);
            if (displayName == BonusInfo.DefenseLethal.DisplayName)
                return Color.FromArgb(alpha, Color.SlateBlue);
            if (displayName == BonusInfo.DefenseMelee.DisplayName)
                return Color.FromArgb(alpha, Color.Indigo);
            if (displayName == BonusInfo.DefenseNegative.DisplayName)
                return Color.FromArgb(alpha, Color.MidnightBlue);
            if (displayName == BonusInfo.DefensePsionic.DisplayName)
                return Color.FromArgb(alpha, Color.DeepPink);
            if (displayName == BonusInfo.DefenseRanged.DisplayName)
                return Color.FromArgb(alpha, Color.Plum);
            if (displayName == BonusInfo.DefenseSmashing.DisplayName)
                return Color.FromArgb(alpha, Color.Navy);
            if (displayName == BonusInfo.EnduranceDiscount.DisplayName)
                return Color.FromArgb(alpha, Color.Aqua);
            if (displayName == BonusInfo.Fly.DisplayName)
                return Color.FromArgb(alpha, Color.Tan);
            if (displayName == BonusInfo.Heal.DisplayName)
                return Color.FromArgb(alpha, Color.Green);
            if (displayName == BonusInfo.HitPoints.DisplayName)
                return Color.FromArgb(alpha, Color.DarkGreen);
            if (displayName == BonusInfo.Hold.DisplayName)
                return Color.FromArgb(alpha, Color.Fuchsia);
            if (displayName == BonusInfo.Immobilize.DisplayName)
                return Color.FromArgb(alpha, Color.HotPink);
            if (displayName == BonusInfo.Jump.DisplayName)
                return Color.FromArgb(alpha, Color.Khaki);
            if (displayName == BonusInfo.KnockbackProtection.DisplayName)
                return Color.FromArgb(alpha, Color.Thistle);
            if (displayName == BonusInfo.Knockdown.DisplayName)
                return Color.FromArgb(alpha, Color.Gainsboro);
            if (displayName == BonusInfo.MaxEndurance.DisplayName)
                return Color.FromArgb(alpha, Color.RoyalBlue);
            if (displayName == BonusInfo.MezEnhance.DisplayName)
                return Color.FromArgb(alpha, Color.Magenta);
            if (displayName == BonusInfo.MezResistance.DisplayName)
                return Color.FromArgb(alpha, Color.DeepPink);
            if (displayName == BonusInfo.PetDefense.DisplayName)
                return Color.FromArgb(alpha, Color.MediumPurple);
            if (displayName == BonusInfo.PetResistance.DisplayName)
                return Color.FromArgb(alpha, Color.Peru);
            if (displayName == BonusInfo.Range.DisplayName)
                return Color.FromArgb(alpha, Color.DarkGray);
            if (displayName == BonusInfo.RechargeTime.DisplayName)
                return Color.FromArgb(alpha, Color.AntiqueWhite);
            if (displayName == BonusInfo.Recovery.DisplayName)
                return Color.FromArgb(alpha, Color.RoyalBlue);
            if (displayName == BonusInfo.ReduceDamage.DisplayName)
                return Color.FromArgb(alpha, Color.MintCream);
            if (displayName == BonusInfo.Regeneration.DisplayName)
                return Color.FromArgb(alpha, Color.Lime);
            if (displayName == BonusInfo.RepelResistance.DisplayName)
                return Color.FromArgb(alpha, Color.Silver);
            if (displayName == BonusInfo.ResistanceCold.DisplayName)
                return Color.FromArgb(alpha, Color.PeachPuff);
            if (displayName == BonusInfo.ResistanceEnergy.DisplayName)
                return Color.FromArgb(alpha, Color.Gold);
            if (displayName == BonusInfo.ResistanceFire.DisplayName)
                return Color.FromArgb(alpha, Color.Orange);
            if (displayName == BonusInfo.ResistanceLethal.DisplayName)
                return Color.FromArgb(alpha, Color.Goldenrod);
            if (displayName == BonusInfo.ResistanceNegative.DisplayName)
                return Color.FromArgb(alpha, Color.Coral);
            if (displayName == BonusInfo.ResistancePsionic.DisplayName)
                return Color.FromArgb(alpha, Color.LightSalmon);
            if (displayName == BonusInfo.ResistanceSmashing.DisplayName)
                return Color.FromArgb(alpha, Color.Chocolate);
            if (displayName == BonusInfo.ResistanceToxic.DisplayName)
                return Color.FromArgb(alpha, Color.SandyBrown);
            if (displayName == BonusInfo.ResistanceSlowMovement.DisplayName)
                return Color.FromArgb(alpha, Color.Turquoise);
            if (displayName == BonusInfo.ResistanceSlowRecharge.DisplayName)
                return Color.FromArgb(alpha, Color.Chartreuse);
            if (displayName == BonusInfo.Running.DisplayName)
                return Color.FromArgb(alpha, Color.DarkKhaki);
            if (displayName == BonusInfo.Slow.DisplayName)
                return Color.FromArgb(alpha, Color.MediumSeaGreen);

            return Color.FromArgb(alpha, Color.SlateGray);
        }

        public void Speak(SpeechSynthesizer speech)
        {
            speech.SpeakAsync(lblIntro1.Text);
            speech.SpeakAsync(lblIntro2.Text);
        }

        void HandleGenerationEvent(object sender, AIEventGeneration e)
        {
            if (_bonusWizard.IsClosing) return;
            object[] setTextArray = new object[2];
            object[] setProgressbarValueArray = new object[2];

            setTextArray[0] = lblCurrentGeneration;
            setTextArray[1] = "Current Generation: " + e.Generation.ToString();
            lblGenerations.BeginInvoke(new SetTextDelegate(SetTextDelegateMethod), setTextArray);
            setProgressbarValueArray[0] = progressBarEvolve;
            setProgressbarValueArray[1] = e.Generation;
            progressBarEvolve.BeginInvoke(new SetProgressValueDelegate(SetProgressValueDelegateMethod), setProgressbarValueArray);
            if (0 == e.Generation % 10 && !e.End)
            {
                AddChartData(e);
            }
            if (e.End)
            {
                object[] enableNextArray = new object[1];
                object[] enableBackArray = new object[1];

                FinalToon = e.Toon.Clone() as AIToon;
                enableNextArray[0] = _bonusWizard.BtnNext;
                _bonusWizard.BtnNext.BeginInvoke(new EnableDelegate(EnableDelegateMethod), enableNextArray);

                AIEvolverToon.RaiseGenerationEvent -= HandleGenerationEvent;
                AIEvolverPowerEntry.RaiseIterationEvent -= HandleIterationEvent;
                if (0 != e.Generation % 10)
                {
                    AddChartData(e);
                }
                Console.Beep();
            }
        }

        void HandleIterationEvent(object sender, AIEventIteration e)
        {
            if (_bonusWizard.IsClosing) return;
            object[] setTextArray = new object[2];

            setTextArray[0] = lblCurrentIteration;
            setTextArray[1] = "Current Iteration: " + e.Iteration.ToString();
            lblGenerations.BeginInvoke(new SetTextDelegate(SetTextDelegateMethod), setTextArray);
        }

        public delegate void SetTextDelegate(Label label, string text);
        public void SetTextDelegateMethod(Label label, string text)
        {
            label.Text = text;
        }
         public delegate void SetProgressValueDelegate(ProgressBar progressBar, int value);
        public void SetProgressValueDelegateMethod(ProgressBar progressBar, int value)
        {
            progressBar.Value = value;
        }
        public delegate void EnableDelegate(Control control);
        public void EnableDelegateMethod(Control control)
        {
            control.Enabled = true;
        }
        public delegate void SetSeriesValueDelegate(System.Windows.Forms.DataVisualization.Charting.Series series, double value);
        public void SetSeriesValueDelegateMethod(System.Windows.Forms.DataVisualization.Charting.Series series, double value)
        {
            series.Points.AddY(value);
        }

        private void btnViewSnapshots_Click(object sender, EventArgs e)
        {
            AIBonusViewAndSaveSnapshots snapshotDialog = new AIBonusViewAndSaveSnapshots();
            snapshotDialog.Setup(_bonusWizard, _bonusWizard.SelectForm, ToonSnapshots);
            snapshotDialog.ShowDialog(this);
        }

        public List<AIToon> ToonSnapshots = new List<AIToon>();
        public AIToon FinalToon { get; private set; }
        private AIBonus _bonusWizard;
        CancellationTokenSource _tokenSource = null;
        CancellationToken _cancellationToken;
    }
}

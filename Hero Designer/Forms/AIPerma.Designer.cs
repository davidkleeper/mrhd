﻿using Base.Master_Classes;

namespace Hero_Designer.Forms
{
    partial class AIPerma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AIPerma));
            this.pnlNavigation = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.checkBoxSpeak = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxAgent = new System.Windows.Forms.PictureBox();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.pnlNavigation.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAgent)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnBack);
            this.pnlNavigation.Controls.Add(this.btnNext);
            this.pnlNavigation.Controls.Add(this.btnCancel);
            this.pnlNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlNavigation.Location = new System.Drawing.Point(0, 406);
            this.pnlNavigation.Name = "pnlNavigation";
            this.pnlNavigation.Size = new System.Drawing.Size(802, 47);
            this.pnlNavigation.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(529, 9);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 28);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "< &Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click_1);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(622, 9);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 28);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "&Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click_1);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(713, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 28);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlLeft
            // 
            this.pnlLeft.Controls.Add(this.checkBoxSpeak);
            this.pnlLeft.Controls.Add(this.label1);
            this.pnlLeft.Controls.Add(this.pictureBoxAgent);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(139, 406);
            this.pnlLeft.TabIndex = 1;
            // 
            // checkBoxSpeak
            // 
            this.checkBoxSpeak.AutoSize = true;
            this.checkBoxSpeak.Location = new System.Drawing.Point(26, 177);
            this.checkBoxSpeak.Name = "checkBoxSpeak";
            this.checkBoxSpeak.Size = new System.Drawing.Size(70, 21);
            this.checkBoxSpeak.TabIndex = 47;
            this.checkBoxSpeak.Text = "Speak";
            this.checkBoxSpeak.UseVisualStyleBackColor = true;
            this.checkBoxSpeak.CheckedChanged += new System.EventHandler(this.checkBoxSpeak_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Robot Woman";
            // 
            // pictureBoxAgent
            // 
            this.pictureBoxAgent.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAgent.Image")));
            this.pictureBoxAgent.Location = new System.Drawing.Point(5, 12);
            this.pictureBoxAgent.Name = "pictureBoxAgent";
            this.pictureBoxAgent.Size = new System.Drawing.Size(130, 130);
            this.pictureBoxAgent.TabIndex = 3;
            this.pictureBoxAgent.TabStop = false;
            // 
            // pnlContent
            // 
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Location = new System.Drawing.Point(139, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(663, 406);
            this.pnlContent.TabIndex = 2;
            // 
            // AIPerma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(802, 453);
            this.Controls.Add(this.pnlContent);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.pnlNavigation);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(820, 500);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(820, 500);
            this.Name = "AIPerma";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Perma Wizard";
            this.pnlNavigation.ResumeLayout(false);
            this.pnlLeft.ResumeLayout(false);
            this.pnlLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAgent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        System.Windows.Forms.Form[] frm = { new AIPermaIntro(), new AIPermaSelect(), new AIPermaResults() };
        int top = -1;
        int count = 5;

        public void LoadForm()
        {
            AIPermaIntro wizard1 = frm[0] as AIPermaIntro;
            AIPermaSelect wizard2 = frm[1] as AIPermaSelect;
            AIPermaResults wizard3 = frm[2] as AIPermaResults;

            wizard3.wizard2 = wizard2;
            frm[top].TopLevel = false;
            frm[top].AutoScroll = true;
            frm[top].Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Controls.Clear();
            if (wizard1 != null) wizard1.Setup(this.btnBack, this.btnNext, this.btnCancel, checkBoxSpeak.Checked && top == 0);
            wizard2 = frm[top] as AIPermaSelect;
            if (wizard2 != null) wizard2.Setup(this.btnBack, this.btnNext, this.btnCancel, checkBoxSpeak.Checked && top == 1);
            wizard3 = frm[top] as AIPermaResults;
            if (wizard3 != null) wizard3.Setup(this.btnBack, this.btnNext, this.btnCancel, checkBoxSpeak.Checked && top == 2);
            this.pnlContent.Controls.Add(frm[top]);
            frm[top].Show();
        }

        public void Back()
        {
            top--;

            if (top <= -1)
                return;
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
                LoadForm();
                if (top - 1 <= -1)
                {
                    btnBack.Enabled = false;
                }
            }

            if (top >= count)
                btnNext.Enabled = false;
        }
        public void Next()
        {
            top++;
            if (top >= count)
                return;
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
                LoadForm();
                if (top + 1 == count)
                {
                    btnNext.Enabled = false;
                }
            }
            if (top <= 0)
                btnBack.Enabled = false;
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Next();
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            Back();
        }

        private System.Windows.Forms.Panel pnlNavigation;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.PictureBox pictureBoxAgent;
        private System.Windows.Forms.Label label1;
        private AIToon origialToon;
        private System.Windows.Forms.CheckBox checkBoxSpeak;
    }
}
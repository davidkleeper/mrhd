﻿namespace Hero_Designer.Forms
{
    partial class AIPermaSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.cboxPrimaryPowersets = new System.Windows.Forms.ComboBox();
            this.lblPrimaryPowersets = new System.Windows.Forms.Label();
            this.lblArchetype = new System.Windows.Forms.Label();
            this.btnAddPower = new System.Windows.Forms.Button();
            this.lblSelectedPowers = new System.Windows.Forms.Label();
            this.lblPower1 = new System.Windows.Forms.Label();
            this.btnRemovePower1 = new System.Windows.Forms.Button();
            this.btnRemovePower2 = new System.Windows.Forms.Button();
            this.lblPower2 = new System.Windows.Forms.Label();
            this.btnRemovePower3 = new System.Windows.Forms.Button();
            this.lblPower3 = new System.Windows.Forms.Label();
            this.btnRemovePower5 = new System.Windows.Forms.Button();
            this.lblPower5 = new System.Windows.Forms.Label();
            this.btnRemovePower4 = new System.Windows.Forms.Button();
            this.lblPower4 = new System.Windows.Forms.Label();
            this.cboxArchetype = new System.Windows.Forms.ComboBox();
            this.lblSecondaryPowersets = new System.Windows.Forms.Label();
            this.cboxSecondaryPowersets = new System.Windows.Forms.ComboBox();
            this.lblEpicPowersets = new System.Windows.Forms.Label();
            this.cboxEpicPowersets = new System.Windows.Forms.ComboBox();
            this.labelPowers = new System.Windows.Forms.Label();
            this.cboxPowers = new System.Windows.Forms.ComboBox();
            this.lblPoolPowersets = new System.Windows.Forms.Label();
            this.cboxPoolPowersets = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(12, 45);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(171, 17);
            this.lblIntro2.TabIndex = 17;
            this.lblIntro2.Text = "Choose up to five powers.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(12, 12);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(397, 17);
            this.lblIntro1.TabIndex = 16;
            this.lblIntro1.Text = "Select the Archetype, Powerset, And Power You Want";
            // 
            // cboxPrimaryPowersets
            // 
            this.cboxPrimaryPowersets.FormattingEnabled = true;
            this.cboxPrimaryPowersets.Location = new System.Drawing.Point(18, 150);
            this.cboxPrimaryPowersets.Name = "cboxPrimaryPowersets";
            this.cboxPrimaryPowersets.Size = new System.Drawing.Size(264, 24);
            this.cboxPrimaryPowersets.TabIndex = 20;
            this.cboxPrimaryPowersets.SelectedIndexChanged += new System.EventHandler(this.cboxPrimaryPowersets_Change);
            // 
            // lblPrimaryPowersets
            // 
            this.lblPrimaryPowersets.AutoSize = true;
            this.lblPrimaryPowersets.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrimaryPowersets.Location = new System.Drawing.Point(16, 129);
            this.lblPrimaryPowersets.Name = "lblPrimaryPowersets";
            this.lblPrimaryPowersets.Size = new System.Drawing.Size(142, 17);
            this.lblPrimaryPowersets.TabIndex = 21;
            this.lblPrimaryPowersets.Text = "Primary Powersets";
            // 
            // lblArchetype
            // 
            this.lblArchetype.AutoSize = true;
            this.lblArchetype.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArchetype.Location = new System.Drawing.Point(12, 75);
            this.lblArchetype.Name = "lblArchetype";
            this.lblArchetype.Size = new System.Drawing.Size(81, 17);
            this.lblArchetype.TabIndex = 22;
            this.lblArchetype.Text = "Archetype";
            // 
            // btnAddPower
            // 
            this.btnAddPower.Location = new System.Drawing.Point(19, 378);
            this.btnAddPower.Name = "btnAddPower";
            this.btnAddPower.Size = new System.Drawing.Size(75, 23);
            this.btnAddPower.TabIndex = 24;
            this.btnAddPower.Text = "&Add";
            this.btnAddPower.UseVisualStyleBackColor = true;
            this.btnAddPower.Click += new System.EventHandler(this.btnAddPower_Click);
            // 
            // lblSelectedPowers
            // 
            this.lblSelectedPowers.AutoSize = true;
            this.lblSelectedPowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedPowers.Location = new System.Drawing.Point(331, 86);
            this.lblSelectedPowers.Name = "lblSelectedPowers";
            this.lblSelectedPowers.Size = new System.Drawing.Size(128, 17);
            this.lblSelectedPowers.TabIndex = 25;
            this.lblSelectedPowers.Text = "Selected Powers";
            // 
            // lblPower1
            // 
            this.lblPower1.AutoSize = true;
            this.lblPower1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower1.Location = new System.Drawing.Point(331, 113);
            this.lblPower1.Name = "lblPower1";
            this.lblPower1.Size = new System.Drawing.Size(83, 17);
            this.lblPower1.TabIndex = 27;
            this.lblPower1.Text = "Placeholder";
            // 
            // btnRemovePower1
            // 
            this.btnRemovePower1.Location = new System.Drawing.Point(496, 110);
            this.btnRemovePower1.Name = "btnRemovePower1";
            this.btnRemovePower1.Size = new System.Drawing.Size(75, 23);
            this.btnRemovePower1.TabIndex = 32;
            this.btnRemovePower1.Text = "Remove";
            this.btnRemovePower1.UseVisualStyleBackColor = true;
            this.btnRemovePower1.Click += new System.EventHandler(this.btnRemovePower1_Click);
            // 
            // btnRemovePower2
            // 
            this.btnRemovePower2.Location = new System.Drawing.Point(496, 139);
            this.btnRemovePower2.Name = "btnRemovePower2";
            this.btnRemovePower2.Size = new System.Drawing.Size(75, 23);
            this.btnRemovePower2.TabIndex = 35;
            this.btnRemovePower2.Text = "Remove";
            this.btnRemovePower2.UseVisualStyleBackColor = true;
            this.btnRemovePower2.Click += new System.EventHandler(this.btnRemovePower2_Click);
            // 
            // lblPower2
            // 
            this.lblPower2.AutoSize = true;
            this.lblPower2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower2.Location = new System.Drawing.Point(331, 145);
            this.lblPower2.Name = "lblPower2";
            this.lblPower2.Size = new System.Drawing.Size(83, 17);
            this.lblPower2.TabIndex = 33;
            this.lblPower2.Text = "Placeholder";
            // 
            // btnRemovePower3
            // 
            this.btnRemovePower3.Location = new System.Drawing.Point(496, 168);
            this.btnRemovePower3.Name = "btnRemovePower3";
            this.btnRemovePower3.Size = new System.Drawing.Size(75, 23);
            this.btnRemovePower3.TabIndex = 38;
            this.btnRemovePower3.Text = "Remove";
            this.btnRemovePower3.UseVisualStyleBackColor = true;
            this.btnRemovePower3.Click += new System.EventHandler(this.btnRemovePower3_Click);
            // 
            // lblPower3
            // 
            this.lblPower3.AutoSize = true;
            this.lblPower3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower3.Location = new System.Drawing.Point(331, 174);
            this.lblPower3.Name = "lblPower3";
            this.lblPower3.Size = new System.Drawing.Size(83, 17);
            this.lblPower3.TabIndex = 36;
            this.lblPower3.Text = "Placeholder";
            // 
            // btnRemovePower5
            // 
            this.btnRemovePower5.Location = new System.Drawing.Point(496, 226);
            this.btnRemovePower5.Name = "btnRemovePower5";
            this.btnRemovePower5.Size = new System.Drawing.Size(75, 23);
            this.btnRemovePower5.TabIndex = 44;
            this.btnRemovePower5.Text = "Remove";
            this.btnRemovePower5.UseVisualStyleBackColor = true;
            this.btnRemovePower5.Click += new System.EventHandler(this.btnRemovePower5_Click);
            // 
            // lblPower5
            // 
            this.lblPower5.AutoSize = true;
            this.lblPower5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower5.Location = new System.Drawing.Point(331, 232);
            this.lblPower5.Name = "lblPower5";
            this.lblPower5.Size = new System.Drawing.Size(83, 17);
            this.lblPower5.TabIndex = 42;
            this.lblPower5.Text = "Placeholder";
            // 
            // btnRemovePower4
            // 
            this.btnRemovePower4.Location = new System.Drawing.Point(496, 197);
            this.btnRemovePower4.Name = "btnRemovePower4";
            this.btnRemovePower4.Size = new System.Drawing.Size(75, 23);
            this.btnRemovePower4.TabIndex = 41;
            this.btnRemovePower4.Text = "Remove";
            this.btnRemovePower4.UseVisualStyleBackColor = true;
            this.btnRemovePower4.Click += new System.EventHandler(this.btnRemovePower4_Click);
            // 
            // lblPower4
            // 
            this.lblPower4.AutoSize = true;
            this.lblPower4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower4.Location = new System.Drawing.Point(331, 203);
            this.lblPower4.Name = "lblPower4";
            this.lblPower4.Size = new System.Drawing.Size(83, 17);
            this.lblPower4.TabIndex = 39;
            this.lblPower4.Text = "Placeholder";
            // 
            // cboxArchetype
            // 
            this.cboxArchetype.FormattingEnabled = true;
            this.cboxArchetype.Location = new System.Drawing.Point(15, 98);
            this.cboxArchetype.Name = "cboxArchetype";
            this.cboxArchetype.Size = new System.Drawing.Size(267, 24);
            this.cboxArchetype.TabIndex = 53;
            this.cboxArchetype.SelectedIndexChanged += new System.EventHandler(this.cboxArchetype_Changed);
            // 
            // lblSecondaryPowersets
            // 
            this.lblSecondaryPowersets.AutoSize = true;
            this.lblSecondaryPowersets.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondaryPowersets.Location = new System.Drawing.Point(16, 180);
            this.lblSecondaryPowersets.Name = "lblSecondaryPowersets";
            this.lblSecondaryPowersets.Size = new System.Drawing.Size(164, 17);
            this.lblSecondaryPowersets.TabIndex = 55;
            this.lblSecondaryPowersets.Text = "Secondary Powersets";
            // 
            // cboxSecondaryPowersets
            // 
            this.cboxSecondaryPowersets.FormattingEnabled = true;
            this.cboxSecondaryPowersets.Location = new System.Drawing.Point(18, 200);
            this.cboxSecondaryPowersets.Name = "cboxSecondaryPowersets";
            this.cboxSecondaryPowersets.Size = new System.Drawing.Size(264, 24);
            this.cboxSecondaryPowersets.TabIndex = 54;
            this.cboxSecondaryPowersets.SelectedIndexChanged += new System.EventHandler(this.cboxSecondaryPowersets_Change);
            // 
            // lblEpicPowersets
            // 
            this.lblEpicPowersets.AutoSize = true;
            this.lblEpicPowersets.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpicPowersets.Location = new System.Drawing.Point(16, 230);
            this.lblEpicPowersets.Name = "lblEpicPowersets";
            this.lblEpicPowersets.Size = new System.Drawing.Size(118, 17);
            this.lblEpicPowersets.TabIndex = 57;
            this.lblEpicPowersets.Text = "Epic Powersets";
            // 
            // cboxEpicPowersets
            // 
            this.cboxEpicPowersets.FormattingEnabled = true;
            this.cboxEpicPowersets.Location = new System.Drawing.Point(18, 250);
            this.cboxEpicPowersets.Name = "cboxEpicPowersets";
            this.cboxEpicPowersets.Size = new System.Drawing.Size(264, 24);
            this.cboxEpicPowersets.TabIndex = 56;
            this.cboxEpicPowersets.SelectedIndexChanged += new System.EventHandler(this.cboxEpicPowersets_Change);
            // 
            // labelPowers
            // 
            this.labelPowers.AutoSize = true;
            this.labelPowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPowers.Location = new System.Drawing.Point(17, 326);
            this.labelPowers.Name = "labelPowers";
            this.labelPowers.Size = new System.Drawing.Size(60, 17);
            this.labelPowers.TabIndex = 59;
            this.labelPowers.Text = "Powers";
            // 
            // cboxPowers
            // 
            this.cboxPowers.FormattingEnabled = true;
            this.cboxPowers.Location = new System.Drawing.Point(19, 346);
            this.cboxPowers.Name = "cboxPowers";
            this.cboxPowers.Size = new System.Drawing.Size(264, 24);
            this.cboxPowers.TabIndex = 58;
            this.cboxPowers.SelectedIndexChanged += new System.EventHandler(this.cboxPowers_Change);
            // 
            // lblPoolPowersets
            // 
            this.lblPoolPowersets.AutoSize = true;
            this.lblPoolPowersets.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoolPowersets.Location = new System.Drawing.Point(18, 279);
            this.lblPoolPowersets.Name = "lblPoolPowersets";
            this.lblPoolPowersets.Size = new System.Drawing.Size(119, 17);
            this.lblPoolPowersets.TabIndex = 61;
            this.lblPoolPowersets.Text = "Pool Powersets";
            // 
            // cboxPoolPowersets
            // 
            this.cboxPoolPowersets.FormattingEnabled = true;
            this.cboxPoolPowersets.Location = new System.Drawing.Point(20, 299);
            this.cboxPoolPowersets.Name = "cboxPoolPowersets";
            this.cboxPoolPowersets.Size = new System.Drawing.Size(264, 24);
            this.cboxPoolPowersets.TabIndex = 60;
            this.cboxPoolPowersets.SelectedIndexChanged += new System.EventHandler(this.cboxPoolPowersets_Change);
            // 
            // AIPerma2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblPoolPowersets);
            this.Controls.Add(this.cboxPoolPowersets);
            this.Controls.Add(this.labelPowers);
            this.Controls.Add(this.cboxPowers);
            this.Controls.Add(this.lblEpicPowersets);
            this.Controls.Add(this.cboxEpicPowersets);
            this.Controls.Add(this.lblSecondaryPowersets);
            this.Controls.Add(this.cboxSecondaryPowersets);
            this.Controls.Add(this.cboxArchetype);
            this.Controls.Add(this.btnRemovePower5);
            this.Controls.Add(this.lblPower5);
            this.Controls.Add(this.btnRemovePower4);
            this.Controls.Add(this.lblPower4);
            this.Controls.Add(this.btnRemovePower3);
            this.Controls.Add(this.lblPower3);
            this.Controls.Add(this.btnRemovePower2);
            this.Controls.Add(this.lblPower2);
            this.Controls.Add(this.btnRemovePower1);
            this.Controls.Add(this.lblPower1);
            this.Controls.Add(this.lblSelectedPowers);
            this.Controls.Add(this.btnAddPower);
            this.Controls.Add(this.lblArchetype);
            this.Controls.Add(this.lblPrimaryPowersets);
            this.Controls.Add(this.cboxPrimaryPowersets);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIPerma2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.AIWizard2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
        private System.Windows.Forms.ComboBox cboxPrimaryPowersets;
        private System.Windows.Forms.Label lblPrimaryPowersets;
        private System.Windows.Forms.Label lblArchetype;
        private System.Windows.Forms.Button btnAddPower;
        private System.Windows.Forms.Label lblSelectedPowers;
        private System.Windows.Forms.Label lblPower1;
        private System.Windows.Forms.Button btnRemovePower1;
        private System.Windows.Forms.Button btnRemovePower2;
        private System.Windows.Forms.Label lblPower2;
        private System.Windows.Forms.Button btnRemovePower3;
        private System.Windows.Forms.Label lblPower3;
        private System.Windows.Forms.Button btnRemovePower5;
        private System.Windows.Forms.Label lblPower5;
        private System.Windows.Forms.Button btnRemovePower4;
        private System.Windows.Forms.Label lblPower4;
        private System.Windows.Forms.ComboBox cboxArchetype;
        private System.Windows.Forms.Label lblSecondaryPowersets;
        private System.Windows.Forms.ComboBox cboxSecondaryPowersets;
        private System.Windows.Forms.Label lblEpicPowersets;
        private System.Windows.Forms.ComboBox cboxEpicPowersets;
        private System.Windows.Forms.Label labelPowers;
        private System.Windows.Forms.ComboBox cboxPowers;
        private System.Windows.Forms.Label lblPoolPowersets;
        private System.Windows.Forms.ComboBox cboxPoolPowersets;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusSlotSelect : Form
    {
        public AIConfig Config;
        public AIConfig ConfigClone;
        public AIToon Toon;
        public int CurrentPowerIndex;
        public AIBonusSlotSelect()
        {
            InitializeComponent();
        }

        public void Setup(AIToon toon, AIConfig config)
        {
            this.Toon = toon;
            this.Config = config;
            this.ConfigClone = config.Clone() as AIConfig;
            PopulatePowers();
            CurrentPowerIndex = 0;
            SelectPower(toon.GetPowerEntries()[0].Power.DisplayName);
            lBoxPowers.SelectedIndex = 0;
        }

        private void PopulatePowers()
        {
            List<PowerEntry> powerEntries = Toon.GetPowerEntries();
            RmoveAllPowers();
            for (int powerIndex = 0; powerIndex < powerEntries.Count; powerIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerIndex];
                bool flag = ConfigClone.PowerFlags[powerIndex];
                if (!flag) continue;
                AddPower(powerEntry.Power);
            }
        }

        private void AddPower(IPower power)
        {
            lBoxPowers.Items.Add(power.DisplayName);
        }

        private void RmoveAllPowers()
        {
            lBoxPowers.Items.Clear();
        }

        private void SetSlots(PowerEntry powerEntry, bool[] slotFlags)
        {
            System.Windows.Forms.CheckBox[] slotCheckbox = new System.Windows.Forms.CheckBox[] {
                cboxSlot1, cboxSlot2, cboxSlot3, cboxSlot4, cboxSlot5, cboxSlot6
            };
            cboxSlot1.Visible = 1 <= powerEntry.SlotCount;
            cboxSlot2.Visible = 2 <= powerEntry.SlotCount;
            cboxSlot3.Visible = 3 <= powerEntry.SlotCount;
            cboxSlot4.Visible = 4 <= powerEntry.SlotCount;
            cboxSlot5.Visible = 5 <= powerEntry.SlotCount;
            cboxSlot6.Visible = 6 <= powerEntry.SlotCount;

            for (int slotIndex = 0; slotIndex < slotCheckbox.Length; slotIndex++)
            {
                slotCheckbox[slotIndex].Visible = false;
            }
                
            for (int slotIndex = 0; slotIndex < powerEntry.SlotCount; slotIndex++)
            {
                slotCheckbox[slotIndex].Visible = true;
                slotCheckbox[slotIndex].Checked = slotFlags[slotIndex];
                slotCheckbox[slotIndex].Text = "Slot " + (slotIndex + 1).ToString();
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(powerEntry.Slots[slotIndex].Enhancement.Enh);
                if (null == enhancement) continue;
                EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
                string enhancementSetName = (null != enhancementSet) ? enhancementSet.DisplayName + " " : "";
                slotCheckbox[slotIndex].Text = "Slot " + (slotIndex + 1).ToString() + " (" + enhancementSetName + enhancement.Name + ")";
            }
        }

        private void SelectPower(string powerName)
        {
            List<PowerEntry> powerEntries = Toon.GetPowerEntries();

            for (int powerIndex = 0; powerIndex < powerEntries.Count; powerIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerIndex];
                if (powerEntry.Power.DisplayName != powerName) continue;
                SetSlots(powerEntry, ConfigClone.SlotFlags[powerIndex]);
            }
        }

        private void UpdateSlotFlags()
        {
            List<PowerEntry> powerEntries = Toon.GetPowerEntries();
            PowerEntry powerEntry = powerEntries[CurrentPowerIndex];
            System.Windows.Forms.CheckBox[] slotCheckbox = new System.Windows.Forms.CheckBox[] {
                cboxSlot1, cboxSlot2, cboxSlot3, cboxSlot4, cboxSlot5, cboxSlot6
            };
            for (int slotIndex = 0; slotIndex < powerEntry.SlotCount; slotIndex++)
            {
                ConfigClone.SlotFlags[CurrentPowerIndex][slotIndex] = slotCheckbox[slotIndex].Checked;
            }
            CurrentPowerIndex = lBoxPowers.SelectedIndex;
        }

        private void lBoxPowers_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSlotFlags();
            SelectPower(lBoxPowers.SelectedItem.ToString());
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            UpdateSlotFlags();
            Config.SlotFlags = ConfigClone.SlotFlags;
        }
    }
}

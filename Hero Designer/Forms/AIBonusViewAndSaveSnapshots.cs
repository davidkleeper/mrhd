﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusViewAndSaveSnapshots : Form
    {
        public List<AIToon> ToonSnapshots;
        public AIBonusSelect Wizard2;
        private AIBonus _wizard;

        public AIBonusViewAndSaveSnapshots()
        {
            InitializeComponent();
        }

        public void Setup(AIBonus wizard, AIBonusSelect wizard2, List<AIToon> snapshots)
        {
            if (null == wizard2 || null == snapshots) return;
            _wizard = wizard;
            Wizard2 = wizard2;
            ToonSnapshots = snapshots;
            this.flowLayoutPanelSnapshots.SuspendLayout();
            this.SuspendLayout();
            for (int snapshotControlIndex = 0; snapshotControlIndex < flowLayoutPanelSnapshots.Controls.Count; snapshotControlIndex++)
            {
                flowLayoutPanelSnapshots.Controls.RemoveAt(snapshotControlIndex);
            }
            for (int snapshotIndex = 0; snapshotIndex < ToonSnapshots.Count; snapshotIndex++)
            {
                AIToon snapShot = ToonSnapshots[snapshotIndex];
                AIBonusViewAndSaveSnapshotsItem snapshotItem = new AIBonusViewAndSaveSnapshotsItem();
                snapshotItem.Setup(_wizard, wizard2, snapShot, snapshotIndex + 1);
                flowLayoutPanelSnapshots.Controls.Add(snapshotItem);
            }
            this.flowLayoutPanelSnapshots.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }
}

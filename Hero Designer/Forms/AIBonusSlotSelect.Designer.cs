﻿namespace Hero_Designer.Forms
{
    partial class AIBonusSlotSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lBoxPowers = new System.Windows.Forms.ListBox();
            this.cboxSlot1 = new System.Windows.Forms.CheckBox();
            this.cboxSlot2 = new System.Windows.Forms.CheckBox();
            this.cboxSlot3 = new System.Windows.Forms.CheckBox();
            this.cboxSlot6 = new System.Windows.Forms.CheckBox();
            this.cboxSlot5 = new System.Windows.Forms.CheckBox();
            this.cboxSlot4 = new System.Windows.Forms.CheckBox();
            this.lblInstructions = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(692, 405);
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(96, 33);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(576, 405);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 33);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lBoxPowers
            // 
            this.lBoxPowers.FormattingEnabled = true;
            this.lBoxPowers.ItemHeight = 16;
            this.lBoxPowers.Location = new System.Drawing.Point(15, 16);
            this.lBoxPowers.Name = "lBoxPowers";
            this.lBoxPowers.Size = new System.Drawing.Size(255, 388);
            this.lBoxPowers.TabIndex = 3;
            this.lBoxPowers.SelectedIndexChanged += new System.EventHandler(this.lBoxPowers_SelectedIndexChanged);
            // 
            // cboxSlot1
            // 
            this.cboxSlot1.AutoSize = true;
            this.cboxSlot1.Location = new System.Drawing.Point(289, 47);
            this.cboxSlot1.Name = "cboxSlot1";
            this.cboxSlot1.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot1.TabIndex = 4;
            this.cboxSlot1.Text = "Slot 1 (Enhancement Name)";
            this.cboxSlot1.UseVisualStyleBackColor = true;
            // 
            // cboxSlot2
            // 
            this.cboxSlot2.AutoSize = true;
            this.cboxSlot2.Location = new System.Drawing.Point(289, 83);
            this.cboxSlot2.Name = "cboxSlot2";
            this.cboxSlot2.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot2.TabIndex = 5;
            this.cboxSlot2.Text = "Slot 2 (Enhancement Name)";
            this.cboxSlot2.UseVisualStyleBackColor = true;
            // 
            // cboxSlot3
            // 
            this.cboxSlot3.AutoSize = true;
            this.cboxSlot3.Location = new System.Drawing.Point(289, 120);
            this.cboxSlot3.Name = "cboxSlot3";
            this.cboxSlot3.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot3.TabIndex = 6;
            this.cboxSlot3.Text = "Slot 3 (Enhancement Name)";
            this.cboxSlot3.UseVisualStyleBackColor = true;
            // 
            // cboxSlot6
            // 
            this.cboxSlot6.AutoSize = true;
            this.cboxSlot6.Location = new System.Drawing.Point(289, 232);
            this.cboxSlot6.Name = "cboxSlot6";
            this.cboxSlot6.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot6.TabIndex = 9;
            this.cboxSlot6.Text = "Slot 6 (Enhancement Name)";
            this.cboxSlot6.UseVisualStyleBackColor = true;
            // 
            // cboxSlot5
            // 
            this.cboxSlot5.AutoSize = true;
            this.cboxSlot5.Location = new System.Drawing.Point(289, 195);
            this.cboxSlot5.Name = "cboxSlot5";
            this.cboxSlot5.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot5.TabIndex = 8;
            this.cboxSlot5.Text = "Slot 5 (Enhancement Name)";
            this.cboxSlot5.UseVisualStyleBackColor = true;
            // 
            // cboxSlot4
            // 
            this.cboxSlot4.AutoSize = true;
            this.cboxSlot4.Location = new System.Drawing.Point(289, 159);
            this.cboxSlot4.Name = "cboxSlot4";
            this.cboxSlot4.Size = new System.Drawing.Size(208, 21);
            this.cboxSlot4.TabIndex = 7;
            this.cboxSlot4.Text = "Slot 4 (Enhancement Name)";
            this.cboxSlot4.UseVisualStyleBackColor = true;
            // 
            // lblInstructions
            // 
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.Location = new System.Drawing.Point(281, 16);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblInstructions.Size = new System.Drawing.Size(392, 17);
            this.lblInstructions.TabIndex = 10;
            this.lblInstructions.Text = "Slect a power then select which slots the Wizard can change.";
            // 
            // AIBonusSlotSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblInstructions);
            this.Controls.Add(this.cboxSlot6);
            this.Controls.Add(this.cboxSlot5);
            this.Controls.Add(this.cboxSlot4);
            this.Controls.Add(this.cboxSlot3);
            this.Controls.Add(this.cboxSlot2);
            this.Controls.Add(this.cboxSlot1);
            this.Controls.Add(this.lBoxPowers);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIBonusSlotSelect";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Slot Selection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lBoxPowers;
        private System.Windows.Forms.CheckBox cboxSlot1;
        private System.Windows.Forms.CheckBox cboxSlot2;
        private System.Windows.Forms.CheckBox cboxSlot3;
        private System.Windows.Forms.CheckBox cboxSlot6;
        private System.Windows.Forms.CheckBox cboxSlot5;
        private System.Windows.Forms.CheckBox cboxSlot4;
        private System.Windows.Forms.Label lblInstructions;
    }
}
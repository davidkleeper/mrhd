﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusPowerSelect : Form
    {
        public bool[] powerFlags;
        public AIToon toon;

        public AIBonusPowerSelect()
        {
            InitializeComponent();
        }

        public void Setup(AIToon toon, bool[] powerFlags, String title = null)
        {
            this.toon = toon;
            this.powerFlags = powerFlags;
            PopulatePowers();
            if (null != title)
                this.Text = title;
        }

        private void PopulatePowers()
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            RmoveAllPowers();
            for (int powerIndex = 0; powerIndex < powerEntries.Count; powerIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerIndex];
                bool flag = powerFlags[powerIndex];
                AddPower(powerEntry.Power, flag);
            }
        }

        private void AddPower(IPower power, bool flag)
        {
            CheckBox cboxPower = new CheckBox();
            cboxPower.Enabled = true;
            cboxPower.Checked = flag;
            cboxPower.Text = power.DisplayName;
            cboxPower.Width = 200;
            flowLayoutPanelPowers.Controls.Add(cboxPower);
        }

        private void RmoveAllPowers()
        {
            flowLayoutPanelPowers.Controls.Clear();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            for (int powerIndex = 0; powerIndex < flowLayoutPanelPowers.Controls.Count; powerIndex++)
            {
                CheckBox cboxPower = flowLayoutPanelPowers.Controls[powerIndex] as CheckBox;
                if (null == cboxPower) continue;
                powerFlags[powerIndex] = cboxPower.Checked;
            }
            RmoveAllPowers();
        }
    }
}

﻿namespace Hero_Designer.Forms
{
    partial class AIPermaResults
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPermaValues = new System.Windows.Forms.TextBox();
            this.lblIntro3 = new System.Windows.Forms.Label();
            this.lblIntro2 = new System.Windows.Forms.Label();
            this.lblIntro1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxPermaValues
            // 
            this.textBoxPermaValues.Location = new System.Drawing.Point(16, 126);
            this.textBoxPermaValues.Multiline = true;
            this.textBoxPermaValues.Name = "textBoxPermaValues";
            this.textBoxPermaValues.ReadOnly = true;
            this.textBoxPermaValues.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxPermaValues.Size = new System.Drawing.Size(603, 285);
            this.textBoxPermaValues.TabIndex = 72;
            // 
            // lblIntro3
            // 
            this.lblIntro3.AutoSize = true;
            this.lblIntro3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro3.Location = new System.Drawing.Point(13, 88);
            this.lblIntro3.Name = "lblIntro3";
            this.lblIntro3.Size = new System.Drawing.Size(417, 17);
            this.lblIntro3.TabIndex = 71;
            this.lblIntro3.Text = "Perma values include recharge placed in the powers themselves.";
            // 
            // lblIntro2
            // 
            this.lblIntro2.AutoSize = true;
            this.lblIntro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro2.Location = new System.Drawing.Point(13, 53);
            this.lblIntro2.Name = "lblIntro2";
            this.lblIntro2.Size = new System.Drawing.Size(623, 17);
            this.lblIntro2.TabIndex = 70;
            this.lblIntro2.Text = "Some powers have multiple effects. Calculations are based on the effect with the " +
    "longest duration.";
            // 
            // lblIntro1
            // 
            this.lblIntro1.AutoSize = true;
            this.lblIntro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro1.Location = new System.Drawing.Point(13, 20);
            this.lblIntro1.Name = "lblIntro1";
            this.lblIntro1.Size = new System.Drawing.Size(219, 17);
            this.lblIntro1.TabIndex = 69;
            this.lblIntro1.Text = "Perma Calculations Complete";
            // 
            // AIPerma3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 403);
            this.Controls.Add(this.textBoxPermaValues);
            this.Controls.Add(this.lblIntro3);
            this.Controls.Add(this.lblIntro2);
            this.Controls.Add(this.lblIntro1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AIPerma3";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPermaValues;
        private System.Windows.Forms.Label lblIntro3;
        private System.Windows.Forms.Label lblIntro2;
        private System.Windows.Forms.Label lblIntro1;
    }
}

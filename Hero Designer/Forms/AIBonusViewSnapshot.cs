﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hero_Designer.Forms
{
    public partial class AIBonusViewSnapshot : Form
    {
        public AIBonusViewSnapshot()
        {
            InitializeComponent();
        }

        public void Setup(string[] lines)
        {
            textBoxBuild.Lines = lines;
        }
    }
}

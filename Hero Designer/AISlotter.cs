﻿using System;
using System.Collections.Generic;

namespace Hero_Designer
{
    public class AISlotter
    {
        public static void SlotToon(AIToon toon, AIConfig config)
        {
            List<PowerEntry> powerEntries = toon.GetPowerEntries();

            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                if (!config.PowerFlags[powerEntryIndex])
                    continue;
                PowerEntry powerEntry = SlotPowerEntry(toon, powerEntries, powerEntryIndex, config);
                powerEntries[powerEntryIndex] = powerEntry;
                toon.SetPowerEntries(powerEntries);
            }
        }

        public static PowerEntry SlotPowerEntry(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, AIConfig config)
        {
            List<PowerEntry> powerEntriesClone = new List<PowerEntry>(powerEntries);
            PowerEntry powerEntry = powerEntries[powerEntryIndex];
            PowerEntry selectedPowerEntry = powerEntry;
            float sum = 0.0f;
            AISlotterData[] slotResultData = new AISlotterData[config.BonusInfoWithWeightArray.Length];
            for (int bonusInfoIndex = 0; bonusInfoIndex < config.BonusInfoWithWeightArray.Length; bonusInfoIndex++)
            {
                BonusInfoWithWeight bonus = config.BonusInfoWithWeightArray[bonusInfoIndex];
                sum = bonus.Sum(powerEntry, toon.Archetype);
                AISlotterData data = new AISlotterData();
                data.Sum = sum;
                data.PowerEntry = powerEntry;
                slotResultData[bonusInfoIndex] = data;
                if (!bonus.BelowCap(toon.Sum(bonus, config)))
                {
                    AILog.Add("Cap Hit", bonus.DisplayName + " cap hit.");
                    continue;
                }

                PowerEntry highest = AIStrategyHighestSet.Do(toon, powerEntries, powerEntryIndex, config, bonusInfoIndex);
                if (!highest.FollowsED())
                    continue;
                if (!AIStrategyProtectPreviousBonuses.ProtectsPreviousBonuses(toon, powerEntriesClone, highest, powerEntryIndex, config, bonusInfoIndex))
                {
                    AILog.Add("Does not protect revious bonuses", "Class: AISlotter, Power: " + powerEntry.Power.DisplayName + ", Bonus: " + bonus.DisplayName + " REJECTED. Lowers Previous Bonus.");
                    toon.SetPowerEntries(powerEntries);
                    continue;
                }
                float newSum = bonus.Sum(highest, toon.Archetype);
                selectedPowerEntry = sum < newSum ? highest : selectedPowerEntry;
                powerEntriesClone[powerEntryIndex] = selectedPowerEntry;
                sum = bonus.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry frankenslot = AIStrategyFrankenslot.Do(toon, powerEntries, powerEntryIndex, config, bonusInfoIndex);
                if (!frankenslot.FollowsED())
                    continue;
                if (!AIStrategyProtectPreviousBonuses.ProtectsPreviousBonuses(toon, powerEntriesClone, frankenslot, powerEntryIndex, config, bonusInfoIndex))
                {
                    AILog.Add("Does not protect revious bonuses", "Class: AISlotter, Power: " + powerEntry.Power.DisplayName + ", Bonus: " + bonus.DisplayName + " REJECTED. Lowers Previous Bonus.");
                    toon.SetPowerEntries(powerEntries);
                    continue;
                }
                newSum = bonus.Sum(frankenslot, toon.Archetype);
                selectedPowerEntry = sum < newSum ? frankenslot : selectedPowerEntry;
                powerEntriesClone[powerEntryIndex] = selectedPowerEntry;

                data.Sum = Math.Max(sum, newSum);
                data.PowerEntry = selectedPowerEntry;
                slotResultData[bonusInfoIndex] = data;
            }
            int highestDataIndex = 0;
            if (1 < slotResultData.Length)
            {
                for (int resultIndex = 1; resultIndex < slotResultData.Length; resultIndex++)
                {
                    if (null != slotResultData[resultIndex] && null != slotResultData[highestDataIndex]) {
                        if (slotResultData[resultIndex].Sum > slotResultData[highestDataIndex].Sum)
                            highestDataIndex = resultIndex;
                    }
                }
            }
            return slotResultData[highestDataIndex].PowerEntry;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Hero_Designer
{
    public static class AIEvolverPowerEntry
    {
        public static event EventHandler<AIEventIteration> RaiseIterationEvent;

        public static void EvolvePower(AIToon toon, List<PowerEntry> powerEntries, int index, AIConfig config, CancellationToken cancellationToken)
        {
            for (int iterationCount = 0; iterationCount < config.Iterations; iterationCount++)
            {
                EvolveSlots(toon, powerEntries, index, config);
                EvolveSpecialIOs(toon, powerEntries, index, config);
                if (cancellationToken.IsCancellationRequested)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                }
                OnRaiseIterationEvent(new AIEventIteration(iterationCount + 1));
            }
        }

        private static void EvolveSlots(
            AIToon toon,
            List<PowerEntry> powerEntries,
            int index,
            AIConfig config
        )
        {
            Random rnd = new Random();
            for (int bonusInfoIndex = 0; bonusInfoIndex < config.BonusInfoWithWeightArray.Length; bonusInfoIndex++)
            {
                BonusInfoWithWeight bonus = config.BonusInfoWithWeightArray[bonusInfoIndex];
                float previousSum = bonus.Sum(powerEntries[index], toon.Archetype);
                if (!bonus.BelowCap(toon.Sum(bonus, config)))
                {
                    AILog.Add("Cap Hit", bonus.DisplayName + " cap hit.");
                    continue;
                }
                AIStrategyEvolveSlotsData slotStrategyData = AIStrategyEvolveSlots.Do(toon, powerEntries, index, config, bonusInfoIndex);
                float sum = slotStrategyData.Sum;
                if (previousSum < sum)
                {
                    int weight = Math.Min(100, (int)(bonus.Weight * 100));
                    int someNumber = rnd.Next(1, 101);
                    if (someNumber <= weight)
                    {
                        powerEntries[slotStrategyData.PowerEntryIndex1] = slotStrategyData.PowerEntry1;
                        powerEntries[slotStrategyData.PowerEntryIndex2] = slotStrategyData.PowerEntry2;
                        toon.SetPowerEntries(powerEntries);
                        return;
                    }
                }
            }
        }

        private static void EvolveSpecialIOs(
            AIToon toon,
            List<PowerEntry> powerEntries,
            int index,
            AIConfig config
        )
        {
            Random rnd = new Random();
            for (int bonusInfoIndex = 0; bonusInfoIndex < config.BonusInfoWithWeightArray.Length; bonusInfoIndex++)
            {
                BonusInfoWithWeight bonus = config.BonusInfoWithWeightArray[bonusInfoIndex];
                float previousSum = bonus.Sum(powerEntries[index], toon.Archetype);
                AIStrategyEvolveSpecialIOsData slotStrategyData = AIStrategyEvolveSpecialIOs.Do(toon, powerEntries, index, config, bonusInfoIndex);
                
                if (!bonus.BelowCap(toon.Sum(bonus, config)))
                {
                    float x = toon.Sum(bonus, config);
                    AILog.Add("Cap Hit", bonus.DisplayName + " cap hit.");
                    continue;
                }
                float sum = slotStrategyData.Sum;
                if (previousSum < sum)
                {
                    int weight = Math.Min(100, (int)(bonus.Weight * 100));
                    int someNumber = rnd.Next(1, 101);
                    if (someNumber <= weight)
                    {
                        powerEntries[slotStrategyData.PowerEntryIndex] = slotStrategyData.PowerEntry;
                        toon.SetPowerEntries(powerEntries);
                        return;
                    }
                }
            }
        }

        /*
    public static void MultiEvolvePower(AIToon toon, List<PowerEntry> powerEntries, int index, int iterations, BonusInfoWithWeight[] bonusInfoWithWeightArray)
    {           
        for (int iterationCount = 0; iterationCount < iterations; iterationCount++)
        {
            EvolveSlots(toon, powerEntries, index, iterations, bonusInfoWithWeightArray);
            EvolveSpecialIOs(toon, powerEntries, index, iterations, bonusInfoWithWeightArray);
            OnRaiseIterationEvent(new AIEventIteration(iterationCount + 1));
        }
    }

    protected static void EvolveSlots(
        AIToon toon,
        List<PowerEntry> powerEntries,
        int index,
        int iterations,
        BonusInfoWithWeight[] bonusInfoWithWeightArray
    )
    {
        Task<AIStrategyEvolveSlotsData>[] taskArray = new Task<AIStrategyEvolveSlotsData>[bonusInfoWithWeightArray.Length];
        Action<Task<AIStrategyEvolveSlotsData>[], AIToon, List<PowerEntry>, int, int, BonusInfoWithWeight[], int> evolve =
            (
                Task<AIStrategyEvolveSlotsData>[] taskArray,
                AIToon toon,
                List<PowerEntry> powerEntries,
                int index,
                int iterations,
                BonusInfoWithWeight[] bonusInfoWithWeightArray,
                int bonusInfoWithWeightIndex
            ) =>
            {
                taskArray[bonusInfoWithWeightIndex] = Task<AIStrategyEvolveSlotsData>.Factory.StartNew(
                    () => AIStrategyEvolveSlots.Do(toon, powerEntries, index, bonusInfoWithWeightArray[bonusInfoWithWeightIndex]),
                    CancellationToken.None,
                    TaskCreationOptions.DenyChildAttach,
                    TaskScheduler.Default);
            };

        for (int i = 0; i < bonusInfoWithWeightArray.Length; i++)
        {
            evolve(taskArray, toon, powerEntries, index, iterations, bonusInfoWithWeightArray, i);
        }

        Task.WaitAll(taskArray);
        float sum = 0.0f;
        AIStrategyEvolveSlotsData result = taskArray[0].Result;
        for (int resultIndex = 0; resultIndex < taskArray.Length; resultIndex++)
        {
            Task<AIStrategyEvolveSlotsData> task = taskArray[resultIndex];
            AIStrategyEvolveSlotsData data = task.Result;
            if (data == null)
                continue;
            float weighedSum = data.Sum * bonusInfoWithWeightArray[resultIndex].Weight;
            if (weighedSum > sum)
            {
                result = data;
                sum = weighedSum;
            }
        }
        powerEntries[result.PowerEntryIndex1] = result.PowerEntry1;
        powerEntries[result.PowerEntryIndex2] = result.PowerEntry2;
        toon.SetPowerEntries(powerEntries);
    }

    protected static void EvolveSpecialIOs(
        AIToon toon,
        List<PowerEntry> powerEntries,
        int index,
        int iterations,
        BonusInfoWithWeight[] bonusInfoWithWeightArray
    )
    {
        Task<AIStrategyEvolveSpecialIOsData>[] taskArray = new Task<AIStrategyEvolveSpecialIOsData>[bonusInfoWithWeightArray.Length];
        Action<Task<AIStrategyEvolveSpecialIOsData>[], AIToon, List<PowerEntry>, int, int, BonusInfoWithWeight[], int> evolve =
            (
                Task<AIStrategyEvolveSpecialIOsData>[] taskArray,
                AIToon toon,
                List<PowerEntry> powerEntries,
                int index,
                int iterations,
                BonusInfoWithWeight[] bonusInfoWithWeightArray,
                int bonusInfoWithWeightIndex
            ) =>
            {
                taskArray[bonusInfoWithWeightIndex] = Task<AIStrategyEvolveSpecialIOsData>.Factory.StartNew(
                    () => AIStrategyEvolveSpecialIOs.Do(toon, powerEntries, index, bonusInfoWithWeightArray[bonusInfoWithWeightIndex]),
                    CancellationToken.None,
                    TaskCreationOptions.DenyChildAttach,
                    TaskScheduler.Default);
            };

        for (int i = 0; i < bonusInfoWithWeightArray.Length; i++)
        {
            evolve(taskArray, toon, powerEntries, index, iterations, bonusInfoWithWeightArray, i);
        }

        Task.WaitAll(taskArray);
        float sum = 0.0f;
        AIStrategyEvolveSpecialIOsData result = taskArray[0].Result;
        for (int resultIndex = 0; resultIndex < taskArray.Length; resultIndex++)
        {
            Task<AIStrategyEvolveSpecialIOsData> task = taskArray[resultIndex];
            AIStrategyEvolveSpecialIOsData data = task.Result;
            if (data == null)
                continue;
            float weighedSum = data.Sum * bonusInfoWithWeightArray[resultIndex].Weight;
            if (weighedSum > sum)
            {
                result = data;
                sum = weighedSum;
            }
        }
        powerEntries[result.PowerEntryIndex] = result.PowerEntry;
        toon.SetPowerEntries(powerEntries);
    }
    */

        private static void OnRaiseIterationEvent(AIEventIteration e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<AIEventIteration> handler = RaiseIterationEvent;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                // Use the () operator to raise the event.
                handler(typeof(AIEvolverPowerEntry), e);
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Diagnostics;


namespace Hero_Designer
{
    public class AIStrategyFrankenslot
    {
        public static PowerEntry Do(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, AIConfig config, int configBonusIndex)
        {
            PowerEntry powerEntry = powerEntries[powerEntryIndex];
            PowerEntry selectedPowerEntry = powerEntry;
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            float sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
            bool[] slotFlags = config.SlotFlags[powerEntryIndex];
            int lockedSlotCount = PowerEntry.GetLockedSlotsCount(slotFlags);

            if (3 >= powerEntry.Slots.Length - lockedSlotCount)
                return powerEntry;
            if (4 == powerEntry.Slots.Length - lockedSlotCount)
            {
                PowerEntry powerEntry2And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 2, config, configBonusIndex);
                float newSum = bonusInfoWithWeight.Sum(powerEntry2And2, toon.Archetype);
                if (sum < newSum)
                    selectedPowerEntry = powerEntry2And2;
            }
            else if (5 == powerEntry.Slots.Length - lockedSlotCount)
            {
                PowerEntry powerEntry2And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 2, config, configBonusIndex);
                float newSum = bonusInfoWithWeight.Sum(powerEntry2And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And2 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry3And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 3, 2, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry3And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry3And2 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry2And3 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 3, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry2And3, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And3 : selectedPowerEntry;
            }
            else if (6 == powerEntry.Slots.Length - lockedSlotCount)
            {
                PowerEntry powerEntry2And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 2, config, configBonusIndex);
                float newSum = bonusInfoWithWeight.Sum(powerEntry2And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And2 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry3And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 3, 2, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry3And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry3And2 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry2And3 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 3, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry2And3, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And3 : selectedPowerEntry;
                PowerEntry powerEntry4And2 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 4, 2, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry4And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry4And2 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry2And4 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 2, 4, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry2And4, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And4 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry3And3 = Frankenslot2SetsStrategy(toon, powerEntries, powerEntryIndex, 3, 3, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry3And3, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry3And3 : selectedPowerEntry;
                sum = bonusInfoWithWeight.Sum(selectedPowerEntry, toon.Archetype);
                PowerEntry powerEntry2And2And2 = Frankenslot3SetsStrategy(toon, powerEntries, powerEntryIndex, config, configBonusIndex);
                newSum = bonusInfoWithWeight.Sum(powerEntry2And2And2, toon.Archetype);
                selectedPowerEntry = sum < newSum ? powerEntry2And2And2 : selectedPowerEntry;
            }
            return selectedPowerEntry;
        }

        public static PowerEntry Frankenslot2SetsStrategy(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, int slots1, int slots2, AIConfig config, int configBonusIndex)
        {
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            HashSet<EnhancementSet> firstSets = powerEntries[powerEntryIndex].Power.GetEnhancementSetsWithBonusEffect(slots1, bonusInfoWithWeight);
            HashSet<EnhancementSet> secondSets = powerEntries[powerEntryIndex].Power.GetEnhancementSetsWithBonusEffect(slots2, bonusInfoWithWeight);
            if (0 == firstSets.Count || 0 == secondSets.Count)
                return powerEntries[powerEntryIndex];
            EnhancementSet[] sortedFirstSets = EnhancementSet.SortEnhancementSets(firstSets, slots1, bonusInfoWithWeight);
            EnhancementSet firstEnhancementSet = toon.GetHighestValidSet(sortedFirstSets, powerEntries[powerEntryIndex], slots1);
            if (null == firstEnhancementSet)
                return powerEntries[powerEntryIndex];
            secondSets.Remove(firstEnhancementSet);
            if (0 == secondSets.Count)
                return powerEntries[powerEntryIndex];
            EnhancementSet[] sortedSecondSets = EnhancementSet.SortEnhancementSets(secondSets, slots2, bonusInfoWithWeight);
            EnhancementSet secondEnhancementSet = toon.GetHighestValidSet(sortedSecondSets, powerEntries[powerEntryIndex], slots2);
            if (null == secondEnhancementSet)
                return powerEntries[powerEntryIndex];
            bool[] slotFlags = config.SlotFlags[powerEntryIndex];
            int lockedSlotCount = PowerEntry.GetLockedSlotsCount(slotFlags);

            PowerEntry powerEntryClone = powerEntries[powerEntryIndex].Clone() as PowerEntry;
            int slotIndex = 0;
            int enhancementsAddedCount = 0;
            for (int enhancementIndex = 0; enhancementIndex < firstEnhancementSet.Enhancements.Length; enhancementIndex++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(firstEnhancementSet.Enhancements[enhancementIndex]);
                if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                    continue;
                while (slotIndex < powerEntryClone.SlotCount && !PowerEntry.IsSlotUnlocked(slotIndex, slotFlags)) { slotIndex++;  };
                if (slotIndex >= powerEntryClone.SlotCount) break;
                powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                slotIndex++;
                enhancementsAddedCount++;
                if (enhancementsAddedCount == slots1)
                    break;
            }
            enhancementsAddedCount = 0;
            for (int enhancementIndex = 0; enhancementIndex < secondEnhancementSet.Enhancements.Length; enhancementIndex++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(secondEnhancementSet.Enhancements[enhancementIndex]);
                if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                    continue;
                while (slotIndex < powerEntryClone.SlotCount && !PowerEntry.IsSlotUnlocked(slotIndex, slotFlags)) { slotIndex++; };
                if (slotIndex >= powerEntryClone.SlotCount) break;
                powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                slotIndex++;
                enhancementsAddedCount++;
                if (enhancementsAddedCount == slots2)
                    break;
            }
            return powerEntryClone;
        }

        public static PowerEntry Frankenslot3SetsStrategy(AIToon toon, List<PowerEntry> powerEntries, int index, AIConfig config, int configBonusIndex)
        {
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            HashSet<EnhancementSet> firstSets = powerEntries[index].Power.GetEnhancementSetsWithBonusEffect(2, bonusInfoWithWeight);
            HashSet<EnhancementSet> secondSets = powerEntries[index].Power.GetEnhancementSetsWithBonusEffect(2, bonusInfoWithWeight);
            HashSet<EnhancementSet> thirdSets = powerEntries[index].Power.GetEnhancementSetsWithBonusEffect(2, bonusInfoWithWeight);
            if (0 == firstSets.Count || 0 == secondSets.Count || 0 == thirdSets.Count)
                return powerEntries[index];
            EnhancementSet[] sortedFirstSets = EnhancementSet.SortEnhancementSets(firstSets, 2, bonusInfoWithWeight);
            EnhancementSet firstEnhancementSet = toon.GetHighestValidSet(sortedFirstSets, powerEntries[index], 2);
            if (null == firstEnhancementSet)
                return powerEntries[index];
            secondSets.Remove(firstEnhancementSet);
            thirdSets.Remove(firstEnhancementSet);
            if (0 == secondSets.Count || 0 == thirdSets.Count)
                return powerEntries[index];
            EnhancementSet[] sortedSecondSets = EnhancementSet.SortEnhancementSets(secondSets, 2, bonusInfoWithWeight);
            EnhancementSet secondEnhancementSet = toon.GetHighestValidSet(sortedSecondSets, powerEntries[index], 2);
            if (null == secondEnhancementSet)
                return powerEntries[index];
            thirdSets.Remove(secondEnhancementSet);
            if (0 == thirdSets.Count)
                return powerEntries[index];
            EnhancementSet[] sortedThirdSets = EnhancementSet.SortEnhancementSets(thirdSets, 2, bonusInfoWithWeight);
            EnhancementSet thirdEnhancementSet = toon.GetHighestValidSet(sortedThirdSets, powerEntries[index], 2);
            if (null == thirdEnhancementSet)
                return powerEntries[index];

            PowerEntry powerEntryClone = powerEntries[index].Clone() as PowerEntry;
            int slotIndex = 0;
            int enhancementsAddedCount = 0;
            for (int enhancementIndex = 0; enhancementIndex < firstEnhancementSet.Enhancements.Length; enhancementIndex++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(firstEnhancementSet.Enhancements[enhancementIndex]);
                if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                    continue;
                powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                slotIndex++;
                enhancementsAddedCount++;
                if (enhancementsAddedCount == 2)
                    break;
            }
            enhancementsAddedCount = 0;
            for (int enhancementIndex = 0; enhancementIndex < secondEnhancementSet.Enhancements.Length; enhancementIndex++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(secondEnhancementSet.Enhancements[enhancementIndex]);
                if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                    continue;
                powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                slotIndex++;
                enhancementsAddedCount++;
                if (enhancementsAddedCount == 2)
                    break;
            }
            enhancementsAddedCount = 0;
            for (int enhancementIndex = 0; enhancementIndex < thirdEnhancementSet.Enhancements.Length; enhancementIndex++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(thirdEnhancementSet.Enhancements[enhancementIndex]);
                if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                    continue;
                powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                slotIndex++;
                enhancementsAddedCount++;
                if (enhancementsAddedCount == 2)
                    break;
            }
            return powerEntryClone;
        }

        public static void TestNewCode()
        {
            TestFrakenslotStrategy();
        }

        public static void TestFrakenslotStrategy()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            AIConfig config = new AIConfig(toon, 1, 1, new BonusInfoWithWeight[] { new BonusInfoWithWeight(BonusInfo.Recovery.CloneWithPvP(), 1.0f) });
            PowerEntry result = Do(toon, toon.GetPowerEntries(), 3, config, 0);
            Debug.Assert(6 == result.Slots.Length, "result is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[0].Enhancement.Enh).UIDSet == "Shield_Wall", "enhancement[0] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[1].Enhancement.Enh).UIDSet == "Shield_Wall", "enhancement[1] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[2].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[2] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[3].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[3] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[4].Enhancement.Enh).UIDSet == "Kismet", "enhancement[4] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[5].Enhancement.Enh).UIDSet == "Kismet", "enhancement[5] is incorrect!");

            config = new AIConfig(toon, 1, 1, new BonusInfoWithWeight[] { new BonusInfoWithWeight(BonusInfo.Recovery, 1.0f) });
            result = Do(toon, toon.GetPowerEntries(), 3, config, 0);
            Debug.Assert(6 == result.Slots.Length, "result is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[0].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[0] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[1].Enhancement.Enh).UIDSet == "Gift_of_the_Ancients", "enhancement[1] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[2].Enhancement.Enh).UIDSet == "Kismet", "enhancement[2] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[3].Enhancement.Enh).UIDSet == "Kismet", "enhancement[3] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[4].Enhancement.Enh).UIDSet == "", "enhancement[4] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[5].Enhancement.Enh).UIDSet == "", "enhancement[5] is incorrect!");
        }
    }
}

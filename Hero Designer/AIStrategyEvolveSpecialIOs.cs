﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Hero_Designer
{
    class AIStrategyEvolveSpecialIOs
    {
        public static AIStrategyEvolveSpecialIOsData Do(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, AIConfig config, int configBonusIndex)
        {
            // Get the special IOs that we can use. If there aren't any, return.
            PowerEntry powerEntry = powerEntries[powerEntryIndex];
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            int[] specialIOIndexes = bonusInfoWithWeight.GetRelatedSpecialIOIndexes();
            AIStrategyEvolveSpecialIOsData data = new AIStrategyEvolveSpecialIOsData();
            data.PowerEntry = powerEntry;
            data.PowerEntryIndex = powerEntryIndex;
            data.Sum = Math.Abs(bonusInfoWithWeight.Sum(powerEntry, toon.Archetype));
            if (0 == specialIOIndexes.Length)
                return data;
            if (!bonusInfoWithWeight.CanEnhance(powerEntry))
                return data;

            // Clone power, record bonus value of power
            PowerEntry powerEntryClone = powerEntry.Clone() as PowerEntry;
            float originalSum = data.Sum;

            // Select a slot, prefering empty slots or slots with regular enhancements over ones with IOs.
            int slotIndex = GetSlotIndex(powerEntryClone);
            if (-1 == slotIndex)
            {
                return data;
            }

            // Set the slot to a special IO and record new bonus value.
            if (!SetSlot(toon, powerEntryClone, slotIndex, specialIOIndexes, bonusInfoWithWeight))
            {
                return data;
            }
            float newSum = bonusInfoWithWeight.Sum(powerEntryClone, toon.Archetype);

            // Set the toon to use the clone.
            List<PowerEntry> powerEntriesClone = toon.GetPowerEntries();
            powerEntriesClone[powerEntryIndex] = powerEntryClone;
            toon.SetPowerEntries(powerEntriesClone);

            // If the resulting build is not valid, use the original.
            string message;
            if (!toon.IsValid(out message))
            {
                AILog.Add(message, "Class: AIStrategyEvolveSpecialIOs " + "Power: " + powerEntry.Power.DisplayName + " Bonus: " + bonusInfoWithWeight.DisplayName + " REJECTED.");
                toon.SetPowerEntries(powerEntries);
                return data;
            }
            if (!AIStrategyProtectPreviousBonuses.ProtectsPreviousBonuses(toon, powerEntries, powerEntryClone, powerEntryIndex, config, configBonusIndex))
            {
                AILog.Add("Does not protect revious bonuses", "Class: AIStrategyEvolveSlots, Power: " + powerEntry.Power.DisplayName + ", Bonus: " + bonusInfoWithWeight.DisplayName + " REJECTED. Lowers Previous Bonus.");
                toon.SetPowerEntries(powerEntries);
                return data;
            }

            // Reset the toon.
            toon.SetPowerEntries(powerEntries);

            // Use the highest bonus value.
            if (originalSum >= newSum) return data;
            data.PowerEntry = powerEntryClone;
            data.Sum = newSum;
            return data;
        }

        public static int GetSlotIndex(PowerEntry powerEntry)
        {
            for (int slotIndex = 0; slotIndex < powerEntry.Slots.Length; slotIndex++)
            {
                SlotEntry slot = powerEntry.Slots[slotIndex];
                if (-1 == slot.Enhancement.Enh)
                    return slotIndex;
            }
            return -1;
        }

        public static bool SetSlot(AIToon toon, PowerEntry powerEntry, int slotIndex, int[] specialIOIndexes, BonusInfoWithWeight bonusInfoWithWeight)
        {
            Func<int[], IEnhancement, int> GetIndex =
            (
                int[] indexes,
                IEnhancement enhancement
            ) => 
            {
                for (int index = 0; index < indexes.Length; index++)
                {
                    IEnhancement e = DatabaseAPI.GetEnhancementByIndex(indexes[index]);
                    if (e.LongName == enhancement.LongName)
                        return index;
                }
                return -1;
            };
            IEnhancement[] specialIOs = GetSpecialIOs(specialIOIndexes, bonusInfoWithWeight);
            for (int specialIOIndex = 0; specialIOIndex < specialIOs.Length; specialIOIndex++)
            {
                IEnhancement candidateEnhancement = specialIOs[specialIOIndex];
                if (!toon.CanAddEnhancement(candidateEnhancement, powerEntry)) continue;
                if (!CanAddEnhancement(candidateEnhancement, powerEntry)) continue;
                if (powerEntry.HasEnhancement(candidateEnhancement)) continue;
                int index = GetIndex(specialIOIndexes, candidateEnhancement);
                if (-1 == index)
                    return false;
                powerEntry.Slots[slotIndex].Enhancement.Enh = specialIOIndexes[index];
                return true;
            }
            return false;
        }

        public static IEnhancement[] GetSpecialIOs(int[] specialIOIndexes, BonusInfoWithWeight bonusInfoWithWeight)
        {
            List<IEnhancement> specialIOs = new List<IEnhancement>();
            foreach (int specialIOIndex in specialIOIndexes)
                specialIOs.Add(DatabaseAPI.GetEnhancementByIndex(specialIOIndex));
            specialIOs.Sort((enhancement1, enhancement2) =>
            {
                float bonus1 = SpecialEnhancements.GetSpecialBonus(bonusInfoWithWeight, enhancement1);
                float bonus2 = SpecialEnhancements.GetSpecialBonus(bonusInfoWithWeight, enhancement2);
                return bonus1.CompareTo(bonus2);
            });
            return specialIOs.Reverse<IEnhancement>().ToArray();
        }

        public static bool CanAddEnhancement(IEnhancement enhancement, PowerEntry powerEntry)
        {
            IEnhancement overwhelmingForce = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.OverwhelmingForceDamageChanceForKnockdownKnockbackToKnockdown);
            IEnhancement suddenAcceleration = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuddenAccelerationKnockbackToKnockdown);

            if (enhancement.LongName != overwhelmingForce.LongName && enhancement.LongName != suddenAcceleration.LongName)
                return true;

            foreach (IEffect effect in powerEntry.Power.Effects)
            {
                if (effect.EffectType == Enums.eEffectType.Mez && effect.MezType == Enums.eMez.Knockback)
                    return true;
            }
            return false;
        }

        public static void TestNewCode()
        {
            TestGetSlotIndex();
            TestSetSlot();
            TestGetSpecialIOs();
        }

        public static void TestGetSlotIndex()
        {
            IPowerset powerset = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            PowerEntry personalForceField = new PowerEntry(0, powerset.Powers[0]);
            int[] enhancementIndexes = { 272, 273, 274, 275, 276, -1 };
            personalForceField.Slot(enhancementIndexes);
            int index = GetSlotIndex(personalForceField);
            Debug.Assert(index == 5, "GetSlotIndex is incorrect!");
            enhancementIndexes = new int[] { 272, 273, 274, 275, (int)EnhancementType.Defense, 277 };
            personalForceField.Slot(enhancementIndexes);
            index = GetSlotIndex(personalForceField);
            Debug.Assert(index == -1, "GetSlotIndex is incorrect!");
        }

        public static void TestSetSlot()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            int[] specialIOs = BonusInfo.Hold.GetRelatedSpecialIOIndexes();
            IEnhancement expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.LockdownChanceForPlus2MagHold);
            bool wasSet = SetSlot(toon, toon.Level6, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.DefenseAll, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level6.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.Recovery.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PerformanceShifterChanceForPlusEndurance);
            wasSet = SetSlot(toon, toon.Stamina, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.Recovery, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Stamina.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.RechargeTime.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.LuckOfTheGamblerGlobalRecharge);
            wasSet = SetSlot(toon, toon.Level1Secondary, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.RechargeTime, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level1Secondary.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.Heal.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PanaceaHitPointsEndurance);
            toon.Level4.DoDefaultSlotting(3);
            toon.Health.DoDefaultSlotting(6);
            wasSet = SetSlot(toon, toon.Health, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.Heal, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Health.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");
            int index = GetSlotIndex(toon.Health);
            wasSet = SetSlot(toon, toon.Health, index, specialIOs, new BonusInfoWithWeight(BonusInfo.Heal, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Health.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PreventiveMedicineChanceForAbsorb);
            Debug.Assert(toon.Health.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.ResistanceCold.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ShieldWallResTeleportation5ResAll);
            wasSet = SetSlot(toon, toon.Level1Secondary, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.ResistanceCold, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level1Secondary.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.KnockbackProtection.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.KarmaKnockbackProtection);
            wasSet = SetSlot(toon, toon.Level1Secondary, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.ResistanceCold, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level1Secondary.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            specialIOs = BonusInfo.Knockdown.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuddenAccelerationKnockbackToKnockdown);
            wasSet = SetSlot(toon, toon.Level47, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.Knockdown, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level47.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");

            toon = AIToonUtils.CreateBuildMastermindDemonsPain();
            specialIOs = BonusInfo.PetDefense.GetRelatedSpecialIOIndexes();
            expectedEnhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorCommandOfTheMastermindRechargePetAoEDefenseAura);
            wasSet = SetSlot(toon, toon.Level1Primary, 0, specialIOs, new BonusInfoWithWeight(BonusInfo.ResistanceCold, 1.0f));
            Debug.Assert(wasSet, "wasSet is incorrect!");
            Debug.Assert(toon.Level1Primary.HasEnhancement(expectedEnhancement), "expectedEnhancement is incorrect!");
        }

        public static void TestGetSpecialIOs()
        {
        }
    }
}

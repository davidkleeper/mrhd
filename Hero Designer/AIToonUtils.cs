﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Base.Master_Classes;
using Base.Data_Classes;

namespace Hero_Designer
{
    class AIToonUtils
    {
        public static bool WriteAIToon(AIToon toon, string fileName)
        {
            Character character = MidsContext.Character;
            clsToonX toonX = AIToonUtils.Convert(toon);
            MidsContext.Character = toonX;
            bool result = toonX.Save(fileName);
            MidsContext.Character = character;
            return result;
        }

        public static string[] GetLines(AIToon toon, BonusInfoWithWeight[] bonusInfoWithWeightArray, AIConfig config)
        {
            List<string> lines = new List<string>();

            float[] sum = null;
            if (null != bonusInfoWithWeightArray)
                sum = new float[bonusInfoWithWeightArray.Length];
            lines.Add(toon.Archetype.DisplayName);
            lines.Add("=== Powersets ===");
            lines.Add("* " + toon.Fitness.DisplayName);
            lines.Add("* " + toon.Primary.DisplayName);
            lines.Add("* " + toon.Secondary.DisplayName);
            if (null != toon.Pool1) lines.Add("* " + toon.Pool1.DisplayName);
            if (null != toon.Pool2) lines.Add("* " + toon.Pool2.DisplayName);
            if (null != toon.Pool3) lines.Add("* " + toon.Pool3.DisplayName);
            if (null != toon.Pool4) lines.Add("* " + toon.Pool4.DisplayName);
            if (null != toon.Epic) lines.Add("* " + toon.Epic.DisplayName);

            if (null != bonusInfoWithWeightArray)
            {
                lines.Add("=== Optimizations ===");
                for (int bonusInfoIndex = 0; bonusInfoIndex < bonusInfoWithWeightArray.Length; bonusInfoIndex++)
                {
                    BonusInfoWithWeight bonusInfoWithWeight = bonusInfoWithWeightArray[bonusInfoIndex];
                    string effect = bonusInfoWithWeight.DisplayName;
                    string pvp = bonusInfoWithWeight.IncludePvP ? " with PvP bonuses" : " without PvP bonuses";
                    if (Enums.eEffectType.None != bonusInfoWithWeight.EffectType)
                    {
                        lines.Add(effect + pvp + " (Weight: " + bonusInfoWithWeight.Weight + ") ");
                    }
                    sum[bonusInfoIndex] = 0.0f;
                }
            }

            lines.Add("=== Powers ===");
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntries.Count; powerEntryIndex++)
            {
                PowerEntry powerEntry = powerEntries[powerEntryIndex];
                if (null == powerEntry.Power)
                    continue;
                string powerSummary = "* " + powerEntry.Power.DisplayName + " (Slots: " + powerEntry.Slots.Length + "): ";
                string comma = ", ";
                for (int slotEntryIndex = 0; slotEntryIndex < powerEntry.Slots.Length; slotEntryIndex++)
                {
                    SlotEntry slot = powerEntry.Slots[slotEntryIndex];
                    if (slotEntryIndex == powerEntry.Slots.Length - 1)
                        comma = " || Set Bonuses: ";
                    IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(slot.Enhancement.Enh);
                    if (null == enhancement)
                        continue;
                    EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
                    if (null != enhancementSet) powerSummary += enhancementSet.DisplayName + " ";
                    powerSummary += enhancement.ShortName + comma;
                }
                if (null != bonusInfoWithWeightArray && config.BonusPowerFlags[powerEntryIndex])
                {
                    for (int bonusInfoIndex = 0; bonusInfoIndex < bonusInfoWithWeightArray.Length; bonusInfoIndex++)
                    {
                        BonusInfoWithWeight bonusInfoWithWeight = bonusInfoWithWeightArray[bonusInfoIndex];
                        if (Enums.eEffectType.None != bonusInfoWithWeight.EffectType)
                        {
                            float powerEntrySum = bonusInfoWithWeight.SumBonuses(powerEntry);
                            string effect = bonusInfoWithWeight.DisplayName;
                            string pvp = bonusInfoWithWeight.IncludePvP ? " w/PvP: " : ": ";
                            powerSummary += effect + pvp + powerEntrySum.ToString() + " " + "(Weighed: " + (powerEntrySum * bonusInfoWithWeight.Weight).ToString() + ") ";
                            sum[bonusInfoIndex] += powerEntrySum;
                        }
                    }
                }
                lines.Add(powerSummary);
            }
            if (null != bonusInfoWithWeightArray)
            {
                lines.Add("=== Optimization Totals ===");
                for (int bonusInfoIndex = 0; bonusInfoIndex < bonusInfoWithWeightArray.Length; bonusInfoIndex++)
                {
                    BonusInfoWithWeight bonusInfoWithWeight = bonusInfoWithWeightArray[bonusInfoIndex];
                    if (Enums.eEffectType.None != bonusInfoWithWeight.EffectType)
                    {
                        string effect = bonusInfoWithWeight.DisplayName;
                        string pvp = bonusInfoWithWeight.IncludePvP ? " w/PvP" : "";
                        lines.Add(effect + pvp + " Total Bonus: " + sum[bonusInfoIndex].ToString() + " (Weighed: " + (sum[bonusInfoIndex] * bonusInfoWithWeight.Weight).ToString() + ")");
                    }
                }
            }
            return lines.ToArray();
        }

        public static void Print(AIToon toon, AIConfig config)
        {
            Print(toon, (BonusInfoWithWeight[])null, config);
        }

        public static void Print(AIToon toon, BonusInfoWithWeight bonusInfoWithWeight, AIConfig config)
        {
            Print(toon, new BonusInfoWithWeight[1] { bonusInfoWithWeight }, config);
        }

        public static void Print(AIToon toon, BonusInfoWithWeight[] bonusInfoWithWeightArray, AIConfig config)
        {
            string[] lines = AIToonUtils.GetLines(toon, bonusInfoWithWeightArray, config);
            foreach (string line in lines)
                Console.WriteLine(line);
        }

        public static void PrintDebug(AIToon toon)
        {
            Debug.Print("Archetype: " + toon.Archetype.DisplayName);
            Debug.Print("Fitness: " + toon.Fitness.DisplayName);
            Debug.Print("Primary: " + toon.Primary.DisplayName);
            Debug.Print("Secondary: " + toon.Secondary.DisplayName);
            if (null != toon.Pool1) Debug.Print("Pool1: " + toon.Pool1.DisplayName);
            if (null != toon.Pool2) Debug.Print("Pool2: " + toon.Pool2.DisplayName);
            if (null != toon.Pool3) Debug.Print("Pool3: " + toon.Pool3.DisplayName);
            if (null != toon.Pool4) Debug.Print("Pool4: " + toon.Pool4.DisplayName);
            if (null != toon.Epic) Debug.Print("Epic: " + toon.Epic.DisplayName);

            Func<PowerEntry, string> getEnh = (p) =>
            {
                string enh = "";
                for (int slotIndex = 0; slotIndex < p.SlotCount; slotIndex++)
                {
                    if (0 < slotIndex)
                        enh += ", ";
                    enh += p.Slots[slotIndex].Enhancement.Enh;
                }
                return enh;
            };
            Func<PowerEntry, string, string> stringifyPowerEntry = (p, name) =>
            {
                return name + ": " + p.Name + " Slots: " + p.SlotCount + " Enh: " + getEnh(p);
            };

            Debug.Print(stringifyPowerEntry(toon.Brawl, "Brawl"));
            Debug.Print(stringifyPowerEntry(toon.Health, "Health"));
            Debug.Print(stringifyPowerEntry(toon.Stamina, "Stamina"));
            Debug.Print(stringifyPowerEntry(toon.Level1Primary, "Level1Primary"));
            Debug.Print(stringifyPowerEntry(toon.Level1Secondary, "Level1Secondary"));
            Debug.Print(stringifyPowerEntry(toon.Level2, "Level2"));
            Debug.Print(stringifyPowerEntry(toon.Level4, "Level4"));
            Debug.Print(stringifyPowerEntry(toon.Level6, "Level6"));
            Debug.Print(stringifyPowerEntry(toon.Level8, "Level8"));
            Debug.Print(stringifyPowerEntry(toon.Level10, "Level10"));
            Debug.Print(stringifyPowerEntry(toon.Level12, "Level12"));
            Debug.Print(stringifyPowerEntry(toon.Level14, "Level14"));
            Debug.Print(stringifyPowerEntry(toon.Level16, "Level16"));
            Debug.Print(stringifyPowerEntry(toon.Level18, "Level18"));
            Debug.Print(stringifyPowerEntry(toon.Level20, "Level20"));
            Debug.Print(stringifyPowerEntry(toon.Level22, "Level22"));
            Debug.Print(stringifyPowerEntry(toon.Level24, "Level24"));
            Debug.Print(stringifyPowerEntry(toon.Level26, "Level26"));
            Debug.Print(stringifyPowerEntry(toon.Level28, "Level28"));
            Debug.Print(stringifyPowerEntry(toon.Level30, "Level30"));
            Debug.Print(stringifyPowerEntry(toon.Level32, "Level32"));
            Debug.Print(stringifyPowerEntry(toon.Level35, "Level35"));
            Debug.Print(stringifyPowerEntry(toon.Level38, "Level38"));
            Debug.Print(stringifyPowerEntry(toon.Level41, "Level41"));
            Debug.Print(stringifyPowerEntry(toon.Level44, "Level44"));
            Debug.Print(stringifyPowerEntry(toon.Level47, "Level47"));
            Debug.Print(stringifyPowerEntry(toon.Level49, "Level49"));
        }

        public static clsToonX Convert(AIToon toon)
        {
            Enums.dmModes buildMode = MidsContext.Config.BuildMode;
            Archetype archtype = MidsContext.Archetype;
            Character character = MidsContext.Character;
            MidsContext.Config.BuildMode = Enums.dmModes.Dynamic;
            MidsContext.Archetype = toon.Archetype;
            clsToonX convertedToon = new clsToonX();
            MidsContext.Character = convertedToon;
            IList<string> powersetNames = new List<string>();
            List<PowerEntry> powerEntries = toon.GetPowerEntries();
            convertedToon.Reset(toon.Archetype);

            convertedToon.Name = toon.Name ?? "toon";
            powersetNames.Add(toon.Primary.FullName);
            powersetNames.Add(toon.Secondary.FullName);
            powersetNames.Add(toon.Fitness.FullName);
            if (null != toon.Pool1) powersetNames.Add(toon.Pool1.FullName);
            if (null != toon.Pool2) powersetNames.Add(toon.Pool2.FullName);
            if (null != toon.Pool3) powersetNames.Add(toon.Pool3.FullName);
            if (null != toon.Pool4) powersetNames.Add(toon.Pool4.FullName);
            if (null != toon.Epic) powersetNames.Add(toon.Epic.FullName);
            while (8 > powersetNames.Count)
                powersetNames.Add("");
            convertedToon.LoadPowersetsByName(powersetNames);
            convertedToon.BuildPower(toon.Level1Primary.Power.PowerSetID, toon.Level1Primary.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level1Secondary.Power.PowerSetID, toon.Level1Secondary.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level2.Power.PowerSetID, toon.Level2.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level4.Power.PowerSetID, toon.Level4.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level6.Power.PowerSetID, toon.Level6.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level8.Power.PowerSetID, toon.Level8.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level10.Power.PowerSetID, toon.Level10.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level12.Power.PowerSetID, toon.Level12.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level14.Power.PowerSetID, toon.Level14.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level16.Power.PowerSetID, toon.Level16.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level18.Power.PowerSetID, toon.Level18.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level20.Power.PowerSetID, toon.Level20.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level22.Power.PowerSetID, toon.Level22.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level24.Power.PowerSetID, toon.Level24.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level26.Power.PowerSetID, toon.Level26.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level28.Power.PowerSetID, toon.Level28.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level30.Power.PowerSetID, toon.Level30.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level32.Power.PowerSetID, toon.Level32.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level35.Power.PowerSetID, toon.Level35.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level38.Power.PowerSetID, toon.Level38.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level41.Power.PowerSetID, toon.Level41.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level44.Power.PowerSetID, toon.Level44.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level47.Power.PowerSetID, toon.Level47.Power.PowerIndex);
            convertedToon.BuildPower(toon.Level49.Power.PowerSetID, toon.Level49.Power.PowerIndex);

            foreach (PowerEntry toonPowerEntry in powerEntries)
            {
                PowerEntry convertedPowerEntry = convertedToon.GetPowerEntryByPowerName(toonPowerEntry.Power.DisplayName);
                if (null == convertedPowerEntry)
                    continue;

                for (int slotIndex = 0; slotIndex < toonPowerEntry.Slots.Length; slotIndex++)
                {
                    if (slotIndex >= convertedPowerEntry.Slots.Length)
                        convertedPowerEntry.AddSlot(toonPowerEntry.Slots[slotIndex].Level);

                    IEnhancement emhancement = DatabaseAPI.GetEnhancementByIndex(toonPowerEntry.Slots[slotIndex].Enhancement.Enh);
                    if (null == emhancement)
                        continue;
                    string loadString = emhancement.GetLoadString(toonPowerEntry.Slots[slotIndex].Enhancement.IOLevel, ":");

                    convertedPowerEntry.Slots[slotIndex].LoadFromString(loadString, ":");
                }
            }
            MidsContext.Config.BuildMode = buildMode;
            MidsContext.Archetype = archtype;
            MidsContext.Character = character;
            return convertedToon;
        }

        public static AIToon Convert(clsToonX toon)
        {
            AIToon convertedToon = new AIToon();

            convertedToon.Name = toon.Name;
            convertedToon.Archetype = toon.Archetype;
            foreach (IPowerset powerset in toon.Powersets)
            {
                if (null == powerset)
                    continue;
                if (Enums.ePowerSetType.Primary == powerset.SetType)
                    convertedToon.Primary = powerset;
                else if (Enums.ePowerSetType.Secondary == powerset.SetType)
                    convertedToon.Secondary = powerset;
                else if (Enums.ePowerSetType.Inherent == powerset.SetType)
                    convertedToon.Fitness = powerset;
                else if (Enums.ePowerSetType.Pool == powerset.SetType)
                {
                    if (null == convertedToon.Pool1)
                        convertedToon.Pool1 = powerset;
                    else if (null == convertedToon.Pool2)
                        convertedToon.Pool2 = powerset;
                    else if (null == convertedToon.Pool3)
                        convertedToon.Pool3 = powerset;
                    else if (null == convertedToon.Pool4)
                        convertedToon.Pool4 = powerset;
                }
                else if (Enums.ePowerSetType.Ancillary == powerset.SetType)
                    convertedToon.Epic = powerset;
            }
            if (null == convertedToon.Fitness)
                convertedToon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);

            convertedToon.Level1Primary = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[0].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level1Secondary = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[1].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level2 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[2].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level4 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[3].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level6 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[4].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level8 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[5].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level10 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[6].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level12 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[7].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level14 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[8].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level16 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[9].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level18 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[10].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level20 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[11].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level22 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[12].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level24 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[13].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level26 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[14].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level28 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[15].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level30 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[16].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level32 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[17].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level35 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[18].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level38 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[19].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level41 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[20].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level44 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[21].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level47 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[22].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Level49 = toon.GetPowerEntryByPowerName(toon.CurrentBuild.Powers[23].Power.DisplayName).Clone() as PowerEntry;
            convertedToon.Brawl = toon.GetPowerEntryByPowerName("Brawl").Clone() as PowerEntry;
            convertedToon.Health = toon.GetPowerEntryByPowerName("Health").Clone() as PowerEntry;
            convertedToon.Stamina = toon.GetPowerEntryByPowerName("Stamina").Clone() as PowerEntry;

            if (toon.Archetype.DisplayName == "Warshade")
            {
                convertedToon.Level1Kheldian = toon.GetPowerEntryByPowerName("Shadow Step").Clone() as PowerEntry;
                convertedToon.Level10Kheldian = toon.GetPowerEntryByPowerName("Shadow Recall").Clone() as PowerEntry;
                if (null != toon.GetPowerEntryByPowerName("Dark Nova"))
                {
                    convertedToon.Nova1 = toon.GetPowerEntryByPowerName("Dark Nova Blast").Clone() as PowerEntry;
                    convertedToon.Nova2 = toon.GetPowerEntryByPowerName("Dark Nova Bolt").Clone() as PowerEntry;
                    convertedToon.Nova3 = toon.GetPowerEntryByPowerName("Dark Nova Emanation").Clone() as PowerEntry;
                    convertedToon.Nova4 = toon.GetPowerEntryByPowerName("Dark Nova Detonation").Clone() as PowerEntry;

                }
                if (null != toon.GetPowerEntryByPowerName("Black Dwarf"))
                {
                    convertedToon.Dwarf1 = toon.GetPowerEntryByPowerName("Black Dwarf Antagonize").Clone() as PowerEntry;
                    convertedToon.Dwarf2 = toon.GetPowerEntryByPowerName("Black Dwarf Drain").Clone() as PowerEntry;
                    convertedToon.Dwarf3 = toon.GetPowerEntryByPowerName("Black Dwarf Mire").Clone() as PowerEntry;
                    convertedToon.Dwarf4 = toon.GetPowerEntryByPowerName("Black Dwarf Smite").Clone() as PowerEntry;
                    convertedToon.Dwarf5 = toon.GetPowerEntryByPowerName("Black Dwarf Step").Clone() as PowerEntry;
                    convertedToon.Dwarf6 = toon.GetPowerEntryByPowerName("Black Dwarf Strike").Clone() as PowerEntry;

                }
            }
            else if (toon.Archetype.DisplayName == "Peacebringer")
            {
                convertedToon.Level1Kheldian = toon.GetPowerEntryByPowerName("Energy Flight").Clone() as PowerEntry;
                convertedToon.Level10Kheldian = toon.GetPowerEntryByPowerName("Combat Flight").Clone() as PowerEntry;
                if (null != toon.GetPowerEntryByPowerName("Bright Nova"))
                {
                    convertedToon.Nova1 = toon.GetPowerEntryByPowerName("Bright Nova Blast").Clone() as PowerEntry;
                    convertedToon.Nova2 = toon.GetPowerEntryByPowerName("Bright Nova Bolt").Clone() as PowerEntry;
                    convertedToon.Nova3 = toon.GetPowerEntryByPowerName("Bright Nova Scatter").Clone() as PowerEntry;
                    convertedToon.Nova4 = toon.GetPowerEntryByPowerName("Bright Nova Detonation").Clone() as PowerEntry;

                }
                if (null != toon.GetPowerEntryByPowerName("White Dwarf"))
                {
                    convertedToon.Dwarf1 = toon.GetPowerEntryByPowerName("White Dwarf Antagonize").Clone() as PowerEntry;
                    convertedToon.Dwarf2 = toon.GetPowerEntryByPowerName("White Dwarf Sublimation").Clone() as PowerEntry;
                    convertedToon.Dwarf3 = toon.GetPowerEntryByPowerName("White Dwarf Flare").Clone() as PowerEntry;
                    convertedToon.Dwarf4 = toon.GetPowerEntryByPowerName("White Dwarf Smite").Clone() as PowerEntry;
                    convertedToon.Dwarf5 = toon.GetPowerEntryByPowerName("White Dwarf Step").Clone() as PowerEntry;
                    convertedToon.Dwarf6 = toon.GetPowerEntryByPowerName("White Dwarf Strike").Clone() as PowerEntry;
                }
            }

            convertedToon.SetDefaultSlotLevels();
            return convertedToon;
        }

        public static AIToon CreateBuildBlasterFireMental()
        {
            AIToon toon = new AIToon();
            toon.Archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            toon.Name = "Fire Mental Blaster";

            toon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            toon.Primary = DatabaseAPI.GetPowersetByName("Fire Blast", Enums.ePowerSetType.Primary);
            toon.Secondary = DatabaseAPI.GetPowersetByName("Mental Manipulation", Enums.ePowerSetType.Secondary);
            toon.Pool1 = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            toon.Pool2 = DatabaseAPI.GetPowersetByName("Fighting", Enums.ePowerSetType.Pool);
            toon.Pool3 = DatabaseAPI.GetPowersetByName("Concealment", Enums.ePowerSetType.Pool);
            toon.Pool4 = DatabaseAPI.GetPowersetByName("Force of Will", Enums.ePowerSetType.Pool);
            toon.Epic = DatabaseAPI.GetPowersetByName("Force Mastery", Enums.ePowerSetType.Ancillary);

            toon.Health = new PowerEntry(0, toon.Fitness.Powers[1]);                    // Health
            toon.Stamina = new PowerEntry(0, toon.Fitness.Powers[3]);                   // Stamina
            toon.Level1Primary = new PowerEntry(0, toon.Primary.Powers[0]);             // Flares
            toon.Level1Secondary = new PowerEntry(0, toon.Secondary.Powers[0]);         // Subdual
            toon.Level2 = new PowerEntry(1, toon.Primary.Powers[2]);                    // Fire Ball
            toon.Level4 = new PowerEntry(3, toon.Pool1.Powers[1]);                      // Hasten
            toon.Level6 = new PowerEntry(5, toon.Primary.Powers[3]);                    // Rain of Fire
            toon.Level8 = new PowerEntry(7, toon.Pool2.Powers[1]);                      // Kick
            toon.Level10 = new PowerEntry(9, toon.Pool3.Powers[1]);                     // Grant Invisibility
            toon.Level12 = new PowerEntry(11, toon.Primary.Powers[5]);                  // Aim
            toon.Level14 = new PowerEntry(13, toon.Secondary.Powers[3]);                // Psychic Scream
            toon.Level16 = new PowerEntry(15, toon.Secondary.Powers[4]);                // Concentration
            toon.Level18 = new PowerEntry(17, toon.Primary.Powers[6]);                  // Blaze
            toon.Level20 = new PowerEntry(19, toon.Pool3.Powers[2]);                    // Invisibility
            toon.Level22 = new PowerEntry(21, toon.Pool2.Powers[2]);                    // Tough
            toon.Level24 = new PowerEntry(23, toon.Pool2.Powers[3]);                    // Weave
            toon.Level26 = new PowerEntry(25, toon.Primary.Powers[7]);                  // Blazing Bolt
            toon.Level28 = new PowerEntry(27, toon.Pool4.Powers[1]);                    // Weaken Resolve
            toon.Level30 = new PowerEntry(29, toon.Pool1.Powers[2]);                    // Super Speed
            toon.Level32 = new PowerEntry(31, toon.Primary.Powers[8]);                  // Inferno
            toon.Level35 = new PowerEntry(34, toon.Epic.Powers[0]);                     // Personal Force Field
            toon.Level38 = new PowerEntry(37, toon.Secondary.Powers[8]);                // Psychic Shockwave
            toon.Level41 = new PowerEntry(40, toon.Epic.Powers[3]);                     // Temp Invulnerability
            toon.Level44 = new PowerEntry(43, toon.Epic.Powers[4]);                     // Force of Nature
            toon.Level47 = new PowerEntry(46, toon.Epic.Powers[1]);                     // Repulsion Field
            toon.Level49 = new PowerEntry(48, toon.Pool1.Powers[3]);                    // Burnout

            toon.Brawl.DoDefaultSlotting(1);
            toon.Health.DoDefaultSlotting(2);
            toon.Stamina.DoDefaultSlotting(4);
            toon.Level1Primary.DoDefaultSlotting(5);
            toon.Level1Secondary.DoDefaultSlotting(1);
            toon.Level2.DoDefaultSlotting(6);
            toon.Level4.DoDefaultSlotting(2);
            toon.Level6.DoDefaultSlotting(6);
            toon.Level8.DoDefaultSlotting(1);
            toon.Level10.DoDefaultSlotting(1);
            toon.Level12.DoDefaultSlotting(2);
            toon.Level14.DoDefaultSlotting(5);
            toon.Level16.DoDefaultSlotting(2);
            toon.Level18.DoDefaultSlotting(5);
            toon.Level20.DoDefaultSlotting(4);
            toon.Level22.DoDefaultSlotting(6);
            toon.Level24.DoDefaultSlotting(5);
            toon.Level26.DoDefaultSlotting(5);
            toon.Level28.DoDefaultSlotting(3);
            toon.Level30.DoDefaultSlotting(1);
            toon.Level32.DoDefaultSlotting(5);
            toon.Level35.DoDefaultSlotting(1);
            toon.Level38.DoDefaultSlotting(5);
            toon.Level41.DoDefaultSlotting(6);
            toon.Level44.DoDefaultSlotting(5);
            toon.Level47.DoDefaultSlotting(3);
            toon.Level49.DoDefaultSlotting(2);
            toon.SetDefaultSlotLevels();

            return toon;
        }

        public static AIToon CreateBuildControllerMindForceField()
        {
            AIToon toon = new AIToon();
            toon.Archetype = DatabaseAPI.GetArchetypeByName("Controller");
            toon.Name = "Mind Force Field Controller";

            toon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            toon.Primary = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            toon.Secondary = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            toon.Pool1 = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            toon.Pool2 = DatabaseAPI.GetPowersetByName("Concealment", Enums.ePowerSetType.Pool);
            toon.Pool3 = DatabaseAPI.GetPowersetByName("Fighting", Enums.ePowerSetType.Pool);
            toon.Pool4 = DatabaseAPI.GetPowersetByName("Force of Will", Enums.ePowerSetType.Pool);
            toon.Epic = DatabaseAPI.GetPowersetByName("Primal Forces Mastery", Enums.ePowerSetType.Ancillary);

            toon.Health = new PowerEntry(0, toon.Fitness.Powers[1]);                    // Health
            toon.Stamina = new PowerEntry(0, toon.Fitness.Powers[3]);                   // Stamina
            toon.Level1Primary = new PowerEntry(0, toon.Primary.Powers[0]);             // Mesmerize
            toon.Level1Secondary = new PowerEntry(0, toon.Secondary.Powers[0]);         // Personal Force Field
            toon.Level2 = new PowerEntry(1, toon.Secondary.Powers[1]);                  // Deflection Shield
            toon.Level4 = new PowerEntry(3, toon.Primary.Powers[1]);                    // Levitate
            toon.Level6 = new PowerEntry(5, toon.Primary.Powers[2]);                    // Dominate
            toon.Level8 = new PowerEntry(7, toon.Primary.Powers[3]);                    // Confuse
            toon.Level10 = new PowerEntry(9, toon.Pool2.Powers[0]);                     // Stealth
            toon.Level12 = new PowerEntry(11, toon.Pool2.Powers[1]);                    // Grant Invisibility
            toon.Level14 = new PowerEntry(13, toon.Secondary.Powers[3]);                // Insulation Shield
            toon.Level16 = new PowerEntry(15, toon.Pool2.Powers[2]);                    // Invisibility
            toon.Level18 = new PowerEntry(17, toon.Primary.Powers[4]);                  // Mass Hypnosis
            toon.Level20 = new PowerEntry(19, toon.Primary.Powers[5]);                  // Telekinesis
            toon.Level22 = new PowerEntry(21, toon.Secondary.Powers[5]);                // Dispersion Bubble
            toon.Level24 = new PowerEntry(23, toon.Primary.Powers[6]);                  // Total Domination
            toon.Level26 = new PowerEntry(25, toon.Primary.Powers[7]);                  // Terrify
            toon.Level28 = new PowerEntry(27, toon.Pool3.Powers[0]);                    // Boxing
            toon.Level30 = new PowerEntry(29, toon.Pool3.Powers[2]);                    // Tough
            toon.Level32 = new PowerEntry(31, toon.Pool3.Powers[3]);                    // Weave
            toon.Level35 = new PowerEntry(34, toon.Primary.Powers[8]);                  // Mass Confusion
            toon.Level38 = new PowerEntry(37, toon.Secondary.Powers[8]);                // Force Bubble
            toon.Level41 = new PowerEntry(40, toon.Secondary.Powers[7]);                // Repulsion Bomb
            toon.Level44 = new PowerEntry(43, toon.Secondary.Powers[2]);                // Force Bolt
            toon.Level47 = new PowerEntry(46, toon.Secondary.Powers[6]);                // Repulsion Field
            toon.Level49 = new PowerEntry(48, toon.Secondary.Powers[4]);                // Detention Field

            toon.Brawl.DoDefaultSlotting(1);
            toon.Health.DoDefaultSlotting(3);
            toon.Stamina.DoDefaultSlotting(3);
            toon.Level1Primary.DoDefaultSlotting(6);
            toon.Level1Secondary.DoDefaultSlotting(6);
            toon.Level2.DoDefaultSlotting(6);
            toon.Level4.DoDefaultSlotting(6);
            toon.Level6.DoDefaultSlotting(6);
            toon.Level8.DoDefaultSlotting(6);
            toon.Level10.DoDefaultSlotting(6);
            toon.Level12.DoDefaultSlotting(6);
            toon.Level14.DoDefaultSlotting(6);
            toon.Level16.DoDefaultSlotting(6);
            toon.Level18.DoDefaultSlotting(6);
            toon.Level20.DoDefaultSlotting(5);
            toon.Level22.DoDefaultSlotting(5);
            toon.Level24.DoDefaultSlotting(1);
            toon.Level26.DoDefaultSlotting(1);
            toon.Level28.DoDefaultSlotting(1);
            toon.Level30.DoDefaultSlotting(1);
            toon.Level32.DoDefaultSlotting(1);
            toon.Level35.DoDefaultSlotting(1);
            toon.Level38.DoDefaultSlotting(1);
            toon.Level41.DoDefaultSlotting(1);
            toon.Level44.DoDefaultSlotting(1);
            toon.Level47.DoDefaultSlotting(1);
            toon.Level49.DoDefaultSlotting(1);
            toon.SetDefaultSlotLevels();

            return toon;
        }

        public static AIToon CreateBuildControllerMindForceFieldWithIOs()
        {
            AIToon toon = CreateBuildControllerMindForceField();
            toon.Name = "Mind Force Field Controller With IOs";
            toon.Health.Slot(new int[] { 36 });
            toon.Stamina.Slot(new int[] { 32 });
            toon.Level1Primary.Slot(new int[] { 26, 29, 29 });
            toon.Level1Secondary.Slot(new int[] { 260, 261, 262, 263, 264, 265 });
            toon.Level2.Slot(new int[] { 260, 261 });
            toon.Level4.Slot(new int[] { 146, 147, 124, 125 });
            toon.Level6.Slot(new int[] { 354, 355, 29 });
            toon.Level8.Slot(new int[] { 26, 28, 28 });
            toon.Level10.Slot(new int[] { 838, 839, 840, 841, 842, 843 });
            toon.Level12.Slot(new int[] { 30 });
            toon.Level14.Slot(new int[] { 260, 261, 249, 250, 33 });
            toon.Level16.Slot(new int[] { 272, 273, 274, 275, 276, 277 });
            toon.Level18.Slot(new int[] { 446, 447, 448, 449 });
            toon.Level20.Slot(new int[] { 33, 33, 33 });
            toon.Level22.Slot(new int[] { 260, 261, 249, 250 });
            toon.Level24.Slot(new int[] { 26, 37, 37, 37 });
            toon.Level26.Slot(new int[] { 179, 180 });
            toon.Level28.Slot(new int[] { 586, 587 });
            toon.Level30.Slot(new int[] { 1012, 1013, 1014, 1015 });
            toon.Level32.Slot(new int[] { 272, 273, 274, 275, 276, 277 });
            toon.Level35.Slot(new int[] { 604, 605, 498, 499 });
            toon.Level38.Slot(new int[] { 43, 43, 43 });
            toon.Level41.Slot(new int[] { 588, 589, 562, 563, 383, 384 });
            toon.Level44.Slot(new int[] { 1192, 1193, 1194, 1195, 809, 810 });
            toon.Level47.Slot(new int[] { 43 });
            toon.Level49.Slot(new int[] { 26, 33, 33 });
            toon.SetDefaultSlotLevels();
            return toon;
        }

        public static AIToon CreateBuildMastermindRoboticsForceField()
        {
            AIToon toon = new AIToon();
            toon.Archetype = DatabaseAPI.GetArchetypeByName("Mastermind");
            toon.Name = "Robotics Force Field Mastermind";
            toon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            toon.Primary = DatabaseAPI.GetPowersetByName("Robotics", Enums.ePowerSetType.Primary);
            toon.Secondary = DatabaseAPI.GetPowersetByArchetypeAndName(toon.Archetype, "Force Field");
            toon.Pool1 = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            toon.Pool2 = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            toon.Pool3 = DatabaseAPI.GetPowersetByName("Concealment", Enums.ePowerSetType.Pool);
            toon.Pool4 = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            toon.Epic = DatabaseAPI.GetPowersetByFullName("Epic.Mastermind_Mace_Mastery");

            toon.Brawl.DoDefaultSlotting(1);
            toon.Health = new PowerEntry(0, toon.Fitness.Powers[1]);                    // Health
            toon.Stamina = new PowerEntry(0, toon.Fitness.Powers[3]);                   // Stamina
            toon.Level1Primary = new PowerEntry(0, toon.Primary.Powers[4]);             // Battle Drones
            toon.Level1Secondary = new PowerEntry(0, toon.Secondary.Powers[0]);         // Force Bolt
            toon.Level2 = new PowerEntry(1, toon.Secondary.Powers[1]);                  // Deflection Shield
            toon.Level4 = new PowerEntry(3, toon.Secondary.Powers[2]);                  // Insulation Shield
            toon.Level6 = new PowerEntry(5, toon.Primary.Powers[6]);                    // Equip Robot
            toon.Level8 = new PowerEntry(7, toon.Pool1.Powers[2]);                      // Hover
            toon.Level10 = new PowerEntry(9, toon.Pool2.Powers[0]);                     // Recall Friend
            toon.Level12 = new PowerEntry(11, toon.Primary.Powers[8]);                  // Protector Bots
            toon.Level14 = new PowerEntry(11, toon.Pool3.Powers[1]);                    // Grant Invisibility
            toon.Level16 = new PowerEntry(13, toon.Secondary.Powers[4]);                // Personal Force Field
            toon.Level18 = new PowerEntry(15, toon.Primary.Powers[9]);                  // Repair
            toon.Level20 = new PowerEntry(17, toon.Secondary.Powers[5]);                // Dispersion Bubble
            toon.Level22 = new PowerEntry(21, toon.Pool3.Powers[2]);                    // Invisibility
            toon.Level24 = new PowerEntry(23, toon.Pool4.Powers[1]);                    // Assault
            toon.Level26 = new PowerEntry(25, toon.Primary.Powers[10]);                 // Assault Bot
            toon.Level28 = new PowerEntry(27, toon.Pool4.Powers[0]);                    // Maneuvers
            toon.Level30 = new PowerEntry(29, toon.Pool4.Powers[2]);                    // Tactics
            toon.Level32 = new PowerEntry(31, toon.Primary.Powers[11]);                 // Upgrade Robot
            toon.Level35 = new PowerEntry(34, toon.Epic.Powers[1]);                     // Scorpion Shield
            toon.Level38 = new PowerEntry(37, toon.Secondary.Powers[6]);                // Repulsion Field
            toon.Level41 = new PowerEntry(40, toon.Epic.Powers[3]);                     // Power Boost
            toon.Level44 = new PowerEntry(43, toon.Pool4.Powers[4]);                    // Victory Rush
            toon.Level47 = new PowerEntry(46, toon.Primary.Powers[7]);                  // Photon Grenade
            toon.Level49 = new PowerEntry(48, toon.Secondary.Powers[7]);                // Repulsion Bomb

            toon.Health.DoDefaultSlotting(3);
            toon.Stamina.DoDefaultSlotting(3);
            toon.Level1Primary.DoDefaultSlotting(6);
            toon.Level1Secondary.DoDefaultSlotting(6);
            toon.Level2.DoDefaultSlotting(6);
            toon.Level4.DoDefaultSlotting(6);
            toon.Level6.DoDefaultSlotting(6);
            toon.Level8.DoDefaultSlotting(6);
            toon.Level10.DoDefaultSlotting(6);
            toon.Level12.DoDefaultSlotting(6);
            toon.Level14.DoDefaultSlotting(6);
            toon.Level16.DoDefaultSlotting(6);
            toon.Level18.DoDefaultSlotting(6);
            toon.Level20.DoDefaultSlotting(5);
            toon.Level22.DoDefaultSlotting(5);
            toon.Level24.DoDefaultSlotting(1);
            toon.Level26.DoDefaultSlotting(1);
            toon.Level28.DoDefaultSlotting(1);
            toon.Level30.DoDefaultSlotting(1);
            toon.Level32.DoDefaultSlotting(1);
            toon.Level35.DoDefaultSlotting(1);
            toon.Level38.DoDefaultSlotting(1);
            toon.Level41.DoDefaultSlotting(1);
            toon.Level44.DoDefaultSlotting(1);
            toon.Level47.DoDefaultSlotting(1);
            toon.Level49.DoDefaultSlotting(1);
            toon.SetDefaultSlotLevels();

            return toon;
        }

        public static AIToon CreateBuildMastermindDemonsPain()
        {
            AIToon toon = new AIToon();
            toon.Archetype = DatabaseAPI.GetArchetypeByName("Mastermind");
            toon.Name = "Demons Pain Mastermind";
            toon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            toon.Primary = DatabaseAPI.GetPowersetByName("Demon Summoning", Enums.ePowerSetType.Primary);
            toon.Secondary = DatabaseAPI.GetPowersetByArchetypeAndName(toon.Archetype, "Pain Domination");
            toon.Pool1 = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            toon.Pool2 = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            toon.Pool3 = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            toon.Pool4 = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            toon.Epic = DatabaseAPI.GetPowersetByFullName("Epic.Mastermind_Soul_Mastery");

            toon.Brawl.DoDefaultSlotting(1);
            toon.Health = new PowerEntry(0, toon.Fitness.Powers[1]);                    // Health
            toon.Stamina = new PowerEntry(0, toon.Fitness.Powers[3]);                   // Stamina
            toon.Level1Primary = new PowerEntry(0, toon.Primary.Powers[7]);             // Summon Demonlings
            toon.Level1Secondary = new PowerEntry(0, toon.Secondary.Powers[0]);         // Nullify Pain
            toon.Level2 = new PowerEntry(1, toon.Secondary.Powers[1]);                  // Soothe
            toon.Level4 = new PowerEntry(3, toon.Pool1.Powers[2]);                      // Hover
            toon.Level6 = new PowerEntry(5, toon.Primary.Powers[9]);                    // Enchant Demon
            toon.Level8 = new PowerEntry(7, toon.Pool2.Powers[0]);                      // Recall Friend
            toon.Level10 = new PowerEntry(9, toon.Secondary.Powers[3]);                 // Conduit of Pain
            toon.Level12 = new PowerEntry(11, toon.Primary.Powers[11]);                 // Summon Demons
            toon.Level14 = new PowerEntry(11, toon.Pool3.Powers[1]);                    // Hasten
            toon.Level16 = new PowerEntry(13, toon.Pool3.Powers[2]);                    // Super Speed
            toon.Level18 = new PowerEntry(15, toon.Primary.Powers[12]);                 // Hell on Earth
            toon.Level20 = new PowerEntry(17, toon.Secondary.Powers[5]);                // Supress Pain
            toon.Level22 = new PowerEntry(21, toon.Pool3.Powers[3]);                    // Burnout
            toon.Level24 = new PowerEntry(23, toon.Pool4.Powers[1]);                    // Assault
            toon.Level26 = new PowerEntry(25, toon.Primary.Powers[13]);                 // Summon Demon Prince
            toon.Level28 = new PowerEntry(27, toon.Pool4.Powers[2]);                    // Tactics
            toon.Level30 = new PowerEntry(29, toon.Pool4.Powers[0]);                    // Maneuvers
            toon.Level32 = new PowerEntry(31, toon.Primary.Powers[14]);                 // Abysmal Empowerment
            toon.Level35 = new PowerEntry(34, toon.Epic.Powers[1]);                     // Dark Embrace
            toon.Level38 = new PowerEntry(37, toon.Pool3.Powers[4]);                    // Whirlwind
            toon.Level41 = new PowerEntry(40, toon.Epic.Powers[2]);                     // Opressive Gloom
            toon.Level44 = new PowerEntry(43, toon.Secondary.Powers[6]);                // World of Pain
            toon.Level47 = new PowerEntry(46, toon.Secondary.Powers[8]);                // Painbringer
            toon.Level49 = new PowerEntry(48, toon.Secondary.Powers[7]);                // Anguishing Cry

            toon.Health.DoDefaultSlotting(3);
            toon.Stamina.DoDefaultSlotting(3);
            toon.Level1Primary.DoDefaultSlotting(6);
            toon.Level1Secondary.DoDefaultSlotting(6);
            toon.Level2.DoDefaultSlotting(6);
            toon.Level4.DoDefaultSlotting(6);
            toon.Level6.DoDefaultSlotting(6);
            toon.Level8.DoDefaultSlotting(6);
            toon.Level10.DoDefaultSlotting(6);
            toon.Level12.DoDefaultSlotting(6);
            toon.Level14.DoDefaultSlotting(6);
            toon.Level16.DoDefaultSlotting(6);
            toon.Level18.DoDefaultSlotting(6);
            toon.Level20.DoDefaultSlotting(5);
            toon.Level22.DoDefaultSlotting(5);
            toon.Level24.DoDefaultSlotting(1);
            toon.Level26.DoDefaultSlotting(1);
            toon.Level28.DoDefaultSlotting(1);
            toon.Level30.DoDefaultSlotting(1);
            toon.Level32.DoDefaultSlotting(1);
            toon.Level35.DoDefaultSlotting(1);
            toon.Level38.DoDefaultSlotting(1);
            toon.Level41.DoDefaultSlotting(1);
            toon.Level44.DoDefaultSlotting(1);
            toon.Level47.DoDefaultSlotting(1);
            toon.Level49.DoDefaultSlotting(1);
            toon.SetDefaultSlotLevels();

            return toon;
        }

        public static AIToon CreateBuildWarshade()
        {
            AIToon toon = new AIToon();
            toon.Name = "Warshade";
            toon.Archetype = DatabaseAPI.GetArchetypeByName("Warshade");
            toon.Fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            toon.Primary = DatabaseAPI.GetPowersetByName("Umbral Blast", Enums.ePowerSetType.Primary);
            toon.Secondary = DatabaseAPI.GetPowersetByArchetypeAndName(toon.Archetype, "Umbral Aura");
            toon.Pool1 = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);

            toon.Brawl.DoDefaultSlotting(1);
            toon.Health = new PowerEntry(0, toon.Fitness.Powers[1]);                    // Health
            toon.Stamina = new PowerEntry(0, toon.Fitness.Powers[3]);                   // Stamina
            toon.Level1Primary = new PowerEntry(0, toon.Primary.Powers[0]);             // Shadow Bolt
            toon.Level1Secondary = new PowerEntry(0, toon.Secondary.Powers[0]);         // Absorption
            toon.Level2 = new PowerEntry(1, toon.Secondary.Powers[1]);                  // Gravity Shield
            toon.Level4 = new PowerEntry(3, toon.Pool1.Powers[1]);                      // Hasten
            toon.Level6 = new PowerEntry(5, toon.Primary.Powers[3]);                    // Dark Nova
            toon.Level8 = new PowerEntry(7, toon.Primary.Powers[5]);                    // Starless Step
            toon.Level10 = new PowerEntry(9, toon.Secondary.Powers[3]);                 // Penumbral Shield
            toon.Level12 = new PowerEntry(11, toon.Primary.Powers[4]);                  // Shadow Blast
            toon.Level14 = new PowerEntry(11, toon.Primary.Powers[6]);                  // Sunless Mire
            toon.Level16 = new PowerEntry(13, toon.Secondary.Powers[5]);                // Twilight Shield
            toon.Level18 = new PowerEntry(15, toon.Primary.Powers[9]);                  // Essence Drain
            toon.Level20 = new PowerEntry(17, toon.Secondary.Powers[6]);                // Black Dwarf
            toon.Level22 = new PowerEntry(21, toon.Primary.Powers[8]);                  // Gravity Well
            toon.Level24 = new PowerEntry(23, toon.Secondary.Powers[7]);                // Stygian Circle
            toon.Level26 = new PowerEntry(25, toon.Primary.Powers[10]);                 // Gravitic Emanation
            toon.Level28 = new PowerEntry(27, toon.Secondary.Powers[10]);               // Inky Aspect
            toon.Level30 = new PowerEntry(29, toon.Primary.Powers[7]);                  // Dark Detonation
            toon.Level32 = new PowerEntry(31, toon.Primary.Powers[12]);                 // Dark Extraction
            toon.Level35 = new PowerEntry(34, toon.Primary.Powers[13]);                 // Quasar
            toon.Level38 = new PowerEntry(37, toon.Secondary.Powers[12]);               // Eclipse
            toon.Level41 = new PowerEntry(40, toon.Secondary.Powers[4]);                // Shadow Cloak
            toon.Level44 = new PowerEntry(43, toon.Secondary.Powers[11]);               // Stygian Return
            toon.Level47 = new PowerEntry(46, toon.Primary.Powers[2]);                  // Gravimetric Snare
            toon.Level49 = new PowerEntry(48, toon.Secondary.Powers[8]);                // Nebulous Form

            toon.Level1Kheldian = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Shadow Step"));
            toon.Level10Kheldian = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Shadow Recall"));
            toon.Nova1 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Dark Nova Blast"));
            toon.Nova2 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Dark Nova Bolt"));
            toon.Nova3 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Dark Nova Emanation"));
            toon.Nova4 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Dark Nova Detonation"));
            toon.Dwarf1 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Antagonize"));
            toon.Dwarf2 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Drain"));
            toon.Dwarf3 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Mire"));
            toon.Dwarf4 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Smite"));
            toon.Dwarf5 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Step"));
            toon.Dwarf6 = new PowerEntry(0, Base.Data_Classes.Power.GetPowerByName("Black Dwarf Strike"));

            toon.Health.DoDefaultSlotting(3);
            toon.Stamina.DoDefaultSlotting(3);
            toon.Level1Primary.DoDefaultSlotting(6);
            toon.Level1Secondary.DoDefaultSlotting(3);
            toon.Level2.DoDefaultSlotting(6);
            toon.Level4.DoDefaultSlotting(3);
            toon.Level6.DoDefaultSlotting(3);
            toon.Level8.DoDefaultSlotting(6);
            toon.Level10.DoDefaultSlotting(6);
            toon.Level12.DoDefaultSlotting(6);
            toon.Level14.DoDefaultSlotting(6);
            toon.Level16.DoDefaultSlotting(6);
            toon.Level18.DoDefaultSlotting(6);
            toon.Level20.DoDefaultSlotting(3);
            toon.Level22.DoDefaultSlotting(5);
            toon.Level24.DoDefaultSlotting(1);
            toon.Level26.DoDefaultSlotting(1);
            toon.Level28.DoDefaultSlotting(1);
            toon.Level30.DoDefaultSlotting(1);
            toon.Level32.DoDefaultSlotting(1);
            toon.Level35.DoDefaultSlotting(1);
            toon.Level38.DoDefaultSlotting(1);
            toon.Level41.DoDefaultSlotting(1);
            toon.Level44.DoDefaultSlotting(1);
            toon.Level47.DoDefaultSlotting(1);
            toon.Level49.DoDefaultSlotting(1);

            toon.Nova1.DoDefaultSlotting(4);
            toon.Nova2.DoDefaultSlotting(4);
            toon.Nova3.DoDefaultSlotting(1);
            toon.Nova4.DoDefaultSlotting(1);
            toon.Dwarf1.DoDefaultSlotting(4);
            toon.Dwarf2.DoDefaultSlotting(3);
            toon.Dwarf3.DoDefaultSlotting(1);
            toon.Dwarf4.DoDefaultSlotting(1);
            toon.Dwarf5.DoDefaultSlotting(1);
            toon.Dwarf6.DoDefaultSlotting(1);
            toon.SetDefaultSlotLevels();

            return toon;
        }

        public static void TestNewCode()
        {
            // TestWriteAIToon();
            TestCreateBuildBlasterFireMental();
            TestCreateBuildControllerMindForceField();
            TestCreateBuildControllerMindForceFieldWithIOs();
            TestCreateBuildWarshade();
        }

        public static void TestWriteAIToon()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            bool result = WriteAIToon(toon, "C:\\Users\\DavidPC\\Documents\\Hero & Villain Builds\\TEST.mxd");
            Debug.Assert(result == true, "result is incorrect!");
        }

        public static void TestCreateBuildBlasterFireMental()
        {
            AIToon toon = CreateBuildBlasterFireMental();
            Debug.Assert(toon.Archetype.DisplayName == "Blaster", "toon.Archetype.DisplayName is incorrect!");

            Debug.Assert(toon.Fitness.DisplayName == "Inherent Fitness", "toon.Fitness.DisplayName is incorrect!");
            Debug.Assert(toon.Primary.DisplayName == "Fire Blast", "toon.Primary.DisplayName is incorrect!");
            Debug.Assert(toon.Secondary.DisplayName == "Mental Manipulation", "toon.Secondary.DisplayName is incorrect!");
            Debug.Assert(toon.Pool1.DisplayName == "Speed", "toon.Pool1.DisplayName is incorrect!");
            Debug.Assert(toon.Pool2.DisplayName == "Fighting", "toon.Pool2.DisplayName is incorrect!");
            Debug.Assert(toon.Pool3.DisplayName == "Concealment", "toon.Pool3.DisplayName is incorrect!");
            Debug.Assert(toon.Pool4.DisplayName == "Force of Will", "toon.Pool4.DisplayName is incorrect!");
            Debug.Assert(toon.Epic.DisplayName == "Force Mastery", "toon.Epic.DisplayName is incorrect!");

            Debug.Assert(toon.Health.Power.DisplayName == "Health", "toon.Health.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Stamina.Power.DisplayName == "Stamina", "toon.Stamina.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Primary.Power.DisplayName == "Flares", "toon.Level1Primary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Secondary.Power.DisplayName == "Subdual", "toon.Level1Secondary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level2.Power.DisplayName == "Fire Ball", "toon.Level2.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level4.Power.DisplayName == "Hasten", "toon.Level4.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level6.Power.DisplayName == "Rain of Fire", "toon.Level6.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level8.Power.DisplayName == "Kick", "toon.Level8.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level10.Power.DisplayName == "Grant Invisibility", "toon.Level10.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level12.Power.DisplayName == "Aim", "toon.Level12.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level14.Power.DisplayName == "Psychic Scream", "toon.Level14.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level16.Power.DisplayName == "Concentration", "toon.Level16.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level18.Power.DisplayName == "Blaze", "toon.Level18.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level20.Power.DisplayName == "Invisibility", "toon.Level20.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level22.Power.DisplayName == "Tough", "toon.Level22.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level24.Power.DisplayName == "Weave", "toon.Level24.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level26.Power.DisplayName == "Blazing Bolt", "toon.Level26.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level28.Power.DisplayName == "Weaken Resolve", "toon.Level28.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level30.Power.DisplayName == "Super Speed", "toon.Level30.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level32.Power.DisplayName == "Inferno", "toon.Level32.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level35.Power.DisplayName == "Personal Force Field", "toon.Level35.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level38.Power.DisplayName == "Psychic Shockwave", "toon.Level38.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level41.Power.DisplayName == "Temp Invulnerability", "toon.Level41.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level44.Power.DisplayName == "Force of Nature", "toon.Level44.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level47.Power.DisplayName == "Repulsion Field", "toon.Level47.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level49.Power.DisplayName == "Burnout", "toon.Level49.Power.DisplayName is incorrect!");

            Debug.Assert(toon.Health.Slots.Length == 2, "toon.Health.Slots.Length is incorrect!");
            Debug.Assert(toon.Stamina.Slots.Length == 4, "toon.Stamina.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots.Length == 5, "toon.Level1Primary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots.Length == 1, "toon.Level1Secondary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level2.Slots.Length == 6, "toon.Level2.Slots.Length is incorrect!");
            Debug.Assert(toon.Level4.Slots.Length == 2, "toon.Level4.Slots.Length is incorrect!");
            Debug.Assert(toon.Level6.Slots.Length == 6, "toon.Level6.Slots.Length is incorrect!");
            Debug.Assert(toon.Level8.Slots.Length == 1, "toon.Level8.Slots.Length is incorrect!");
            Debug.Assert(toon.Level10.Slots.Length == 1, "toon.Level10.Slots.Length is incorrect!");
            Debug.Assert(toon.Level12.Slots.Length == 2, "toon.Level12.Slots.Length is incorrect!");
            Debug.Assert(toon.Level14.Slots.Length == 5, "toon.Level14.Slots.Length is incorrect!");
            Debug.Assert(toon.Level16.Slots.Length == 2, "toon.Level16.Slots.Length is incorrect!");
            Debug.Assert(toon.Level18.Slots.Length == 5, "toon.Level18.Slots.Length is incorrect!");
            Debug.Assert(toon.Level20.Slots.Length == 4, "toon.Level20.Slots.Length is incorrect!");
            Debug.Assert(toon.Level22.Slots.Length == 6, "toon.Level22.Slots.Length is incorrect!");
            Debug.Assert(toon.Level24.Slots.Length == 5, "toon.Level24.Slots.Length is incorrect!");
            Debug.Assert(toon.Level26.Slots.Length == 5, "toon.Level26.Slots.Length is incorrect!");
            Debug.Assert(toon.Level28.Slots.Length == 3, "toon.Level28.Slots.Length is incorrect!");
            Debug.Assert(toon.Level30.Slots.Length == 1, "toon.Level30.Slots.Length is incorrect!");
            Debug.Assert(toon.Level32.Slots.Length == 5, "toon.Level32.Slots.Length is incorrect!");
            Debug.Assert(toon.Level35.Slots.Length == 1, "toon.Level35.Slots.Length is incorrect!");
            Debug.Assert(toon.Level38.Slots.Length == 5, "toon.Level38.Slots.Length is incorrect!");
            Debug.Assert(toon.Level41.Slots.Length == 6, "toon.Level41.Slots.Length is incorrect!");
            Debug.Assert(toon.Level44.Slots.Length == 5, "toon.Level44.Slots.Length is incorrect!");
            Debug.Assert(toon.Level47.Slots.Length == 3, "toon.Level47.Slots.Length is incorrect!");
            Debug.Assert(toon.Level49.Slots.Length == 2, "toon.Level49.Slots.Length is incorrect!");
            Debug.Assert(toon.Health.Slots[0].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Health.Slots[1].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level1Primary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level1Primary.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level1Secondary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level2.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level2.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level2.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level2.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level2.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level2.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level4.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[1].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level4.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level6.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level6.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level6.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level8.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level10.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level12.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level12.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level14.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level14.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level16.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level16.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level18.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level18.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level20.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level20.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level20.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level20.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level22.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level22.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level22.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level22.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level22.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level22.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level24.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level24.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level24.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level24.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level24.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level26.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level26.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level26.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level26.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level26.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level28.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level28.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level28.Slots[1].Enhancement.Enh == (int)EnhancementType.DefenseDebuff, "toon.Level28.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level28.Slots[2].Enhancement.Enh == (int)EnhancementType.DefenseDebuff, "toon.Level28.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level30.Slots[0].Enhancement.Enh == (int)EnhancementType.Run, "toon.Level30.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level32.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level32.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level32.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level32.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level32.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level35.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level35.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level38.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level38.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level38.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level38.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[4].Enhancement.Enh == (int)EnhancementType.Stun, "toon.Level38.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level41.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level41.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level41.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level41.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level41.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level41.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level44.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level44.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level44.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level44.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level44.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level47.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level47.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level47.Slots[1].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level47.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level47.Slots[2].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level47.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level49.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level49.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level49.Slots[1].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level49.Slots[1].Enhancement.Enh is incorrect!");
        }

        public static void TestCreateBuildControllerMindForceField()
        {
            AIToon toon = CreateBuildControllerMindForceField();
            Debug.Assert(toon.Archetype.DisplayName == "Controller", "toon.Archetype.DisplayName is incorrect!");

            Debug.Assert(toon.Fitness.DisplayName == "Inherent Fitness", "toon.Fitness.DisplayName is incorrect!");
            Debug.Assert(toon.Primary.DisplayName == "Mind Control", "toon.Primary.DisplayName is incorrect!");
            Debug.Assert(toon.Secondary.DisplayName == "Force Field", "toon.Secondary.DisplayName is incorrect!");
            Debug.Assert(toon.Pool1.DisplayName == "Speed", "toon.Pool1.DisplayName is incorrect!");
            Debug.Assert(toon.Pool2.DisplayName == "Concealment", "toon.Pool2.DisplayName is incorrect!");
            Debug.Assert(toon.Pool3.DisplayName == "Fighting", "toon.Pool3.DisplayName is incorrect!");
            Debug.Assert(toon.Pool4.DisplayName == "Force of Will", "toon.Pool4.DisplayName is incorrect!");
            Debug.Assert(toon.Epic.DisplayName == "Primal Forces Mastery", "toon.Epic.DisplayName is incorrect!");

            Debug.Assert(toon.Health.Power.DisplayName == "Health", "toon.Health.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Stamina.Power.DisplayName == "Stamina", "toon.Stamina.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Primary.Power.DisplayName == "Mesmerize", "toon.Level1Primary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Secondary.Power.DisplayName == "Personal Force Field", "toon.Level1Secondary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level2.Power.DisplayName == "Deflection Shield", "toon.Level2.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level4.Power.DisplayName == "Levitate", "toon.Level4.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level6.Power.DisplayName == "Dominate", "toon.Level6.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level8.Power.DisplayName == "Confuse", "toon.Level8.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level10.Power.DisplayName == "Stealth", "toon.Level10.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level12.Power.DisplayName == "Grant Invisibility", "toon.Level12.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level14.Power.DisplayName == "Insulation Shield", "toon.Level14.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level16.Power.DisplayName == "Invisibility", "toon.Level16.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level18.Power.DisplayName == "Mass Hypnosis", "toon.Level18.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level20.Power.DisplayName == "Telekinesis", "toon.Level20.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level22.Power.DisplayName == "Dispersion Bubble", "toon.Level22.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level24.Power.DisplayName == "Total Domination", "toon.Level24.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level26.Power.DisplayName == "Terrify", "toon.Level26.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level28.Power.DisplayName == "Boxing", "toon.Level28.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level30.Power.DisplayName == "Tough", "toon.Level30.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level32.Power.DisplayName == "Weave", "toon.Level32.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level35.Power.DisplayName == "Mass Confusion", "toon.Level35.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level38.Power.DisplayName == "Force Bubble", "toon.Level38.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level41.Power.DisplayName == "Repulsion Bomb", "toon.Level41.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level44.Power.DisplayName == "Force Bolt", "toon.Level44.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level47.Power.DisplayName == "Repulsion Field", "toon.Level47.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level49.Power.DisplayName == "Detention Field", "toon.Level49.Power.DisplayName is incorrect!");

            Debug.Assert(toon.Stamina.Slots.Length == 3, "toon.Stamina.Slots.Length is incorrect!");
            Debug.Assert(toon.Health.Slots.Length == 3, "toon.Health.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots.Length == 6, "toon.Level1Primary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots.Length == 6, "toon.Level1Secondary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level2.Slots.Length == 6, "toon.Level2.Slots.Length is incorrect!");
            Debug.Assert(toon.Level4.Slots.Length == 6, "toon.Level4.Slots.Length is incorrect!");
            Debug.Assert(toon.Level6.Slots.Length == 6, "toon.Level6.Slots.Length is incorrect!");
            Debug.Assert(toon.Level8.Slots.Length == 6, "toon.Level8.Slots.Length is incorrect!");
            Debug.Assert(toon.Level10.Slots.Length == 6, "toon.Level10.Slots.Length is incorrect!");
            Debug.Assert(toon.Level12.Slots.Length == 6, "toon.Level12.Slots.Length is incorrect!");
            Debug.Assert(toon.Level14.Slots.Length == 6, "toon.Level14.Slots.Length is incorrect!");
            Debug.Assert(toon.Level16.Slots.Length == 6, "toon.Level16.Slots.Length is incorrect!");
            Debug.Assert(toon.Level18.Slots.Length == 6, "toon.Level18.Slots.Length is incorrect!");
            Debug.Assert(toon.Level20.Slots.Length == 5, "toon.Level20.Slots.Length is incorrect!");
            Debug.Assert(toon.Level22.Slots.Length == 5, "toon.Level22.Slots.Length is incorrect!");
            Debug.Assert(toon.Level24.Slots.Length == 1, "toon.Level24.Slots.Length is incorrect!");
            Debug.Assert(toon.Level26.Slots.Length == 1, "toon.Level26.Slots.Length is incorrect!");
            Debug.Assert(toon.Level28.Slots.Length == 1, "toon.Level28.Slots.Length is incorrect!");
            Debug.Assert(toon.Level30.Slots.Length == 1, "toon.Level30.Slots.Length is incorrect!");
            Debug.Assert(toon.Level32.Slots.Length == 1, "toon.Level32.Slots.Length is incorrect!");
            Debug.Assert(toon.Level35.Slots.Length == 1, "toon.Level35.Slots.Length is incorrect!");
            Debug.Assert(toon.Level38.Slots.Length == 1, "toon.Level38.Slots.Length is incorrect!");
            Debug.Assert(toon.Level41.Slots.Length == 1, "toon.Level41.Slots.Length is incorrect!");
            Debug.Assert(toon.Level44.Slots.Length == 1, "toon.Level44.Slots.Length is incorrect!");
            Debug.Assert(toon.Level47.Slots.Length == 1, "toon.Level47.Slots.Length is incorrect!");
            Debug.Assert(toon.Level49.Slots.Length == 1, "toon.Level49.Slots.Length is incorrect!");
            Debug.Assert(toon.Health.Slots[0].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Health.Slots[1].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Health.Slots[2].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level1Primary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[4].Enhancement.Enh == (int)EnhancementType.Sleep, "toon.Level1Primary.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[5].Enhancement.Enh == (int)EnhancementType.Sleep, "toon.Level1Primary.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level1Secondary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level1Secondary.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level1Secondary.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level1Secondary.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level1Secondary.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level1Secondary.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level2.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level2.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level2.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level2.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level2.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level2.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level4.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level4.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level4.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level4.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level4.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level4.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level6.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level6.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[4].Enhancement.Enh == (int)EnhancementType.Hold, "toon.Level6.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[5].Enhancement.Enh == (int)EnhancementType.Hold, "toon.Level6.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level8.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[1].Enhancement.Enh == (int)EnhancementType.Confuse, "toon.Level8.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[2].Enhancement.Enh == (int)EnhancementType.Confuse, "toon.Level8.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[3].Enhancement.Enh == (int)EnhancementType.Confuse, "toon.Level8.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level8.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level8.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level10.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level10.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level10.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level12.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level12.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level12.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level12.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level12.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level12.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level14.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level14.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level14.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level14.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level14.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level14.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level16.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level16.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level16.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level18.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[1].Enhancement.Enh == (int)EnhancementType.Sleep, "toon.Level18.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[2].Enhancement.Enh == (int)EnhancementType.Sleep, "toon.Level18.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[3].Enhancement.Enh == (int)EnhancementType.Sleep, "toon.Level18.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level18.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level18.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level20.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level20.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level20.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level20.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level20.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level22.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level22.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level22.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level22.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level22.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level24.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level26.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level28.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level28.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level30.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level30.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level32.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level35.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level35.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level38.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level41.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level44.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level47.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level47.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level49.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level49.Slots[0].Enhancement.Enh is incorrect!");

            clsToonX t = AIToonUtils.Convert(toon);
            t.GenerateBuffedPowerArray();
            int x = 1;
        }

        public static void TestCreateBuildControllerMindForceFieldWithIOs()
        {
            AIToon toon = CreateBuildControllerMindForceFieldWithIOs();
            string message;
            Debug.Assert(toon.IsValid(out message), "toon.IsValid() is incorrect!");
        }

        public static void TestCreateBuildWarshade()
        {
            AIToon toon = CreateBuildWarshade();
            Debug.Assert(toon.Archetype.DisplayName == "Warshade", "toon.Archetype.DisplayName is incorrect!");

            Debug.Assert(toon.Fitness.DisplayName == "Inherent Fitness", "toon.Fitness.DisplayName is incorrect!");
            Debug.Assert(toon.Primary.DisplayName == "Umbral Blast", "toon.Primary.DisplayName is incorrect!");
            Debug.Assert(toon.Secondary.DisplayName == "Umbral Aura", "toon.Secondary.DisplayName is incorrect!");
            Debug.Assert(toon.Pool1.DisplayName == "Speed", "toon.Pool1.DisplayName is incorrect!");
            Debug.Assert(toon.Pool2 == null, "toon.Pool2 is incorrect!");
            Debug.Assert(toon.Pool3 == null, "toon.Pool3 is incorrect!");
            Debug.Assert(toon.Pool4 == null, "toon.Pool4 is incorrect!");
            Debug.Assert(toon.Epic == null, "toon.Epic is incorrect!");

            Debug.Assert(toon.Health.Power.DisplayName == "Health", "toon.Health.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Stamina.Power.DisplayName == "Stamina", "toon.Stamina.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Primary.Power.DisplayName == "Shadow Bolt", "toon.Level1Primary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Secondary.Power.DisplayName == "Absorption", "toon.Level1Secondary.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level2.Power.DisplayName == "Gravity Shield", "toon.Level2.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level4.Power.DisplayName == "Hasten", "toon.Level4.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level6.Power.DisplayName == "Dark Nova", "toon.Level6.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level8.Power.DisplayName == "Starless Step", "toon.Level8.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level10.Power.DisplayName == "Penumbral Shield", "toon.Level10.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level12.Power.DisplayName == "Shadow Blast", "toon.Level12.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level14.Power.DisplayName == "Sunless Mire", "toon.Level14.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level16.Power.DisplayName == "Twilight Shield", "toon.Level16.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level18.Power.DisplayName == "Essence Drain", "toon.Level18.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level20.Power.DisplayName == "Black Dwarf", "toon.Level20.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level22.Power.DisplayName == "Gravity Well", "toon.Level22.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level24.Power.DisplayName == "Stygian Circle", "toon.Level24.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level26.Power.DisplayName == "Gravitic Emanation", "toon.Level26.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level28.Power.DisplayName == "Inky Aspect", "toon.Level28.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level30.Power.DisplayName == "Dark Detonation", "toon.Level30.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level32.Power.DisplayName == "Dark Extraction", "toon.Level32.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level35.Power.DisplayName == "Quasar", "toon.Level35.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level38.Power.DisplayName == "Eclipse", "toon.Level38.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level41.Power.DisplayName == "Shadow Cloak", "toon.Level41.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level44.Power.DisplayName == "Stygian Return", "toon.Level44.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level47.Power.DisplayName == "Gravimetric Snare", "toon.Level47.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level49.Power.DisplayName == "Nebulous Form", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level1Kheldian.Power.DisplayName == "Shadow Step", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Level10Kheldian.Power.DisplayName == "Shadow Recall", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Nova1.Power.DisplayName == "Dark Nova Blast", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Nova2.Power.DisplayName == "Dark Nova Bolt", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Nova3.Power.DisplayName == "Dark Nova Emanation", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Nova4.Power.DisplayName == "Dark Nova Detonation", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf1.Power.DisplayName == "Black Dwarf Antagonize", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf2.Power.DisplayName == "Black Dwarf Drain", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf3.Power.DisplayName == "Black Dwarf Mire", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf4.Power.DisplayName == "Black Dwarf Smite", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf5.Power.DisplayName == "Black Dwarf Step", "toon.Level49.Power.DisplayName is incorrect!");
            Debug.Assert(toon.Dwarf6.Power.DisplayName == "Black Dwarf Strike", "toon.Level49.Power.DisplayName is incorrect!");

            Debug.Assert(toon.Stamina.Slots.Length == 3, "toon.Stamina.Slots.Length is incorrect!");
            Debug.Assert(toon.Health.Slots.Length == 3, "toon.Health.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots.Length == 6, "toon.Level1Primary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots.Length == 3, "toon.Level1Secondary.Slots.Length is incorrect!");
            Debug.Assert(toon.Level2.Slots.Length == 6, "toon.Level2.Slots.Length is incorrect!");
            Debug.Assert(toon.Level4.Slots.Length == 3, "toon.Level4.Slots.Length is incorrect!");
            Debug.Assert(toon.Level6.Slots.Length == 3, "toon.Level6.Slots.Length is incorrect!");
            Debug.Assert(toon.Level8.Slots.Length == 6, "toon.Level8.Slots.Length is incorrect!");
            Debug.Assert(toon.Level10.Slots.Length == 6, "toon.Level10.Slots.Length is incorrect!");
            Debug.Assert(toon.Level12.Slots.Length == 6, "toon.Level12.Slots.Length is incorrect!");
            Debug.Assert(toon.Level14.Slots.Length == 6, "toon.Level14.Slots.Length is incorrect!");
            Debug.Assert(toon.Level16.Slots.Length == 6, "toon.Level16.Slots.Length is incorrect!");
            Debug.Assert(toon.Level18.Slots.Length == 6, "toon.Level18.Slots.Length is incorrect!");
            Debug.Assert(toon.Level20.Slots.Length == 3, "toon.Level20.Slots.Length is incorrect!");
            Debug.Assert(toon.Level22.Slots.Length == 5, "toon.Level22.Slots.Length is incorrect!");
            Debug.Assert(toon.Level24.Slots.Length == 1, "toon.Level24.Slots.Length is incorrect!");
            Debug.Assert(toon.Level26.Slots.Length == 1, "toon.Level26.Slots.Length is incorrect!");
            Debug.Assert(toon.Level28.Slots.Length == 1, "toon.Level28.Slots.Length is incorrect!");
            Debug.Assert(toon.Level30.Slots.Length == 1, "toon.Level30.Slots.Length is incorrect!");
            Debug.Assert(toon.Level32.Slots.Length == 1, "toon.Level32.Slots.Length is incorrect!");
            Debug.Assert(toon.Level35.Slots.Length == 1, "toon.Level35.Slots.Length is incorrect!");
            Debug.Assert(toon.Level38.Slots.Length == 1, "toon.Level38.Slots.Length is incorrect!");
            Debug.Assert(toon.Level41.Slots.Length == 1, "toon.Level41.Slots.Length is incorrect!");
            Debug.Assert(toon.Level44.Slots.Length == 1, "toon.Level44.Slots.Length is incorrect!");
            Debug.Assert(toon.Level47.Slots.Length == 1, "toon.Level47.Slots.Length is incorrect!");
            Debug.Assert(toon.Level49.Slots.Length == 1, "toon.Level49.Slots.Length is incorrect!");
            Debug.Assert(toon.Nova1.Slots.Length == 4, "toon.Nova1.Slots.Length is incorrect!");
            Debug.Assert(toon.Nova2.Slots.Length == 4, "toon.Nova2.Slots.Length is incorrect!");
            Debug.Assert(toon.Nova3.Slots.Length == 1, "toon.Nova3.Slots.Length is incorrect!");
            Debug.Assert(toon.Nova4.Slots.Length == 1, "toon.Nova4.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf1.Slots.Length == 4, "toon.Dwarf1.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf2.Slots.Length == 3, "toon.Dwarf2.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf3.Slots.Length == 1, "toon.Dwarf3.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf4.Slots.Length == 1, "toon.Dwarf4.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf5.Slots.Length == 1, "toon.Dwarf5.Slots.Length is incorrect!");
            Debug.Assert(toon.Dwarf6.Slots.Length == 1, "toon.Dwarf6.Slots.Length is incorrect!");

            Debug.Assert(toon.Health.Slots[0].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Health.Slots[1].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Health.Slots[2].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Health.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Stamina.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Stamina.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level1Primary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level1Primary.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[4].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level1Primary.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Primary.Slots[5].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level1Primary.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level1Secondary.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level1Secondary.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level1Secondary.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level1Secondary.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level2.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level2.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level2.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level2.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level2.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level2.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level2.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level4.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[1].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level4.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level4.Slots[2].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level4.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level6.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level6.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level6.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level6.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level8.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level8.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level8.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level8.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level8.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level8.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level8.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level10.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level10.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level10.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level10.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level10.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level12.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level12.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level12.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level12.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[4].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level12.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level12.Slots[5].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level12.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level14.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level14.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[4].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level14.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level14.Slots[5].Enhancement.Enh == (int)EnhancementType.Slow, "toon.Level14.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[0].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level16.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[1].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level16.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[2].Enhancement.Enh == (int)EnhancementType.Resistance, "toon.Level16.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[3].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[4].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level16.Slots[5].Enhancement.Enh == (int)EnhancementType.Recharge, "toon.Level16.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level18.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level18.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[4].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Level18.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level18.Slots[5].Enhancement.Enh == (int)EnhancementType.Heal, "toon.Level18.Slots[5].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level20.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level20.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level20.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level20.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level22.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level22.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level22.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level22.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level22.Slots[4].Enhancement.Enh == (int)EnhancementType.Hold, "toon.Level22.Slots[4].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level24.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "toon.Level24.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level26.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level26.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level28.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level28.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level30.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level30.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level32.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level32.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level35.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level35.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level38.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level38.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level41.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "toon.Level41.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level44.Slots[0].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Level44.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level47.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Level47.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Level49.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Level49.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova1.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Nova1.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova1.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova1.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova1.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova1.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova1.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova1.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova2.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Nova2.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova2.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova2.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova2.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova2.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova2.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Nova2.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova3.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Nova3.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Nova4.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Nova4.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf1.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Dwarf1.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf1.Slots[1].Enhancement.Enh == (int)EnhancementType.Taunt, "toon.Dwarf1.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf1.Slots[2].Enhancement.Enh == (int)EnhancementType.Taunt, "toon.Dwarf1.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf1.Slots[3].Enhancement.Enh == (int)EnhancementType.Taunt, "toon.Dwarf1.Slots[3].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf2.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Dwarf2.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf2.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Dwarf2.Slots[1].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf2.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "toon.Dwarf2.Slots[2].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf3.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Dwarf3.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf4.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Dwarf4.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf5.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "toon.Dwarf5.Slots[0].Enhancement.Enh is incorrect!");
            Debug.Assert(toon.Dwarf6.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "toon.Dwarf6.Slots[0].Enhancement.Enh is incorrect!");

            AIToon toon2 = AIToonUtils.CreateBuildControllerMindForceField();
            IEffect[] effects = toon2.Level47.Power.Effects;
            int x = 1;

        }
    }
}

﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Hero_Designer
{
    public class AIStrategyHighestSet
    {
        public static PowerEntry Do(AIToon toon, List<PowerEntry> powerEntries, int powerEntryIndex, AIConfig config, int configBonusIndex)
        {
            PowerEntry powerEntry = powerEntries[powerEntryIndex];
            PowerEntry selectedPowerEntry = powerEntry;
            if (2 > powerEntry.Slots.Length)
                return powerEntry;
            int slots = 2;
            BonusInfoWithWeight bonusInfoWithWeight = config.BonusInfoWithWeightArray[configBonusIndex];
            float sum = bonusInfoWithWeight.Sum(powerEntry, toon.Archetype);
            HashSet<EnhancementSet> enhancementSets = null;
            EnhancementSet[] sortedEnhancementSets = null;
            EnhancementSet enhancementSet = null;
            bool[] slotFlags = config.SlotFlags[powerEntryIndex];
            int lockedSlotCount = PowerEntry.GetLockedSlotsCount(slotFlags);

            while (powerEntry.Slots.Length >= slots)
            {
                PowerEntry powerEntryClone = powerEntry.Clone() as PowerEntry;
                enhancementSets = powerEntry.Power.GetEnhancementSetsWithBonusEffect(slots - lockedSlotCount, bonusInfoWithWeight);
                sortedEnhancementSets = EnhancementSet.SortEnhancementSets(enhancementSets, slots - lockedSlotCount, bonusInfoWithWeight);
                enhancementSet = toon.GetHighestValidSet(sortedEnhancementSets, powerEntry, slots - lockedSlotCount);
                if (null == enhancementSet)
                {
                    slots++;
                    continue;
                }

                int enhancementsAddedCount = 0;
                int slotIndex = 0;
                for (int enhancementIndex = 0; enhancementIndex < enhancementSet.Enhancements.Length; enhancementIndex++)
                {
                    IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementSet.Enhancements[enhancementIndex]);
                    if (!toon.CanAddEnhancement(enhancement, powerEntryClone))
                        continue;
                    while (slotIndex < powerEntryClone.SlotCount && !PowerEntry.IsSlotUnlocked(slotIndex, slotFlags)) slotIndex++;
                    if (slotIndex >= powerEntryClone.SlotCount) break;
                    powerEntryClone.Slots[slotIndex].LoadFromString(enhancement.GetLoadString(), ":");
                    enhancementsAddedCount++;
                    slotIndex++;
                    if (enhancementsAddedCount == slots)
                        break;
                }

                float cloneSum = bonusInfoWithWeight.Sum(powerEntryClone, toon.Archetype);
                if (sum < cloneSum)
                {
                    selectedPowerEntry = powerEntryClone;
                    sum = cloneSum;
                }
                slots++;
            }
            return selectedPowerEntry;
        }

        public static void TestNewCode()
        {
            TestHighestSetStrategy();
        }

        public static void TestHighestSetStrategy()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            AIConfig config = new AIConfig(toon, 1, 1, new BonusInfoWithWeight[] { new BonusInfoWithWeight(BonusInfo.DefenseSmashing.CloneWithPvP(), 1.0f) });
            PowerEntry result = Do(toon, toon.GetPowerEntries(), 2, config, 0);
            Debug.Assert(6 == result.Slots.Length, "result is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[0].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[0] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[1].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[1] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[2].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[2] is incorrect!");
            Debug.Assert(DatabaseAPI.GetEnhancementByIndex(result.Slots[3].Enhancement.Enh).UIDSet == "Lethargic_Repose", "enhancement[3] is incorrect!");
            IEnhancement e = DatabaseAPI.GetEnhancementByIndex(result.Slots[4].Enhancement.Enh);
            Debug.Assert(string.IsNullOrEmpty(e.UIDSet), "enhancement[4] is incorrect!");
            e = DatabaseAPI.GetEnhancementByIndex(result.Slots[5].Enhancement.Enh);
            Debug.Assert(string.IsNullOrEmpty(e.UIDSet), "enhancement[5] is incorrect!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using Hero_Designer;
using AIData;

namespace HeroDesignerAIData
{
    public static class AIBuildPOCOJSONSerializer
    {
        public static void Write(string filename, AIBuildPOCO aiBuildPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(aiBuildPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class AIBuildPOCOConvert
    {
        public static AIBuildPOCO Convert(Hero_Designer.AIToon toon)
        {
            AIBuildPOCO aiBuildPOCO = new AIBuildPOCO();
            if (null != toon.Fitness) aiBuildPOCO.power_sets.Add("Fitness", toon.Fitness.FullName);
            if (null != toon.Primary) aiBuildPOCO.power_sets.Add("Primary", toon.Primary.FullName);
            if (null != toon.Secondary) aiBuildPOCO.power_sets.Add("Secondary", toon.Secondary.FullName);
            if (null != toon.Pool1) aiBuildPOCO.power_sets.Add("Pool1", toon.Pool1.FullName);
            if (null != toon.Pool2) aiBuildPOCO.power_sets.Add("Pool2", toon.Pool2.FullName);
            if (null != toon.Pool3) aiBuildPOCO.power_sets.Add("Pool3", toon.Pool3.FullName);
            if (null != toon.Pool4) aiBuildPOCO.power_sets.Add("Pool4", toon.Pool4.FullName);
            if (null != toon.Epic) aiBuildPOCO.power_sets.Add("Epic", toon.Epic.FullName);

            if (null != toon.Brawl) aiBuildPOCO.power_entries.Add("Brawl", AIData.PowerEntryPOCOConvert.Convert(toon.Brawl));
            if (null != toon.Health) aiBuildPOCO.power_entries.Add("Health", AIData.PowerEntryPOCOConvert.Convert(toon.Health));
            if (null != toon.Stamina) aiBuildPOCO.power_entries.Add("Stamina", AIData.PowerEntryPOCOConvert.Convert(toon.Stamina));
            if (null != toon.Level1Primary) aiBuildPOCO.power_entries.Add("Level1Primary", AIData.PowerEntryPOCOConvert.Convert(toon.Level1Primary));
            if (null != toon.Level1Secondary) aiBuildPOCO.power_entries.Add("Level1Secondary", AIData.PowerEntryPOCOConvert.Convert(toon.Level1Secondary));
            if (null != toon.Level2) aiBuildPOCO.power_entries.Add("Level2", AIData.PowerEntryPOCOConvert.Convert(toon.Level2));
            if (null != toon.Level4) aiBuildPOCO.power_entries.Add("Level4", AIData.PowerEntryPOCOConvert.Convert(toon.Level4));
            if (null != toon.Level6) aiBuildPOCO.power_entries.Add("Level6", AIData.PowerEntryPOCOConvert.Convert(toon.Level6));
            if (null != toon.Level8) aiBuildPOCO.power_entries.Add("Level8", AIData.PowerEntryPOCOConvert.Convert(toon.Level8));
            if (null != toon.Level10) aiBuildPOCO.power_entries.Add("Level10", AIData.PowerEntryPOCOConvert.Convert(toon.Level10));
            if (null != toon.Level12) aiBuildPOCO.power_entries.Add("Level12", AIData.PowerEntryPOCOConvert.Convert(toon.Level12));
            if (null != toon.Level14) aiBuildPOCO.power_entries.Add("Level14", AIData.PowerEntryPOCOConvert.Convert(toon.Level14));
            if (null != toon.Level16) aiBuildPOCO.power_entries.Add("Level16", AIData.PowerEntryPOCOConvert.Convert(toon.Level16));
            if (null != toon.Level18) aiBuildPOCO.power_entries.Add("Level18", AIData.PowerEntryPOCOConvert.Convert(toon.Level18));
            if (null != toon.Level20) aiBuildPOCO.power_entries.Add("Level20", AIData.PowerEntryPOCOConvert.Convert(toon.Level20));
            if (null != toon.Level22) aiBuildPOCO.power_entries.Add("Level22", AIData.PowerEntryPOCOConvert.Convert(toon.Level22));
            if (null != toon.Level24) aiBuildPOCO.power_entries.Add("Level24", AIData.PowerEntryPOCOConvert.Convert(toon.Level24));
            if (null != toon.Level26) aiBuildPOCO.power_entries.Add("Level26", AIData.PowerEntryPOCOConvert.Convert(toon.Level26));
            if (null != toon.Level28) aiBuildPOCO.power_entries.Add("Level28", AIData.PowerEntryPOCOConvert.Convert(toon.Level28));
            if (null != toon.Level30) aiBuildPOCO.power_entries.Add("Level30", AIData.PowerEntryPOCOConvert.Convert(toon.Level30));
            if (null != toon.Level32) aiBuildPOCO.power_entries.Add("Level32", AIData.PowerEntryPOCOConvert.Convert(toon.Level32));
            if (null != toon.Level35) aiBuildPOCO.power_entries.Add("Level35", AIData.PowerEntryPOCOConvert.Convert(toon.Level35));
            if (null != toon.Level38) aiBuildPOCO.power_entries.Add("Level38", AIData.PowerEntryPOCOConvert.Convert(toon.Level38));
            if (null != toon.Level41) aiBuildPOCO.power_entries.Add("Level41", AIData.PowerEntryPOCOConvert.Convert(toon.Level41));
            if (null != toon.Level44) aiBuildPOCO.power_entries.Add("Level44", AIData.PowerEntryPOCOConvert.Convert(toon.Level44));
            if (null != toon.Level47) aiBuildPOCO.power_entries.Add("Level47", AIData.PowerEntryPOCOConvert.Convert(toon.Level47));
            if (null != toon.Level49) aiBuildPOCO.power_entries.Add("Level49", AIData.PowerEntryPOCOConvert.Convert(toon.Level49));
            if (null != toon.Level1Kheldian) aiBuildPOCO.power_entries.Add("Level1Kheldian", AIData.PowerEntryPOCOConvert.Convert(toon.Level1Kheldian));
            if (null != toon.Level10Kheldian) aiBuildPOCO.power_entries.Add("Level10Kheldian", AIData.PowerEntryPOCOConvert.Convert(toon.Level10Kheldian));
            if (null != toon.Nova1) aiBuildPOCO.power_entries.Add("Nova1", AIData.PowerEntryPOCOConvert.Convert(toon.Nova1));
            if (null != toon.Nova2) aiBuildPOCO.power_entries.Add("Nova2", AIData.PowerEntryPOCOConvert.Convert(toon.Nova2));
            if (null != toon.Nova3) aiBuildPOCO.power_entries.Add("Nova3", AIData.PowerEntryPOCOConvert.Convert(toon.Nova3));
            if (null != toon.Nova4) aiBuildPOCO.power_entries.Add("Nova4", AIData.PowerEntryPOCOConvert.Convert(toon.Nova4));
            if (null != toon.Dwarf1) aiBuildPOCO.power_entries.Add("Dwarf1", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf1));
            if (null != toon.Dwarf2) aiBuildPOCO.power_entries.Add("Dwarf2", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf2));
            if (null != toon.Dwarf3) aiBuildPOCO.power_entries.Add("Dwarf3", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf3));
            if (null != toon.Dwarf4) aiBuildPOCO.power_entries.Add("Dwarf4", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf4));
            if (null != toon.Dwarf5) aiBuildPOCO.power_entries.Add("Dwarf5", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf5));
            if (null != toon.Dwarf6) aiBuildPOCO.power_entries.Add("Dwarf6", AIData.PowerEntryPOCOConvert.Convert(toon.Dwarf6));
            return aiBuildPOCO;
        }
    }

    public class AIBuildPOCO
    {
        public Dictionary<string, string> power_sets { get; set; }
        public Dictionary<string, PowerEntryPOCO> power_entries { get; set; }

        public AIBuildPOCO()
        {
            power_sets = new Dictionary<string, string>();
            power_entries = new Dictionary<string, PowerEntryPOCO>();
        }
    }
}

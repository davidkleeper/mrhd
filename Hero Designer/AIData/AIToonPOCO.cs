﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using Hero_Designer;

namespace HeroDesignerAIData
{
    public static class AIToonPOCOJSONSerializer
    {
        public static void Write(string filename, AIToonPOCO aiToonPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(aiToonPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class AIToonPOCOConvert
    {
        public static AIToonPOCO Convert(Hero_Designer.AIToon toon)
        {
            AIToonPOCO aiToonPOCO = new AIToonPOCO();
            aiToonPOCO.name = toon.Name;
            aiToonPOCO.archetype_id = toon.Archetype?.ClassName ?? "";
            aiToonPOCO.builds = new AIBuildPOCO[1];
            aiToonPOCO.current_build = 0;
            aiToonPOCO.builds[0] = AIBuildPOCOConvert.Convert(toon);
            return aiToonPOCO;
        }
    }

    public class AIToonPOCO
    {
        public string name { get; set; }
        public string archetype_id { get; set; }
        public int current_build { get; set; }
        public AIBuildPOCO[] builds { get; set; }
        
    }

    public static class AIToonPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            AIToon toon = AIToonUtils.CreateBuildControllerMindForceField();
            AIToonPOCO aiToonPOCO = AIToonPOCOConvert.Convert(toon);
            AIToonPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\ai-toon.json", aiToonPOCO);
            AIBuildPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\ai-build.json", aiToonPOCO.builds[0]);
        }
    }
}

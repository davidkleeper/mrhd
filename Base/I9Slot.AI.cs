﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class I9Slot : ICloneable, IComparable
{
    public int CompareTo(object slot)
    {
        I9Slot s = slot as I9Slot;
        if (null == s) return 1;
        if (Enh != s.Enh) return Enh.CompareTo(s.Enh);
        if (RelativeLevel != s.RelativeLevel) return RelativeLevel.CompareTo(s.RelativeLevel);
        if (Grade != s.Grade) return Grade.CompareTo(s.Grade);
        if (IOLevel != s.IOLevel) return IOLevel.CompareTo(s.IOLevel);
        return 0;
    }
}

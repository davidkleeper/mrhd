﻿using System.Diagnostics;

public partial struct SlotEntry
{

    public int CompareTo(SlotEntry slotEntry)
    {
        if (Level != slotEntry.Level) return Level.CompareTo(slotEntry.Level);
        int result = Enhancement.CompareTo(slotEntry.Enhancement);
        if (0 != result) return result;
        result = FlippedEnhancement.CompareTo(slotEntry.FlippedEnhancement);
        if (0 != result) return result;
        return 0;
    }

    public void ClearEnhancements()
    {
        I9Slot i9Slot = new I9Slot();
        I9Slot i9SlotFlipped = new I9Slot();
        Enhancement = i9Slot;
        FlippedEnhancement = i9SlotFlipped;
    }

    public static void TestNewCode()
    {
        TestClearEnhancements();
    }

    public static void TestClearEnhancements()
    {
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        int acc = (int)EnhancementType.Accuracy;
        mesmerize.Slot(new int[] { acc });
        Debug.Assert(mesmerize.SlotCount == 1, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[0].Enhancement.Enh == acc, "ClearEnhancements is incorrect!");
        mesmerize.Slots[0].ClearEnhancements();
        Debug.Assert(mesmerize.SlotCount == 1, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[0].Enhancement.Enh == -1, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[0].FlippedEnhancement.Enh == -1, "ClearEnhancements is incorrect!");
    }
}

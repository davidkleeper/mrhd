﻿using System;
using System.Diagnostics;
using System.Linq;
using Base;
using Base.Data_Classes;

public static partial class DatabaseAPI
{
    public static Archetype[] GetPlayerArchetypes()
    {
        Archetype[] playeArchetypes = Database.Classes.Take(15).ToArray();
        return playeArchetypes;
    }

    public static IPower GetPowerByIndex(int index)
    {
        if (index < 0 || index > DatabaseAPI.Database.Power.Length - 1)
            return null;
        return DatabaseAPI.Database.Power[index];
    }

    public static IPowerset GetPowersetByFullName(string fullName)
    {
        foreach (Powerset powerset in Database.Powersets)
        {
            if (powerset.FullName != fullName)
                continue;
            return powerset;
        }
        return null;
    }

    public static IPowerset GetPowersetByIndex(int index)
    {
        if (0 > index || index >= Database.Powersets.Length)
            return null;
        return Database.Powersets[index];
    }

    public static IPowerset GetPowersetByArchetypeAndName(Archetype archetype, string name)
    {
        foreach (Powerset powerset in Database.Powersets)
        {
            if (powerset.ATClass != archetype.ClassName || powerset.DisplayName != name)
                continue;
            return powerset;
        }
        return null;
    }

    public static IEnhancement GetEnhancementByIndex(int index)
    {
        if (index < 0 || index > Database.Enhancements.Length - 1)
            return null;
        return Database.Enhancements[index];
    }

    public static int GetEnhancementSetIndexByName(string iName)
        => Database.EnhancementSets.TryFindIndex(enh => string.Equals(enh.ShortName, iName, StringComparison.OrdinalIgnoreCase) || string.Equals(enh.DisplayName, iName, StringComparison.OrdinalIgnoreCase));

    public static EnhancementSet GetEnhancementSetByIndex(int index)
    {
        if (0 > index || index >= Database.EnhancementSets.Count)
            return null;
        return Database.EnhancementSets[index];
    }

    public static float GetScheduleMultiplier(Enums.eEnhGrade grade, Enums.eSchedule schedule, int ioLevel = 0)
    {
        if (Enums.eEnhGrade.None == grade) return 0.0f;
        if (schedule == Enums.eSchedule.None || schedule == Enums.eSchedule.Multiple) return 0.0f;
        if (Enums.eEnhGrade.TrainingO == grade) return DatabaseAPI.Database.MultTO[0][(int)schedule]; ;
        if (Enums.eEnhGrade.DualO == grade) return DatabaseAPI.Database.MultDO[0][(int)schedule];
        return DatabaseAPI.Database.MultSO[0][(int)schedule];
    }

    public static float GetScheduleMultiplier(Enums.eType type, Enums.eSchedule schedule, int ioLevel)
    {
        if (Enums.eType.None == type) return 0.0f;
        if (Enums.eType.Normal == type) return -1.0f;
        if (schedule == Enums.eSchedule.None || schedule == Enums.eSchedule.Multiple) return 0.0f;
        if (Enums.eType.SpecialO == type) return DatabaseAPI.Database.MultSO[0][(int)schedule];
        return DatabaseAPI.Database.MultIO[ioLevel][(int)schedule];
    }

    public static float GetScheduleMultiplier(Enums.eType type, Enums.eEnhGrade grade, Enums.eSchedule schedule, int ioLevel)
    {
        if (Enums.eType.Normal == type) return GetScheduleMultiplier(grade, schedule);
        return GetScheduleMultiplier(type, schedule, ioLevel);
    }

    // Call this after all database info has loaded, such as at the end of LoadEnhancementDb()
    public static void TestNewCode()
    {
        Debug.Print("Debug version");
        TestGetPlayerArchetypes();
        TestGetPowersetByIndex();
        TestGetPowerByIndex();
        TestGetEnhancementByIndex();
        TestGetEnhancementSet();
        TestGetMultiplier();
        Enhancement.TestNewCode();
        EnhancementSet.TestNewCode();
        Power.TestNewCode();
        PowerEntry.TestNewCode();
        BaseUtils.TestNewCode();
        BonusInfo.TestNewCode();
        EnumUtils.TestNewCode();
        SpecialEnhancements.TestNewCode();
        SlotEntry.TestNewCode();
        AILogic.EffectLogic.TestNewCode();
        AILogic.PowerLogic.TestNewCode();
        AILogic.STwinIDLogic.TestNewCode();
        AILogic.EnhancementLogic.TestNewCode();
        AIMath.EffectMath.TestNewCode();
        AIMath.PowerMath.TestNewCode();
        AIMath.I9SlotMath.TestNewCode();
        AIMath.EnhancementMath.TestNewCode();
        AIData.PowerPOCOTest.TestNewCode();
        AIData.ArchetypePOCOTest.TestNewCode();
        AIData.EffectPOCOTest.TestNewCode();
        AIData.EnhancementPOCOTest.TestNewCode();
        AIData.PowerEntryPOCOTest.TestNewCode();
        AIData.PowersetPOCOTest.TestNewCode();
        AIData.PowerSubEntryPOCOTest.TestNewCode();
        AIData.RequirementPOCOTest.TestNewCode();
        AIData.SEffectPOCOTest.TestNewCode();
        AIData.SlotPOCOTest.TestNewCode();
        AIData.STwinIDPOCOTest.TestNewCode();
        AIData.BonusItemPOCOTest.TestNewCode();
        AIData.EnhancementSetPOCOTest.TestNewCode();
    }

    public static void TestGetPlayerArchetypes()
    {
        Archetype[] playerArchetypes = DatabaseAPI.GetPlayerArchetypes();
        Debug.Assert(15 == playerArchetypes.Length, "playerArchetypes is incorrect!");
        Debug.Assert("Blaster" == playerArchetypes[0].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Controller" == playerArchetypes[1].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Defender" == playerArchetypes[2].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Scrapper" == playerArchetypes[3].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Tanker" == playerArchetypes[4].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Peacebringer" == playerArchetypes[5].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Warshade" == playerArchetypes[6].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Sentinel" == playerArchetypes[7].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Brute" == playerArchetypes[8].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Stalker" == playerArchetypes[9].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Mastermind" == playerArchetypes[10].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Dominator" == playerArchetypes[11].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Corruptor" == playerArchetypes[12].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Arachnos Widow" == playerArchetypes[13].DisplayName, "playerArchetypes is incorrect!");
        Debug.Assert("Arachnos Soldier" == playerArchetypes[14].DisplayName, "playerArchetypes is incorrect!");
    }

    public static void TestGetPowersetByIndex()
    {
        Archetype tanker = GetArchetypeByName("Tanker");
        int[] tankerPrimaryIndexes = tanker.Primary;
        IPowerset powerset = GetPowersetByIndex(tankerPrimaryIndexes[0]);
        Debug.Assert("Bio Armor" == powerset.DisplayName, "powerset is incorrect!");
    }

    public static void TestGetPowerByIndex()
    {
        IPower power = DatabaseAPI.GetPowerByIndex(0);
        Debug.Assert(power != null, "power is null!");
        Debug.Assert(power.DisplayName == "Single Shot", "power.DisplayName is incorrect!");

        power = DatabaseAPI.GetPowerByIndex(100);
        Debug.Assert(power != null, "power is null!");
        Debug.Assert(power.DisplayName == "Blaze", "power.DisplayName is incorrect!");

        power = DatabaseAPI.GetPowerByIndex(11000);
        Debug.Assert(power == null, "power is not null!");
    }

    public static void TestGetEnhancementByIndex()
    {
        IEnhancement enhancement = GetEnhancementByIndex(0);
        Debug.Assert(enhancement != null, "enhancement is null!");
        Debug.Assert(enhancement.Name == "Accuracy", "enhancement.Name is incorrect!");

        enhancement = GetEnhancementByIndex(100);
        Debug.Assert(enhancement != null, "enhancement is null!");
        Debug.Assert(enhancement.Name == "Accuracy/Damage/Endurance", "enhancement.Name is incorrect!");

        enhancement = GetEnhancementByIndex(1300);
        Debug.Assert(enhancement == null, "enhancement is not null!");
    }

    public static void TestGetEnhancementSet()
    {
        int index = GetEnhancementSetIndexByName("Superior Winter's Bite");
        EnhancementSet enhancementSet = GetEnhancementSetByIndex(index);
        Debug.Assert(enhancementSet.DisplayName == "Superior Winter's Bite", "enhancementSet.DisplayName is incorrect!");
    }

    public static void TestGetMultiplier()
    {
        float answer = GetScheduleMultiplier(Enums.eEnhGrade.None, Enums.eSchedule.A);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.None, Enums.eSchedule.B);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.None, Enums.eSchedule.C);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.None, Enums.eSchedule.D);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.None);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.Multiple);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.A);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.B);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.C);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.TrainingO, Enums.eSchedule.D);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.DualO, Enums.eSchedule.A);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.DualO, Enums.eSchedule.B);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.DualO, Enums.eSchedule.C);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.DualO, Enums.eSchedule.D);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.SingleO, Enums.eSchedule.A);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.SingleO, Enums.eSchedule.B);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.SingleO, Enums.eSchedule.C);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eEnhGrade.SingleO, Enums.eSchedule.D);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.D], answer, 0.001f));

        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.None, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.Multiple, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(-1.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(-1.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(-1.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(-1.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.D], answer, 0.001f));

        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.None, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.Multiple, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.TrainingO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultTO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.DualO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.DualO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.DualO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.DualO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultDO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.SingleO, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.SingleO, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.SingleO, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.SingleO, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.D], answer, 0.001f));

        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.None, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.None, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.Multiple, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.Normal, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultSO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.InventO, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.D], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eEnhGrade.None, Enums.eSchedule.A, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.A], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eEnhGrade.None, Enums.eSchedule.B, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.B], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eEnhGrade.None, Enums.eSchedule.C, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.C], answer, 0.001f));
        answer = GetScheduleMultiplier(Enums.eType.SetO, Enums.eEnhGrade.None, Enums.eSchedule.D, 0);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(DatabaseAPI.Database.MultIO[0][(int)Enums.eSchedule.D], answer, 0.001f));
    }
}

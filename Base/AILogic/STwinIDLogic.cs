﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AILogic
{
    class STwinIDLogic
    {
        public static bool IsDefense(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Defense && twinId.SubID == -1;
        }

        public static bool IsResistance(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Resistance && twinId.SubID == -1;
        }

        public static bool IsDamage(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Damage;
        }

        public static bool IsAccuracy(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Accuracy && twinId.SubID == -1;
        }

        public static bool IsFly(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.SpeedFlying && twinId.SubID == -1;
        }

        public static bool IsJump(Enums.sTwinID twinId)
        {
            return (twinId.ID == (int)Enums.eEnhance.JumpHeight && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.SpeedJumping && twinId.SubID == -1);
        }

        public static bool IsRun(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.SpeedRunning && twinId.SubID == -1;
        }

        public static bool IsConfuse(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Confused;
        }

        public static bool IsHold(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Held;
        }

        public static bool IsImmobilize(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Immobilized;
        }

        public static bool IsSleep(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Sleep;
        }

        public static bool IsStun(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Stunned;
        }

        public static bool IsTerrorize(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Terrorized;
        }

        public static bool IsKnockBack(Enums.sTwinID twinId)
        {
            return (twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Knockback)
                || (twinId.ID == (int)Enums.eEnhance.Mez && twinId.SubID == (int)Enums.eMez.Knockup);
        }

        public static bool IsHeal(Enums.sTwinID twinId)
        {
            return (twinId.ID == (int)Enums.eEnhance.Heal && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.HitPoints && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.Regeneration && twinId.SubID == -1)
                || ((twinId.ID == (int)Enums.eEnhance.Absorb && twinId.SubID == -1));
        }

        public static bool IsRecovery(Enums.sTwinID twinId)
        {
            return (twinId.ID == (int)Enums.eEnhance.Recovery && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.Endurance && twinId.SubID == -1);
        }

        public static bool IsEnduranceDiscount(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.EnduranceDiscount && twinId.SubID == -1;
        }

        public static bool IsRechargeReduction(Enums.sTwinID twinId)
        {
            return twinId.ID == (int)Enums.eEnhance.RechargeTime && twinId.SubID == -1;
        }

        public static bool IsSlow(Enums.sTwinID twinId)
        {
            return (twinId.ID == (int)Enums.eEnhance.SpeedFlying && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.SpeedJumping && twinId.SubID == -1)
                || (twinId.ID == (int)Enums.eEnhance.SpeedRunning && twinId.SubID == -1)
                || ((twinId.ID == (int)Enums.eEnhance.Slow && twinId.SubID == -1));
        }

        public static void TestNewCode()
        {
            TestIsDefense();
            TestIsResistance();
            TestIsDamage();
            TestIsAccuracy();
            TestIsFly();
            TestIsJump();
            TestIsRun();
            TestIsConfuse();
            TestIsHold();
            TestIsImmobilize();
            TestIsSleep();
            TestIsStun();
            TestIsTerrorize();
            TestIsKnockBack();
            TestIsHeal();
            TestIsRecovery();
            TestIsEnduranceDiscount();
            TestIsRechargeReduction();
            TestIsSlow();
        }

        public static void TestIsDefense()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Defense);
            Debug.Assert(IsDefense(enhancement.Effect[0].Enhance), "IsDefense() is incorrect!");
        }

        public static void TestIsResistance()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Resistance);
            Debug.Assert(IsResistance(enhancement.Effect[0].Enhance), "IsResistance() is incorrect!");
        }

        public static void TestIsDamage()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            Debug.Assert(IsDamage(enhancement.Effect[0].Enhance), "IsDamage() is incorrect!");
        }

        public static void TestIsAccuracy()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            Debug.Assert(IsDamage(enhancement.Effect[0].Enhance), "IsDamage() is incorrect!");
        }

        public static void TestIsFly()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Fly);
            Debug.Assert(IsFly(enhancement.Effect[0].Enhance), "IsFly() is incorrect!");
        }

        public static void TestIsJump()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Jump);
            Debug.Assert(IsJump(enhancement.Effect[0].Enhance), "IsJump() is incorrect!");
            Debug.Assert(IsJump(enhancement.Effect[1].Enhance), "IsJump() is incorrect!");
        }

        public static void TestIsRun()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Run);
            Debug.Assert(IsRun(enhancement.Effect[0].Enhance), "IsRun() is incorrect!");
        }

        public static void TestIsConfuse()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Confuse);
            Debug.Assert(IsConfuse(enhancement.Effect[0].Enhance), "IsConfuse() is incorrect!");
        }

        public static void TestIsHold()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Hold);
            Debug.Assert(IsHold(enhancement.Effect[0].Enhance), "IsHold() is incorrect!");
        }

        public static void TestIsImmobilize()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Immobilize);
            Debug.Assert(IsImmobilize(enhancement.Effect[0].Enhance), "IsImmobilize() is incorrect!");
        }

        public static void TestIsSleep()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Sleep);
            Debug.Assert(IsSleep(enhancement.Effect[0].Enhance), "IsSleep() is incorrect!");
        }

        public static void TestIsStun()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Stun);
            Debug.Assert(IsStun(enhancement.Effect[0].Enhance), "IsStun() is incorrect!");
        }

        public static void TestIsTerrorize()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Fear);
            Debug.Assert(IsTerrorize(enhancement.Effect[0].Enhance), "IsTerrorize() is incorrect!");
        }

        public static void TestIsKnockBack()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Knockback);
            Debug.Assert(IsKnockBack(enhancement.Effect[0].Enhance), "IsKnockBack() is incorrect!");
            Debug.Assert(IsKnockBack(enhancement.Effect[1].Enhance), "IsKnockBack() is incorrect!");
        }

        public static void TestIsHeal()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Heal);
            Debug.Assert(IsHeal(enhancement.Effect[0].Enhance), "IsHeal() is incorrect!");
            Debug.Assert(IsHeal(enhancement.Effect[1].Enhance), "IsHeal() is incorrect!");
            Debug.Assert(IsHeal(enhancement.Effect[2].Enhance), "IsHeal() is incorrect!");
            Debug.Assert(IsHeal(enhancement.Effect[3].Enhance), "IsHeal() is incorrect!");
        }

        public static void TestIsRecovery()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceModification);
            Debug.Assert(IsRecovery(enhancement.Effect[0].Enhance), "IsRecovery() is incorrect!");
            Debug.Assert(IsRecovery(enhancement.Effect[1].Enhance), "IsRecovery() is incorrect!");
        }

        public static void TestIsEnduranceDiscount()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
            Debug.Assert(IsEnduranceDiscount(enhancement.Effect[0].Enhance), "IsEnduranceDiscount() is incorrect!");
        }

        public static void TestIsRechargeReduction()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Recharge);
            Debug.Assert(IsRechargeReduction(enhancement.Effect[0].Enhance), "IsRechargeReduction() is incorrect!");
        }

        public static void TestIsSlow()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Slow);
            Debug.Assert(IsSlow(enhancement.Effect[0].Enhance), "IsSlow() is incorrect!");
            Debug.Assert(IsSlow(enhancement.Effect[1].Enhance), "IsSlow() is incorrect!");
            Debug.Assert(IsSlow(enhancement.Effect[2].Enhance), "IsSlow() is incorrect!");
            Debug.Assert(IsSlow(enhancement.Effect[3].Enhance), "IsSlow() is incorrect!");
        }
    }
}

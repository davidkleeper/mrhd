﻿using Base.Data_Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AILogic
{
    public static class EffectLogic
    {
        public static bool IsDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsAllDefense(effect, archetype)
                || IsMeleeDefense(effect, archetype)
                || IsRangedDefense(effect, archetype)
                || IsAoEDefense(effect, archetype)
                || IsSmashingDefense(effect, archetype)
                || IsLethalDefense(effect, archetype)
                || IsFireDefense(effect, archetype)
                || IsColdDefense(effect, archetype)
                || IsEnergyDefense(effect, archetype)
                || IsNegativeEnergyDefense(effect, archetype)
                || IsPsionicDefense(effect, archetype);
        }

        public static bool IsAllDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            if (IsDefenseType(effect, Enums.eDamage.None, archetype)) return true;
            return false;
        }

        public static bool IsMeleeDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Melee, archetype);
        }

        public static bool IsRangedDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Ranged, archetype);
        }

        public static bool IsAoEDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.AoE, archetype);
        }

        public static bool IsSmashingDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Smashing, archetype);
        }

        public static bool IsLethalDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Lethal, archetype);
        }

        public static bool IsFireDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Fire, archetype);
        }

        public static bool IsColdDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Cold, archetype);
        }

        public static bool IsEnergyDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Energy, archetype);
        }

        public static bool IsNegativeEnergyDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Negative, archetype);
        }

        public static bool IsPsionicDefense(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsDefenseType(effect, Enums.eDamage.Psionic, archetype);
        }

        public static bool IsDefenseType(IEffect effect, Enums.eDamage damageType, Base.Data_Classes.Archetype archetype)
        {
            return effect.EffectType == Enums.eEffectType.Defense
                && effect.DamageType == damageType
                && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsSmashingResistance(effect, archetype)
                || IsLethalResistance(effect, archetype)
                || IsFireResistance(effect, archetype)
                || IsColdResistance(effect, archetype)
                || IsEnergyResistance(effect, archetype)
                || IsNegativeEnergyResistance(effect, archetype)
                || IsPsionicResistance(effect, archetype)
                || IsToxicResistance(effect, archetype);
        }

        public static bool IsSmashingResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Smashing, archetype);
        }

        public static bool IsLethalResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Lethal, archetype);
        }

        public static bool IsFireResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Fire, archetype);
        }

        public static bool IsColdResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Cold, archetype);
        }

        public static bool IsEnergyResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Energy, archetype);
        }

        public static bool IsNegativeEnergyResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Negative, archetype);
        }

        public static bool IsPsionicResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Psionic, archetype);
        }

        public static bool IsToxicResistance(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsResistanceType(effect, Enums.eDamage.Toxic, archetype);
        }

        public static bool IsResistanceType(IEffect effect, Enums.eDamage damageType, Base.Data_Classes.Archetype archetype)
        {
            return effect.EffectType == Enums.eEffectType.Resistance
                && effect.DamageType == damageType
                && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsDamage(IEffect effect)
        {
            return (effect.EffectType == Enums.eEffectType.Damage);
        }

        public static bool IsAccuracy(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Enhancement
                && effect.ETModifies == Enums.eEffectType.Accuracy;
        }

        public static bool IsTravelPower(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return IsFly(effect, archetype) || IsJump(effect, archetype) || IsRun(effect, archetype) || IsTeleport(effect);
        }

        public static bool IsFly(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return (effect.EffectType == Enums.eEffectType.Fly || effect.EffectType == Enums.eEffectType.MaxFlySpeed || effect.EffectType == Enums.eEffectType.SpeedFlying)
                && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsJump(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return (effect.EffectType == Enums.eEffectType.JumpHeight || effect.EffectType == Enums.eEffectType.MaxJumpSpeed || effect.EffectType == Enums.eEffectType.SpeedJumping)
                 && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsRun(IEffect effect, Base.Data_Classes.Archetype archetype)
        {
            return (effect.EffectType == Enums.eEffectType.MaxRunSpeed || effect.EffectType == Enums.eEffectType.SpeedRunning)
                 && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsTeleport(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Teleport
                && effect.ToWho == Enums.eToWho.ToSelf;
        }

        public static bool IsControl(IEffect effect)
        {
            return IsConfuse(effect) || IsHold(effect) || IsImmobilize(effect) || IsSleep(effect) || IsStun(effect) || IsTerrorize(effect);
        }

        public static bool IsConfuse(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Confused
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsHold(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Held
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsImmobilize(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Immobilized
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsSleep(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Sleep
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsStun(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Stunned
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsTerrorize(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && (effect.MezType == Enums.eMez.Afraid || effect.MezType == Enums.eMez.Terrorized)
                && effect.ToWho == Enums.eToWho.Target;
        }

        public static bool IsControlResistance(IEffect effect)
        {
            return IsConfuseResistance(effect) || IsHoldResistance(effect) || IsImmobilizeResistance(effect) || IsSleepResistance(effect) || IsStunResistance(effect) || IsTerrorizeResistance(effect);
        }

        public static bool IsConfuseResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Confused;
        }

        public static bool IsHoldResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Held;
        }

        public static bool IsImmobilizeResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Immobilized;
        }

        public static bool IsSleepResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Sleep;
        }

        public static bool IsStunResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Stunned;
        }

        public static bool IsTerrorizeResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && (effect.MezType == Enums.eMez.Afraid || effect.MezType == Enums.eMez.Terrorized);
        }

        public static bool IsKnock(IEffect effect)
        {
            return IsKnockBack(effect) || IsKnockDown(effect) || IsKnockUp(effect);
        }

        public static bool IsKnockBack(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Knockback
                && effect.Mag >= 1.0f;
        }

        public static bool IsKnockDown(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Knockback
                && effect.Mag < 1.0f;
        }

        public static bool IsKnockUp(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Knockup;
        }

        public static bool IsKnockResistance(IEffect effect)
        {
            return IsKnockBackResistance(effect) || IsKnockDownResistance(effect) || IsKnockUpResistance(effect);
        }

        public static bool IsKnockBackResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Knockback;
        }

        public static bool IsKnockDownResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Knockback;
        }

        public static bool IsKnockUpResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Knockup;
        }

        public static bool IsRepel(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Mez
                && effect.MezType == Enums.eMez.Repel;
        }

        public static bool IsRepelResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.MezResist
                && effect.MezType == Enums.eMez.Repel;
        }

        public static bool IsHeal(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Heal;
        }

        public static bool IsHitPoints(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.HitPoints;
        }

        public static bool IsRegeneration(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Regeneration;
        }

        public static bool IsRecovery(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Recovery;
        }

        public static bool IsEnduranceDiscount(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Enhancement
                && effect.ETModifies == Enums.eEffectType.EnduranceDiscount;
        }

        public static bool IsEnduranceIncrease(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.Endurance;
        }

        public static bool IsRechargeReduction(IEffect effect, Archetype archetype)
        {
            return effect.EffectType == Enums.eEffectType.Enhancement
                && effect.ETModifies == Enums.eEffectType.RechargeTime
                && AIMath.EffectMath.CalcMag(effect, archetype) > 0.0f;
        }

        public static bool IsSlow(IEffect effect, Archetype archetype)
        {
            return IsMovementSlow(effect, archetype) || IsRechargeSlow(effect, archetype);
        }

        public static bool IsMovementSlow(IEffect effect, Archetype archetype)
        {
            if (effect.EffectType != Enums.eEffectType.Enhancement) return false;
            if (AIMath.EffectMath.CalcMag(effect, archetype) >= 0.0f) return false;
            bool isMovement = effect.ETModifies == Enums.eEffectType.Fly;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxFlySpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedFlying;
            isMovement |= effect.ETModifies == Enums.eEffectType.JumpHeight;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxJumpSpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedJumping;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxRunSpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedRunning;
            return isMovement;
        }

        public static bool IsRechargeSlow(IEffect effect, Archetype archetype)
        {
            return effect.EffectType == Enums.eEffectType.Enhancement
                && effect.ETModifies == Enums.eEffectType.RechargeTime
                && AIMath.EffectMath.CalcMag(effect, archetype) < 0.0f;
        }

        public static bool IsSlowResistance(IEffect effect)
        {
            return IsMovementSlowResistance(effect) || IsRechargeSlowResistance(effect);
        }

        public static bool IsMovementSlowResistance(IEffect effect)
        {
            if (effect.EffectType != Enums.eEffectType.ResEffect) return false;
            bool isMovement = effect.ETModifies == Enums.eEffectType.Fly;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxFlySpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedFlying;
            isMovement |= effect.ETModifies == Enums.eEffectType.JumpHeight;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxJumpSpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedJumping;
            isMovement |= effect.ETModifies == Enums.eEffectType.MaxRunSpeed;
            isMovement |= effect.ETModifies == Enums.eEffectType.SpeedRunning;
            return isMovement;
        }

        public static bool IsRechargeSlowResistance(IEffect effect)
        {
            return effect.EffectType == Enums.eEffectType.ResEffect
                && effect.ETModifies == Enums.eEffectType.RechargeTime;
        }

        public static void TestNewCode()
        {
            TestIsDefense();
            TestIsAllDefense();
            TestIsMeleeDefense();
            TestIsRangedDefense();
            TestIsAoEDefense();
            TestIsSmashingDefense();
            TestIsLethalDefense();
            TestIsFireDefense();
            TestIsColdDefense();
            TestIsEnergyDefense();
            TestIsNegativeEnergyDefense();
            TestIsPsionicDefense();
            TestIsResistance();
            TestIsSmashingResistance();
            TestIsLethalResistance();
            TestIsFireResistance();
            TestIsColdResistance();
            TestIsEnergyResistance();
            TestIsNegativeEnergyResistance();
            TestIsPsionicResistance();
            TestIsToxicResistance();
            TestIsDamage();
            TestIsAccuracy();
            TestIsTravelPower();
            TestIsFly();
            TestIsJump();
            TestIsRun();
            TestIsTeleport();
            TestIsControl();
            TestIsConfuse();
            TestIsHold();
            TestIsImmobilize();
            TestIsSleep();
            TestIsStun();
            TestIsTerrorize();
            TestIsControlResistance();
            TestIsConfuseResistance();
            TestIsHoldResistance();
            TestIsImmobilizeResistance();
            TestIsSleepResistance();
            TestIsStunResistance();
            TestIsTerrorizeResistance();
            TestIsKnock();
            TestIsKnockBack();
            TestIsKnockDown();
            TestIsKnockUp();
            TestIsKnockResistance();
            TestIsKnockBackResistance();
            TestIsKnockDownResistance();
            TestIsKnockUpResistance();
            TestIsRepel();
            TestIsRepelResistance();
            TestIsHeal();
            TestIsHitPoints();
            TestIsRegeneration();
            TestIsRecovery();
            TestIsEnduranceDiscount();
            TestIsEnduranceIncrease();
            TestIsRechargeReduction();
            TestIsSlow();
            TestIsMovementSlow();
            TestIsRechargeSlow();
            TestIsSlowResistance();
            TestIsMovementSlowResistance();
            TestIsRechargeSlowResistance();
        }

        public static void TestIsDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            Debug.Assert(IsDefense(personalForceField.Effects[1], archetype), "IsDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsDefense(deflection.Effects[0], archetype), "IsDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsDefense(battleAgility.Effects[0], archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(battleAgility.Effects[1], archetype), "IsDefense() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsDefense(rockArmor.Effects[0], archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(rockArmor.Effects[1], archetype), "IsDefense() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsDefense(wetIce.Effects[31], archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(wetIce.Effects[32], archetype), "IsDefense() is incorrect!");

            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsDefense(crystalArmor.Effects[0], archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(crystalArmor.Effects[1], archetype), "IsDefense() is incorrect!");

            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsDefense(minerals.Effects[0], archetype), "IsDefense() is incorrect!");
        }

        public static void TestIsAllDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            Debug.Assert(IsAllDefense(personalForceField.Effects[1], archetype), "IsAllDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsAllDefense(battleAgility.Effects[0], archetype) == false, "IsAllDefense() is incorrect!");
        }

        public static void TestIsMeleeDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsMeleeDefense(deflection.Effects[0], archetype), "IsMeleeDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsMeleeDefense(battleAgility.Effects[0], archetype) == false, "IsMeleeDefense() is incorrect!");
        }

        public static void TestIsRangedDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsRangedDefense(battleAgility.Effects[0], archetype), "IsRangedDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsRangedDefense(deflection.Effects[0], archetype) == false, "IsRangedDefense() is incorrect!");
        }

        public static void TestIsAoEDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsAoEDefense(battleAgility.Effects[1], archetype), "IsAoEDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsAoEDefense(deflection.Effects[0], archetype) == false, "IsAoEDefense() is incorrect!");
        }

        public static void TestIsSmashingDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsSmashingDefense(rockArmor.Effects[0], archetype), "IsSmashingDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsSmashingDefense(fireShield.Effects[0], archetype) == false, "IsSmashingDefense() is incorrect!");
        }

        public static void TestIsLethalDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsLethalDefense(rockArmor.Effects[1], archetype), "IsLethalDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsLethalDefense(fireShield.Effects[1], archetype) == false, "IsLethalDefense() is incorrect!");
        }

        public static void TestIsFireDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsFireDefense(wetIce.Effects[31], archetype), "IsFireDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsFireDefense(temperatureProtection.Effects[1], archetype) == false, "IsFireDefense() is incorrect!");
        }

        public static void TestIsColdDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsColdDefense(wetIce.Effects[32], archetype), "IsColdDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsColdDefense(temperatureProtection.Effects[0], archetype) == false, "IsColdDefense() is incorrect!");
        }

        public static void TestIsEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsEnergyDefense(crystalArmor.Effects[0], archetype), "IsEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsEnergyDefense(plasmaShield.Effects[0], archetype) == false, "IsEnergyDefense() is incorrect!");
        }

        public static void TestIsNegativeEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsNegativeEnergyDefense(crystalArmor.Effects[1], archetype), "IsNegativeEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsNegativeEnergyDefense(plasmaShield.Effects[1], archetype) == false, "IsNegativeEnergyDefense() is incorrect!");
        }

        public static void TestIsPsionicDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsPsionicDefense(minerals.Effects[0], archetype), "IsPsionicDefense() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsPsionicDefense(strengthOfWill.Effects[6], archetype) == false, "IsPsionicDefense() is incorrect!");
        }

        public static void TestIsResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsResistance(fireShield.Effects[0], archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(fireShield.Effects[1], archetype), "IsResistance() is incorrect!");

            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsResistance(temperatureProtection.Effects[1], archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(temperatureProtection.Effects[0], archetype), "IsResistance() is incorrect!");

            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsResistance(plasmaShield.Effects[0], archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(plasmaShield.Effects[1], archetype), "IsResistance() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsResistance(strengthOfWill.Effects[6], archetype), "IsResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            Debug.Assert(IsResistance(earthsEnbrace.Effects[2], archetype), "IsResistance() is incorrect!");
        }

        public static void TestIsSmashingResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsSmashingResistance(fireShield.Effects[0], archetype), "IsSmashingResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsSmashingResistance(rockArmor.Effects[0], archetype) == false, "IsSmashingResistance() is incorrect!");
        }

        public static void TestIsLethalResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsLethalResistance(fireShield.Effects[1], archetype), "IsLethalResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsLethalResistance(rockArmor.Effects[1], archetype) == false, "IsLethalResistance() is incorrect!");
        }

        public static void TestIsFireResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsFireResistance(temperatureProtection.Effects[1], archetype), "IsFireResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsFireResistance(wetIce.Effects[31], archetype) == false, "IsFireResistance() is incorrect!");
        }

        public static void TestIsColdResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsColdResistance(temperatureProtection.Effects[0], archetype), "IsColdResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsColdResistance(wetIce.Effects[32], archetype) == false, "IsColdResistance() is incorrect!");
        }

        public static void TestIsEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsEnergyResistance(plasmaShield.Effects[0], archetype), "IsEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsEnergyResistance(crystalArmor.Effects[0], archetype) == false, "IsEnergyResistance() is incorrect!");
        }

        public static void TestIsNegativeEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsNegativeEnergyResistance(plasmaShield.Effects[1], archetype), "IsNegativeEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsNegativeEnergyResistance(crystalArmor.Effects[1], archetype) == false, "IsNegativeEnergyResistance() is incorrect!");
        }

        public static void TestIsPsionicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsPsionicResistance(strengthOfWill.Effects[6], archetype), "IsPsionicResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsPsionicResistance(minerals.Effects[0], archetype) == false, "IsPsionicResistance() is incorrect!");
        }

        public static void TestIsToxicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            Debug.Assert(IsToxicResistance(earthsEnbrace.Effects[2], archetype), "IsToxicResistance() is incorrect!");
        }

        public static void TestIsDamage()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower mudPots = stoneArmor.Powers[3];
            Debug.Assert(IsDamage(mudPots.Effects[6]), "IsDamage() is incorrect!");
            Debug.Assert(IsDamage(mudPots.Effects[9]), "IsDamage() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsDamage(brimstoneArmor.Effects[0]) == false, "IsDamage() is incorrect!");
        }

        public static void TestIsAccuracy()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
            IPower focusedAccuracy = maceMastery.Powers[3];
            Debug.Assert(IsAccuracy(focusedAccuracy.Effects[2]), "IsAccuracy() is incorrect!");

            Debug.Assert(IsAccuracy(focusedAccuracy.Effects[0]) == false, "IsAccuracy() is incorrect!");
        }

        public static void TestIsTravelPower()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsTravelPower(fly.Effects[0], archetype), "IsTravelPower() is incorrect!");

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsTravelPower(superJump.Effects[0], archetype), "IsTravelPower() is incorrect!");

            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsTravelPower(superSpeed.Effects[0], archetype), "IsTravelPower() is incorrect!");

            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsTravelPower(teleport.Effects[1], archetype), "IsTravelPower() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower mudPots = stoneArmor.Powers[3];
            Debug.Assert(IsTravelPower(mudPots.Effects[6], archetype) == false, "IsTravelPower() is incorrect!");
        }

        public static void TestIsFly()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsFly(fly.Effects[0], archetype), "IsFly() is incorrect!");

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsFly(superJump.Effects[0], archetype) == false, "IsFly() is incorrect!");
        }

        public static void TestIsJump()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsJump(superJump.Effects[0], archetype), "IsJump() is incorrect!");

            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsJump(fly.Effects[0], archetype) == false, "IsJump() is incorrect!");
        }

        public static void TestIsRun()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsRun(superSpeed.Effects[0], archetype), "IsRun() is incorrect!");

            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsRun(teleport.Effects[0], archetype) == false, "IsRun() is incorrect!");
        }

        public static void TestIsTeleport()
        {
            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsTeleport(teleport.Effects[1]), "IsTeleport() is incorrect!");

            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsTeleport(superSpeed.Effects[0]) == false, "IsTeleport() is incorrect!");
        }

        public static void TestIsControl()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsControl(confuse.Effects[1]), "IsControl() is incorrect!");

            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsControl(dominate.Effects[0]), "IsControl() is incorrect!");

            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsControl(ringOfFire.Effects[4]), "IsControl() is incorrect!");

            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsControl(mesmerize.Effects[0]), "IsControl() is incorrect!");

            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsControl(flashFire.Effects[3]), "IsControl() is incorrect!");

            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsControl(terrify.Effects[0]), "IsControl() is incorrect!");
        }

        public static void TestIsConfuse()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsConfuse(confuse.Effects[1]), "IsConfuse() is incorrect!");

            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsConfuse(dominate.Effects[0]) == false, "IsConfuse() is incorrect!");
        }

        public static void TestIsHold()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsHold(dominate.Effects[0]), "IsHold() is incorrect!");

            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsHold(confuse.Effects[1]) == false, "IsHold() is incorrect!");
        }

        public static void TestIsImmobilize()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsImmobilize(ringOfFire.Effects[4]), "IsImmobilize() is incorrect!");

            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsImmobilize(flashFire.Effects[3]) == false, "IsImmobilize() is incorrect!");
        }

        public static void TestIsSleep()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsSleep(mesmerize.Effects[0]), "IsSleep() is incorrect!");

            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsSleep(terrify.Effects[0]) == false, "IsSleep() is incorrect!");
        }

        public static void TestIsStun()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsStun(flashFire.Effects[3]), "IsStun() is incorrect!");

            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsStun(ringOfFire.Effects[4]) == false, "IsStun() is incorrect!");
        }

        public static void TestIsTerrorize()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsTerrorize(terrify.Effects[0]), "IsTerrorize() is incorrect!");

            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsTerrorize(mesmerize.Effects[0]) == false, "IsTerrorize() is incorrect!");
        }

        public static void TestIsControlResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsControlResistance(tactics.Effects[5]), "IsControlResistance() is incorrect!");

            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsControlResistance(dispersionBubble.Effects[13]), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(dispersionBubble.Effects[15]), "IsControlResistance() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsControlResistance(health.Effects[1]), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(dispersionBubble.Effects[14]), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(tactics.Effects[6]), "IsControlResistance() is incorrect!");
        }

        public static void TestIsConfuseResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsConfuseResistance(tactics.Effects[5]), "IsConfuseResistance() is incorrect!");

            IPower assault = leadership.Powers[1];
            Debug.Assert(IsConfuseResistance(assault.Effects[8]) == false, "IsConfuseResistance() is incorrect!");
        }

        public static void TestIsHoldResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsHoldResistance(dispersionBubble.Effects[13]), "IsHoldResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsHoldResistance(tactics.Effects[5]) == false, "IsHoldResistance() is incorrect!");
        }

        public static void TestIsImmobilizeResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsImmobilizeResistance(dispersionBubble.Effects[15]), "IsImmobilizeResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsImmobilizeResistance(tactics.Effects[5]) == false, "IsImmobilizeResistance() is incorrect!");
        }

        public static void TestIsSleepResistance()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsSleepResistance(health.Effects[1]), "IsSleepResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsSleepResistance(tactics.Effects[5]) == false, "IsSleepResistance() is incorrect!");
        }

        public static void TestIsStunResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsStunResistance(dispersionBubble.Effects[14]), "IsStunResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsStunResistance(tactics.Effects[5]) == false, "IsStunResistance() is incorrect!");
        }

        public static void TestIsTerrorizeResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsTerrorizeResistance(tactics.Effects[6]), "IsTerrorizeResistance() is incorrect!");

            IPower assault = leadership.Powers[1];
            Debug.Assert(IsTerrorizeResistance(assault.Effects[8]) == false, "IsTerrorizeResistance() is incorrect!");
        }

        public static void TestIsKnock()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnock(repulsionField.Effects[0]), "IsKnock() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnock(fissure.Effects[1]), "IsKnock() is incorrect!");

            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            Debug.Assert(IsKnock(levitate.Effects[0]), "IsKnock() is incorrect!");
        }

        public static void TestIsKnockBack()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnockBack(repulsionField.Effects[0]), "IsKnockBack() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockBack(fissure.Effects[1]) == false, "IsKnockBack() is incorrect!");
        }

        public static void TestIsKnockDown()
        {
            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockDown(fissure.Effects[1]), "IsKnockDown() is incorrect!");

            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnockDown(repulsionField.Effects[0]) == false, "IsKnockDown() is incorrect!");
        }

        public static void TestIsKnockUp()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            Debug.Assert(IsKnockUp(levitate.Effects[0]), "IsKnockUp() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockUp(fissure.Effects[1]) == false, "IsKnockUp() is incorrect!");
        }

        public static void TestIsKnockResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockResistance(rooted.Effects[0]), "IsKnockResistance() is incorrect!");
        }

        public static void TestIsKnockBackResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockBackResistance(rooted.Effects[0]), "IsKnockBackResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockBackResistance(brimstoneArmor.Effects[0]) == false, "IsKnockBackResistance() is incorrect!");
        }

        public static void TestIsKnockDownResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockDownResistance(rooted.Effects[0]), "IsKnockDownResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockDownResistance(brimstoneArmor.Effects[0]) == false, "IsKnockDownResistance() is incorrect!");
        }

        public static void TestIsKnockUpResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockUpResistance(rooted.Effects[1]), "IsKnockUpResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockUpResistance(brimstoneArmor.Effects[0]) == false, "IsKnockUpResistance() is incorrect!");
        }

        public static void TestIsRepel()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower forceBubble = forceField.Powers[8];
            Debug.Assert(IsRepel(forceBubble.Effects[0]), "IsRepel() is incorrect!");

            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsRepel(repulsionField.Effects[0]) == false, "IsRepel() is incorrect!");
        }

        public static void TestIsRepelResistance()
        {
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower activeDefense = shieldDefense.Powers[3];
            Debug.Assert(IsRepelResistance(activeDefense.Effects[16]), "IsRepelResistance() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsRepelResistance(deflection.Effects[1]) == false, "IsRepelResistance() is incorrect!");
        }

        public static void TestIsHeal()
        {
            IPowerset painDomination = DatabaseAPI.GetPowersetByName("Pain Domination", Enums.ePowerSetType.Primary);
            IPower soothe = painDomination.Powers[1];
            Debug.Assert(IsHeal(soothe.Effects[0]), "IsHeal() is incorrect!");

            IPowerset plantControl = DatabaseAPI.GetPowersetByName("Plant Control", Enums.ePowerSetType.Primary);
            IPower spiritTree = plantControl.Powers[5];
            Debug.Assert(IsHeal(spiritTree.Effects[0]) == false, "IsHeal() is incorrect!");
        }

        public static void TestIsHitPoints()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEmbrace = stoneArmor.Powers[2];
            Debug.Assert(IsHitPoints(earthsEmbrace.Effects[1]), "IsHitPoints() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower healingFlame = fieryAura.Powers[2];
            Debug.Assert(IsHitPoints(healingFlame.Effects[1]) == false, "IsHitPoints() is incorrect!");
        }

        public static void TestIsRegeneration()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsRegeneration(health.Effects[0]), "IsRegeneration() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower healingFlame = fieryAura.Powers[2];
            Debug.Assert(IsRegeneration(healingFlame.Effects[1]) == false, "IsRegeneration() is incorrect!");
        }

        public static void TestIsRecovery()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsRecovery(stamina.Effects[0]), "IsRecovery() is incorrect!");

            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            Debug.Assert(IsRecovery(conservePower.Effects[0]) == false, "IsRecovery() is incorrect!");
        }

        public static void TestIsEnduranceDiscount()
        {
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            Debug.Assert(IsEnduranceDiscount(conservePower.Effects[0]), "IsEnduranceDiscount() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsEnduranceDiscount(stamina.Effects[0]) == false, "IsEnduranceDiscount() is incorrect!");
        }

        public static void TestIsEnduranceIncrease()
        {
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2736);
            IPower superiorConditioning = energyMastery.Powers[0];
            Debug.Assert(IsEnduranceIncrease(superiorConditioning.Effects[0]), "IsEnduranceIncrease() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsEnduranceIncrease(stamina.Effects[0]) == false, "IsEnduranceIncrease() is incorrect!");
        }

        public static void TestIsRechargeReduction()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower hasten = speed.Powers[1];
            Debug.Assert(IsRechargeReduction(hasten.Effects[0], archetype), "IsRechargeReduction() is incorrect!");

            IPowerset iceMelee = DatabaseAPI.GetPowersetByName("Ice Melee", Enums.ePowerSetType.Primary);
            IPower frozenFists = iceMelee.Powers[0];
            Debug.Assert(IsRechargeReduction(frozenFists.Effects[8], archetype) == false, "IsRechargeReduction() is incorrect!");
        }

        public static void TestIsSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsMovementSlow(timesJuncture.Effects[18], archetype), "IsMovementSlow() is incorrect!");

            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsRechargeSlow(webGrenade.Effects[2], archetype), "IsRechargeSlow() is incorrect!");
        }

        public static void TestIsMovementSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsMovementSlow(timesJuncture.Effects[18], archetype), "IsMovementSlow() is incorrect!");

            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsMovementSlow(webGrenade.Effects[2], archetype) == false, "IsMovementSlow() is incorrect!");
        }

        public static void TestIsRechargeSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsRechargeSlow(webGrenade.Effects[2], archetype), "IsRechargeSlow() is incorrect!");

            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsRechargeSlow(timesJuncture.Effects[18], archetype) == false, "IsRechargeSlow() is incorrect!");
        }

        public static void TestIsSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsSlowResistance(speedBoost.Effects[4]), "IsSlowResistance() is incorrect!");

            Debug.Assert(IsSlowResistance(speedBoost.Effects[5]), "IsSlowResistance() is incorrect!");
        }

        public static void TestIsMovementSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsMovementSlowResistance(speedBoost.Effects[5]), "IsMovementSlowResistance() is incorrect!");

            Debug.Assert(IsMovementSlowResistance(speedBoost.Effects[4]) == false, "IsMovementSlowResistance() is incorrect!");
        }

        public static void TestIsRechargeSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsRechargeSlowResistance(speedBoost.Effects[4]), "IsRechargeSlowResistance() is incorrect!");

            Debug.Assert(IsRechargeSlowResistance(speedBoost.Effects[5]) == false, "IsRechargeSlowResistance() is incorrect!");
        }
    }
}

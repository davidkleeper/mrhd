﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AILogic
{
    public static class PowerLogic
    {
        public static bool IsDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            return IsAllDefense(power, archetype)
                || IsMeleeDefense(power, archetype)
                || IsRangedDefense(power, archetype)
                || IsAoEDefense(power, archetype)
                || IsSmashingDefense(power, archetype)
                || IsLethalDefense(power, archetype)
                || IsFireDefense(power, archetype)
                || IsColdDefense(power, archetype)
                || IsEnergyDefense(power, archetype)
                || IsNegativeEnergyDefense(power, archetype)
                || IsPsionicDefense(power, archetype);
        }

        public static bool IsAllDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsAllDefense(effect, archetype)) return true;
            }
            return IsMeleeDefense(power, archetype)
                && IsRangedDefense(power, archetype)
                && IsAoEDefense(power, archetype)
                && IsSmashingDefense(power, archetype)
                && IsLethalDefense(power, archetype)
                && IsFireDefense(power, archetype)
                && IsColdDefense(power, archetype)
                && IsEnergyDefense(power, archetype)
                && IsNegativeEnergyDefense(power, archetype)
                && IsPsionicDefense(power, archetype);
        }

        public static bool IsMeleeDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsMeleeDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsRangedDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRangedDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsAoEDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsAoEDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsSmashingDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsSmashingDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsLethalDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsLethalDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsFireDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsFireDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsColdDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsColdDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsEnergyDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsEnergyDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsNegativeEnergyDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsNegativeEnergyDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsPsionicDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsPsionicDefense(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            return IsSmashingResistance(power, archetype)
                || IsLethalResistance(power, archetype)
                || IsFireResistance(power, archetype)
                || IsColdResistance(power, archetype)
                || IsEnergyResistance(power, archetype)
                || IsNegativeEnergyResistance(power, archetype)
                || IsPsionicResistance(power, archetype)
                || IsToxicResistance(power, archetype);
        }

        public static bool IsSmashingResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsSmashingResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsLethalResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsLethalResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsFireResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsFireResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsColdResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsColdResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsEnergyResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsEnergyResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsNegativeEnergyResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsNegativeEnergyResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsPsionicResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsPsionicResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsToxicResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsToxicResistance(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsDamage(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsDamage(effect)) return true;
            }
            return false;
        }

        public static bool IsAccuracy(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsAccuracy(effect)) return true;
            }
            return false;
        }

        public static bool IsTravelPower(IPower power, Base.Data_Classes.Archetype archetype)
        {
            return IsFly(power, archetype) || IsJump(power, archetype) || IsRun(power, archetype) || IsTeleport(power, archetype);
        }

        public static bool IsFly(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsFly(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsJump(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsJump(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsRun(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRun(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsTeleport(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsTeleport(effect)) return true;
            }
            return false;
        }

        public static bool IsControl(IPower power)
        {
            return IsConfuse(power) || IsHold(power) || IsImmobilize(power) || IsSleep(power) || IsStun(power) || IsTerrorize(power);
        }

        public static bool IsConfuse(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsConfuse(effect)) return true;
            }
            return false;
        }

        public static bool IsHold(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsHold(effect)) return true;
            }
            return false;
        }

        public static bool IsImmobilize(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsImmobilize(effect)) return true;
            }
            return false;
        }

        public static bool IsSleep(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsSleep(effect)) return true;
            }
            return false;
        }

        public static bool IsStun(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsStun(effect)) return true;
            }
            return false;
        }

        public static bool IsTerrorize(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsTerrorize(effect)) return true;
            }
            return false;
        }

        public static bool IsControlResistance(IPower power)
        {
            return IsConfuseResistance(power) || IsHoldResistance(power) || IsImmobilizeResistance(power) || IsSleepResistance(power) || IsStunResistance(power) || IsTerrorizeResistance(power);
        }

        public static bool IsConfuseResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsConfuseResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsHoldResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsHoldResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsImmobilizeResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsImmobilizeResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsSleepResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsSleepResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsStunResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsStunResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsTerrorizeResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsTerrorizeResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsKnock(IPower power)
        {
            return IsKnockBack(power) || IsKnockDown(power) || IsKnockUp(power);
        }

        public static bool IsKnockBack(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockBack(effect)) return true;
            }
            return false;
        }

        public static bool IsKnockDown(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockDown(effect)) return true;
            }
            return false;
        }

        public static bool IsKnockUp(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockUp(effect)) return true;
            }
            return false;
        }

        public static bool IsKnockResistance(IPower power)
        {
            return IsKnockBackResistance(power) || IsKnockDownResistance(power) || IsKnockUpResistance(power);
        }

        public static bool IsKnockBackResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockBackResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsKnockDownResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockDownResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsKnockUpResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsKnockUpResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsRepel(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRepel(effect)) return true;
            }
            return false;
        }

        public static bool IsRepelResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRepelResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsHeal(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsHeal(effect)) return true;
            }
            return false;
        }

        public static bool IsHitPoints(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsHitPoints(effect)) return true;
            }
            return false;
        }

        public static bool IsRegeneration(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRegeneration(effect)) return true;
            }
            return false;
        }

        public static bool IsRecovery(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRecovery(effect)) return true;
            }
            return false;
        }

        public static bool IsEnduranceDiscount(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsEnduranceDiscount(effect)) return true;
            }
            return false;
        }

        public static bool IsEnduranceIncrease(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsEnduranceIncrease(effect)) return true;
            }
            return false;
        }

        public static bool IsRechargeReduction(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRechargeReduction(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsSlow(IPower power, Base.Data_Classes.Archetype archetype)
        {
            return IsMovementSlow(power, archetype) || IsRechargeSlow(power, archetype);
        }

        public static bool IsMovementSlow(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsMovementSlow(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsRechargeSlow(IPower power, Base.Data_Classes.Archetype archetype)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRechargeSlow(effect, archetype)) return true;
            }
            return false;
        }

        public static bool IsSlowResistance(IPower power)
        {
            return IsMovementSlowResistance(power) || IsRechargeSlowResistance(power);
        }

        public static bool IsMovementSlowResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsMovementSlowResistance(effect)) return true;
            }
            return false;
        }

        public static bool IsRechargeSlowResistance(IPower power)
        {
            foreach (IEffect effect in power.Effects)
            {
                if (AILogic.EffectLogic.IsRechargeSlowResistance(effect)) return true;
            }
            return false;
        }

        public static void TestNewCode()
        {
            TestIsDefense();
            TestIsAllDefense();
            TestIsMeleeDefense();
            TestIsRangedDefense();
            TestIsAoEDefense();
            TestIsSmashingDefense();
            TestIsLethalDefense();
            TestIsFireDefense();
            TestIsColdDefense();
            TestIsEnergyDefense();
            TestIsNegativeEnergyDefense();
            TestIsPsionicDefense();
            TestIsResistance();
            TestIsSmashingResistance();
            TestIsLethalResistance();
            TestIsFireResistance();
            TestIsColdResistance();
            TestIsEnergyResistance();
            TestIsNegativeEnergyResistance();
            TestIsPsionicResistance();
            TestIsToxicResistance();
            TestIsDamage();
            TestIsAccuracy();
            TestIsTravelPower();
            TestIsFly();
            TestIsJump();
            TestIsRun();
            TestIsTeleport();
            TestIsControl();
            TestIsConfuse();
            TestIsHold();
            TestIsImmobilize();
            TestIsSleep();
            TestIsStun();
            TestIsTerrorize();
            TestIsControlResistance();
            TestIsConfuseResistance();
            TestIsHoldResistance();
            TestIsImmobilizeResistance();
            TestIsSleepResistance();
            TestIsStunResistance();
            TestIsTerrorizeResistance();
            TestIsKnock();
            TestIsKnockBack();
            TestIsKnockDown();
            TestIsKnockUp();
            TestIsKnockResistance();
            TestIsKnockBackResistance();
            TestIsKnockDownResistance();
            TestIsKnockUpResistance();
            TestIsRepel();
            TestIsRepelResistance();
            TestIsHeal();
            TestIsHitPoints();
            TestIsRegeneration();
            TestIsRecovery();
            TestIsEnduranceDiscount();
            TestIsEnduranceIncrease();
            TestIsRechargeReduction();
            TestIsSlow();
            TestIsMovementSlow();
            TestIsRechargeSlow();
            TestIsSlowResistance();
            TestIsMovementSlowResistance();
            TestIsRechargeSlowResistance();
        }

        public static void TestIsDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            Debug.Assert(IsDefense(personalForceField, archetype), "IsDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsDefense(deflection, archetype), "IsDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsDefense(battleAgility, archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(battleAgility, archetype), "IsDefense() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsDefense(rockArmor, archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(rockArmor, archetype), "IsDefense() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsDefense(wetIce, archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(wetIce, archetype), "IsDefense() is incorrect!");

            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsDefense(crystalArmor, archetype), "IsDefense() is incorrect!");

            Debug.Assert(IsDefense(crystalArmor, archetype), "IsDefense() is incorrect!");

            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsDefense(minerals, archetype), "IsDefense() is incorrect!");
        }

        public static void TestIsAllDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            Debug.Assert(IsAllDefense(personalForceField, archetype), "IsAllDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsAllDefense(battleAgility, archetype) == false, "IsAllDefense() is incorrect!");
        }

        public static void TestIsMeleeDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsMeleeDefense(deflection, archetype), "IsMeleeDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsMeleeDefense(battleAgility, archetype) == false, "IsMeleeDefense() is incorrect!");
        }

        public static void TestIsRangedDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsRangedDefense(battleAgility, archetype), "IsRangedDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsRangedDefense(deflection, archetype) == false, "IsRangedDefense() is incorrect!");
        }

        public static void TestIsAoEDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            Debug.Assert(IsAoEDefense(battleAgility, archetype), "IsAoEDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsAoEDefense(deflection, archetype) == false, "IsAoEDefense() is incorrect!");
        }

        public static void TestIsSmashingDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsSmashingDefense(rockArmor, archetype), "IsSmashingDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsSmashingDefense(fireShield, archetype) == false, "IsSmashingDefense() is incorrect!");
        }

        public static void TestIsLethalDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsLethalDefense(rockArmor, archetype), "IsLethalDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsLethalDefense(fireShield, archetype) == false, "IsLethalDefense() is incorrect!");
        }

        public static void TestIsFireDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsFireDefense(wetIce, archetype), "IsFireDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsFireDefense(temperatureProtection, archetype) == false, "IsFireDefense() is incorrect!");
        }

        public static void TestIsColdDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsColdDefense(wetIce, archetype), "IsColdDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsColdDefense(temperatureProtection, archetype) == false, "IsColdDefense() is incorrect!");
        }

        public static void TestIsEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsEnergyDefense(crystalArmor, archetype), "IsEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsEnergyDefense(plasmaShield, archetype) == false, "IsEnergyDefense() is incorrect!");
        }

        public static void TestIsNegativeEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsNegativeEnergyDefense(crystalArmor, archetype), "IsNegativeEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsNegativeEnergyDefense(plasmaShield, archetype) == false, "IsNegativeEnergyDefense() is incorrect!");
        }

        public static void TestIsPsionicDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsPsionicDefense(minerals, archetype), "IsPsionicDefense() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsPsionicDefense(strengthOfWill, archetype) == false, "IsPsionicDefense() is incorrect!");
        }

        public static void TestIsResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsResistance(fireShield, archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(fireShield, archetype), "IsResistance() is incorrect!");

            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsResistance(temperatureProtection, archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(temperatureProtection, archetype), "IsResistance() is incorrect!");

            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsResistance(plasmaShield, archetype), "IsResistance() is incorrect!");

            Debug.Assert(IsResistance(plasmaShield, archetype), "IsResistance() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsResistance(strengthOfWill, archetype), "IsResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            Debug.Assert(IsResistance(earthsEnbrace, archetype), "IsResistance() is incorrect!");
        }

        public static void TestIsSmashingResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsSmashingResistance(fireShield, archetype), "IsSmashingResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsSmashingResistance(rockArmor, archetype) == false, "IsSmashingResistance() is incorrect!");
        }

        public static void TestIsLethalResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            Debug.Assert(IsLethalResistance(fireShield, archetype), "IsLethalResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            Debug.Assert(IsLethalResistance(rockArmor, archetype) == false, "IsLethalResistance() is incorrect!");
        }

        public static void TestIsFireResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsFireResistance(temperatureProtection, archetype), "IsFireResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            Debug.Assert(IsFireResistance(wetIce, archetype) == false, "IsFireResistance() is incorrect!");
        }

        public static void TestIsColdResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            Debug.Assert(IsColdResistance(temperatureProtection, archetype), "IsColdResistance() is incorrect!");
        }

        public static void TestIsEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsEnergyResistance(plasmaShield, archetype), "IsEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsEnergyResistance(crystalArmor, archetype) == false, "IsEnergyResistance() is incorrect!");
        }

        public static void TestIsNegativeEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            Debug.Assert(IsNegativeEnergyResistance(plasmaShield, archetype), "IsNegativeEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            Debug.Assert(IsNegativeEnergyResistance(crystalArmor, archetype) == false, "IsNegativeEnergyResistance() is incorrect!");
        }

        public static void TestIsPsionicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            Debug.Assert(IsPsionicResistance(strengthOfWill, archetype), "IsPsionicResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            Debug.Assert(IsPsionicResistance(minerals, archetype) == false, "IsPsionicResistance() is incorrect!");
        }

        public static void TestIsToxicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            Debug.Assert(IsToxicResistance(earthsEnbrace, archetype), "IsToxicResistance() is incorrect!");
        }

        public static void TestIsDamage()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower mudPots = stoneArmor.Powers[3];
            Debug.Assert(IsDamage(mudPots, archetype), "IsDamage() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsDamage(brimstoneArmor, archetype) == false, "IsDamage() is incorrect!");
        }

        public static void TestIsAccuracy()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
            IPower focusedAccuracy = maceMastery.Powers[3];
            Debug.Assert(IsAccuracy(focusedAccuracy, archetype), "IsAccuracy() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsAccuracy(brimstoneArmor, archetype) == false, "IsAccuracy() is incorrect!");
        }

        public static void TestIsTravelPower()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsTravelPower(fly, archetype), "IsTravelPower() is incorrect!");

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsTravelPower(superJump, archetype), "IsTravelPower() is incorrect!");

            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsTravelPower(superSpeed, archetype), "IsTravelPower() is incorrect!");

            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsTravelPower(teleport, archetype), "IsTravelPower() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower mudPots = stoneArmor.Powers[3];
            Debug.Assert(IsTravelPower(mudPots, archetype) == false, "IsTravelPower() is incorrect!");
        }

        public static void TestIsFly()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsFly(fly, archetype), "IsFly() is incorrect!");

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsFly(superJump, archetype) == false, "IsFly() is incorrect!");
        }

        public static void TestIsJump()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            Debug.Assert(IsJump(superJump, archetype), "IsJump() is incorrect!");

            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            Debug.Assert(IsJump(fly, archetype) == false, "IsJump() is incorrect!");
        }

        public static void TestIsRun()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsRun(superSpeed, archetype), "IsRun() is incorrect!");

            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsRun(teleport, archetype) == false, "IsRun() is incorrect!");
        }

        public static void TestIsTeleport()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            Debug.Assert(IsTeleport(teleport, archetype), "IsTeleport() is incorrect!");

            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Debug.Assert(IsTeleport(superSpeed, archetype) == false, "IsTeleport() is incorrect!");
        }

        public static void TestIsControl()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsControl(confuse), "IsControl() is incorrect!");

            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsControl(dominate), "IsControl() is incorrect!");

            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsControl(ringOfFire), "IsControl() is incorrect!");

            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsControl(mesmerize), "IsControl() is incorrect!");

            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsControl(flashFire), "IsControl() is incorrect!");

            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsControl(terrify), "IsControl() is incorrect!");
        }

        public static void TestIsConfuse()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsConfuse(confuse), "IsConfuse() is incorrect!");

            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsConfuse(dominate) == false, "IsConfuse() is incorrect!");
        }

        public static void TestIsHold()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower dominate = mindControl.Powers[2];
            Debug.Assert(IsHold(dominate), "IsHold() is incorrect!");

            IPower confuse = mindControl.Powers[3];
            Debug.Assert(IsHold(confuse) == false, "IsHold() is incorrect!");
        }

        public static void TestIsImmobilize()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsImmobilize(ringOfFire), "IsImmobilize() is incorrect!");

            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsImmobilize(flashFire) == false, "IsImmobilize() is incorrect!");
        }

        public static void TestIsSleep()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsSleep(mesmerize), "IsSleep() is incorrect!");

            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsSleep(terrify) == false, "IsSleep() is incorrect!");
        }

        public static void TestIsStun()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower flashFire = fireControl.Powers[5];
            Debug.Assert(IsStun(flashFire), "IsStun() is incorrect!");

            IPower ringOfFire = fireControl.Powers[2];
            Debug.Assert(IsStun(ringOfFire) == false, "IsStun() is incorrect!");
        }

        public static void TestIsTerrorize()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower terrify = mindControl.Powers[7];
            Debug.Assert(IsTerrorize(terrify), "IsTerrorize() is incorrect!");

            IPower mesmerize = mindControl.Powers[0];
            Debug.Assert(IsTerrorize(mesmerize) == false, "IsTerrorize() is incorrect!");
        }

        public static void TestIsControlResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsControlResistance(tactics), "IsControlResistance() is incorrect!");

            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsControlResistance(dispersionBubble), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(dispersionBubble), "IsControlResistance() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsControlResistance(health), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(dispersionBubble), "IsControlResistance() is incorrect!");

            Debug.Assert(IsControlResistance(tactics), "IsControlResistance() is incorrect!");
        }

        public static void TestIsConfuseResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsConfuseResistance(tactics), "IsConfuseResistance() is incorrect!");

            IPower assault = leadership.Powers[1];
            Debug.Assert(IsConfuseResistance(assault) == false, "IsConfuseResistance() is incorrect!");
        }

        public static void TestIsHoldResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsHoldResistance(dispersionBubble), "IsHoldResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsHoldResistance(tactics) == false, "IsHoldResistance() is incorrect!");
        }

        public static void TestIsImmobilizeResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsImmobilizeResistance(dispersionBubble), "IsImmobilizeResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsImmobilizeResistance(tactics) == false, "IsImmobilizeResistance() is incorrect!");
        }

        public static void TestIsSleepResistance()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsSleepResistance(health), "IsSleepResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsSleepResistance(tactics) == false, "IsSleepResistance() is incorrect!");
        }

        public static void TestIsStunResistance()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            Debug.Assert(IsStunResistance(dispersionBubble), "IsStunResistance() is incorrect!");

            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsStunResistance(tactics) == false, "IsStunResistance() is incorrect!");
        }

        public static void TestIsTerrorizeResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            Debug.Assert(IsTerrorizeResistance(tactics), "IsTerrorizeResistance() is incorrect!");

            IPower assault = leadership.Powers[1];
            Debug.Assert(IsTerrorizeResistance(assault) == false, "IsTerrorizeResistance() is incorrect!");
        }

        public static void TestIsKnock()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnock(repulsionField), "IsKnock() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnock(fissure), "IsKnock() is incorrect!");

            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            Debug.Assert(IsKnock(levitate), "IsKnock() is incorrect!");
        }

        public static void TestIsKnockBack()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnockBack(repulsionField), "IsKnockBack() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockBack(fissure) == false, "IsKnockBack() is incorrect!");
        }

        public static void TestIsKnockDown()
        {
            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockDown(fissure), "IsKnockDown() is incorrect!");

            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsKnockDown(repulsionField) == false, "IsKnockDown() is incorrect!");
        }

        public static void TestIsKnockUp()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            Debug.Assert(IsKnockUp(levitate), "IsKnockUp() is incorrect!");

            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            Debug.Assert(IsKnockUp(fissure) == false, "IsKnockUp() is incorrect!");
        }

        public static void TestIsKnockResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockResistance(rooted), "IsKnockResistance() is incorrect!");
        }

        public static void TestIsKnockBackResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockBackResistance(rooted), "IsKnockBackResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockBackResistance(brimstoneArmor) == false, "IsKnockBackResistance() is incorrect!");
        }

        public static void TestIsKnockDownResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockDownResistance(rooted), "IsKnockDownResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockDownResistance(brimstoneArmor) == false, "IsKnockDownResistance() is incorrect!");
        }

        public static void TestIsKnockUpResistance()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            Debug.Assert(IsKnockUpResistance(rooted), "IsKnockUpResistance() is incorrect!");

            IPower brimstoneArmor = stoneArmor.Powers[5];
            Debug.Assert(IsKnockUpResistance(brimstoneArmor) == false, "IsKnockUpResistance() is incorrect!");
        }

        public static void TestIsRepel()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower forceBubble = forceField.Powers[8];
            Debug.Assert(IsRepel(forceBubble), "IsRepel() is incorrect!");

            IPower repulsionField = forceField.Powers[6];
            Debug.Assert(IsRepel(repulsionField) == false, "IsRepel() is incorrect!");
        }

        public static void TestIsRepelResistance()
        {
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower activeDefense = shieldDefense.Powers[3];
            Debug.Assert(IsRepelResistance(activeDefense), "IsRepelResistance() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            Debug.Assert(IsRepelResistance(deflection) == false, "IsRepelResistance() is incorrect!");
        }

        public static void TestIsHeal()
        {
            IPowerset painDomination = DatabaseAPI.GetPowersetByName("Pain Domination", Enums.ePowerSetType.Primary);
            IPower soothe = painDomination.Powers[1];
            Debug.Assert(IsHeal(soothe), "IsHeal() is incorrect!");

            IPowerset plantControl = DatabaseAPI.GetPowersetByName("Plant Control", Enums.ePowerSetType.Primary);
            IPower spiritTree = plantControl.Powers[5];
            Debug.Assert(IsHeal(spiritTree) == false, "IsHeal() is incorrect!");
        }

        public static void TestIsHitPoints()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEmbrace = stoneArmor.Powers[2];
            Debug.Assert(IsHitPoints(earthsEmbrace), "IsHitPoints() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower healingFlame = fieryAura.Powers[2];
            Debug.Assert(IsHitPoints(healingFlame) == false, "IsHitPoints() is incorrect!");
        }

        public static void TestIsRegeneration()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            Debug.Assert(IsRegeneration(health), "IsRegeneration() is incorrect!");

            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            Debug.Assert(IsRecovery(conservePower) == false, "IsRecovery() is incorrect!");
        }

        public static void TestIsRecovery()
        {
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsRecovery(stamina), "IsRecovery() is incorrect!");

            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            Debug.Assert(IsRecovery(conservePower) == false, "IsRecovery() is incorrect!");
        }

        public static void TestIsEnduranceDiscount()
        {
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            Debug.Assert(IsEnduranceDiscount(conservePower), "IsEnduranceDiscount() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsEnduranceDiscount(stamina) == false, "IsEnduranceDiscount() is incorrect!");
        }

        public static void TestIsEnduranceIncrease()
        {
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2736);
            IPower superiorConditioning = energyMastery.Powers[0];
            Debug.Assert(IsEnduranceIncrease(superiorConditioning), "IsEnduranceIncrease() is incorrect!");

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            Debug.Assert(IsEnduranceIncrease(stamina) == false, "IsEnduranceIncrease() is incorrect!");
        }

        public static void TestIsRechargeReduction()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower hasten = speed.Powers[1];
            Debug.Assert(IsRechargeReduction(hasten, archetype), "IsRechargeReduction() is incorrect!");

            IPowerset iceMelee = DatabaseAPI.GetPowersetByName("Ice Melee", Enums.ePowerSetType.Primary);
            IPower frozenFists = iceMelee.Powers[0];
            Debug.Assert(IsRechargeReduction(frozenFists, archetype) == false, "IsRechargeReduction() is incorrect!");
        }

        public static void TestIsSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsSlow(timesJuncture, archetype), "IsSlow() is incorrect!");

            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsSlow(webGrenade, archetype), "IsSlow() is incorrect!");
        }

        public static void TestIsMovementSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsMovementSlow(timesJuncture, archetype), "IsMovementSlow() is incorrect!");

            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsMovementSlow(webGrenade, archetype) == false, "IsMovementSlow() is incorrect!");
        }

        public static void TestIsRechargeSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            Debug.Assert(IsRechargeSlow(webGrenade, archetype), "IsRechargeSlow() is incorrect!");

            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            Debug.Assert(IsRechargeSlow(timesJuncture, archetype) == false, "IsRechargeSlow() is incorrect!");
        }

        public static void TestIsSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsSlowResistance(speedBoost), "IsSlowResistance() is incorrect!");
        }

        public static void TestIsMovementSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsMovementSlowResistance(speedBoost), "IsMovementSlowResistance() is incorrect!");
        }

        public static void TestIsRechargeSlowResistance()
        {
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            Debug.Assert(IsRechargeSlowResistance(speedBoost), "IsRechargeSlowResistance() is incorrect!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AILogic
{
    public static class EnhancementLogic
    {
        public static bool IsDefense(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsDefense(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsResistance(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsResistance(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsDamage(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsDamage(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsAccuracy(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsAccuracy(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsFly(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsFly(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsJump(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsJump(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsRun(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsRun(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsConfuse(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsConfuse(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsHold(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsHold(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsImmobilize(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsImmobilize(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsSleep(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsSleep(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsStun(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsStun(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsTerrorize(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsTerrorize(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsKnockBack(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsKnockBack(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsHeal(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsHeal(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsRecovery(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsRecovery(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsEnduranceDiscount(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsEnduranceDiscount(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsRechargeReduction(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsRechargeReduction(effect.Enhance)) return true;
            }
            return false;
        }

        public static bool IsSlow(IEnhancement enhancement)
        {
            foreach (Enums.sEffect effect in enhancement.Effect)
            {
                if (AILogic.STwinIDLogic.IsSlow(effect.Enhance)) return true;
            }
            return false;
        }

        public static void TestNewCode()
        {
            TestIsDefense();
            TestIsResistance();
            TestIsDamage();
            TestIsAccuracy();
            TestIsFly();
            TestIsJump();
            TestIsRun();
            TestIsConfuse();
            TestIsHold();
            TestIsImmobilize();
            TestIsSleep();
            TestIsStun();
            TestIsTerrorize();
            TestIsKnockBack();
            TestIsHeal();
            TestIsRecovery();
            TestIsEnduranceDiscount();
            TestIsRechargeReduction();
            TestIsSlow();
        }

        public static void TestIsDefense()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Defense);
            Debug.Assert(IsDefense(enhancement), "IsDefense() is incorrect!");
        }

        public static void TestIsResistance()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Resistance);
            Debug.Assert(IsResistance(enhancement), "IsResistance() is incorrect!");
        }

        public static void TestIsDamage()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            Debug.Assert(IsDamage(enhancement), "IsDamage() is incorrect!");
        }

        public static void TestIsAccuracy()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
            Debug.Assert(IsDamage(enhancement), "IsDamage() is incorrect!");
        }

        public static void TestIsFly()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Fly);
            Debug.Assert(IsFly(enhancement), "IsFly() is incorrect!");
        }

        public static void TestIsJump()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Jump);
            Debug.Assert(IsJump(enhancement), "IsJump() is incorrect!");
        }

        public static void TestIsRun()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Run);
            Debug.Assert(IsRun(enhancement), "IsRun() is incorrect!");
        }

        public static void TestIsConfuse()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Confuse);
            Debug.Assert(IsConfuse(enhancement), "IsConfuse() is incorrect!");
        }

        public static void TestIsHold()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Hold);
            Debug.Assert(IsHold(enhancement), "IsHold() is incorrect!");
        }

        public static void TestIsImmobilize()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Immobilize);
            Debug.Assert(IsImmobilize(enhancement), "IsImmobilize() is incorrect!");
        }

        public static void TestIsSleep()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Sleep);
            Debug.Assert(IsSleep(enhancement), "IsSleep() is incorrect!");
        }

        public static void TestIsStun()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Stun);
            Debug.Assert(IsStun(enhancement), "IsStun() is incorrect!");
        }

        public static void TestIsTerrorize()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Fear);
            Debug.Assert(IsTerrorize(enhancement), "IsTerrorize() is incorrect!");
        }

        public static void TestIsKnockBack()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Knockback);
            Debug.Assert(IsKnockBack(enhancement), "IsKnockBack() is incorrect!");
        }

        public static void TestIsHeal()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Heal);
            Debug.Assert(IsHeal(enhancement), "IsHeal() is incorrect!");
        }

        public static void TestIsRecovery()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceModification);
            Debug.Assert(IsRecovery(enhancement), "IsRecovery() is incorrect!");
        }

        public static void TestIsEnduranceDiscount()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
            Debug.Assert(IsEnduranceDiscount(enhancement), "IsEnduranceDiscount() is incorrect!");
        }

        public static void TestIsRechargeReduction()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Recharge);
            Debug.Assert(IsRechargeReduction(enhancement), "IsRechargeReduction() is incorrect!");
        }

        public static void TestIsSlow()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Slow);
            Debug.Assert(IsSlow(enhancement), "IsSlow() is incorrect!");
        }
    }
}

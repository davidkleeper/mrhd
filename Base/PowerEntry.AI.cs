﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public partial class PowerEntry : ICloneable
{
    public static List<PowerEntry> SortByLevel(List<PowerEntry> powerEntries)
    {
        return powerEntries.OrderBy(powerEntry => powerEntry.Level).ToList<PowerEntry>();
    }

    public override string ToString()
    {
        return Power?.DisplayName ?? "PowerEntry";
    }

    public int GetEnhancementCount(IEnhancement enhancement)
    {
        int count = 0;
        foreach (SlotEntry slot in Slots)
        {
            if (-1 == slot.Enhancement.Enh)
                continue;
            IEnhancement e = DatabaseAPI.GetEnhancementByIndex(slot.Enhancement.Enh);
            if (null == e)
                continue;
            if (enhancement.UIDSet == e.UIDSet && enhancement.Name == e.Name)
                count++;
        }
        return count;
    }

    public bool HasEnhancement(IEnhancement enhancement)
    {
        return 0 < GetEnhancementCount(enhancement);
    }

    public bool CanAddEnhancement(IEnhancement enhancement)
    {
        if (enhancement.Unique && HasEnhancement(enhancement))
            return false;
        if ("" != enhancement.UIDSet && HasEnhancement(enhancement))
            return false;
        if ("" == enhancement.UIDSet && 3 <= GetEnhancementCount(enhancement))
            return false;
        if ("" != enhancement.UIDSet)
        {
            int[] validEnhancements = Power.GetValidEnhancementsFromSets();
            bool valid = validEnhancements.Any(item => DatabaseAPI.GetEnhancementByIndex(item).LongName == enhancement.LongName);
            if (!valid)
                return false;
        }
        else
        {
            EnhancementType[] enhancementTypes = enhancement.GetEnhancementTypes();
            bool allow = enhancementTypes.Length > 0 ? false : true;
            foreach (EnhancementType enhancementType in enhancementTypes)
            {
                if (Power.AllowsEnhancement(enhancementType))
                {
                    allow = true;
                    break;
                }
            }
            if (!allow)
                return false;
        }
        IEnhancement superiorVersion = enhancement.GetSuperiorVersion();
        if (null != superiorVersion && HasEnhancement(superiorVersion))
            return false;
        if (null != superiorVersion && HasEnhancement(enhancement))
            return false;
        IEnhancement nonSuperiorVersion = enhancement.GetNonSuperiorVersion();
        if (null != nonSuperiorVersion && HasEnhancement(nonSuperiorVersion))
            return false;
        if (null != nonSuperiorVersion && HasEnhancement(enhancement))
            return false;
        return true;
    }

    public bool RemoveSlot(int slotIndex)
    {
        string msg;
        if (!CanRemoveSlot(slotIndex, out msg))
            return false;
        if (slotIndex < 1 || Slots.Length <= slotIndex)
            return false;
        List<SlotEntry> slotList = Slots.ToList();
        slotList.RemoveAt(slotIndex);
        Slots = slotList.ToArray();
        return true;
    }

    public bool DoDefaultSlotting(int slots)
    {
        if (1 > slots || 6 < slots)
            return false;
        while (slots < Slots.Length)
            RemoveSlot(Slots.Length - 1);
        while (slots > Slots.Length)
            AddSlot(-1);

        List<IEnhancement> defaultSlotting = Power.GetDefaultSlotting();
        for (int slotsAdded = 0; slotsAdded < defaultSlotting.Count; slotsAdded++)
        {
            Slots[slotsAdded].LoadFromString(defaultSlotting[slotsAdded].GetLoadString(), ":");
            if (slotsAdded == slots - 1)
                break;
        }
        return true;
    }

    public Dictionary<string, List<IEnhancement>> GetEnhancementsGroupedBySet()
    {
        Dictionary<string, List<IEnhancement>> enhancementSets = new Dictionary<string, List<IEnhancement>>();
        foreach (SlotEntry slotEntry in Slots)
        {
            int enhancementIndex = slotEntry.Enhancement.Enh;
            if (-1 == enhancementIndex)
                continue;
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementIndex);
            EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
            string enhancementSetName = "None";
            List<IEnhancement> enhancementList;
            if (null != enhancementSet)
                enhancementSetName = enhancementSet.DisplayName;
            if (!enhancementSets.ContainsKey(enhancementSetName))
                enhancementSets.Add(enhancementSetName, new List<IEnhancement>());
            if (enhancementSets.TryGetValue(enhancementSetName, out enhancementList))
            {
                enhancementList.Add(enhancement);
            }
        }
        return enhancementSets;
    }

    public int CompareTo(PowerEntry powerEntry)
    {
        if (Level != powerEntry.Level) return Level.CompareTo(powerEntry.Level);
        if (NIDPowerset != powerEntry.NIDPowerset) return NIDPowerset.CompareTo(powerEntry.NIDPowerset);
        if (IDXPower != powerEntry.IDXPower) return IDXPower.CompareTo(powerEntry.IDXPower);
        if (NIDPower != powerEntry.NIDPower) return NIDPower.CompareTo(powerEntry.NIDPower);
        if (Tag != powerEntry.Tag) return Tag.CompareTo(powerEntry.Tag);
        if (StatInclude != powerEntry.StatInclude) return StatInclude.CompareTo(powerEntry.StatInclude);
        if (VariableValue != powerEntry.VariableValue) return VariableValue.CompareTo(powerEntry.VariableValue);
        if (Slots.Length != powerEntry.Slots.Length) return Slots.Length.CompareTo(powerEntry.Slots.Length);
        for (int slotEntryIndex = 0; slotEntryIndex < Slots.Length; slotEntryIndex++)
        {
            int slotResult = Slots[slotEntryIndex].CompareTo(powerEntry.Slots[slotEntryIndex]);
            if (0 != slotResult) return slotResult;
        }
        if (SubPowers.Length != powerEntry.SubPowers.Length) return SubPowers.Length.CompareTo(powerEntry.SubPowers.Length);
        for (int powerSubEntryIndex = 0; powerSubEntryIndex < SubPowers.Length; powerSubEntryIndex++)
        {
            int powerSubEntryResult = SubPowers[powerSubEntryIndex].CompareTo(powerEntry.SubPowers[powerSubEntryIndex]);
            if (0 != powerSubEntryResult) return powerSubEntryResult;
        }
        if (Chosen != powerEntry.Chosen) return Chosen.CompareTo(powerEntry.Chosen);
        if (State != powerEntry.State) return State.CompareTo(powerEntry.State);
        if (State != powerEntry.State) return State.CompareTo(powerEntry.State);
        if (null == Power && null != powerEntry.Power) return -1;
        if (null != Power && null == powerEntry.Power) return 1;
        int powerResult = Power.CompareTo(powerEntry.Power);
        if (0 != powerResult) return powerResult;
        if (null == PowerSet && null != powerEntry.PowerSet) return -1;
        if (null != PowerSet && null == powerEntry.PowerSet) return 1;
        int powerSetResult = PowerSet.CompareTo(powerEntry.PowerSet);
        if (0 != powerSetResult) return powerSetResult;
        if (AllowFrontLoading != powerEntry.AllowFrontLoading) return AllowFrontLoading.CompareTo(powerEntry.AllowFrontLoading);
        if (Name != powerEntry.Name) return Name.CompareTo(powerEntry.Name);
        if (Virtual != powerEntry.Virtual) return Virtual.CompareTo(powerEntry.Virtual);
        if (SlotCount != powerEntry.SlotCount) return SlotCount.CompareTo(powerEntry.SlotCount);
        return 0;
    }

    public bool FollowsED()
    {
        for (int slotIndex1 = 0; slotIndex1 < SlotCount - 1; slotIndex1++)
        {
            int count = 0;
            for (int slotIndex2 = slotIndex1 + 1; slotIndex2 < SlotCount; slotIndex2++)
            {
                if (-1 != Slots[slotIndex1].Enhancement.Enh && Slots[slotIndex1].Enhancement.Enh == Slots[slotIndex2].Enhancement.Enh)
                    count++;
                if (3 < count)
                    return false;
            }
        }
        return true;
    }

    public bool Slot(int[] enhancementIndexes)
    {
        if (1 > enhancementIndexes.Length || 6 < enhancementIndexes.Length)
            return false;
        while (enhancementIndexes.Length < Slots.Length)
            RemoveSlot(Slots.Length - 1);
        while (enhancementIndexes.Length > Slots.Length)
            AddSlot(-1);

        for (int slotsAdded = 0; slotsAdded < enhancementIndexes.Length; slotsAdded++)
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementIndexes[slotsAdded]);
            if (null == enhancement)
                continue;
            Slots[slotsAdded].LoadFromString(enhancement.GetLoadString(), ":");
            if (slotsAdded == enhancementIndexes.Length - 1)
                break;
        }
        return true;
    }

    public void ClearEnhancements()
    {
        for (int slotIndex = 0; slotIndex < Slots.Length; slotIndex++)
            Slots[slotIndex].ClearEnhancements();
    }

    public static bool CanMoveSlots(PowerEntry source, PowerEntry dest, bool[] sourceSlotFlags)
    {
        if (null == source || null == dest || null == source.Slots || null == dest.Slots)
            return false;
        int sourceSlotsAvailable = source.Slots.Length - GetLockedSlotsCount(sourceSlotFlags);
        if (sourceSlotsAvailable < 2)
            return false;
        if (dest.Level == 48 && dest.Slots.Length >= 4)
            return false;
        return dest.Slots.Length < 6;
    }

    public static int GetMoveSlotCount(PowerEntry source, PowerEntry dest, bool[] sourceSlotFlags)
    {
        if (!CanMoveSlots(source, dest, sourceSlotFlags))
            return -1;

        int maxReceiveSlots = 6 - dest.Slots.Length;
        int maxSendSlots = source.Slots.Length - 1 - GetLockedSlotsCount(sourceSlotFlags);
        int maxSlots = Math.Min(maxReceiveSlots, maxSendSlots);
        Random rnd = new Random();
        int slotsToMove = rnd.Next(1, maxSlots + 1);
        return slotsToMove;
    }

    public static bool MoveSlots(PowerEntry source, PowerEntry dest, int slotsToMove, bool[] sourceSlotFlags)
    {
        if (!CanMoveSlots(source, dest, sourceSlotFlags))
            return false;
        if (0 >= slotsToMove || source.Slots.Length - 1 - GetLockedSlotsCount(sourceSlotFlags) < slotsToMove || dest.Slots.Length + slotsToMove > 6)
            return false;

        int slotsMoved = 0;
        for (int slotIndex = 1; slotIndex < source.Slots.Length; slotIndex++)
        {
            if (!IsSlotUnlocked(slotIndex, sourceSlotFlags)) continue;
            int level = source.Slots[source.Slots.Length - 1].Level;
            dest.AddSlot(level);
            source.RemoveSlot(slotIndex);
            slotsMoved++;
            if (slotsMoved == slotsToMove) break;
        }
        return true;
    }

    public static int GetLockedSlotsCount(bool[] slotFlags)
    {
        return slotFlags.Where(f => !f).Count();
    }

    public static bool IsSlotUnlocked(int slotIndex, bool[] slotFlags)
    {
        if (slotIndex >= slotFlags.Length) return false;
        return slotFlags[slotIndex];
    }

    public static void TestNewCode()
    {
        TestGetEnhancementCount();
        TestHasEnhancement();
        TestCanAddEnhancement();
        TestClearEnhancements();
        TestRemoveSlot();
        TestDoDefaultSlotting();
        TestGetEnhancementsGroupedBySet();
        TestSlot();
        TestGetLockedSlotsCount();
        TesIsSlotUnlocked();
        TestCanMoveSlots();
        TestGetMoveSlotCount();
        TestMoveSlots();
    }

    public static void TestGetEnhancementCount()
    {
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        IEnhancement endRed = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
        IEnhancement damage = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);

        mesmerize.DoDefaultSlotting(5);
        mesmerize.Slots[0].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[1].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[2].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[3].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[4].LoadFromString(endRed.GetLoadString(), ":");
        Debug.Assert(mesmerize.GetEnhancementCount(damage) == 0, "GetEnhancementCount is incorrect!");
        mesmerize.Slots[0].LoadFromString(damage.GetLoadString(), ":");
        Debug.Assert(mesmerize.GetEnhancementCount(damage) == 1, "GetEnhancementCount is incorrect!");
        mesmerize.Slots[1].LoadFromString(damage.GetLoadString(), ":");
        Debug.Assert(mesmerize.GetEnhancementCount(damage) == 2, "GetEnhancementCount is incorrect!");
        mesmerize.Slots[2].LoadFromString(damage.GetLoadString(), ":");
        Debug.Assert(mesmerize.GetEnhancementCount(damage) == 3, "GetEnhancementCount is incorrect!");
    }

    public static void TestHasEnhancement()
    {
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        EnhancementSet thunderstrike = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Thunderstrike"));
        IEnhancement thunderstrike0 = DatabaseAPI.GetEnhancementByIndex(thunderstrike.Enhancements[0]);

        mesmerize.DoDefaultSlotting(1);
        Debug.Assert(mesmerize.HasEnhancement(thunderstrike0) == false, "HasEnhancement is incorrect!");
        mesmerize.Slots[0].LoadFromString(thunderstrike0.GetLoadString(), ":");
        Debug.Assert(mesmerize.HasEnhancement(thunderstrike0) == true, "HasEnhancement is incorrect!");
    }

    public static void TestCanAddEnhancement()
    {
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        EnhancementSet apocalypse = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Apocalypse"));
        IEnhancement apocalypse0 = DatabaseAPI.GetEnhancementByIndex(apocalypse.Enhancements[0]);
        IEnhancement apocalypse1 = DatabaseAPI.GetEnhancementByIndex(apocalypse.Enhancements[1]);

        mesmerize.DoDefaultSlotting(5);
        mesmerize.Slots[0].LoadFromString(apocalypse0.GetLoadString(), ":");
        Debug.Assert(mesmerize.CanAddEnhancement(apocalypse0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(mesmerize.CanAddEnhancement(apocalypse1) == true, "CanAddEnhancement is incorrect!");

        IEnhancement damage = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage);
        IEnhancement slow = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Slow);
        IEnhancement endRed = DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction);
        mesmerize.Slots[0].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[1].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[2].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[3].LoadFromString(endRed.GetLoadString(), ":");
        mesmerize.Slots[4].LoadFromString(endRed.GetLoadString(), ":");
        Debug.Assert(mesmerize.CanAddEnhancement(damage) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(mesmerize.CanAddEnhancement(slow) == false, "CanAddEnhancement is incorrect!");
        mesmerize.Slots[0].LoadFromString(damage.GetLoadString(), ":");
        mesmerize.Slots[1].LoadFromString(damage.GetLoadString(), ":");
        mesmerize.Slots[2].LoadFromString(damage.GetLoadString(), ":");
        Debug.Assert(mesmerize.CanAddEnhancement(damage) == false, "CanAddEnhancement is incorrect!");

        PowerEntry flares = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Fire Blast", Enums.ePowerSetType.Primary).Powers[0]);
        EnhancementSet blastersWrath = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Blaster's Wrath"));
        IEnhancement blastersWrath0 = DatabaseAPI.GetEnhancementByIndex(blastersWrath.Enhancements[0]);
        IEnhancement blastersWrath1 = DatabaseAPI.GetEnhancementByIndex(blastersWrath.Enhancements[1]);
        IEnhancement superiorBlastersWrath0 = blastersWrath0.GetSuperiorVersion();
        IEnhancement superiorBlastersWrath1 = blastersWrath1.GetSuperiorVersion();

        mesmerize.DoDefaultSlotting(1);
        Debug.Assert(flares.CanAddEnhancement(blastersWrath0) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath0) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath0) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath0) == true, "CanAddEnhancement is incorrect!");
        flares.Slots[0].LoadFromString(superiorBlastersWrath0.GetLoadString(), ":");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath1) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath1) == true, "CanAddEnhancement is incorrect!");
        flares.Slots[0].LoadFromString(blastersWrath0.GetLoadString(), ":");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath0) == false, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(superiorBlastersWrath1) == true, "CanAddEnhancement is incorrect!");
        Debug.Assert(flares.CanAddEnhancement(blastersWrath1) == true, "CanAddEnhancement is incorrect!");
    }

    public static void TestClearEnhancements()
    {
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        int acc = (int)EnhancementType.Accuracy;
        mesmerize.Slot(new int[] { acc, acc, acc });
        Debug.Assert(mesmerize.SlotCount == 3, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[0].Enhancement.Enh == acc, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[1].Enhancement.Enh == acc, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[2].Enhancement.Enh == acc, "ClearEnhancements is incorrect!");
        mesmerize.ClearEnhancements();
        Debug.Assert(mesmerize.SlotCount == 3, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[0].Enhancement.Enh == -1, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[1].Enhancement.Enh == -1, "ClearEnhancements is incorrect!");
        Debug.Assert(mesmerize.Slots[2].Enhancement.Enh == -1, "ClearEnhancements is incorrect!");
    }

    public static void TestRemoveSlot()
    {
        IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
        IPower powerHealth = fitness.Powers[1];
        PowerEntry health = new PowerEntry(1, powerHealth);
        health.AddSlot(5);
        System.Diagnostics.Debug.Assert(health.Slots.Length == 2);
        bool removed = health.RemoveSlot(1);
        System.Diagnostics.Debug.Assert(health.Slots.Length == 1);
        System.Diagnostics.Debug.Assert(removed == true);
        removed = health.RemoveSlot(1);
        System.Diagnostics.Debug.Assert(health.Slots.Length == 1);
        System.Diagnostics.Debug.Assert(removed == false);
    }

    public static void TestDoDefaultSlotting()
    {
        PowerEntry health = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent).Powers[1]);
        PowerEntry stamina = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent).Powers[3]);
        PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
        PowerEntry dispersionBubble = new PowerEntry(22, DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary).Powers[5]);
        PowerEntry repulsionField = new PowerEntry(47, DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary).Powers[6]);

        Debug.Assert(health.DoDefaultSlotting(3) == true, "health DoDefaultSlotting is incorrect!");
        Debug.Assert(health.Slots.Length == 3, "health.Slots.Length is incorrect!");
        Debug.Assert(health.Slots[0].Enhancement.Enh == (int)EnhancementType.Heal, "health.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(health.Slots[1].Enhancement.Enh == (int)EnhancementType.Heal, "health.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(health.Slots[2].Enhancement.Enh == (int)EnhancementType.Heal, "health.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(stamina.DoDefaultSlotting(3) == true, "stamina DoDefaultSlotting is incorrect!");
        Debug.Assert(stamina.Slots.Length == 3, "stamina.Slots.Length is incorrect!");
        Debug.Assert(stamina.Slots[0].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "stamina.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(stamina.Slots[1].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "stamina.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(stamina.Slots[2].Enhancement.Enh == (int)EnhancementType.EnduranceModification, "stamina.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.DoDefaultSlotting(6) == true, "mesmerize DoDefaultSlotting is incorrect!");
        Debug.Assert(mesmerize.Slots.Length == 6, "mesmerize.Slots.Length is incorrect!");
        Debug.Assert(mesmerize.Slots[0].Enhancement.Enh == (int)EnhancementType.Accuracy, "mesmerize.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.Slots[1].Enhancement.Enh == (int)EnhancementType.Damage, "mesmerize.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.Slots[2].Enhancement.Enh == (int)EnhancementType.Damage, "mesmerize.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.Slots[3].Enhancement.Enh == (int)EnhancementType.Damage, "mesmerize.Slots[3].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.Slots[4].Enhancement.Enh == (int)EnhancementType.Sleep, "mesmerize.Slots[4].Enhancement.Enh is incorrect!");
        Debug.Assert(mesmerize.Slots[5].Enhancement.Enh == (int)EnhancementType.Sleep, "mesmerize.Slots[5].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.DoDefaultSlotting(6) == true, "dispersionBubble DoDefaultSlotting is incorrect!");
        Debug.Assert(dispersionBubble.Slots.Length == 6, "dispersionBubble.Slots.Length is incorrect!");
        Debug.Assert(dispersionBubble.Slots[0].Enhancement.Enh == (int)EnhancementType.Defense, "dispersionBubble.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.Slots[1].Enhancement.Enh == (int)EnhancementType.Defense, "dispersionBubble.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.Slots[2].Enhancement.Enh == (int)EnhancementType.Defense, "dispersionBubble.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.Slots[3].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "dispersionBubble.Slots[3].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.Slots[4].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "dispersionBubble.Slots[4].Enhancement.Enh is incorrect!");
        Debug.Assert(dispersionBubble.Slots[5].Enhancement.Enh == (int)EnhancementType.EnduranceReduction, "dispersionBubble.Slots[5].Enhancement.Enh is incorrect!");
        Debug.Assert(repulsionField.DoDefaultSlotting(3) == true, "repulsionField DoDefaultSlotting is incorrect!");
        Debug.Assert(repulsionField.Slots.Length == 3, "repulsionField.Slots.Length is incorrect!");
        Debug.Assert(repulsionField.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "repulsionField.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(repulsionField.Slots[1].Enhancement.Enh == (int)EnhancementType.Recharge, "repulsionField.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(repulsionField.Slots[2].Enhancement.Enh == (int)EnhancementType.Recharge, "repulsionField.Slots[2].Enhancement.Enh is incorrect!");

        Debug.Assert(repulsionField.DoDefaultSlotting(1) == true, "repulsionField DoDefaultSlotting is incorrect!");
        Debug.Assert(repulsionField.Slots.Length == 1, "repulsionField.Slots.Length is incorrect!");
        Debug.Assert(repulsionField.Slots[0].Enhancement.Enh == (int)EnhancementType.Recharge, "repulsionField.Slots[0].Enhancement.Enh is incorrect!");
    }

    public static void TestGetEnhancementsGroupedBySet()
    {
        IPowerset powerset = DatabaseAPI.GetPowersetByName("Fire Blast", Enums.ePowerSetType.Primary);
        PowerEntry flares = new PowerEntry(0, powerset.Powers[0]);
        int[] enhancements = new int[] {
            (int)EnhancementType.Accuracy,
            (int)EnhancementType.Damage,
            (int)EnhancementType.Damage,
            (int)EnhancementType.Damage,
            (int)EnhancementType.EnduranceReduction
        };
        flares.Slot(enhancements);
        Dictionary<string, List<IEnhancement>> enhancementSets = flares.GetEnhancementsGroupedBySet();
        List<IEnhancement> enhancementList;
        Debug.Assert(enhancementSets.Count == 1, "enhancementSets.Count is incorrect!");
        Debug.Assert(enhancementSets.ContainsKey("None"), "enhancementSets.ContainsKey is incorrect!");
        Debug.Assert(enhancementSets.TryGetValue("None", out enhancementList), "enhancementSets.TryGetValue is incorrect!");
        if (enhancementSets.TryGetValue("None", out enhancementList))
        {
            Debug.Assert(enhancementList.Count == 5, "enhancementList.Count is incorrect!");
            Debug.Assert(enhancementList[0].Name == "Accuracy", "enhancementList[0].Name is incorrect!");
            Debug.Assert(enhancementList[1].Name == "Damage Increase", "enhancementList[1].Name is incorrect!");
            Debug.Assert(enhancementList[2].Name == "Damage Increase", "enhancementList[2].Name is incorrect!");
            Debug.Assert(enhancementList[3].Name == "Damage Increase", "enhancementList[3].Name is incorrect!");
            Debug.Assert(enhancementList[4].Name == "Endurance Reduction", "enhancementList[4].Name is incorrect!");
        }
        EnhancementSet[] defenseBonusSets = flares.Power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefenseAll).ToArray();
        EnhancementSet set1 = defenseBonusSets[0];
        EnhancementSet set2 = defenseBonusSets[1];
        flares.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(set1.Enhancements[0]).GetLoadString(), ":");
        flares.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(set1.Enhancements[1]).GetLoadString(), ":");
        flares.Slots[2].LoadFromString(DatabaseAPI.GetEnhancementByIndex(set1.Enhancements[2]).GetLoadString(), ":");
        flares.Slots[3].LoadFromString(DatabaseAPI.GetEnhancementByIndex(set2.Enhancements[0]).GetLoadString(), ":");
        flares.Slots[4].LoadFromString(DatabaseAPI.GetEnhancementByIndex(set2.Enhancements[1]).GetLoadString(), ":");
        Debug.Assert(flares.Slots[0].Enhancement.Enh == set1.Enhancements[0], "flares.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(flares.Slots[1].Enhancement.Enh == set1.Enhancements[1], "flares.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(flares.Slots[2].Enhancement.Enh == set1.Enhancements[2], "flares.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(flares.Slots[3].Enhancement.Enh == set2.Enhancements[0], "flares.Slots[3].Enhancement.Enh is incorrect!");
        Debug.Assert(flares.Slots[4].Enhancement.Enh == set2.Enhancements[1], "flares.Slots[4].Enhancement.Enh is incorrect!");

        enhancementSets = flares.GetEnhancementsGroupedBySet();
        Debug.Assert(enhancementSets.Count == 2, "enhancementSets.Count is incorrect!");
        Debug.Assert(enhancementSets.ContainsKey("Maelstrom's Fury"), "enhancementSets.ContainsKey is incorrect!");
        Debug.Assert(enhancementSets.ContainsKey("Ruin"), "enhancementSets.ContainsKey is incorrect!");
        Debug.Assert(enhancementSets.TryGetValue("Maelstrom's Fury", out enhancementList), "enhancementSets.TryGetValue is incorrect!");
        Debug.Assert(enhancementSets.TryGetValue("Ruin", out enhancementList), "enhancementSets.TryGetValue is incorrect!");
        if (enhancementSets.TryGetValue("Maelstrom's Fury", out enhancementList))
        {
            Debug.Assert(enhancementList.Count == 3, "enhancementList.Count is incorrect!");
            Debug.Assert(enhancementList[0].Name == "Accuracy/Damage", "enhancementList[0].Name is incorrect!");
            Debug.Assert(enhancementList[1].Name == "Damage/Endurance", "enhancementList[1].Name is incorrect!");
            Debug.Assert(enhancementList[2].Name == "Damage/Recharge", "enhancementList[2].Name is incorrect!");
        }
        if (enhancementSets.TryGetValue("Ruin", out enhancementList))
        {
            Debug.Assert(enhancementList.Count == 2, "enhancementList.Count is incorrect!");
            Debug.Assert(enhancementList[0].Name == "Accuracy/Damage", "enhancementList[0].Name is incorrect!");
            Debug.Assert(enhancementList[1].Name == "Damage/Endurance", "enhancementList[2].Name is incorrect!");
        }
    }

    public static void TestSlot()
    {
        IPowerset powerset = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
        PowerEntry personalForceField = new PowerEntry(0, powerset.Powers[0]);
        int[] enhancementIndexes = { 272, 273, 274, 275, 276, 277 };
        personalForceField.Slot(enhancementIndexes);
        Debug.Assert(personalForceField.SlotCount == 6, "toon.Level1Secondary.SlotCount is incorrect!");
        Debug.Assert(personalForceField.Slots[0].Enhancement.Enh == 272, "personalForceField.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(personalForceField.Slots[1].Enhancement.Enh == 273, "personalForceField.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(personalForceField.Slots[2].Enhancement.Enh == 274, "personalForceField.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(personalForceField.Slots[3].Enhancement.Enh == 275, "personalForceField.Slots[3].Enhancement.Enh is incorrect!");
        Debug.Assert(personalForceField.Slots[4].Enhancement.Enh == 276, "personalForceField.Slots[4].Enhancement.Enh is incorrect!");
        Debug.Assert(personalForceField.Slots[5].Enhancement.Enh == 277, "personalForceField.Slots[5].Enhancement.Enh is incorrect!");
    }

    public static void TestGetLockedSlotsCount()
    {
        bool[] slotFlags = new bool[] { true, true, true, true, true, true };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 0, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { false, false, false, false, false, false };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 6, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { false, true, false, true, false, true };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 3, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { true, false, true, false, true, false };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 3, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { true, true, true };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 0, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { false, false, false };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 3, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { false, true, false };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 2, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { true, false, true };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 1, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { true };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 0, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[] { false };
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 1, "GetLockedSlotsCount() is incorrect!");
        slotFlags = new bool[0];
        Debug.Assert(GetLockedSlotsCount(slotFlags) == 0, "GetLockedSlotsCount() is incorrect!");
    }

    public static void TesIsSlotUnlocked()
    {
        bool[] slotFlags = new bool[] { true, true, true, true, true, true };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { false, false, false, false, false, false };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { false, true, false, true, false, true };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { true, false, true, false, true, false };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { true, true, true };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { false, false, false };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { false, true, false };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { true, false, true };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { true };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == true, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[] { false };
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        slotFlags = new bool[0];
        Debug.Assert(IsSlotUnlocked(0, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(1, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(2, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(3, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(4, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
        Debug.Assert(IsSlotUnlocked(5, slotFlags) == false, "IsSlotUnlocked() is incorrect!");
    }

    public static void TestCanMoveSlots()
    {
        IPowerset forceFieldPowerset = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
        IPowerset mindControlPowerset = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
        IPowerset fightingPowerset = DatabaseAPI.GetPowersetByName("Fighting", Enums.ePowerSetType.Pool);
        PowerEntry dispersionBubble = new PowerEntry(0, forceFieldPowerset.Powers[5]);
        PowerEntry mesmerize = new PowerEntry(0, mindControlPowerset.Powers[0]);
        PowerEntry terrify = new PowerEntry(0, mindControlPowerset.Powers[7]);
        PowerEntry telekinesis = new PowerEntry(0, mindControlPowerset.Powers[5]);
        PowerEntry personalForceField = new PowerEntry(0, forceFieldPowerset.Powers[0]);
        PowerEntry boxing = new PowerEntry(0, fightingPowerset.Powers[0]);
        bool[] slotFlags = new bool[] { true, true, true, true, true, true };
        dispersionBubble.DoDefaultSlotting(5);
        mesmerize.DoDefaultSlotting(6);
        terrify.DoDefaultSlotting(1);
        telekinesis.DoDefaultSlotting(5);
        personalForceField.DoDefaultSlotting(6);
        boxing.DoDefaultSlotting(1);
        dispersionBubble.Level = 19;
        mesmerize.Level = 0;
        telekinesis.Level = 17;
        personalForceField.Level = 0;
        boxing.Level = 23;

        // Bad data checks.
        Debug.Assert(PowerEntry.CanMoveSlots(null, dispersionBubble, slotFlags) == false, "CanMoveSlots(null, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.CanMoveSlots(dispersionBubble, null, slotFlags) == false, "CanMoveSlots(dispersionBubble, null) is incorrect!");
        PowerEntry powerEntryClone = dispersionBubble.Clone() as PowerEntry;
        powerEntryClone.Slots = null;
        Debug.Assert(PowerEntry.CanMoveSlots(powerEntryClone, dispersionBubble, slotFlags) == false, "CanMoveSlots(powerEntryClone, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.CanMoveSlots(dispersionBubble, powerEntryClone, slotFlags) == false, "CanMoveSlots(dispersionBubble, powerEntryClone) is incorrect!");

        // Source slots = 6, dest slots = 6, result = false
        Debug.Assert(PowerEntry.CanMoveSlots(mesmerize, personalForceField, slotFlags) == false, "CanMoveSlots(mesmerize, personalForceField) is incorrect!");
        // Source slots = 1, dest slots = 1, result = false
        Debug.Assert(PowerEntry.CanMoveSlots(terrify, boxing, slotFlags) == false, "CanMoveSlots(terrify, boxing) is incorrect!");
        // Source slots = 1, dest slots = 6, result = false
        Debug.Assert(PowerEntry.CanMoveSlots(terrify, mesmerize, slotFlags) == false, "CanMoveSlots(terrify, mesmerize) is incorrect!");
        // Source slots = 6, dest slots = 1, result = true
        Debug.Assert(PowerEntry.CanMoveSlots(mesmerize, terrify, slotFlags) == true, "CanMoveSlots(mesmerize, terrify) is incorrect!");
        // Source slots = 5, dest slots = 5, result = true
        Debug.Assert(PowerEntry.CanMoveSlots(telekinesis, dispersionBubble, slotFlags) == true, "CanMoveSlots(telekinesis, dispersionBubble) is incorrect!");
    }

    public static void TestGetMoveSlotCount()
    {
        IPowerset forceFieldPowerset = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
        IPowerset mindControlPowerset = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
        IPowerset fightingPowerset = DatabaseAPI.GetPowersetByName("Fighting", Enums.ePowerSetType.Pool);
        PowerEntry dispersionBubble = new PowerEntry(0, forceFieldPowerset.Powers[5]);
        PowerEntry mesmerize = new PowerEntry(0, mindControlPowerset.Powers[0]);
        PowerEntry terrify = new PowerEntry(0, mindControlPowerset.Powers[7]);
        PowerEntry telekinesis = new PowerEntry(0, mindControlPowerset.Powers[5]);
        PowerEntry personalForceField = new PowerEntry(0, forceFieldPowerset.Powers[0]);
        PowerEntry boxing = new PowerEntry(0, fightingPowerset.Powers[0]);
        bool[] slotFlags = new bool[] { true, true, true, true, true, true };
        dispersionBubble.DoDefaultSlotting(5);
        mesmerize.DoDefaultSlotting(6);
        terrify.DoDefaultSlotting(1);
        telekinesis.DoDefaultSlotting(5);
        personalForceField.DoDefaultSlotting(6);
        boxing.DoDefaultSlotting(1);

        // Bad data checks.
        Debug.Assert(PowerEntry.GetMoveSlotCount(null, dispersionBubble, slotFlags) == -1, "GetMoveSlotCount(null, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.GetMoveSlotCount(dispersionBubble, null, slotFlags) == -1, "GetMoveSlotCount(dispersionBubble, null) is incorrect!");
        PowerEntry powerEntryClone = dispersionBubble.Clone() as PowerEntry;
        powerEntryClone.Slots = null;
        Debug.Assert(PowerEntry.GetMoveSlotCount(powerEntryClone, dispersionBubble, slotFlags) == -1, "GetMoveSlotCount(powerEntryClone, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.GetMoveSlotCount(dispersionBubble, powerEntryClone, slotFlags) == -1, "GetMoveSlotCount(dispersionBubble, powerEntryClone) is incorrect!");

        // Source slots = 6, dest slots = 6, result = -1
        Debug.Assert(PowerEntry.GetMoveSlotCount(mesmerize, personalForceField, slotFlags) == -1, "GetMoveSlotCount(mesmerize, personalForceField) is incorrect!");
        // Source slots = 1, dest slots = 1, result = -1
        Debug.Assert(PowerEntry.GetMoveSlotCount(terrify, boxing, slotFlags) == -1, "GetMoveSlotCount(terrify, boxing) is incorrect!");
        // Source slots = 1, dest slots = 6, result = -1
        Debug.Assert(PowerEntry.GetMoveSlotCount(terrify, mesmerize, slotFlags) == -1, "GetMoveSlotCount(terrify, mesmerize) is incorrect!");
        // Source slots = 6, dest slots = 1, result > 0 and < 6
        int count = PowerEntry.GetMoveSlotCount(mesmerize, terrify, slotFlags);
        Debug.Assert(count > 0, "GetMoveSlotCount(mesmerize, terrify) is incorrect!");
        Debug.Assert(count < 6, "GetMoveSlotCount(mesmerize, terrify) is incorrect!");
        // Source slots = 5, dest slots = 5, result = 1
        Debug.Assert(PowerEntry.GetMoveSlotCount(telekinesis, dispersionBubble, slotFlags) == 1, "GetMoveSlotCount(telekinesis, dispersionBubble) is incorrect!");
    }

    public static void TestMoveSlots()
    {
        IPowerset forceFieldPowerset = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
        IPowerset mindControlPowerset = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
        IPowerset fightingPowerset = DatabaseAPI.GetPowersetByName("Fighting", Enums.ePowerSetType.Pool);
        PowerEntry dispersionBubble = new PowerEntry(0, forceFieldPowerset.Powers[5]);
        PowerEntry telekinesis = new PowerEntry(0, mindControlPowerset.Powers[5]);
        bool[] slotFlags = new bool[] { true, true, true, true, true, true };
        dispersionBubble.DoDefaultSlotting(5);
        telekinesis.DoDefaultSlotting(5);

        // Bad data checks.
        Debug.Assert(PowerEntry.MoveSlots(null, dispersionBubble, 1, slotFlags) == false, "MoveSlots(null, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.MoveSlots(dispersionBubble, null, 1, slotFlags) == false, "MoveSlots(dispersionBubble, null) is incorrect!");
        PowerEntry powerEntryClone20 = telekinesis.Clone() as PowerEntry;
        powerEntryClone20.Slots = null;
        Debug.Assert(PowerEntry.MoveSlots(powerEntryClone20, dispersionBubble, 1, slotFlags) == false, "MoveSlots(powerEntryClone, dispersionBubble) is incorrect!");
        Debug.Assert(PowerEntry.MoveSlots(dispersionBubble, powerEntryClone20, 1, slotFlags) == false, "MoveSlots(dispersionBubble, powerEntryClone) is incorrect!");

        // Trying to move too much
        // Source slots = 5, dest slots = 5, move = 2, result = false
        Debug.Assert(PowerEntry.MoveSlots(telekinesis, dispersionBubble, 2, slotFlags) == false, "MoveSlots(telekinesis, dispersionBubble, 2) is incorrect!");

        // Trying to move too little
        // Source slots = 5, dest slots = 5, move = 0, result = false
        Debug.Assert(PowerEntry.MoveSlots(telekinesis, dispersionBubble, 0, slotFlags) == false, "MoveSlots(telekinesis, dispersionBubble, 0) is incorrect!");

        powerEntryClone20 = telekinesis.Clone() as PowerEntry;
        PowerEntry powerEntryClone22 = dispersionBubble.Clone() as PowerEntry;
        Debug.Assert(PowerEntry.MoveSlots(powerEntryClone20, powerEntryClone22, 1, slotFlags) == true, "MoveSlots(powerEntryClone20, powerEntryClone22) is incorrect!");
        Debug.Assert(powerEntryClone20.Slots.Length == 4, "powerEntryClone20.Slots.Length is incorrect!");
        Debug.Assert(powerEntryClone22.Slots.Length == 6, "powerEntryClone22.Slots.Length is incorrect!");
    }
}

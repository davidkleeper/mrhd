﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Base.Data_Classes
{
    public partial class Effect : IEffect, IComparable, ICloneable
    {
        public float GetModifyEffects()
        {
            if (Base.Master_Classes.MidsContext.Character != null && !string.IsNullOrEmpty(EffectId) && Base.Master_Classes.MidsContext.Character.ModifyEffects.ContainsKey(EffectId))
                return Base.Master_Classes.MidsContext.Character.ModifyEffects[EffectId];
            return 0.0f;
        }

        public float GetBuffHaste()
        {
            return Base.Master_Classes.MidsContext.Character.DisplayStats.BuffHaste(false);
        }

        public float CalcProbability()
        {
            return AIMath.EffectMath.CalcProbability(power, this, GetBuffHaste(), GetModifyEffects()); ;
        }

        public Tuple<float, bool> GetEnhancementMag(Archetype archetype, Enums.eEffectType effectType, int subType = 0)
        {
            Tuple<float, bool> no = new Tuple<float, bool>(0.0f, false);
            if (!PvXInclude()) return no;
            if (CalcProbability() <= 0.0) return no;
            if (ETModifies != effectType) return no;
            if (!CanInclude()) return no;
            if (EffectType != Enums.eEffectType.Enhancement && EffectType != Enums.eEffectType.DamageBuff) return no;
            if (Absorbed_Effect && Absorbed_PowerType == Enums.ePowerType.GlobalBoost) return no;

            if (effectType == Enums.eEffectType.Mez && ToWho != Enums.eToWho.Target)
            {
                if ((Enums.eMez)subType == MezType || subType < 0) return new Tuple<float, bool>(AIMath.EffectMath.CalcMag(this, archetype), true);
            }
            else if (ToWho != Enums.eToWho.Target)
            {
                return new Tuple<float, bool>(AIMath.EffectMath.CalcMag(this, archetype), true);
            }
            return no;
        }

        public Tuple<float, bool> GetEffectMag(
            Archetype archetype,
            Enums.eEffectType effectType,
            Enums.eSuppress suppress = Enums.eSuppress.None,
            bool includeDelayed = false,
            bool onlySelf = false,
            bool onlyTarget = false,
            bool maxMode = false
        )
        {
            Tuple<float, bool> no = new Tuple<float, bool>(0.0f, false);
            bool flag = onlySelf == onlyTarget;
            flag |= !onlySelf && ToWho == Enums.eToWho.Target;
            flag |= !onlyTarget && ToWho == Enums.eToWho.ToSelf;
            flag |= onlySelf && ToWho != Enums.eToWho.Target;
            flag |= onlyTarget && ToWho != Enums.eToWho.ToSelf;
            if (!flag) return no;
            if (effectType == Enums.eEffectType.SpeedFlying & !maxMode && Aspect == Enums.eAspect.Max) return no;
            if (effectType == Enums.eEffectType.SpeedRunning & !maxMode && Aspect == Enums.eAspect.Max) return no;
            if (effectType == Enums.eEffectType.SpeedJumping & !maxMode && Aspect == Enums.eAspect.Max) return no;
            if ((Suppression & suppress) != Enums.eSuppress.None) return no;
            if (CalcProbability() <= 0.0) return no;
            if (maxMode && Aspect != Enums.eAspect.Max) return no;
            if (EffectType != effectType) return no;
            if (EffectClass == Enums.eEffectClass.Ignored) return no;
            if (EffectClass == Enums.eEffectClass.Special) return no;
            if (DelayedTime > 5.0 && !includeDelayed) return no;
            if (!CanInclude()) return no;
            if (!PvXInclude()) return no;

            float mag = AIMath.EffectMath.CalcMag(this, archetype);
            if (Ticks > 1 && Stacking == Enums.eStacking.Yes)
                mag *= Ticks;
            return new Tuple<float, bool>(mag, true);
        }

        public Tuple<float, bool> GetDamageMag(Archetype archetype, Enums.eEffectType effectType, Enums.eDamage damageType, Enums.ePowerType powerType, bool includeDelayed = false)
        {
            Tuple<float, bool> no = new Tuple<float, bool>(0.0f, false);
            if (!CanInclude()) return no;
            if (EffectType != effectType && EffectClass != Enums.eEffectClass.Ignored) return no;
            if (!PvXInclude()) return no;
            if (DelayedTime >= 5.0 && !includeDelayed) return no;
            if (DamageType != damageType) return no;
            float mag = AIMath.EffectMath.CalcMag(this, archetype);
            if (powerType == Enums.ePowerType.Toggle & isEnhancementEffect)
                mag /= 10f;
            return new Tuple<float, bool>(mag, true);
        }

        public Tuple<float, bool> CalcEffectMag(Archetype archetype, Enums.eEffectType iEffect, int hitpoints = 0, Enums.eToWho iTarget = Enums.eToWho.Unspecified, bool allowDelay = false)
        {
            Tuple<float, bool> no = new Tuple<float, bool>(0.0f, false);
            if (EffectType != iEffect) return no;
            if (EffectClass == Enums.eEffectClass.Ignored) return no;
            if (InherentSpecial) return no;
            if (!PvXInclude()) return no;
            if (DelayedTime > 5.0 && !allowDelay) return no;
            if (iTarget != Enums.eToWho.Unspecified && ToWho != Enums.eToWho.All && iTarget != ToWho) return no;

            float mag = AIMath.EffectMath.CalcMag(this, archetype);
            if (Ticks > 1)
                mag *= Ticks;
            if (DisplayPercentage && (EffectType == Enums.eEffectType.Heal || EffectType == Enums.eEffectType.HitPoints))
                return new Tuple<float, bool>(mag / 100f * hitpoints, true);
            else if (EffectType == Enums.eEffectType.Heal || EffectType == Enums.eEffectType.HitPoints)
                return new Tuple<float, bool>((float)(mag / (double)hitpoints * 100.0), true);
            else
                return new Tuple<float, bool>(mag, true);
        }
    }
}

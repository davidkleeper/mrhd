﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Base.Data_Classes
{
    public partial class Power : IPower, IComparable
    {
        public static IPower GetPowerByName(string name)
        {
            foreach (IPower power in DatabaseAPI.Database.Power)
            {
                if (power.DisplayName == name)
                    return power;
                if (power.FullName == name)
                    return power;
                if (power.PowerName == name)
                    return power;
            }
            return null;
        }

        public List<IEnhancement> GetDefaultSlotting()
        {
            int[] validEnhancements = GetValidEnhancements(Enums.eType.InventO);
            List<IEnhancement> defaultEnhancements = new List<IEnhancement>();

            for (int enhancementIndex = 0; enhancementIndex < validEnhancements.Length; enhancementIndex++)
            {
                if ((int)EnhancementType.Accuracy == validEnhancements[enhancementIndex])
                {
                    defaultEnhancements.Add(DatabaseAPI.GetEnhancementByIndex(validEnhancements[enhancementIndex]));
                    break;
                }
            }
            int count = defaultEnhancements.Count;
            while (defaultEnhancements.Count < count + 3)
            {
                bool hasDamage = false;
                for (int enhancementIndex = 0; enhancementIndex < validEnhancements.Length; enhancementIndex++)
                {
                    if ((int)EnhancementType.Damage == validEnhancements[enhancementIndex])
                    {
                        defaultEnhancements.Add(DatabaseAPI.GetEnhancementByIndex(validEnhancements[enhancementIndex]));
                        hasDamage = true;
                        break;
                    }
                }
                if (!hasDamage)
                    break;
            }
            count = defaultEnhancements.Count;
            while (defaultEnhancements.Count < count + 3 && defaultEnhancements.Count < 6)
            {
                bool hasEnhancement = false;
                for (int enhancementIndex = 0; enhancementIndex < validEnhancements.Length; enhancementIndex++)
                {
                    if ((int)EnhancementType.Confuse == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Defense == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.DefenseDebuff == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.EnduranceModification == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Fear == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Fly == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Heal == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Hold == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Immobilize == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Resistance == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Run == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Sleep == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Slow == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Stun == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.Taunt == validEnhancements[enhancementIndex]
                    || (int)EnhancementType.ToHitDebuff == validEnhancements[enhancementIndex])
                    {
                        defaultEnhancements.Add(DatabaseAPI.GetEnhancementByIndex(validEnhancements[enhancementIndex]));
                        hasEnhancement = true;
                        break;
                    }
                }
                if (!hasEnhancement)
                    break;
            }

            if (EndCost > 1.0f)
            {
                count = defaultEnhancements.Count;
                while (defaultEnhancements.Count < count + 3 && defaultEnhancements.Count < 6)
                {
                    bool hasEnduranceReduction = false;
                    for (int searchIndex = 0; searchIndex < validEnhancements.Length; searchIndex++)
                    {
                        if ((int)EnhancementType.EnduranceReduction == validEnhancements[searchIndex])
                        {
                            defaultEnhancements.Add(DatabaseAPI.GetEnhancementByIndex(validEnhancements[searchIndex]));
                            hasEnduranceReduction = true;
                            break;
                        }
                    }
                    if (!hasEnduranceReduction)
                        break;
                }
            }

            int rechargeCount = 0;
            while (defaultEnhancements.Count < 6 && 3 > rechargeCount)
            {
                for (int searchIndex = 0; searchIndex < validEnhancements.Length; searchIndex++)
                {
                    if ((int)EnhancementType.Recharge == validEnhancements[searchIndex])
                    {
                        defaultEnhancements.Add(DatabaseAPI.GetEnhancementByIndex(validEnhancements[searchIndex]));
                        rechargeCount++;
                        break;
                    }
                }
                if (0 == rechargeCount)
                    break;
            }
            return defaultEnhancements;
        }

        public int[] GetValidEnhancementsFromSets()
        {
            List<int> intList = new List<int>();
            foreach (EnhancementSet enhancementSet in DatabaseAPI.Database.EnhancementSets)
            {
                bool flag = SetTypes.Any(setType => enhancementSet.SetType == setType);
                if (!flag)
                    continue;
                intList.AddRange(enhancementSet.Enhancements);
            }
            return intList.ToArray();
        }

        public HashSet<EnhancementSet> GetEnhancementSetsWithBonusEffect(int slots, BonusInfo bonusInfo)
        {
            HashSet<EnhancementSet> enhancementSets = new HashSet<EnhancementSet>();
            if (1 > slots || 6 < slots)
                return enhancementSets;

            int[] validEnhancements = GetValidEnhancements(Enums.eType.SetO);

            for (int validEnhancementsIndex = 0; validEnhancementsIndex <= validEnhancements.Length - 1; ++validEnhancementsIndex)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(validEnhancements[validEnhancementsIndex]);
                if (null == enhancement)
                    continue;
                EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
                if (null == enhancementSet)
                    continue;
                if (!enhancementSet.HasBonusEffect(slots, bonusInfo))
                    continue;
                enhancementSets.Add(enhancementSet);
            }
            return enhancementSets;
        }

        public bool AllowsEnhancement(EnhancementType enhancementType)
        {
            int[] allowedEnhancements = GetValidEnhancements(Enums.eType.InventO);
            foreach (int enhancementID in allowedEnhancements)
            {
                if (enhancementID == (int)enhancementType)
                    return true;
            }
            return false;
        }

        public float GetMaxDuration()
        {
            float duration = 0.0f;
            foreach (IEffect effect in Effects)
            {
                if (effect.Duration > duration)
                    duration = effect.Duration;
            }
            return duration;
        }

        public bool CanBePerma()
        {
            if (Enums.ePowerType.Click != PowerType)
                return false;
            if ("Class_Minion_Henchman" == ForcedClass || "Class_Lt_Henchman" == ForcedClass || "Class_Boss_Henchman" == ForcedClass)
                return false;
            if (Enums.eEntity.MyPet == EntitiesAffected && Enums.eEntity.MyPet == EntitiesAutoHit)
                return false;
            if (Enums.eEntity.DeadPlayerFriend == EntitiesAffected && Enums.eEntity.DeadPlayerFriend == EntitiesAutoHit)
                return false;
            if (Enums.eCastFlags.CastableAfterDeath == CastFlags)
                return false;
            float maxDuration = GetMaxDuration();
            if (BaseUtils.IsKindaSortaEqual(0.0f, maxDuration, 0.0001f))
                return false;
            if (BaseUtils.IsKindaSortaEqual(99999.0f, maxDuration, 0.0001f))
                return false;
            if (RechargeTime <= maxDuration)
                return false;
            return true;
        }

        public float GetPermaValue()
        {
            float duration = GetMaxDuration();
            float recharge = RechargeTime;
            float rechargeRatio = duration / recharge;
            float permaValue = (100 / rechargeRatio) - 100;
            return permaValue;
        }

        public Enums.ShortFX SumDamageMag(Archetype archetype, Enums.eEffectType effectType, Enums.eDamage damageType, bool includeDelayed = false)
        {
            Enums.ShortFX shortFx = new Enums.ShortFX();
            for (int effectIndex = 0; effectIndex <= Effects.Length - 1; ++effectIndex)
            {
                IEffect effect = Effects[effectIndex];
                Tuple<float, bool> damageInfo = effect.GetDamageMag(archetype, effectType, damageType, PowerType, includeDelayed);
                if (!damageInfo.Item2) continue;
                shortFx.Add(effectIndex, damageInfo.Item1);
            }
            return shortFx;
        }

        public Enums.ShortFX SumEnhancementMag(Archetype archetype, Enums.eEffectType effectType, int subType = 0)
        {
            Enums.ShortFX shortFx = new Enums.ShortFX();
            for (int effectIndex = 0; effectIndex <= Effects.Length - 1; ++effectIndex)
            {
                IEffect effect = Effects[effectIndex];
                Tuple<float, bool> magInfo = effect.GetEnhancementMag(archetype, effectType, subType);
                if (!magInfo.Item2) continue;
                shortFx.Add(effectIndex, magInfo.Item1);
            }
            return shortFx;
        }

        public Enums.ShortFX CalcEffectMag(Archetype archetype, Enums.eEffectType iEffect, int hitpoints = 0, Enums.eToWho iTarget = Enums.eToWho.Unspecified, bool allowDelay = false)
        {
            Enums.ShortFX shortFx = new Enums.ShortFX();
            for (int effectIndex = 0; effectIndex <= Effects.Length - 1; ++effectIndex)
            {
                IEffect effect = Effects[effectIndex];
                Tuple<float, bool> magInfo = effect.CalcEffectMag(archetype, iEffect, hitpoints, iTarget, allowDelay);
                if (!magInfo.Item2) continue;
                shortFx.Add(effectIndex, magInfo.Item1);
                return shortFx;
            }
            return shortFx;
        }

        public Enums.ShortFX SumEffectMag(
            Archetype archetype,
            Enums.eEffectType effectType,
            Enums.eSuppress suppress = Enums.eSuppress.None,
            bool includeDelayed = false,
            bool onlySelf = false,
            bool onlyTarget = false,
            bool maxMode = false
        )
        {
            Enums.ShortFX shortFx = new Enums.ShortFX();
            for (int effectIndex = 0; effectIndex <= Effects.Length - 1; ++effectIndex)
            {
                IEffect effect = Effects[effectIndex];
                Tuple<float, bool> magInfo = effect.GetEffectMag(archetype, effectType, suppress, includeDelayed, onlySelf, onlyTarget, maxMode);
                if (!magInfo.Item2) continue;
                shortFx.Add(effectIndex, magInfo.Item1);
            }
            return shortFx;
        }

        public static void TestNewCode()
        {
            TestAllowsEnhancement();
            TestGetDefaultSlotting();
            TestGetEnhancementSetsWithBonusEffect();
            TestGetPermaValue();
            TestSumDamageMag();
            TestSumEnhancementMag();
            TestSumEffectMag();
            TestCalcEffectMag();
        }

        public static void TestAllowsEnhancement()
        {
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);

            Debug.Assert(mesmerize.Power.AllowsEnhancement(EnhancementType.Accuracy) == true, "AllowsEnhancement EnhancementTypes.Accuracy is incorrect!");
            Debug.Assert(mesmerize.Power.AllowsEnhancement(EnhancementType.Damage) == true, "AllowsEnhancement EnhancementTypes.Damage is incorrect!");
            Debug.Assert(mesmerize.Power.AllowsEnhancement(EnhancementType.EnduranceReduction) == true, "AllowsEnhancement EnhancementTypes.EnduranceReduction is incorrect!");
            Debug.Assert(mesmerize.Power.AllowsEnhancement(EnhancementType.Recharge) == true, "AllowsEnhancement EnhancementTypes.Recharge is incorrect!");
            Debug.Assert(mesmerize.Power.AllowsEnhancement(EnhancementType.Heal) == false, "AllowsEnhancement EnhancementTypes.Heal is incorrect!");
        }

        public static void TestGetDefaultSlotting()
        {
            PowerEntry health = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent).Powers[1]);
            PowerEntry stamina = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent).Powers[3]);
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
            PowerEntry dispersionBubble = new PowerEntry(22, DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary).Powers[5]);
            PowerEntry repulsionField = new PowerEntry(47, DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary).Powers[6]);

            List<IEnhancement> defaultSlotting = health.Power.GetDefaultSlotting();
            Debug.Assert(defaultSlotting.Count == 3, "health defaultSlotting is incorrect!");
            Debug.Assert(defaultSlotting[0] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Heal), "health defaultSlotting[0] is incorrect!");
            Debug.Assert(defaultSlotting[1] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Heal), "health defaultSlotting[1] is incorrect!");
            Debug.Assert(defaultSlotting[2] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Heal), "health defaultSlotting[2] is incorrect!");

            defaultSlotting = stamina.Power.GetDefaultSlotting();
            Debug.Assert(defaultSlotting.Count == 3, "stamina defaultSlotting is incorrect!");
            Debug.Assert(defaultSlotting[0] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceModification), "stamina defaultSlotting[0] is incorrect!");
            Debug.Assert(defaultSlotting[1] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceModification), "stamina defaultSlotting[1] is incorrect!");
            Debug.Assert(defaultSlotting[2] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceModification), "stamina defaultSlotting[2] is incorrect!");

            defaultSlotting = mesmerize.Power.GetDefaultSlotting();
            Debug.Assert(defaultSlotting.Count == 6, "mesmerize defaultSlotting is incorrect!");
            Debug.Assert(defaultSlotting[0] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Accuracy), "mesmerize defaultSlotting[0] is incorrect!");
            Debug.Assert(defaultSlotting[1] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage), "mesmerize defaultSlotting[1] is incorrect!");
            Debug.Assert(defaultSlotting[2] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage), "mesmerize defaultSlotting[2] is incorrect!");
            Debug.Assert(defaultSlotting[3] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Damage), "mesmerize defaultSlotting[3] is incorrect!");
            Debug.Assert(defaultSlotting[4] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Sleep), "mesmerize defaultSlotting[4] is incorrect!");
            Debug.Assert(defaultSlotting[5] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Sleep), "mesmerize defaultSlotting[5] is incorrect!");

            defaultSlotting = dispersionBubble.Power.GetDefaultSlotting();
            Debug.Assert(defaultSlotting.Count == 6, "dispersionBubble defaultSlotting is incorrect!");
            Debug.Assert(defaultSlotting[0] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Defense), "dispersionBubble defaultSlotting[0] is incorrect!");
            Debug.Assert(defaultSlotting[1] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Defense), "dispersionBubble defaultSlotting[1] is incorrect!");
            Debug.Assert(defaultSlotting[2] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Defense), "dispersionBubble defaultSlotting[2] is incorrect!");
            Debug.Assert(defaultSlotting[3] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction), "dispersionBubble defaultSlotting[3] is incorrect!");
            Debug.Assert(defaultSlotting[4] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction), "dispersionBubble defaultSlotting[4] is incorrect!");
            Debug.Assert(defaultSlotting[5] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.EnduranceReduction), "dispersionBubble defaultSlotting[5] is incorrect!");

            defaultSlotting = repulsionField.Power.GetDefaultSlotting();
            Debug.Assert(defaultSlotting.Count == 3, "repulsionField defaultSlotting is incorrect!");
            Debug.Assert(defaultSlotting[0] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Recharge), "repulsionField defaultSlotting[0] is incorrect!");
            Debug.Assert(defaultSlotting[1] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Recharge), "repulsionField defaultSlotting[1] is incorrect!");
            Debug.Assert(defaultSlotting[2] == DatabaseAPI.GetEnhancementByIndex((int)EnhancementType.Recharge), "repulsionField defaultSlotting[2] is incorrect!");
        }

        public static void TestGetEnhancementSetsWithBonusEffect()
        {
            IPower power = DatabaseAPI.GetPowerByIndex(0);
            HashSet<EnhancementSet> defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(6, new BonusInfo(Enums.eEffectType.Defense));
            Debug.Print("Defense (6 slots)");
            foreach (EnhancementSet enhancementSet in defenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Assert(defenseBonusSets.Count == 14, "defenseBonusSets.Count is incorrect!");
            EnhancementSet[] defenseBonusSetsArray = defenseBonusSets.ToArray();
            Debug.Assert(defenseBonusSetsArray[0].DisplayName == "Maelstrom's Fury", "defenseBonusSetsArray[0].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[1].DisplayName == "Ruin", "defenseBonusSetsArray[1].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[2].DisplayName == "Thunderstrike", "defenseBonusSetsArray[2].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[3].DisplayName == "Devastation", "defenseBonusSetsArray[3].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[4].DisplayName == "Exploited Vulnerability", "defenseBonusSetsArray[4].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[5].DisplayName == "Achilles' Heel", "defenseBonusSetsArray[5].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[6].DisplayName == "Undermined Defenses", "defenseBonusSetsArray[6].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[7].DisplayName == "Apocalypse", "defenseBonusSetsArray[7].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[8].DisplayName == "Shield Breaker", "defenseBonusSetsArray[8].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[9].DisplayName == "Overwhelming Force", "defenseBonusSetsArray[9].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[10].DisplayName == "Spider's Bite", "defenseBonusSetsArray[10].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[11].DisplayName == "Superior Spider's Bite", "defenseBonusSetsArray[11].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[12].DisplayName == "Winter's Bite", "defenseBonusSetsArray[12].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[13].DisplayName == "Superior Winter's Bite", "defenseBonusSetsArray[13].DisplayName is incorrect!");

            HashSet<EnhancementSet> psionicDefenseBonusSets = power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefensePsionic);
            Debug.Assert(psionicDefenseBonusSets.Count == 2, "psionicDefenseBonusSets.Count is incorrect!");
            EnhancementSet[] psionicDefenseBonusSetsArray = psionicDefenseBonusSets.ToArray();

            Debug.Assert(psionicDefenseBonusSetsArray[0].DisplayName == "Devastation", "psionicDefenseBonusSetsArray[0].DisplayName is incorrect!");
            Debug.Assert(psionicDefenseBonusSetsArray[1].DisplayName == "Apocalypse", "psionicDefenseBonusSetsArray[1].DisplayName is incorrect!");
            Debug.Print(" ");
            Debug.Print("Psionic Defense (6 slots)");
            foreach (EnhancementSet enhancementSet in psionicDefenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }

            HashSet<EnhancementSet> enduranceDiscountBonusSets = power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.EnduranceDiscount);
            Debug.Assert(enduranceDiscountBonusSets.Count == 0, "enduranceDiscountBonusSets.Count is incorrect!");
            Debug.Print(" ");
            Debug.Print("EnduranceDiscount (6 slots)");
            foreach (EnhancementSet enhancementSet in enduranceDiscountBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Print(" ");

            defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(5, BonusInfo.DefenseAll);
            Debug.Print("Defense (5 slots)");
            foreach (EnhancementSet enhancementSet in defenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Assert(defenseBonusSets.Count == 11, "defenseBonusSets.Count is incorrect!");
            defenseBonusSetsArray = defenseBonusSets.ToArray();
            Debug.Assert(defenseBonusSetsArray[0].DisplayName == "Maelstrom's Fury", "defenseBonusSetsArray[0].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[1].DisplayName == "Ruin", "defenseBonusSetsArray[1].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[2].DisplayName == "Thunderstrike", "defenseBonusSetsArray[2].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[3].DisplayName == "Exploited Vulnerability", "defenseBonusSetsArray[3].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[4].DisplayName == "Achilles' Heel", "defenseBonusSetsArray[4].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[5].DisplayName == "Shield Breaker", "defenseBonusSetsArray[5].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[6].DisplayName == "Overwhelming Force", "defenseBonusSetsArray[6].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[7].DisplayName == "Spider's Bite", "defenseBonusSetsArray[7].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[8].DisplayName == "Superior Spider's Bite", "defenseBonusSetsArray[8].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[9].DisplayName == "Winter's Bite", "defenseBonusSetsArray[9].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[10].DisplayName == "Superior Winter's Bite", "defenseBonusSetsArray[10].DisplayName is incorrect!");
            Debug.Print(" ");

            defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(4, BonusInfo.DefenseAll);
            Debug.Print("Defense (4 slots)");
            foreach (EnhancementSet enhancementSet in defenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Assert(defenseBonusSets.Count == 7, "defenseBonusSets.Count is incorrect!");
            defenseBonusSetsArray = defenseBonusSets.ToArray();
            Debug.Assert(defenseBonusSetsArray[0].DisplayName == "Maelstrom's Fury", "defenseBonusSetsArray[0].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[1].DisplayName == "Thunderstrike", "defenseBonusSetsArray[1].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[2].DisplayName == "Exploited Vulnerability", "defenseBonusSetsArray[2].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[3].DisplayName == "Achilles' Heel", "defenseBonusSetsArray[3].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[4].DisplayName == "Shield Breaker", "defenseBonusSetsArray[4].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[5].DisplayName == "Spider's Bite", "defenseBonusSetsArray[5].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[6].DisplayName == "Superior Spider's Bite", "defenseBonusSetsArray[6].DisplayName is incorrect!");
            Debug.Print(" ");

            defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(3, BonusInfo.DefenseAll);
            Debug.Print("Defense (3 slots)");
            foreach (EnhancementSet enhancementSet in defenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Assert(defenseBonusSets.Count == 4, "defenseBonusSets.Count is incorrect!");
            defenseBonusSetsArray = defenseBonusSets.ToArray();
            Debug.Assert(defenseBonusSetsArray[0].DisplayName == "Maelstrom's Fury", "defenseBonusSetsArray[0].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[1].DisplayName == "Thunderstrike", "defenseBonusSetsArray[1].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[2].DisplayName == "Exploited Vulnerability", "defenseBonusSetsArray[2].DisplayName is incorrect!");
            Debug.Assert(defenseBonusSetsArray[3].DisplayName == "Achilles' Heel", "defenseBonusSetsArray[3].DisplayName is incorrect!");
            Debug.Print(" ");

            defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(2, BonusInfo.DefenseAll);
            Debug.Print("Defense (2 slots)");
            foreach (EnhancementSet enhancementSet in defenseBonusSets)
            {
                Debug.Print(enhancementSet.DisplayName);
            }
            Debug.Assert(defenseBonusSets.Count == 0, "defenseBonusSets.Count is incorrect!");
            Debug.Print(" ");
        }

        public static void TestGetPermaValue()
        {
            IPowerset illusion = DatabaseAPI.GetPowersetByName("Illusion Control", Enums.ePowerSetType.Primary);
            IPower phantomArmy = illusion.Powers[6];
            float permaValue = phantomArmy.GetPermaValue();
            Debug.Assert(permaValue == 300, "permaValue is incorrect!");
        }

        public static void TestSumDamageMag() 
        {
            IPowerset illusion = DatabaseAPI.GetPowersetByName("Illusion Control", Enums.ePowerSetType.Primary);
            IPower blind = illusion.Powers[1];
            Enums.ShortFX originalAnswer = blind.GetDamageMagSum(Enums.eEffectType.Damage, Enums.eDamage.Psionic);
            Enums.ShortFX answer = blind.SumDamageMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Damage, Enums.eDamage.Psionic);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "SumDamageMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "SumDamageMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "SumDamageMag() is incorrect!");
            }
        }

        public static void TestSumEnhancementMag()
        {
            Base.Data_Classes.Archetype archetype = Base.Master_Classes.MidsContext.Archetype;
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower hasten = speed.Powers[1];
            Enums.ShortFX originalAnswer = hasten.GetEnhancementMagSum(Enums.eEffectType.RechargeTime, 0);
            Enums.ShortFX answer = hasten.SumEnhancementMag(archetype, Enums.eEffectType.RechargeTime, 0);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "SumEnhancementMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "SumEnhancementMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "SumEnhancementMag() is incorrect!");
            }

            IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
            IPower focusedAccuracy = maceMastery.Powers[3];
            originalAnswer = focusedAccuracy.GetEnhancementMagSum(Enums.eEffectType.Accuracy, 0);
            answer = focusedAccuracy.SumEnhancementMag(archetype, Enums.eEffectType.Accuracy, 0);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "SumEnhancementMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "SumEnhancementMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "SumEnhancementMag() is incorrect!");
            }

            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            originalAnswer = conservePower.GetEnhancementMagSum(Enums.eEffectType.EnduranceDiscount, 0);
            answer = conservePower.SumEnhancementMag(archetype, Enums.eEffectType.EnduranceDiscount, 0);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "SumEnhancementMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "SumEnhancementMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "SumEnhancementMag() is incorrect!");
            }
        }

        public static void TestSumEffectMag()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Enums.ShortFX originalAnswer = superSpeed.GetEffectMagSum(Enums.eEffectType.SpeedRunning);
            Enums.ShortFX answer = superSpeed.SumEffectMag(archetype, Enums.eEffectType.SpeedRunning);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "SumEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "SumEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "SumEffectMag() is incorrect!");
            }

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[2];
            originalAnswer = superJump.GetEffectMagSum(Enums.eEffectType.SpeedJumping);
            answer = superJump.SumEffectMag(archetype, Enums.eEffectType.SpeedJumping);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[2];
            originalAnswer = fly.GetEffectMagSum(Enums.eEffectType.SpeedFlying);
            answer = fly.SumEffectMag(archetype, Enums.eEffectType.SpeedFlying);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthEmbrace = stoneArmor.Powers[2];
            originalAnswer = earthEmbrace.GetEffectMagSum(Enums.eEffectType.HitPoints);
            answer = earthEmbrace.SumEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.HitPoints);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }
            originalAnswer = earthEmbrace.GetEffectMagSum(Enums.eEffectType.Heal);
            answer = earthEmbrace.SumEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Heal);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            originalAnswer = health.GetEffectMagSum(Enums.eEffectType.Regeneration);
            answer = health.SumEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Regeneration);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPower stamina = fitness.Powers[3];
            originalAnswer = stamina.GetEffectMagSum(Enums.eEffectType.Recovery);
            answer = stamina.SumEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Recovery);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }
        }

        public static void TestCalcEffectMag()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            Enums.ShortFX originalAnswer = superSpeed.GetEffectMag(Enums.eEffectType.SpeedRunning);
            Enums.ShortFX answer = superSpeed.CalcEffectMag(archetype, Enums.eEffectType.SpeedRunning);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[2];
            originalAnswer = superJump.GetEffectMag(Enums.eEffectType.SpeedJumping);
            answer = superJump.CalcEffectMag(archetype, Enums.eEffectType.SpeedJumping);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[2];
            originalAnswer = fly.GetEffectMag(Enums.eEffectType.SpeedFlying);
            answer = fly.CalcEffectMag(archetype, Enums.eEffectType.SpeedFlying);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthEmbrace = stoneArmor.Powers[2];
            originalAnswer = earthEmbrace.GetEffectMag(Enums.eEffectType.HitPoints);
            answer = earthEmbrace.CalcEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.HitPoints, Base.Master_Classes.MidsContext.Archetype.Hitpoints);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }
            originalAnswer = earthEmbrace.GetEffectMag(Enums.eEffectType.Heal);
            answer = earthEmbrace.CalcEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Heal, Base.Master_Classes.MidsContext.Archetype.Hitpoints);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            originalAnswer = health.GetEffectMag(Enums.eEffectType.Regeneration);
            answer = health.CalcEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Regeneration);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }

            IPower stamina = fitness.Powers[3];
            originalAnswer = stamina.GetEffectMag(Enums.eEffectType.Recovery);
            answer = stamina.CalcEffectMag(Base.Master_Classes.MidsContext.Archetype, Enums.eEffectType.Recovery);
            Debug.Assert(originalAnswer.Sum == answer.Sum, "CalcEffectMag() is incorrect!");
            Debug.Assert(originalAnswer.Index.Length == answer.Index.Length, "CalcEffectMag() is incorrect!");
            for (int index = 0; index < answer.Index.Length; index++)
            {
                Debug.Assert(originalAnswer.Index[index] == answer.Index[index], "CalcEffectMag() is incorrect!");
            }
        }
    }
}

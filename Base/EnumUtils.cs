﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

class EnumUtils
{
    public static EnhancementType[] Convert(Enums.sTwinID effectAndSubEffectIDs, Enums.eBuffDebuff buffMode = Enums.eBuffDebuff.Any)
    {
        List<EnhancementType> result = new List<EnhancementType>();

        if (Enums.eEnhance.Accuracy == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Accuracy);
        else if (Enums.eEnhance.Damage == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Damage);
        else if (Enums.eEnhance.Defense == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eBuffDebuff.DeBuffOnly == buffMode) result.Add(EnhancementType.DefenseDebuff);
            else result.Add(EnhancementType.Defense);
        }
        else if (Enums.eEnhance.EnduranceDiscount == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.EnduranceReduction);
        else if (Enums.eEnhance.Endurance == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.EnduranceModification);
        else if (Enums.eEnhance.SpeedFlying == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eBuffDebuff.DeBuffOnly == buffMode)
                result.Add(EnhancementType.Slow);
            else
                result.Add(EnhancementType.Fly);
        }
        else if (Enums.eEnhance.Heal == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Heal);
        else if (Enums.eEnhance.HitPoints == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Heal);
        else if (Enums.eEnhance.Interrupt == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Interrupt);
        else if (Enums.eEnhance.JumpHeight == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Jump);
        else if (Enums.eEnhance.SpeedJumping == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eBuffDebuff.DeBuffOnly == buffMode)
                result.Add(EnhancementType.Slow);
            else
                result.Add(EnhancementType.Jump);
        }
        else if (Enums.eEnhance.Mez == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eMez.Confused == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Confuse);
            else if (Enums.eMez.Held == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Hold);
            else if (Enums.eMez.Immobilized == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Immobilize);
            else if (Enums.eMez.Knockback == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Knockback);
            else if (Enums.eMez.Knockup == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Knockback);
            else if (Enums.eMez.Sleep == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Sleep);
            else if (Enums.eMez.Stunned == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Stun);
            else if (Enums.eMez.Taunt == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Taunt);
            else if (Enums.eMez.Terrorized == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Fear);
            else if (Enums.eMez.Untouchable == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Intangible);
            else if (Enums.eMez.Afraid == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Fear);
            else if (Enums.eMez.Intangible == (Enums.eMez)effectAndSubEffectIDs.SubID)
                result.Add(EnhancementType.Intangible);
        }
        else if (Enums.eEnhance.Range == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Range);
        else if (Enums.eEnhance.RechargeTime == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Recharge);
        else if (Enums.eEnhance.Recovery == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.EnduranceModification);
        else if (Enums.eEnhance.Regeneration == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Heal);
        else if (Enums.eEnhance.Resistance == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Resistance);
        else if (Enums.eEnhance.SpeedRunning == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eBuffDebuff.DeBuffOnly == buffMode)
                result.Add(EnhancementType.Slow);
            else
                result.Add(EnhancementType.Run);
        }
        else if (Enums.eEnhance.ToHit == (Enums.eEnhance)effectAndSubEffectIDs.ID)
        {
            if (Enums.eBuffDebuff.BuffOnly == buffMode) result.Add(EnhancementType.ToHit);
            else result.Add(EnhancementType.ToHitDebuff);
        }
        else if (Enums.eEnhance.Slow == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Slow);
        else if (Enums.eEnhance.Absorb == (Enums.eEnhance)effectAndSubEffectIDs.ID) result.Add(EnhancementType.Heal);
            
        return result.ToArray();
    }
    public static void TestNewCode()
    {
        TestConvertEffectAndSubEffectIDs();
    }

    public static void TestConvertEffectAndSubEffectIDs()
    {
        Enums.sTwinID effectAnSubEffect = new Enums.sTwinID();
        effectAnSubEffect.ID = (int)Enums.eEnhance.Accuracy;
        effectAnSubEffect.SubID = -1;
        EnhancementType[] enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Accuracy == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Defense;
        enhancementTypes = Convert(effectAnSubEffect, Enums.eBuffDebuff.DeBuffOnly);
        Debug.Assert(EnhancementType.DefenseDebuff == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Defense == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.EnduranceDiscount;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.EnduranceReduction == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Endurance;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.EnduranceModification == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.SpeedFlying;
        enhancementTypes = Convert(effectAnSubEffect, Enums.eBuffDebuff.DeBuffOnly);
        Debug.Assert(EnhancementType.Slow == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Fly == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Heal;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Heal == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.HitPoints;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Heal == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Interrupt;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Interrupt == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.JumpHeight;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Jump == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.SpeedJumping;
        enhancementTypes = Convert(effectAnSubEffect, Enums.eBuffDebuff.DeBuffOnly);
        Debug.Assert(EnhancementType.Slow == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Jump == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Mez;
        effectAnSubEffect.SubID = (int)Enums.eMez.Confused;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Confuse == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Held;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Hold == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Immobilized;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Immobilize == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Knockback;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Knockback == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Knockup;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Knockback == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Sleep;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Sleep == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Stunned;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Stun == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Taunt;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Taunt == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Terrorized;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Fear == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Untouchable;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Intangible == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Afraid;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Fear == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.SubID = (int)Enums.eMez.Intangible;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Intangible == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Range;
        effectAnSubEffect.SubID = -1;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Range == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.RechargeTime;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Recharge == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Recovery;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.EnduranceModification == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Regeneration;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Heal == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Resistance;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Resistance == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.SpeedRunning;
        enhancementTypes = Convert(effectAnSubEffect, Enums.eBuffDebuff.DeBuffOnly);
        Debug.Assert(EnhancementType.Slow == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Run == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.ToHit;
        enhancementTypes = Convert(effectAnSubEffect, Enums.eBuffDebuff.BuffOnly);
        Debug.Assert(EnhancementType.ToHit == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.ToHitDebuff == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Slow;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Slow == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
        effectAnSubEffect.ID = (int)Enums.eEnhance.Absorb;
        enhancementTypes = Convert(effectAnSubEffect);
        Debug.Assert(EnhancementType.Heal == enhancementTypes[0], "ConvertEffectAndSubEffectIDs is incorrect!");
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

public class SpecialEnhancements
{
    public readonly static int AbsoluteAmazementChanceForToHitDebuff = 591;
    public readonly static int AegisPsionicStatusResistance = 309;
    public readonly static int AnalyzeWeaknessChanceForToHit = 723;
    public readonly static int AnnihilationChanceForResDebuff = 999;
    public readonly static int ApocalypseChanceOfDamageNegative = 561;
    public readonly static int ArmageddonChanceForFireDamage = 573;
    public readonly static int AscendencyOfTheDominatorRechargeChanceForDamage = 885;
    public readonly static int AssassinsMarkRechargeTimeRchgBuildUp = 1143;
    public readonly static int AvalancheRechargeChanceForKnockdown = 1179;
    public readonly static int BasilisksGazeChanceForRechargeSlow = 729;
    public readonly static int BlastersWrathRechargeChanceForFireDamage = 855;
    public readonly static int BlessingOfTheZephyrKnockbackReduction = 732;
    public readonly static int BlisteringColdChanceForHold = 1167;
    public readonly static int BrutesFuryRechargeFuryBonus = 861;
    public readonly static int CallOfTheSandmanChanceOfHealSelf = 457;
    public readonly static int CallToArmsDefenseBonusAuraForPets = 738;
    public readonly static int CloudSensesChanceForNegativeEnergyDamage = 744;
    public readonly static int CoercivePersuasionContagiousConfusion = 609;
    public readonly static int CommandOfTheMastermindRechargePetAoEDefenseAura = 891;
    public readonly static int CriticalStrikesRechargeTime50CritProc = 1119;
    public readonly static int DarkWatchersDespairChanceForRechargeSlow = 699;
    public readonly static int DecimationChanceOfBuildUp = 156;
    public readonly static int DefendersBastionRechargeChanceForMinorPBAoEHeal = 879;
    public readonly static int DefiantBarrageRechargeTimeStatus = 1023;
    public readonly static int DevastationChanceForHold = 168;
    public readonly static int DominatingGraspRechargeTimeFieryOrb = 1083;
    public readonly static int DominionOfArachnosRechargeChanceForMinusDmgAndTerrorize = 849;
    public readonly static int EdictOfTheMasterDefenseBonus = 233;
    public readonly static int EntombRechargeChanceForAbsorb = 1215;
    public readonly static int EntropicChaosChanceOfHealSelf = 145;
    public readonly static int EradicationChanceForEnergyDamage = 750;
    public readonly static int EssenceTransferRechargeTimeGlobalHeal = 1095;
    public readonly static int ExecutionersContractDisorientBonus = 200;
    public readonly static int ExpedientReinforcementResistBonusAuraForPets = 756;
    public readonly static int ForceFeedbackChanceForRecharge = 645;
    public readonly static int FortunataHypnosisChanceForPlacate = 603;
    public readonly static int FrozenBlastRechargeChanceForImmobilize = 1203;
    public readonly static int FuryOfTheGladiatorChanceForResDebuff = 831;
    public readonly static int GauntletedFistRechargeTimeAbsorb = 1155;
    public readonly static int GaussiansSynchronizedFireControlChanceForBuildUp = 681;
    public readonly static int GhostWidowsEmbraceChanceOfDamagePsionic = 365;
    public readonly static int GiftOfTheAncientsRunSpeed = 265;
    public readonly static int GladiatorsArmorTPProtection3DefAll = 820;
    public readonly static int GladiatorsJavelinChanceOfDamageToxic = 812;
    public readonly static int GladiatorsNetChanceOfDamageLethal = 837;
    public readonly static int GladiatorsStrikeChanceForSmashingDamage = 808;
    public readonly static int GlimpseOfTheAbyssChanceOfDamagePsionic = 480;
    public readonly static int GravitationalAnchorChanceForHold = 597;
    public readonly static int HecatombChanceOfDamageNegative = 555;
    public readonly static int ImpededSwiftnessChanceOfDamageSmashing = 422;
    public readonly static int ImperviousSkinStatusResistance = 285;
    public readonly static int ImperviumArmorResPsi = 297;
    public readonly static int InducedComaChanceOfMinusRecharge = 445;
    public readonly static int JavelinVolleyChanceOfDamageLethal = 802;
    public readonly static int KarmaKnockbackProtection = 248;
    public readonly static int KineticCombatKnockdownBonus = 84;
    public readonly static int KheldiansGraceRechargeFormEmpowerment = 915;
    public readonly static int KismetPlusAccuracy = 253;
    public readonly static int LockdownChanceForPlus2MagHold = 762;
    public readonly static int LuckOfTheGamblerGlobalRecharge = 277;
    public readonly static int MakosBiteChanceOfDamageLethal = 107;
    public readonly static int MalaisesIllusionsChanceOfDamagePsionic = 503;
    public readonly static int MaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage = 873;
    public readonly static int MarkOfSupremacyEndurancePetResistRegen = 1107;
    public readonly static int MightOfTheTankerRechargeChanceForResAll = 909;
    public readonly static int MiracleRecovery = 330;
    public readonly static int NeuronicShutdownChanceOfDamagePsionic = 353;
    public readonly static int NuminasConvalesenceRegenerationRecovery = 342;
    public readonly static int ObliterationChanceForSmashingDamage = 768;
    public readonly static int OpportunityStrikesRechargeTimeChanceForOpportunity = 1239;
    public readonly static int OverpoweringPresenceRechargeTimeEnergyFont = 1047;
    public readonly static int OverwhelmingForceDamageChanceForKnockdownKnockbackToKnockdown = 993;
    public readonly static int PacingOfTheTurtleChanceOfMinusRecharge = 434;
    public readonly static int PanaceaHitPointsEndurance = 801;
    public readonly static int PerfectZingerChanceForPsiDamage = 627;
    public readonly static int PerformanceShifterChanceForPlusEndurance = 663;
    public readonly static int PositronsBlastChanceOfDamageEnergy = 184;
    public readonly static int PoundingSlugfestDisorientBonus = 75;
    public readonly static int PreventiveMedicineChanceForAbsorb = 1005;
    public readonly static int RagnarokChanceForKnockdown = 567;
    public readonly static int RazzleDazzleChanceOfImmobilize = 376;
    public readonly static int ReactiveDefensesScalingResistDamage = 1011;
    public readonly static int RegenerativeTissueRegeneration = 318;
    public readonly static int ScourgingBlastRechargeTimePBAoEEnd = 1059;
    public readonly static int SciroccosDervishChanceOfDamageLethal = 123;
    public readonly static int ScrappersStrikeRechargeCriticalHitBonus = 897;
    public readonly static int SentinelsWardRechargeTimeChanceForAbsorb = 1227;
    public readonly static int ShieldBreakerChanceForLethalDamage = 774;
    public readonly static int ShieldWallResTeleportation5ResAll = 843;
    public readonly static int SiphonInsightChanceForToHit = 780;
    public readonly static int SoulboundAllegianceChanceForBuildUp = 579;
    public readonly static int SovereignRightResistBonus = 245;
    public readonly static int SpidersBiteRechargeTimeGlobalToxic = 1131;
    public readonly static int StalkersGuileRechargeChanceToHide = 903;
    public readonly static int SteadfastProtectionKnockbackProtection = 280;
    public readonly static int SteadfastProtectionPlusDefense = 279;
    public readonly static int StingOfTheManticoreChanceOfDamageToxic = 212;
    public readonly static int StupefyChanceOfKnockback = 388;
    public readonly static int SuddenAccelerationKnockbackToKnockdown = 1251;
    public readonly static int SuperiorAscendencyOfTheDominatorRechargeChanceForDamage = 957;
    public readonly static int SuperiorAssassinsMarkRechargeTimeRchgBuildUp = 1149;
    public readonly static int SuperiorAvalancheRechargeChanceForKnockdown = 1185;
    public readonly static int SuperiorBlastersWrathRechargeChanceForFireDamage = 927;
    public readonly static int SuperiorBlisteringColdChanceForHold = 1173;
    public readonly static int SuperiorBrutesFuryRechargeFuryBonus = 933;
    public readonly static int SuperiorCommandOfTheMastermindRechargePetAoEDefenseAura = 969;
    public readonly static int SuperiorCriticalStrikesRechargeTime50CritProc = 1125;
    public readonly static int SuperiorDefendersBastionRechargeChanceForMinorPBAoEHeal = 951;
    public readonly static int SuperiorDefiantBarrageRechargeTimeStatus = 1029;
    public readonly static int SuperiorDominatingGraspRechargeTimeFieryOrb = 1089;
    public readonly static int SuperiorDominionOfArachnosRechargeChanceForMinusDmgAndTerrorize = 921;
    public readonly static int SuperiorEntombRechargeChanceForAbsorb = 1221;
    public readonly static int SuperiorEssenceTransferRechargeTimeGlobalHeal = 1101;
    public readonly static int SuperiorFrozenBlastRechargeChanceForImmobilize = 1209;
    public readonly static int SuperiorGauntletedFistRechargeTimeAbsorb = 1161;
    public readonly static int SuperiorKheldiansGraceRechargeFormEmpowerment = 963;
    public readonly static int SuperiorMaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage = 945;
    public readonly static int SuperiorMarkOfSupremacyEndurancePetResistRegen = 1113;
    public readonly static int SuperiorMightOfTheTankerRechargeChanceForResAll = 987;
    public readonly static int SuperiorOpportunityStrikesRechargeTimeChanceForOpportunity = 1245;
    public readonly static int SuperiorOverpoweringPresenceRechargeTimeEnergyFont = 1053;
    public readonly static int SuperiorScourgingBlastRechargeTimePBAoEEnd = 1065;
    public readonly static int SuperiorScrappersStrikeRechargeCriticalHitBonus = 975;
    public readonly static int SuperiorSentinelsWardRechargeTimeChanceForAbsorb = 1233;
    public readonly static int SuperiorSpidersBiteRechargeTimeGlobalToxic = 1137;
    public readonly static int SuperiorStalkersGuileRechargeChanceToHide = 981;
    public readonly static int SuperiorUnrelentingFuryRechargeTimeRegenEnd = 1041;
    public readonly static int SuperiorVigilantAssaultRechargeTimePBAoEAbsorb = 1077;
    public readonly static int SuperiorWillOfTheControllerRechargeChanceForPsionicDamage = 939;
    public readonly static int SuperiorWintersBiteRechargeChanceForMinusSpeedAndMinusRecharge = 1197;
    public readonly static int TempestChanceOfEndDrain = 136;
    public readonly static int TheftOfEssenceChanceForEndurance = 786;
    public readonly static int TouchOfDeathChanceOfDamageNegative = 95;
    public readonly static int TouchOfLadyGreyChanceForNegativeDamage = 717;
    public readonly static int TouchOfTheNictusChanceForNegativeEnergyDamage = 792;
    public readonly static int TrapOfTheHunterChanceOfDamageLethal = 411;
    public readonly static int UnbreakableConstraintChanceForSmashingDamage = 585;
    public readonly static int UnbreakableGuardMaxHP = 1017;
    public readonly static int UnrelentingFuryRechargeTimeRegenEnd = 1035;
    public readonly static int UnspeakableTerrorDisorientBonus = 468;
    public readonly static int VigilantAssaultRechargeTimePBAoEAbsorb = 1071;
    public readonly static int WillOfTheControllerRechargeChanceForPsionicDamage = 867;
    public readonly static int WintersBiteRechargeChanceForMinusSpeedAndMinusRecharge = 1191;

    private readonly static float Special = 0.01f;
    private readonly static float SuperiorSpecial = 0.015f;

    public static int[] GetSpecialEnhancementIndexes()
    {
        int[] indexes = new int[] {
            309, 885, 1143, 855, 1167, 861, 738, 609, 891, 1119, 879, 1023, 168, 1083, 849, 233, 1095, 756, 1155, 265, 820,
            285, 297, 248, 915, 253, 277, 873, 1107, 909, 330, 342, 1239, 1047, 801, 663, 1005, 1011, 318, 897, 1059, 1227,
            843, 245, 1131, 903, 280, 279, 957, 1149, 927, 1173, 933, 969, 1125, 951, 1029, 1089, 921, 1101, 1161, 963, 945,
            1113, 987, 1245, 1053, 975, 1065, 1233, 1137, 981, 1041, 1077, 939, 1017, 1035, 1071, 867, 1215, 1221, 1179, 1185,
            1191, 1197, 1203, 1209, 993, 585, 597, 555, 573, 579, 561, 603, 591, 567, 645, 1251, 732, 723, 792, 780, 503, 717,
            480, 365, 762, 411, 107, 768, 123, 457, 434, 200, 212, 388, 999, 184, 627, 699, 681, 95, 156, 774, 786, 744, 468,
            729, 353, 84, 75, 750, 145, 136, 445, 422, 376, 837, 808, 831, 812, 802
        };

        return indexes;
    }

    public static IEnhancement[] GetSpecialEnhancements()
    {
        int[] indexes = GetSpecialEnhancementIndexes();
        List<IEnhancement> enhancements = new List<IEnhancement>();

        foreach (int index in indexes)
            enhancements.Add(DatabaseAPI.GetEnhancementByIndex(index));
        return enhancements.ToArray();
    }

    public static bool IsSpecialEnhancement(IEnhancement enhancement)
    {
        IEnhancement[] enhancements = GetSpecialEnhancements();
        return enhancements.Any(item => item.LongName == enhancement.LongName);
    }

    public static float GetSpecialBonus(BonusInfo bonusInfo, IEnhancement enhancement)
    {
        int[] specialIOIndexes = bonusInfo.GetRelatedSpecialIOIndexes();

        foreach (int enhancementIndex in specialIOIndexes) {
            IEnhancement specialIO = DatabaseAPI.GetEnhancementByIndex(enhancementIndex);
            if (null == specialIO || enhancement.LongName != specialIO.LongName)
                continue;
            IPower power = specialIO.GetPower();
            if (null == power)
            {
                if ("Opportunity Strikes: RechargeTime/Chance for Opportunity" == specialIO.LongName) return Special;
                if ("Sentinel's Ward: RechargeTime/Chance for +Absorb" == specialIO.LongName) return Special;
                if ("Sudden Acceleration: Knockback to Knockdown" == specialIO.LongName) return Special;
                if ("Superior Opportunity Strikes: RechargeTime/Chance for Opportunity" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Sentinel's Ward: RechargeTime/Chance for +Absorb" == specialIO.LongName) return SuperiorSpecial;
                return 0.0f;
            }
            foreach (IEffect effect in power.Effects) {
                if ("Absolute Amazement: Chance for ToHit Debuff" == specialIO.LongName) return Special;
                if ("Aegis: Psionic/Status Resistance" == specialIO.LongName) {
                    if (0 == bonusInfo.CompareTo(BonusInfo.ResistancePsionic) && Enums.eEffectType.ResEffect == effect.EffectType && "Psionic" == effect.EffectId)
                        return effect.Scale;
                    if (0 == bonusInfo.CompareTo(BonusInfo.MezResistance) && Enums.eEffectType.MezResist == effect.EffectType)
                        return effect.Scale;
                }
                if ("Analyze Weakness: Chance for +ToHit" == specialIO.LongName) return Special;
                if ("Annihilation: Chance for Res Debuff" == specialIO.LongName) return Special;
                if ("Apocalypse: Chance of Damage(Negative)" == specialIO.LongName) return Special;
                if ("Armageddon: Chance for Fire Damage" == specialIO.LongName) return Special;
                if ("Ascendency of the Dominator: Recharge/Chance for +Damage" == specialIO.LongName) return Special;
                if ("Assassin's Mark: RechargeTime/Rchg Build Up" == specialIO.LongName) return Special;
                if ("Avalanche: Recharge/Chance for Knockdown" == specialIO.LongName) return Special;
                if ("Blaster's Wrath: Recharge/Chance for Fire Damage" == specialIO.LongName) return Special;
                if ("Basilisk's Gaze: Chance for Recharge Slow" == specialIO.LongName) return Special;
                if ("Blistering Cold: Recharge/Chance for Hold" == specialIO.LongName) return Special;
                if ("Blessing of the Zephyr: Knockback Reduction (4 points)" == specialIO.LongName) return 4;
                if ("Brute's Fury: Recharge/Fury Bonus" == specialIO.LongName) return Special;
                if ("Call of the Sandman: Chance of Heal Self" == specialIO.LongName) return Special;
                if ("Call to Arms: Defense Bonus Aura for Pets" == specialIO.LongName) {
                    if ("Defense" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Cloud Senses: Chance for Negative Energy Damage" == specialIO.LongName) return Special;
                if ("Coercive Persuasion : Contagious Confusion" == specialIO.LongName) {
                    if (Enums.eEffectType.GrantPower == effect.EffectType && "Boost" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Command of the Mastermind: Recharge/Pet +AoE Defense Aura" == specialIO.LongName) return Special;
                if ("Critical Strikes: RechargeTime/+50% Crit Proc" == specialIO.LongName) return Special;
                if ("Dark Watcher's Despair: Chance for Recharge Slow" == specialIO.LongName) return Special;
                if ("Defender's Bastion: Recharge/Chance for Minor PBAoE Heal" == specialIO.LongName) return Special;
                if ("Defiant Barrage: RechargeTime/+Status" == specialIO.LongName) return Special;
                if ("Decimation: Chance of Build Up" == specialIO.LongName) return Special;
                if ("Devastation: Chance of Hold" == specialIO.LongName)
                {
                    if (Enums.eEffectType.Mez == effect.EffectType && "Hold" == effect.EffectId)
                        return effect.Mag;
                }
                if ("Dominating Grasp: RechargeTime/Fiery Orb" == specialIO.LongName) return Special;
                if ("Dominion of Arachnos: Recharge/Chance for -Dmg and Terrorize" == specialIO.LongName) return Special;
                if ("Edict of the Master: Defense Bonus" == specialIO.LongName)
                {
                    if ("Defense" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Entomb: Recharge/Chance for +Absorb" == specialIO.LongName) return Special;
                if ("Entropic Chaos: Chance of Heal Self" == specialIO.LongName) return Special;
                if ("Eradication: Chance for Energy Damage" == specialIO.LongName) return Special;
                if ("Essence Transfer: RechargeTime/Global Heal" == specialIO.LongName) return Special;
                if ("Executioner's Contract: Disorient Bonus" == specialIO.LongName) return Special;
                if ("Expedient Reinforcement: Resist Bonus Aura for Pets" == specialIO.LongName) {
                    if ("Resist" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Force Feedback: Chance for +Recharge" == specialIO.LongName) return Special;
                if ("Fortunata Hypnosis: Chance for Placate" == specialIO.LongName) return Special;
                if ("Frozen Blast: Recharge/Chance for Immobilize" == specialIO.LongName) return Special;
                if ("Fury of the Gladiator: Chance for Res Debuff" == specialIO.LongName) return Special;
                if ("Gauntleted Fist: RechargeTime/+Absorb" == specialIO.LongName) return Special;
                if ("Gaussian's Synchronized Fire-Control: Chance for Build Up" == specialIO.LongName) return Special;
                if ("Ghost Widow's Embrace: Chance of Damage(Psionic)" == specialIO.LongName) return Special;
                if ("Gift of the Ancients: Run Speed +7.5%" == specialIO.LongName)
                {
                    if ("Run" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Gladiator's Armor: TP Protection +3% Def (All)" == specialIO.LongName) {
                    if ("Defense" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Gladiator's Javelin: Chance of Damage(Toxic)" == specialIO.LongName) return Special;
                if ("Gladiator's Net: Chance of Damage(Lethal)" == specialIO.LongName) return Special;
                if ("Gladiator's Strike: Chance for Smashing Damage" == specialIO.LongName) return Special;
                if ("Glimpse of the Abyss: Chance of Damage(Psionic)" == specialIO.LongName) return Special;
                if ("Gravitational Anchor: Chance for Hold" == specialIO.LongName) return Special;
                if ("Hecatomb: Chance of Damage(Negative)" == specialIO.LongName) return Special;
                if ("Impervious Skin: Status Resistance" == specialIO.LongName) {
                    if ("Status" == effect.EffectId)
                        return effect.Scale;
                }
                
                if ("Impeded Swiftness: Chance of Damage(Smashing)" == specialIO.LongName) return Special;
                if ("Impervium Armor: Psionic Resistance" == specialIO.LongName) {
                    if ("Resist" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Induced Coma: Chance of -Recharge" == specialIO.LongName) return Special;
                if ("Javelin Volley: Chance of Damage(Lethal)" == specialIO.LongName) return Special;
                if ("Karma: Knockback Protection" == specialIO.LongName) {
                    if ("Knockback" == effect.EffectId)
                        return Math.Abs(effect.Scale);
                }
                if ("Kheldian's Grace: Recharge/Form Empowerment" == specialIO.LongName) return Special;
                if ("Kinetic Combat: Knockdown Bonus" == specialIO.LongName) return Special;
                if ("Kismet: Accuracy +6%" == specialIO.LongName)
                {
                    if ("Accuracy" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Lockdown: Chance for +2 Mag Hold" == specialIO.LongName) return Special;
                if ("Luck of the Gambler: Defense/Increased Global Recharge Speed" == specialIO.LongName) {
                    if (Enums.eEffectType.Enhancement == effect.EffectType && "Recharge" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Mako's Bite: Chance of Damage(Lethal)" == specialIO.LongName) return Special;
                if ("Malaise's Illusions: Chance of Damage(Psionic)" == specialIO.LongName) return Special;
                if ("Malice of the Corruptor: Recharge/Chance for Negative Energy Damage" == specialIO.LongName) return Special;
                if ("Mark of Supremacy: Endurance/Pet +Resist +Regen" == specialIO.LongName) return Special;
                if ("Might of the Tanker: Recharge/Chance for +Res(All)" == specialIO.LongName) return Special;
                if ("Miracle: +Recovery" == specialIO.LongName) {
                    if ("Recovery" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Neuronic Shutdown: Chance of Damage(Psionic)" == specialIO.LongName) return Special;
                if ("Numina's Convalesence: +Regeneration/+Recovery" == specialIO.LongName) {
                    if (0 == bonusInfo.CompareTo(BonusInfo.Recovery) && Enums.eEffectType.Recovery == effect.EffectType)
                        return effect.Scale;
                    if (0 == bonusInfo.CompareTo(BonusInfo.Regeneration) && Enums.eEffectType.Regeneration == effect.EffectType)
                        return effect.Scale;
                }
                if ("Obliteration: Chance for Smashing Damage" == specialIO.LongName) return Special;
                if ("Overpowering Presence: RechargeTime/Energy Font" == specialIO.LongName) return Special;
                if ("Overwhelming Force: Damage/Chance for Knockdown/Knockback to Knockdown" == specialIO.LongName) return Special - 0.001f;
                if ("Pacing of the Turtle: Chance of -Recharge" == specialIO.LongName) return Special;
                if ("Panacea: +Hit Points/Endurance" == specialIO.LongName) {
                    if (0 == bonusInfo.CompareTo(BonusInfo.Recovery) && Enums.eEffectType.Endurance == effect.EffectType)
                        return effect.Scale;
                    if (0 == bonusInfo.CompareTo(BonusInfo.Regeneration) && Enums.eEffectType.Regeneration == effect.EffectType)
                        return effect.Scale;
                    if (0 == bonusInfo.CompareTo(BonusInfo.Heal) && Enums.eEffectType.Heal == effect.EffectType)
                        return effect.Scale;
                }
                if ("Perfect Zinger: Chance for Psi Damage" == specialIO.LongName) return Special;
                if ("Performance Shifter: Chance for +End" == specialIO.LongName) {
                    if (Enums.eEffectType.Endurance == effect.EffectType)
                        return effect.Scale;
                }
                if ("Preventive Medicine: Chance for +Absorb" == specialIO.LongName) {
                    return 0.01f;
                }
                if ("Ragnarok: Chance for Knockdown" == specialIO.LongName) return Special;
                if ("Reactive Defenses: Scaling Resist Damage" == specialIO.LongName) {
                    return effect.Scale;
                }
                if ("Positron's Blast: Chance of Damage(Energy)" == specialIO.LongName) return Special;
                if ("Pounding Slugfest: Disorient Bonus" == specialIO.LongName) return Special;
                if ("Razzle Dazzle: Chance of Immobilize" == specialIO.LongName) return Special;
                if ("Regenerative Tissue: +Regeneration" == specialIO.LongName) {
                    if (Enums.eEffectType.Regeneration == effect.EffectType)
                        return effect.Scale;
                }
                if ("Scirocco's Dervish: Chance of Damage(Lethal)" == specialIO.LongName) return Special;
                if ("Scourging Blast: RechargeTime/PBAoE +End" == specialIO.LongName) return Special;
                if ("Scrapper's Strike: Recharge/Critical Hit Bonus" == specialIO.LongName) return Special;
                if ("Shield Breaker: Chance for Lethal Damage" == specialIO.LongName) return Special;
                if ("Siphon Insight: Chance for +ToHit" == specialIO.LongName) return Special;
                if ("Soulbound Allegiance: Chance for Build Up" == specialIO.LongName) return Special;
                if ("Sovereign Right: Resistance Bonus" == specialIO.LongName) {
                    if ("Resist" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Shield Wall: +Res (Teleportation), +5% Res (All)" == specialIO.LongName) {
                    if ("Res" == effect.EffectId)
                        return effect.Scale;
                }
                if ("Spider's Bite: RechargeTime/Global Toxic" == specialIO.LongName) return Special;
                if ("Stalker's Guile: Recharge/Chance to Hide" == specialIO.LongName) return Special;
                if ("Steadfast Protection: Knockback Protection" == specialIO.LongName) {
                    if ("Knockback" == effect.EffectId)
                        return Math.Abs(effect.Scale);
                }
                if ("Steadfast Protection: Resistance/+Def 3%" == specialIO.LongName) {
                    return 0.03f;
                }

                if ("Sting of the Manticore: Chance of Damage(Toxic)" == specialIO.LongName) return Special;
                if ("Stupefy: Chance of Knockback" == specialIO.LongName) return Special;
                if ("Superior Ascendency of the Dominator: Recharge/Chance for +Damage" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Assassin's Mark: RechargeTime/Rchg Build Up" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Avalanche: Recharge/Chance for Knockdown" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Blaster's Wrath: Recharge/Chance for Fire Damage" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Blistering Cold: Recharge/Chance for Hold" == specialIO.LongName) {
                    if ("Hold" == effect.EffectId)
                        return effect.Mag;
                }
                if ("Superior Brute's Fury: Recharge/Fury Bonus" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Command of the Mastermind: Recharge/Pet +AoE Defense Aura" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Blaster's Wrath: Recharge/Chance for Fire Damage" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Critical Strikes: RechargeTime/+50% Crit Proc" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Defender's Bastion: Recharge/Chance for Minor PBAoE Heal" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Defiant Barrage: RechargeTime/+Status" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Dominating Grasp: RechargeTime/Fiery Orb" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Dominion of Arachnos: Recharge/Chance for -Dmg and Terrorize" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Entomb: Recharge/Chance for +Absorb" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Essence Transfer: RechargeTime/Global Heal" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Frozen Blast: Recharge/Chance for Immobilize" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Gauntleted Fist: RechargeTime/+Absorb" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Kheldian's Grace: Recharge/Form Empowerment" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Malice of the Corruptor: Recharge/Chance for Negative Energy Damage" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Mark of Supremacy: Endurance/Pet +Resist +Regen" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Might of the Tanker: Recharge/Chance for +Res(All)" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Overpowering Presence: RechargeTime/Energy Font" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Scourging Blast: RechargeTime/PBAoE +End" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Scrapper's Strike: Recharge/Critical Hit Bonus" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Spider's Bite: RechargeTime/Global Toxic" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Stalker's Guile: Recharge/Chance to Hide" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Unrelenting Fury: RechargeTime/+Regen/+End" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Vigilant Assault: RechargeTime/PBAoE +Absorb" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Will of the Controller: Recharge/Chance for Psionic Damage" == specialIO.LongName) return SuperiorSpecial;
                if ("Superior Winter's Bite: Recharge/Chance for -Speed & -Recharge" == specialIO.LongName) return SuperiorSpecial;
                if ("Tempest: Chance of End Drain" == specialIO.LongName) return Special;
                if ("Theft of Essence: Chance for +Endurance" == specialIO.LongName) return Special;
                if ("Trap of the Hunter: Chance of Damage(Lethal)" == specialIO.LongName) return Special;
                if ("Touch of Death: Chance of Damage(Negative)" == specialIO.LongName) return Special;
                if ("Touch of Lady Grey: Chance for Negative Damage" == specialIO.LongName) return Special;
                if ("Touch of the Nictus: Chance for Negative Energy Damage" == specialIO.LongName) return Special;
                if ("Unbreakable Constraint: Chance for Smashing Damage" == specialIO.LongName) return Special;
                if ("Unbreakable Guard: +Max HP" == specialIO.LongName) return effect.Scale;
                if ("Unrelenting Fury: RechargeTime/+Regen/+End" == specialIO.LongName) return Special;
                if ("Unspeakable Terror: Disorient Bonus" == specialIO.LongName) return Special;
                if ("Vigilant Assault: RechargeTime/PBAoE +Absorb" == specialIO.LongName) return Special;
                if ("Will of the Controller: Recharge/Chance for Psionic Damage" == specialIO.LongName) return Special;
                if ("Winter's Bite: Recharge/Chance for -Speed & -Recharge" == specialIO.LongName) return Special;
            }
        }
        return 0.0f;
    }

    // Call this after all database info has loaded, such as at the end of LoadEnhancementDb()
    public static void TestNewCode()
    {
        TestGetSpecialBonus();
    }

    public static void TestGetSpecialBonus()
    {
        IEnhancement specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AbsoluteAmazementChanceForToHitDebuff);
        float bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ReduceDamage, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DefenseAll, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.0f, 0.001f));

        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AegisPsionicStatusResistance);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistancePsionic, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.05f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.2f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AnalyzeWeaknessChanceForToHit);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Accuracy, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AnnihilationChanceForResDebuff);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ApocalypseChanceOfDamageNegative);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ArmageddonChanceForFireDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AscendencyOfTheDominatorRechargeChanceForDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AssassinsMarkRechargeTimeRchgBuildUp);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AvalancheRechargeChanceForKnockdown);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.BasilisksGazeChanceForRechargeSlow);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.BlastersWrathRechargeChanceForFireDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.BlessingOfTheZephyrKnockbackReduction);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.KnockbackProtection, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 4.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.BlisteringColdChanceForHold);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Hold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.BrutesFuryRechargeFuryBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CallOfTheSandmanChanceOfHealSelf);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CallToArmsDefenseBonusAuraForPets);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetDefense, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CloudSensesChanceForNegativeEnergyDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CoercivePersuasionContagiousConfusion);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Confuse, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CommandOfTheMastermindRechargePetAoEDefenseAura);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetDefense, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.CriticalStrikesRechargeTime50CritProc);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DarkWatchersDespairChanceForRechargeSlow);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DecimationChanceOfBuildUp);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DefendersBastionRechargeChanceForMinorPBAoEHeal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DefiantBarrageRechargeTimeStatus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DevastationChanceForHold);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Hold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 2.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DominatingGraspRechargeTimeFieryOrb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.DominionOfArachnosRechargeChanceForMinusDmgAndTerrorize);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezEnhance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ReduceDamage, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.EdictOfTheMasterDefenseBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetDefense, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.EntombRechargeChanceForAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.EntropicChaosChanceOfHealSelf);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.EradicationChanceForEnergyDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.EssenceTransferRechargeTimeGlobalHeal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ExecutionersContractDisorientBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Stun, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ExpedientReinforcementResistBonusAuraForPets);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ForceFeedbackChanceForRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.RechargeTime, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.FortunataHypnosisChanceForPlacate);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ReduceDamage, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.FrozenBlastRechargeChanceForImmobilize);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Immobilize, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.FuryOfTheGladiatorChanceForResDebuff);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GauntletedFistRechargeTimeAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GaussiansSynchronizedFireControlChanceForBuildUp);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GhostWidowsEmbraceChanceOfDamagePsionic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GiftOfTheAncientsRunSpeed);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Running, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.075f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GladiatorsArmorTPProtection3DefAll);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DefenseAll, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GladiatorsJavelinChanceOfDamageToxic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GladiatorsNetChanceOfDamageLethal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GladiatorsStrikeChanceForSmashingDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GlimpseOfTheAbyssChanceOfDamagePsionic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.GravitationalAnchorChanceForHold);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Hold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.HecatombChanceOfDamageNegative);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ImpededSwiftnessChanceOfDamageSmashing);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ImperviousSkinStatusResistance);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ImperviumArmorResPsi);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistancePsionic, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.06f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.InducedComaChanceOfMinusRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.JavelinVolleyChanceOfDamageLethal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.KarmaKnockbackProtection);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.KnockbackProtection, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 4.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.KheldiansGraceRechargeFormEmpowerment);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceCold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.KineticCombatKnockdownBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.KismetPlusAccuracy);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Accuracy, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.06f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.LockdownChanceForPlus2MagHold);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Hold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.LuckOfTheGamblerGlobalRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.RechargeTime, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.075f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MakosBiteChanceOfDamageLethal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MalaisesIllusionsChanceOfDamagePsionic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MarkOfSupremacyEndurancePetResistRegen);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MightOfTheTankerRechargeChanceForResAll);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceEnergy, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.MiracleRecovery);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.15f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.NuminasConvalesenceRegenerationRecovery);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.10f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Regeneration, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.20f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.NeuronicShutdownChanceOfDamagePsionic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ObliterationChanceForSmashingDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.OpportunityStrikesRechargeTimeChanceForOpportunity);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.OverpoweringPresenceRechargeTimeEnergyFont);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.OverwhelmingForceDamageChanceForKnockdownKnockbackToKnockdown);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special - 0.001f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PacingOfTheTurtleChanceOfMinusRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PanaceaHitPointsEndurance);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.075f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Regeneration, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.2f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.67f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PerfectZingerChanceForPsiDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PerformanceShifterChanceForPlusEndurance);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.10f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PoundingSlugfestDisorientBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Stun, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.01f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PreventiveMedicineChanceForAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.01f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.PositronsBlastChanceOfDamageEnergy);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.RagnarokChanceForKnockdown);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.RazzleDazzleChanceOfImmobilize);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Immobilize, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ReactiveDefensesScalingResistDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceCold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceEnergy, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.RegenerativeTissueRegeneration);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Regeneration, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.25f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ScourgingBlastRechargeTimePBAoEEnd);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ScrappersStrikeRechargeCriticalHitBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SentinelsWardRechargeTimeChanceForAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ShieldBreakerChanceForLethalDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.ShieldWallResTeleportation5ResAll);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceLethal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceNegative, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SciroccosDervishChanceOfDamageLethal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SiphonInsightChanceForToHit);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Accuracy, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SovereignRightResistBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 1.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SoulboundAllegianceChanceForBuildUp);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SpidersBiteRechargeTimeGlobalToxic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.StalkersGuileRechargeChanceToHide);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SteadfastProtectionKnockbackProtection);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.KnockbackProtection, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 4.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SteadfastProtectionPlusDefense);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DefenseFire, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DefensePsionic, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.03f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.StingOfTheManticoreChanceOfDamageToxic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuddenAccelerationKnockbackToKnockdown);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.StupefyChanceOfKnockback);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAscendencyOfTheDominatorRechargeChanceForDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAssassinsMarkRechargeTimeRchgBuildUp);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAvalancheRechargeChanceForKnockdown);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Knockdown, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorBlastersWrathRechargeChanceForFireDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorBlisteringColdChanceForHold);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Hold, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 3.0f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorBrutesFuryRechargeFuryBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorCommandOfTheMastermindRechargePetAoEDefenseAura);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetDefense, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorCriticalStrikesRechargeTime50CritProc);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorDefendersBastionRechargeChanceForMinorPBAoEHeal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorDefiantBarrageRechargeTimeStatus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorDominatingGraspRechargeTimeFieryOrb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorDominionOfArachnosRechargeChanceForMinusDmgAndTerrorize);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.MezEnhance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorEntombRechargeChanceForAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorEssenceTransferRechargeTimeGlobalHeal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorFrozenBlastRechargeChanceForImmobilize);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Immobilize, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorGauntletedFistRechargeTimeAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorKheldiansGraceRechargeFormEmpowerment);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistanceNegative, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorMaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorMarkOfSupremacyEndurancePetResistRegen);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.PetResistance, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorMightOfTheTankerRechargeChanceForResAll);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ResistancePsionic, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorOpportunityStrikesRechargeTimeChanceForOpportunity);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorOverpoweringPresenceRechargeTimeEnergyFont);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorScourgingBlastRechargeTimePBAoEEnd);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorScrappersStrikeRechargeCriticalHitBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorSentinelsWardRechargeTimeChanceForAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorSpidersBiteRechargeTimeGlobalToxic);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorStalkersGuileRechargeChanceToHide);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorUnrelentingFuryRechargeTimeRegenEnd);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Regeneration, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorVigilantAssaultRechargeTimePBAoEAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorWillOfTheControllerRechargeChanceForPsionicDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorWintersBiteRechargeChanceForMinusSpeedAndMinusRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, SuperiorSpecial, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TempestChanceOfEndDrain);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.ReduceDamage, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TheftOfEssenceChanceForEndurance);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TrapOfTheHunterChanceOfDamageLethal);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TouchOfDeathChanceOfDamageNegative);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TouchOfLadyGreyChanceForNegativeDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.TouchOfTheNictusChanceForNegativeEnergyDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.UnbreakableConstraintChanceForSmashingDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.UnbreakableGuardMaxHP);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, 0.075f, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.UnrelentingFuryRechargeTimeRegenEnd);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Recovery, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Regeneration, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.UnspeakableTerrorDisorientBonus);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Stun, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.VigilantAssaultRechargeTimePBAoEAbsorb);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Heal, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.HitPoints, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.WillOfTheControllerRechargeChanceForPsionicDamage);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.DamageBuff, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));
        specialIO = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.WintersBiteRechargeChanceForMinusSpeedAndMinusRecharge);
        bonus = SpecialEnhancements.GetSpecialBonus(BonusInfo.Slow, specialIO);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(bonus, Special, 0.001f));


        Action<int> printEnh = (int enhIndex) =>
        {
            IEnhancement e = DatabaseAPI.GetEnhancementByIndex(enhIndex);
            Debug.Print(e.LongName);
            IPower power = e.GetPower();
            foreach (IEffect effect in power.Effects)
            {
                Debug.Print("Type: " + effect.EffectType.ToString());
                Debug.Print("EffectID: " + effect.EffectId);
                Debug.Print("Mag: " + effect.Mag.ToString());
                Debug.Print("Scale: " + effect.Scale.ToString());
                Debug.Print("-");
            }
        };
        printEnh(SpecialEnhancements.WillOfTheControllerRechargeChanceForPsionicDamage);
        int x = 1;

    }
}

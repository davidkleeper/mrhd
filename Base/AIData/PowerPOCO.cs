﻿using System.Text.Json;

namespace AIData
{
    public static class PowerPOCOJSONSerializer
    {
        public static void Write(string filename, PowerPOCO powerPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, PowerPOCO[] powerPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class PowerPOCOConvert
    {
        public static PowerPOCO[] Convert(IPower[] powers)
        {
            PowerPOCO[] powerPOCOs = new PowerPOCO[powers.Length];
            for (int powerIndex = 0; powerIndex < powerPOCOs.Length; powerIndex++)
            {
                powerPOCOs[powerIndex] = Convert(powers[powerIndex]);
            }
            return powerPOCOs;
        }

        public static PowerPOCO Convert(IPower power)
        {
            PowerPOCO powerPOCO = new PowerPOCO();
            powerPOCO.power_set_id = power.FullSetName;
            powerPOCO.toggle_cost = power.ToggleCost;
            powerPOCO.is_epic = power.IsEpic;
            powerPOCO.slottable = power.Slottable;
            powerPOCO.location_index = power.LocationIndex;
            powerPOCO.cast_time = power.CastTime;
            powerPOCO.cast_time_real = power.CastTimeReal;
            powerPOCO.has_absorbed_effects = power.HasAbsorbedEffects;
            powerPOCO.full_name = power.FullName;
            powerPOCO.id = power.FullName;
            powerPOCO.group_name = power.GroupName;
            powerPOCO.set_name = power.SetName;
            powerPOCO.power_name = power.PowerName;
            powerPOCO.display_name = power.DisplayName;
            powerPOCO.available = power.Available;
            powerPOCO.requires = RequirementPOCOConvert.Convert(power.Requires);
            powerPOCO.modes_required = (int)power.ModesRequired;
            powerPOCO.modes_disallowed = (int)power.ModesDisallowed;
            powerPOCO.power_type = power.PowerType.ToString();
            powerPOCO.accuracy = power.Accuracy;
            powerPOCO.accuracy_mult = power.AccuracyMult;
            powerPOCO.attack_types = (int)power.AttackTypes;
            powerPOCO.group_membership = power.GroupMembership;
            powerPOCO.entities_affected = (int)power.EntitiesAffected;
            powerPOCO.entities_auto_hit = (int)power.EntitiesAutoHit;
            powerPOCO.target = (int)power.Target;
            powerPOCO.target_los = power.TargetLoS;
            powerPOCO.range = power.Range;
            powerPOCO.target_secondary = (int)power.TargetSecondary;
            powerPOCO.range_secondary = power.RangeSecondary;
            powerPOCO.end_cost = power.EndCost;
            powerPOCO.interrupt_time = power.InterruptTime;
            powerPOCO.recharge_time = power.RechargeTime;
            powerPOCO.base_recharge_time = power.BaseRechargeTime;
            powerPOCO.activate_period = power.ActivatePeriod;
            powerPOCO.effect_area = power.EffectArea.ToString();
            powerPOCO.radius = power.Radius;
            powerPOCO.arc = power.Arc;
            powerPOCO.max_targets = power.MaxTargets;
            powerPOCO.max_boosts = power.MaxBoosts;
            powerPOCO.cast_flags = (int)power.CastFlags;
            powerPOCO.ai_report = power.AIReport.ToString();
            powerPOCO.num_charges = power.NumCharges;
            powerPOCO.usage_time = power.UsageTime;
            powerPOCO.life_time = power.LifeTime;
            powerPOCO.life_time_in_game = power.LifeTimeInGame;
            powerPOCO.num_allowed = power.NumAllowed;
            powerPOCO.do_not_save = power.DoNotSave;
            powerPOCO.boosts_allowed = power.BoostsAllowed;
            powerPOCO.enhancement_ids = ConvertEnhancements(power.Enhancements);
            powerPOCO.cast_through_hold = power.CastThroughHold;
            powerPOCO.ignore_strength = power.IgnoreStrength;
            powerPOCO.description_short = power.DescShort;
            powerPOCO.description_long = power.DescLong;
            powerPOCO.sort_override = power.SortOverride;
            powerPOCO.hidden_power = power.HiddenPower;
            powerPOCO.set_types = ConvertSetTypes(power.SetTypes);
            powerPOCO.click_buff = power.ClickBuff;
            powerPOCO.always_toggle = power.AlwaysToggle;
            powerPOCO.level = power.Level;
            if ((power.DisplayName == "Health")
            || (power.DisplayName == "Stamina")
            || (power.DisplayName == "Brawl"))
            {
                powerPOCO.level = 1;
            }

            powerPOCO.allow_front_loading = power.AllowFrontLoading;
            powerPOCO.variable_enabled = power.VariableEnabled;
            powerPOCO.variable_name = power.VariableName;
            powerPOCO.variable_min = power.VariableMin;
            powerPOCO.variable_max = power.VariableMax;
            powerPOCO.sub_power_ids = power.UIDSubPower;
            powerPOCO.sub_is_alt_colour = power.SubIsAltColour;
            powerPOCO.ignore_enhance = ConvertEnhance(power.IgnoreEnh);
            powerPOCO.ignore_buff = ConvertEnhance(power.Ignore_Buff);
            powerPOCO.skip_max = power.SkipMax;
            powerPOCO.display_location = power.DisplayLocation;
            powerPOCO.mutex_auto = power.MutexAuto;
            powerPOCO.mutex_ignore = power.MutexIgnore;
            powerPOCO.absorb_summon_effects = power.AbsorbSummonEffects;
            powerPOCO.absorb_summon_attributes = power.AbsorbSummonAttributes;
            powerPOCO.show_summon_anyway = power.ShowSummonAnyway;
            powerPOCO.never_auto_update = power.NeverAutoUpdate;
            powerPOCO.never_auto_update_requirements = power.NeverAutoUpdateRequirements;
            powerPOCO.include_flag = power.IncludeFlag;
            powerPOCO.boost_boostable = power.BoostBoostable;
            powerPOCO.boost_use_player_level = power.BoostUsePlayerLevel;
            powerPOCO.forced_class = power.ForcedClass;
            powerPOCO.effects = EffectPOCOConvert.Convert(power.Effects);
            powerPOCO.buff_mode = power.BuffMode.ToString();
            powerPOCO.has_grant_power_effect = power.HasGrantPowerEffect;
            powerPOCO.has_power_override_effect = power.HasPowerOverrideEffect;
            return powerPOCO;
        }
        public static string[] ConvertEnhance(Enums.eEnhance[] enhance)
        {
            string[] converted = new string[enhance.Length];
            for (int i = 0; i < enhance.Length; i++)
            {
                converted[i] = enhance[i].ToString();
            }
            return converted;
        }
        public static string[] ConvertSetTypes(Enums.eSetType[] setTypes)
        {
            string[] converted = new string[setTypes.Length];
            for (int i = 0; i < setTypes.Length; i++)
            {
                converted[i] = setTypes[i].ToString();
            }
            return converted;
        }
        public static string[] ConvertEnhancements(int[] enhancements)
        {
            string[] converted = new string[enhancements.Length];
            for (int i = 0; i < enhancements.Length; i++)
            {
                converted[i] = DatabaseAPI.GetEnhancementByIndex(enhancements[i]).UID;
            }
            return converted;
        }
    }

    public class PowerPOCO
    {
        public bool absorb_summon_attributes { get; set; }
        public bool absorb_summon_effects { get; set; }
        public float accuracy { get; set; }
        public float accuracy_mult { get; set; }
        public float activate_period { get; set; }
        public string ai_report { get; set; }
        public bool allow_front_loading { get; set; }
        public bool always_toggle { get; set; }
        public int arc { get; set; }
        public int attack_types { get; set; }
        public int available { get; set; }
        public float base_recharge_time { get; set; }
        public bool boost_boostable { get; set; }
        public bool boost_use_player_level { get; set; }
        public string[] boosts_allowed { get; set; }
        public string buff_mode { get; set; }
        public int cast_flags { get; set; }
        public bool cast_through_hold { get; set; }
        public float cast_time { get; set; }
        public float cast_time_real { get; set; }
        public bool click_buff { get; set; }
        public string description_long { get; set; }
        public string description_short { get; set; }
        public int display_location { get; set; }
        public string display_name { get; set; }
        public bool do_not_save { get; set; }
        public string effect_area { get; set; }
        public EffectPOCO[] effects { get; set; }
        public float end_cost { get; set; }
        public string[] enhancement_ids { get; set; }
        public int entities_affected { get; set; }
        public int entities_auto_hit { get; set; }
        public string forced_class { get; set; }
        public string full_name { get; set; }
        public string[] group_membership { get; set; }
        public string group_name { get; set; }
        public bool has_absorbed_effects { get; set; }
        public bool has_grant_power_effect { get; set; }
        public bool has_power_override_effect { get; set; }
        public bool hidden_power { get; set; }
        public string id { get; set; }
        public string[] ignore_buff { get; set; }
        public string[] ignore_enhance { get; set; }
        public bool ignore_strength { get; set; }
        public bool include_flag { get; set; }
        public float interrupt_time { get; set; }
        public bool is_epic { get; set; }
        public int level { get; set; }
        public int life_time { get; set; }
        public int life_time_in_game { get; set; }
        public int location_index { get; set; }
        public string max_boosts { get; set; }
        public int max_targets { get; set; }
        public int modes_disallowed { get; set; }
        public int modes_required { get; set; }
        public bool mutex_auto { get; set; }
        public bool mutex_ignore { get; set; }
        public bool never_auto_update { get; set; }
        public bool never_auto_update_requirements { get; set; }
        public int num_allowed { get; set; }
        public int num_charges { get; set; }
        public string power_name { get; set; }
        public string power_set_id { get; set; }
        public string power_type { get; set; }
        public float radius { get; set; }
        public float range { get; set; }
        public float range_secondary { get; set; }
        public float recharge_time { get; set; }
        public RequirementPOCO requires { get; set; }
        public string set_name { get; set; }
        public string[] set_types { get; set; }
        public bool show_summon_anyway { get; set; }
        public bool skip_max { get; set; }
        public bool slottable { get; set; }
        public bool sort_override { get; set; }
        public bool sub_is_alt_colour { get; set; }
        public string[] sub_power_ids { get; set; }
        public int target { get; set; }
        public bool target_los { get; set; }
        public int target_secondary { get; set; }
        public float toggle_cost { get; set; }
        public int usage_time { get; set; }
        public bool variable_enabled { get; set; }
        public int variable_max { get; set; }
        public int variable_min { get; set; }
        public string variable_name { get; set; }
    }

    public static class PowerPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            PowerPOCO crystalArmorPOCO = PowerPOCOConvert.Convert(crystalArmor);
            PowerPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power.json", crystalArmorPOCO);
            PowerPOCO[] powerPOCOs = PowerPOCOConvert.Convert(DatabaseAPI.Database.Power);
            PowerPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\powers.json", powerPOCOs);
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class PowerSubEntryPOCOJSONSerializer
    {
        public static void Write(string filename, PowerSubEntryPOCO powerSubEntryPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerSubEntryPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, PowerSubEntryPOCO[] powerSubEntryPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerSubEntryPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class PowerSubEntryPOCOConvert
    {
        public static PowerSubEntryPOCO[] Convert(PowerSubEntry[] powerSubEntries)
        {
            PowerSubEntryPOCO[] powerSubEntryPOCOs = new PowerSubEntryPOCO[powerSubEntries.Length];
            for (int powerSubEntryIndex = 0; powerSubEntryIndex < powerSubEntryPOCOs.Length; powerSubEntryIndex++)
            {
                powerSubEntryPOCOs[powerSubEntryIndex] = Convert(powerSubEntries[powerSubEntryIndex]);
            }
            return powerSubEntryPOCOs;
        }

        public static PowerSubEntryPOCO Convert(PowerSubEntry powerSubEntry)
        {
            PowerSubEntryPOCO powerSubEntryPOCO = new PowerSubEntryPOCO();
            IPowerset powerset = DatabaseAPI.GetPowersetByIndex(powerSubEntry.Powerset);
            IPower power = powerset.Powers[powerSubEntry.Power];
            powerSubEntryPOCO.powerset_id = powerset.FullName;
            powerSubEntryPOCO.power_id = power.FullName;
            powerSubEntryPOCO.stat_include = powerSubEntry.StatInclude;

            return powerSubEntryPOCO;
        }
    }

    public class PowerSubEntryPOCO
    {
        public string powerset_id { get; set; }
        public string power_id { get; set; }
        public bool stat_include { get; set; }
    }


    public static class PowerSubEntryPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            PowerEntry personalForceFieldPowerEntry = new PowerEntry(-1, personalForceField);
            personalForceFieldPowerEntry.Slot(new int[] { (int)EnhancementType.Defense, (int)EnhancementType.Defense, (int)EnhancementType.Defense });

            // PowerSubEntryPOCO powerSubEntryPOCO = PowerSubEntryPOCOConvert.Convert(personalForceFieldPowerEntry.SubPowers[0]);
            // PowerSubEntryPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-sub-entry.json", powerSubEntryPOCO);
            // PowerSubEntryPOCO[] powerSubEntryPOCOs = PowerSubEntryPOCOConvert.Convert(personalForceFieldPowerEntry.SubPowers);
            // PowerSubEntryPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-sub-entries.json", powerSubEntryPOCOs);
        }
    }
}

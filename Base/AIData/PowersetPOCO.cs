﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using Base.Data_Classes;

namespace AIData
{
    public static class PowersetPOCOJSONSerializer
    {
        public static void Write(string filename, PowersetPOCO powersetPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powersetPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, PowersetPOCO[] powersetPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powersetPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class PowersetPOCOConvert
    {
        public static PowersetPOCO[] Convert(IPowerset[] powersets)
        {
            PowersetPOCO[] powersetPOCOs = new PowersetPOCO[powersets.Length];
            for (int powersetIndex = 0; powersetIndex < powersetPOCOs.Length; powersetIndex++)
            {
                powersetPOCOs[powersetIndex] = Convert(powersets[powersetIndex]);
            }
            return powersetPOCOs;
        }

        public static PowersetPOCO Convert(IPowerset powerset)
        {
            PowersetPOCO powersetPOCO = new PowersetPOCO();
            Archetype archetype = (0 <= powerset.nArchetype)? DatabaseAPI.Database.Classes[powerset.nArchetype] : null;
            powersetPOCO.id = powerset.FullName;
            powersetPOCO.archetype_id = archetype?.ClassName ?? "";
            powersetPOCO.display_name = powerset.DisplayName;
            powersetPOCO.set_type = powerset.SetType.ToString();
            powersetPOCO.power_ids = ConvertPowers(powerset.Powers);
            powersetPOCO.image_name = powerset.ImageName;
            powersetPOCO.full_name = powerset.FullName;
            powersetPOCO.set_name = powerset.SetName;
            powersetPOCO.description = powerset.Description;
            powersetPOCO.sub_name = powerset.SubName;
            powersetPOCO.group_name = powerset.GroupName;

            return powersetPOCO;
        }
        public static string[] ConvertPowers(IPower[] powers)
        {
            string[] result = new string[powers.Length];
            for (int powerIndex = 0; powerIndex < powers.Length; powerIndex++)
            {
                result[powerIndex] = powers[powerIndex].FullName;
            }
            return result;
        }
    }

    public class PowersetPOCO
    {
        public string archetype_id { get; set; }
        public string description { get; set; }
        public string display_name { get; set; }
        public string full_name { get; set; }
        public string group_name { get; set; }
        public string id { get; set; }
        public string image_name { get; set; }
        public string[] power_ids { get; set; }
        public string set_name { get; set; }
        public string set_type { get; set; }
        public string sub_name { get; set; }
    }

    public static class PowersetPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            PowersetPOCO effectPOCO = PowersetPOCOConvert.Convert(DatabaseAPI.Database.Powersets[0]);
            PowersetPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-set.json", effectPOCO);
            PowersetPOCO[] effectPOCOs = PowersetPOCOConvert.Convert(DatabaseAPI.Database.Powersets);
            PowersetPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-sets.json", effectPOCOs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class SlotPOCOJSONSerializer
    {
        public static void Write(string filename, SlotPOCO slotPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(slotPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, SlotPOCO[] slotPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(slotPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class SlotPOCOConvert
    {
        public static SlotPOCO[] Convert(SlotEntry[] slots)
        {
            SlotPOCO[] slotPOCOs = new SlotPOCO[slots.Length];
            for (int slotIndex = 0; slotIndex < slotPOCOs.Length; slotIndex++)
            {
                slotPOCOs[slotIndex] = Convert(slots[slotIndex]);
            }
            return slotPOCOs;
        }

        public static SlotPOCO Convert(SlotEntry slot)
        {
            SlotPOCO slotPOCO = new SlotPOCO();
            slotPOCO.level = slot.Level;
            if (null == slot.Enhancement) return slotPOCO;
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(slot.Enhancement.Enh);
            slotPOCO.enhancement_id = (enhancement != null) ? enhancement.UID : "";
            slotPOCO.relative_level = slot.Enhancement.RelativeLevel.ToString();
            slotPOCO.grade = Convert(slot.Enhancement.Grade);
            slotPOCO.enhancement_level = slot.Enhancement.IOLevel;

            return slotPOCO;
        }
        public static string Convert(Enums.eEnhGrade grade)
        {
            if (Enums.eEnhGrade.DualO == grade)
                return "Dual";
            if (Enums.eEnhGrade.SingleO == grade)
                return "Single";
            if (Enums.eEnhGrade.TrainingO == grade)
                return "Training";
            return "None";
        }
    }

    public class SlotPOCO
    {
        public string enhancement_id { get; set; }
        public int enhancement_level { get; set; }
        public string grade { get; set; }
        public int level { get; set; }
        public string relative_level { get; set; }
    }

    public static class SlotPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            SlotEntry slotEntry = new SlotEntry();
            I9Slot slot = new I9Slot();
            slotEntry.Enhancement = slot;
            slot.Enh = SpecialEnhancements.AbsoluteAmazementChanceForToHitDebuff;
            slot.RelativeLevel = Enums.eEnhRelative.Even;
            slot.Grade = Enums.eEnhGrade.None;
            slot.IOLevel = 49;
            SlotPOCO slotPOCO = SlotPOCOConvert.Convert(slotEntry);
            SlotPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\slot.json", slotPOCO);
            slot.Enh = (int)EnhancementType.Recharge;
            SlotEntry[] slotEntries = new SlotEntry[3] { slotEntry, slotEntry, slotEntry };
            SlotPOCO[] slotPOCOs = SlotPOCOConvert.Convert(slotEntries);
            SlotPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\slots.json", slotPOCOs);
        }
    }
}

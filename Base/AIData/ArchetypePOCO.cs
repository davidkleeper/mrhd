﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class ArchetypePOCOJSONSerializer
    {
        public static void Write(string filename, ArchetypePOCO archetypePOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(archetypePOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, ArchetypePOCO[] archetypePOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(archetypePOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class ArchetypePOCOConvert
    {
        public static ArchetypePOCO[] Convert(Base.Data_Classes.Archetype[] archetypes)
        {
            ArchetypePOCO[] archetypePOCOs = new ArchetypePOCO[archetypes.Length];
            for (int archetypeIndex = 0; archetypeIndex < archetypePOCOs.Length; archetypeIndex++)
            {
                archetypePOCOs[archetypeIndex] = Convert(archetypes[archetypeIndex]);
            }
            return archetypePOCOs;
        }

        public static ArchetypePOCO Convert(Base.Data_Classes.Archetype archetype)
        {
            ArchetypePOCO archetypePOCO = new ArchetypePOCO();
            archetypePOCO.display_name = archetype.DisplayName;
            archetypePOCO.class_type = archetype.ClassType.ToString();
            archetypePOCO.hit_points = archetype.Hitpoints;
            archetypePOCO.cap_hit_points = archetype.HPCap;
            archetypePOCO.description_long = archetype.DescLong;
            archetypePOCO.description_short = archetype.DescShort;
            archetypePOCO.cap_resistance = archetype.ResCap;
            archetypePOCO.cap_recharge = archetype.RechargeCap;
            archetypePOCO.cap_damage = archetype.DamageCap;
            archetypePOCO.cap_regeneration = archetype.RegenCap;
            archetypePOCO.cap_recovery = archetype.RecoveryCap;
            archetypePOCO.origin = archetype.Origin;
            archetypePOCO.playable = archetype.Playable;
            archetypePOCO.powersets_primary = ConvertPowersetIndexes(archetype.Primary);
            archetypePOCO.powersets_secondary = ConvertPowersetIndexes(archetype.Secondary);
            archetypePOCO.powersets_ancillary = ConvertPowersetIndexes(archetype.Ancillary);
            archetypePOCO.cap_perception = archetype.PerceptionCap;
            archetypePOCO.class_name = archetype.ClassName;
            archetypePOCO.id = archetype.ClassName;
            archetypePOCO.group_primary = archetype.PrimaryGroup;
            archetypePOCO.group_secondary = archetype.SecondaryGroup;
            archetypePOCO.group_epic = archetype.EpicGroup;
            archetypePOCO.group_pool = archetype.PoolGroup;
            archetypePOCO.base_recovery = archetype.BaseRecovery;
            archetypePOCO.base_regen = archetype.BaseRegen;
            archetypePOCO.base_threat = archetype.BaseThreat;
            return archetypePOCO;
        }
        public static string[] ConvertPowersetIndexes(int[] powersetIndexes)
        {
            string[] result = new string[powersetIndexes.Length];
            for (int i = 0; i < powersetIndexes.Length; i++)
            {
                IPowerset powerset = DatabaseAPI.GetPowersetByIndex(powersetIndexes[i]);
                result[i] = powerset.FullName;
            }
            return result;
        }
    }

    public class ArchetypePOCO
    {
        public float base_recovery { get; set; }
        public float base_regen { get; set; }
        public float base_threat { get; set; }
        public float cap_damage { get; set; }
        public float cap_hit_points { get; set; }
        public float cap_perception { get; set; }
        public float cap_recharge { get; set; }
        public float cap_recovery { get; set; }
        public float cap_regeneration { get; set; }
        public float cap_resistance { get; set; }
        public string class_name { get; set; }
        public string class_type { get; set; }
        public string description_long { get; set; }
        public string description_short { get; set; }
        public string display_name { get; set; }
        public string group_epic { get; set; }
        public string group_primary { get; set; }
        public string group_pool { get; set; }
        public string group_secondary { get; set; }
        public int hit_points { get; set; }
        public string id { get; set; }
        public string[] origin { get; set; }
        public bool playable { get; set; }
        public string[] powersets_ancillary { get; set; }
        public string[] powersets_primary { get; set; }
        public string[] powersets_secondary { get; set; }

    }

    public static class ArchetypePOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            ArchetypePOCO archetypePOCO = ArchetypePOCOConvert.Convert(archetype);
            ArchetypePOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\archetype.json", archetypePOCO);
            ArchetypePOCO[] archetypePOCOs = ArchetypePOCOConvert.Convert(DatabaseAPI.Database.Classes);
            ArchetypePOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\archetypes.json", archetypePOCOs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class RequirementPOCOJSONSerializer
    {
        public static void Write(string filename, RequirementPOCO requirementPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(requirementPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class RequirementPOCOConvert
    {
        public static RequirementPOCO Convert(Requirement requirement)
        {
            RequirementPOCO requirementPOCO = new RequirementPOCO();

            for (int i = 0; i < requirement.ClassName.Length; i++)
            {
                requirementPOCO.class_name.Add(requirement.ClassName[i]);
            }
            for (int i = 0; i < requirement.ClassNameNot.Length; i++)
            {
                requirementPOCO.class_name_not.Add(requirement.ClassNameNot[i]);
            }
            for (int i = 0; i < requirement.PowerID.Length; i++)
            {
                requirementPOCO.power_id.Add(new List<string>());
                List<string> powerIdListDest = requirementPOCO.power_id[i];
                string[] powerIdListSource = requirement.PowerID[i];
                for (int j = 0; j < powerIdListSource.Length; j++)
                {
                    powerIdListDest.Add(powerIdListSource[j]);
                }
            }
            for (int i = 0; i < requirement.PowerIDNot.Length; i++)
            {
                requirementPOCO.power_id_not.Add(new List<string>());
                List<string> powerIdNotListDest = requirementPOCO.power_id_not[i];
                string[] powerIdNotListSource = requirement.PowerIDNot[i];
                for (int j = 0; j < powerIdNotListSource.Length; j++)
                {
                    powerIdNotListDest.Add(powerIdNotListSource[j]);
                }
            }
            return requirementPOCO;
        }
    }

    public class RequirementPOCO
    {
        public List<string> class_name { get; set; }
        public List<string> class_name_not { get; set; }
        public List<List<string>> power_id { get; set; }
        public List<List<string>> power_id_not { get; set; }
        public RequirementPOCO()
        {
            class_name = new List<string>();
            class_name_not = new List<string>();
            power_id = new List<List<string>>();
            power_id_not = new List<List<string>>();
        }
    }

    public static class RequirementPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            RequirementPOCO requirementPOCO = RequirementPOCOConvert.Convert(crystalArmor.Requires);
            RequirementPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\requirement.json", requirementPOCO);
        }
    }
}
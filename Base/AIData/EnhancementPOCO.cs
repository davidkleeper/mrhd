﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class EnhancementPOCOJSONSerializer
    {
        public static void Write(string filename, EnhancementPOCO enhancementPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(enhancementPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, EnhancementPOCO[] enhancementPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(enhancementPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }
    
    public static class EnhancementPOCOConvert
    {
        public static EnhancementPOCO[] Convert(IEnhancement[] enhancements)
        {
            EnhancementPOCO[] enhancementPOCOs = new EnhancementPOCO[enhancements.Length];
            for (int slotIndex = 0; slotIndex < enhancementPOCOs.Length; slotIndex++)
            {
                enhancementPOCOs[slotIndex] = Convert(enhancements[slotIndex]);
            }
            return enhancementPOCOs;
        }

        public static EnhancementPOCO Convert(IEnhancement enhancement)
        {
            EnhancementPOCO enhancementPOCO = new EnhancementPOCO();
            enhancementPOCO.schedule = enhancement.Schedule.ToString();
            enhancementPOCO.probability = enhancement.Probability;
            enhancementPOCO.name = enhancement.Name;
            enhancementPOCO.short_name = enhancement.ShortName;
            enhancementPOCO.description = enhancement.Desc;
            enhancementPOCO.type_id = enhancement.TypeID.ToString();
            enhancementPOCO.sub_type_id = enhancement.SubTypeID.ToString();
            if ("-1" == enhancementPOCO.sub_type_id)
                enhancementPOCO.sub_type_id = Enums.eSubtype.None.ToString();
            enhancementPOCO.class_id = enhancement.ClassID;
            enhancementPOCO.image = enhancement.Image;
            enhancementPOCO.image_index = enhancement.ImageIdx;
            enhancementPOCO.enhancement_set_id = enhancement.UIDSet;
            enhancementPOCO.power_id = (enhancement.GetPower() != null) ? enhancement.GetPower().FullName : "";
            enhancementPOCO.effects = SEffectPOCOConvert.Convert(enhancement.Effect);
            enhancementPOCO.effect_chance = enhancement.EffectChance;
            enhancementPOCO.level_min = enhancement.LevelMin + 1;
            enhancementPOCO.level_max = enhancement.LevelMax + 1;
            enhancementPOCO.unique = enhancement.Unique;
            enhancementPOCO.mutex_id = enhancement.MutExID.ToString();
            enhancementPOCO.buff_debuff = enhancement.BuffMode.ToString();
            enhancementPOCO.id = enhancement.UID;
            enhancementPOCO.superior = enhancement.Superior;
            enhancementPOCO.long_name = enhancement.LongName;

            return enhancementPOCO;
        }
    }

    public class EnhancementPOCO
    {
        public string buff_debuff { get; set; }
        public int[] class_id { get; set; }
        public string description { get; set; }
        public float effect_chance { get; set; }
        public SEffectPOCO[] effects { get; set; }
        public string enhancement_set_id { get; set; }
        public string id { get; set; }
        public string image { get; set; }
        public int image_index { get; set; }
        public int level_max { get; set; }
        public int level_min { get; set; }
        public string long_name { get; set; }
        public string mutex_id { get; set; }
        public string name { get; set; }
        public string power_id { get; set; }
        public float probability { get; set; }
        public string schedule { get; set; }
        public string short_name { get; set; }
        public string sub_type_id { get; set; }
        public bool superior { get; set; }
        public string type_id { get; set; }
        public bool unique { get; set; }
    }

    public static class EnhancementPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.AbsoluteAmazementChanceForToHitDebuff);
            EnhancementPOCO enhancementPOCO = EnhancementPOCOConvert.Convert(enhancement);
            EnhancementPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\enhancement.json", enhancementPOCO);
            EnhancementPOCO[] enhancementPOCOs = EnhancementPOCOConvert.Convert(DatabaseAPI.Database.Enhancements);
            EnhancementPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\enhancements.json", enhancementPOCOs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class PowerEntryPOCOJSONSerializer
    {
        public static void Write(string filename, PowerEntryPOCO powerEntryPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerEntryPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, PowerEntryPOCO[] powerEntryPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(powerEntryPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class PowerEntryPOCOConvert
    {
        public static PowerEntryPOCO[] Convert(PowerEntry[] powerEntries)
        {
            PowerEntryPOCO[] powerEntryPOCOs = new PowerEntryPOCO[powerEntries.Length];
            for (int powerEntryIndex = 0; powerEntryIndex < powerEntryPOCOs.Length; powerEntryIndex++)
            {
                powerEntryPOCOs[powerEntryIndex] = Convert(powerEntries[powerEntryIndex]);
            }
            return powerEntryPOCOs;
        }

        public static PowerEntryPOCO Convert(PowerEntry powerEntry)
        {
            PowerEntryPOCO powerEntryPOCO = new PowerEntryPOCO();
            powerEntryPOCO.level = powerEntry.Level + 1;
            if ((powerEntry.Power.DisplayName == "Health")
            || (powerEntry.Power.DisplayName == "Stamina")
            || (powerEntry.Power.DisplayName == "Brawl"))
            {
                powerEntryPOCO.level = 1;
            }
            powerEntryPOCO.power_id = powerEntry.Power.FullName;
            powerEntryPOCO.tag = powerEntry.Tag;
            powerEntryPOCO.stat_include = powerEntry.StatInclude;
            powerEntryPOCO.variable_value = powerEntry.VariableValue;
            powerEntryPOCO.slots = SlotPOCOConvert.Convert(powerEntry.Slots);
            powerEntryPOCO.sub_powers = PowerSubEntryPOCOConvert.Convert(powerEntry.SubPowers);

            return powerEntryPOCO;
        }
    }

    public class PowerEntryPOCO
    {
        public int level { get; set; }
        public string power_id { get; set; }
        public bool tag { get; set; }
        public bool stat_include { get; set; }
        public int variable_value { get; set; }
        public SlotPOCO[] slots { get; set; }
        public PowerSubEntryPOCO[] sub_powers { get; set; }
    }

    public static class PowerEntryPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            PowerEntry personalForceFieldPowerEntry = new PowerEntry(-1, personalForceField);
            personalForceFieldPowerEntry.Slot(new int[] { (int)EnhancementType.Defense, (int)EnhancementType.Defense, (int)EnhancementType.Defense });
            PowerEntryPOCO powerEntryPOCO = PowerEntryPOCOConvert.Convert(personalForceFieldPowerEntry);
            PowerEntryPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-entry.json", powerEntryPOCO);

            IPower deflectionShield = forceField.Powers[1];
            PowerEntry deflectionShieldPowerEntry = new PowerEntry(-1, deflectionShield);
            deflectionShieldPowerEntry.Slot(new int[] { (int)EnhancementType.Defense, (int)EnhancementType.Defense, (int)EnhancementType.Defense });
            IPower insulationShield = forceField.Powers[3];
            PowerEntry insulationShieldPowerEntry = new PowerEntry(-1, insulationShield);
            insulationShieldPowerEntry.Slot(new int[] { (int)EnhancementType.Defense, (int)EnhancementType.Defense, (int)EnhancementType.Defense });
            PowerEntry[] powerEntries = new PowerEntry[3] { personalForceFieldPowerEntry, deflectionShieldPowerEntry, insulationShieldPowerEntry };

            PowerEntryPOCO[] powerEntryPOCOs = PowerEntryPOCOConvert.Convert(powerEntries);
            PowerEntryPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\power-entries.json", powerEntryPOCOs);
        }
    }
}

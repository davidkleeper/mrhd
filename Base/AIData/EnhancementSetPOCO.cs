﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class EnhancementSetPOCOJSONSerializer
    {
        public static void Write(string filename, EnhancementSetPOCO enhancementSet)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(enhancementSet, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, EnhancementSetPOCO[] enhancementSets)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(enhancementSets, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class EnhancementSetPOCOConvert
    {
        public static EnhancementSetPOCO[] Convert(EnhancementSet[] enhancementSets)
        {
            EnhancementSetPOCO[] enhancementSetPOCOs = new EnhancementSetPOCO[enhancementSets.Length];
            for (int enhancementSetIndex = 0; enhancementSetIndex < enhancementSetPOCOs.Length; enhancementSetIndex++)
            {
                enhancementSetPOCOs[enhancementSetIndex] = Convert(enhancementSets[enhancementSetIndex]);
            }
            return enhancementSetPOCOs;
        }

        public static EnhancementSetPOCO Convert(EnhancementSet enhancementSet)
        {
            EnhancementSetPOCO enhancementSetPOCO = new EnhancementSetPOCO();
            enhancementSetPOCO.bonuses = BonusItemPOCOConvert.Convert(enhancementSet.Bonus);
            enhancementSetPOCO.bonuses_special = BonusItemPOCOConvert.Convert(enhancementSet.SpecialBonus);
            enhancementSetPOCO.description = enhancementSet.Desc;
            enhancementSetPOCO.enhancement_ids = Convert(enhancementSet.Enhancements);
            enhancementSetPOCO.id = enhancementSet.Uid;
            enhancementSetPOCO.image = enhancementSet.Image;
            enhancementSetPOCO.image_index = enhancementSet.ImageIdx;
            enhancementSetPOCO.level_max = enhancementSet.LevelMax;
            enhancementSetPOCO.level_min = enhancementSet.LevelMin;
            enhancementSetPOCO.set_type = enhancementSet.SetType.ToString();
            enhancementSetPOCO.short_name = enhancementSet.ShortName;

            return enhancementSetPOCO;
        }

        public static string[] Convert(int[] enhancementIndexes)
        {
            string[] results = new string[enhancementIndexes.Length];
            for (int i = 0; i < enhancementIndexes.Length; i++)
            {
                IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementIndexes[i]);
                results[i] = enhancement.UID;
            }
            return results;
        }
    }

    public class EnhancementSetPOCO
    {
        public BonusItemPOCO[] bonuses { get; set; }
        public BonusItemPOCO[] bonuses_special { get; set; }
        public string description { get; set; }
        public string display_name { get; set; }
        public string[] enhancement_ids { get; set; }
        public string id { get; set; }
        public string image { get; set; }
        public int image_index { get; set; }
        public int level_max { get; set; }
        public int level_min { get; set; }
        public string set_type { get; set; }
        public string short_name { get; set; }
    }

    public static class EnhancementSetPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            EnhancementSetPOCO enhancementSetPOCO = EnhancementSetPOCOConvert.Convert(DatabaseAPI.GetEnhancementSetByIndex(10));
            EnhancementSetPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\enhancement-set.json", enhancementSetPOCO);
            EnhancementSetPOCO[] bonusItemPOCOs = EnhancementSetPOCOConvert.Convert(DatabaseAPI.Database.EnhancementSets.ToArray());
            EnhancementSetPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\enhancement-sets.json", bonusItemPOCOs);
        }
    }
}

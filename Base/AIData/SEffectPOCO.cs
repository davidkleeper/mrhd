﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class SEffectPOCOJSONSerializer
    {
        public static void Write(string filename, SEffectPOCO sEffectPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(sEffectPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, SEffectPOCO[] sEffectPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(sEffectPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class SEffectPOCOConvert
    {
        public static SEffectPOCO[] Convert(Enums.sEffect[] sEffects)
        {
            SEffectPOCO[] sEffectPOCOs = new SEffectPOCO[sEffects.Length];
            for (int slotIndex = 0; slotIndex < sEffectPOCOs.Length; slotIndex++)
            {
                sEffectPOCOs[slotIndex] = Convert(sEffects[slotIndex]);
            }
            return sEffectPOCOs;
        }

        public static SEffectPOCO Convert(Enums.sEffect sEffect)
        {
            SEffectPOCO sEffectPOCO = new SEffectPOCO();
            sEffectPOCO.mode = sEffect.Mode.ToString();
            sEffectPOCO.buff_debuff = sEffect.BuffMode.ToString();
            sEffectPOCO.enhance = STwinIDPOCOConvert.Convert(sEffect.Enhance);
            sEffectPOCO.schedule = sEffect.Schedule.ToString();
            sEffectPOCO.multiplier = sEffect.Multiplier;
            sEffectPOCO.fx = (null != sEffect.FX)? EffectPOCOConvert.Convert(sEffect.FX) : null;

            return sEffectPOCO;
        }
    }

    public class SEffectPOCO
    {
        public string mode { get; set; }
        public string buff_debuff { get; set; }
        public STwinIDPOCO enhance { get; set; }
        public string schedule { get; set; }
        public float multiplier { get; set; }
        public EffectPOCO fx { get; set; }
    }

    public static class SEffectPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAssassinsMarkRechargeTimeRchgBuildUp);
            SEffectPOCO requirementPOCO = SEffectPOCOConvert.Convert(enhancement.Effect[0]);
            SEffectPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\s-effect.json", requirementPOCO);
            SEffectPOCO[] requirementPOCOs = SEffectPOCOConvert.Convert(enhancement.Effect);
            SEffectPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\s-effects.json", requirementPOCOs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class EffectPOCOJSONSerializer
    {
        public static void Write(string filename, EffectPOCO effectPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(effectPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, EffectPOCO[] effectPOCOs)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(effectPOCOs, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class EffectPOCOConvert
    {
        public static EffectPOCO[] Convert(IEffect[] effects)
        {
            EffectPOCO[] effectPOCOs = new EffectPOCO[effects.Length];
            for (int effectIndex = 0; effectIndex < effectPOCOs.Length; effectIndex++)
            {
                effectPOCOs[effectIndex] = Convert(effects[effectIndex]);
            }
            return effectPOCOs;
        }

        public static EffectPOCO Convert(IEffect effect)
        {
            EffectPOCO effectPOCO = new EffectPOCO();
            effectPOCO.unique_id = effect.UniqueID;
            effectPOCO.probability = effect.Probability;
            effectPOCO.probability_base = effect.BaseProbability;
            effectPOCO.mag = effect.Mag;
            effectPOCO.mag_percent = effect.MagPercent;
            effectPOCO.duration = effect.Duration;
            effectPOCO.display_percentage = effect.DisplayPercentage;
            effectPOCO.variable_modified = effect.VariableModified;
            effectPOCO.inherent_special = effect.InherentSpecial;
            effectPOCO.reward = effect.Reward;
            effectPOCO.effect_id = effect.EffectId;
            effectPOCO.special = effect.Special;
            effectPOCO.enhancement_long_name = (effect.Enhancement != null)? effect.Enhancement.LongName : "";
            effectPOCO.effect_class = effect.EffectClass.ToString();
            effectPOCO.effect_type = effect.EffectType.ToString();
            effectPOCO.display_percentage_override = effect.DisplayPercentageOverride.ToString();
            effectPOCO.damage_type = effect.DamageType.ToString();
            effectPOCO.mez_type = effect.MezType.ToString();
            effectPOCO.effect_type_modifies = effect.ETModifies.ToString();
            effectPOCO.summon = effect.Summon;
            effectPOCO.summon_count = effect.nSummon;
            effectPOCO.ticks = effect.Ticks;
            effectPOCO.delayed_time = effect.DelayedTime;
            effectPOCO.stacking = effect.Stacking.ToString();
            effectPOCO.suppression = (int)effect.Suppression;
            effectPOCO.buffable = effect.Buffable;
            effectPOCO.resistible = effect.Resistible;
            effectPOCO.special_case = effect.SpecialCase.ToString();
            effectPOCO.id_class_name = effect.UIDClassName;
            effectPOCO.variable_modified_override = effect.VariableModifiedOverride;
            effectPOCO.is_enhancement_effect = effect.isEnhancementEffect;
            effectPOCO.pv_mode = effect.PvMode.ToString();
            effectPOCO.to_who = effect.ToWho.ToString();
            effectPOCO.scale = effect.Scale;
            effectPOCO.magnitude = effect.nMagnitude;
            effectPOCO.attribute_type = effect.AttribType.ToString();
            effectPOCO.aspect = effect.Aspect.ToString();
            effectPOCO.modifier_table = effect.ModifierTable;
            effectPOCO.modifier_table_index = effect.nModifierTable;
            effectPOCO.power_id = effect.PowerFullName;
            effectPOCO.near_ground = effect.NearGround;
            effectPOCO.requires_to_hit_check = effect.RequiresToHitCheck;
            effectPOCO.math_mag = effect.Math_Mag;
            effectPOCO.math_duration = effect.Math_Duration;
            effectPOCO.absorbed_effect = effect.Absorbed_Effect;
            effectPOCO.absorbed_power_id = (-1 == effect.Absorbed_Power_nID)? "" : DatabaseAPI.GetPowerByIndex(effect.Absorbed_Power_nID).FullName;
            effectPOCO.absorbed_power_type = effect.Absorbed_PowerType.ToString();
            effectPOCO.absorbed_duration = effect.Absorbed_Duration;
            effectPOCO.absorbed_interval = effect.Absorbed_Interval;
            effectPOCO.absorbed_effect_id = effect.Absorbed_EffectID;
            effectPOCO.absorbed_class_id = effect.Absorbed_Class_nID;
            effectPOCO.buff_mode = effect.buffMode.ToString();
            effectPOCO.override_value = effect.Override;
            effectPOCO.override_count = effect.nOverride;
            effectPOCO.magnitude_expression = effect.MagnitudeExpression;
            effectPOCO.cancel_on_miss = effect.CancelOnMiss;
            effectPOCO.procs_per_minute = effect.ProcsPerMinute;
            return effectPOCO;
        }
    }

    public class EffectPOCO
    {
        public int absorbed_class_id { get; set; }
        public float absorbed_duration { get; set; }
        public bool absorbed_effect { get; set; }
        public int absorbed_effect_id { get; set; }
        public float absorbed_interval { get; set; }
        public string absorbed_power_id { get; set; }
        public string absorbed_power_type { get; set; }
        public string aspect { get; set; }
        public string attribute_type { get; set; }
        public string buff_mode { get; set; }
        public bool buffable { get; set; }
        public bool cancel_on_miss { get; set; }
        public string damage_type { get; set; }
        public float delayed_time { get; set; }
        public bool display_percentage { get; set; }
        public string display_percentage_override { get; set; }
        public float duration { get; set; }
        public string effect_class { get; set; }
        public string effect_id { get; set; }
        public string effect_type { get; set; }
        public string effect_type_modifies { get; set; }
        public string enhancement_long_name { get; set; }
        public bool inherent_special { get; set; }
        public bool is_enhancement_effect { get; set; }
        public float mag { get; set; }
        public float mag_percent { get; set; }
        public float magnitude { get; set; }
        public string magnitude_expression { get; set; }
        public float math_duration { get; set; }
        public float math_mag { get; set; }
        public string mez_type { get; set; }
        public string modifier_table { get; set; }
        public int modifier_table_index { get; set; }
        public bool near_ground { get; set; }
        public string override_value { get; set; }
        public int override_count { get; set; }
        public string power_id { get; set; }
        public float probability { get; set; }
        public float probability_base { get; set; }
        public float procs_per_minute { get; set; }
        public string pv_mode { get; set; }
        public bool requires_to_hit_check { get; set; }
        public bool resistible { get; set; }
        public string reward { get; set; }
        public float scale { get; set; }
        public string special { get; set; }
        public string special_case { get; set; }
        public string stacking { get; set; }
        public string summon { get; set; }
        public int summon_count { get; set; }
        public int suppression { get; set; }
        public int ticks { get; set; }
        public string to_who { get; set; }
        public string id_class_name { get; set; }
        public int unique_id { get; set; }
        public bool variable_modified { get; set; }
        public bool variable_modified_override { get; set; }
    }

    public static class EffectPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            EffectPOCO effectPOCO = EffectPOCOConvert.Convert(personalForceField.Effects[0]);
            EffectPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\effect.json", effectPOCO);
            EffectPOCO[] effectPOCOs = EffectPOCOConvert.Convert(personalForceField.Effects);
            EffectPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\effects.json", effectPOCOs);
        }
    }
}

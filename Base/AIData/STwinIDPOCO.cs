﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class STwinIDPOCOJSONSerializer
    {
        public static void Write(string filename, STwinIDPOCO sTwinIDPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(sTwinIDPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, STwinIDPOCO[] sTwinIDPOCO)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(sTwinIDPOCO, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class STwinIDPOCOConvert
    {

        public static STwinIDPOCO[] Convert(Enums.sTwinID[] twinIDs)
        {
            STwinIDPOCO[] sTwinIDPOCOs = new STwinIDPOCO[twinIDs.Length];
            for (int sTwinIDIndex = 0; sTwinIDIndex < sTwinIDPOCOs.Length; sTwinIDIndex++)
            {
                sTwinIDPOCOs[sTwinIDIndex] = Convert(twinIDs[sTwinIDIndex]);
            }
            return sTwinIDPOCOs;
        }

        public static STwinIDPOCO Convert(Enums.sTwinID twinID)
        {
            STwinIDPOCO sTwinIDPOCO = new STwinIDPOCO();
            sTwinIDPOCO.id = twinID.ID;
            sTwinIDPOCO.sub_id = twinID.SubID;

            return sTwinIDPOCO;
        }
    }

    public class STwinIDPOCO
    {
        public int id { get; set; }
        public int sub_id { get; set; }
    }

    public static class STwinIDPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAssassinsMarkRechargeTimeRchgBuildUp);
            STwinIDPOCO sTwinIdPOCO = STwinIDPOCOConvert.Convert(enhancement.Effect[0].Enhance);
            STwinIDPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\s-twin-id.json", sTwinIdPOCO);
        }
    }
}

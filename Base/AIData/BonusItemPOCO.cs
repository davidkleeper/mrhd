﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace AIData
{
    public static class BonusItemPOCOJSONSerializer
    {
        public static void Write(string filename, BonusItemPOCO bonusItem)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(bonusItem, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
        public static void Write(string filename, BonusItemPOCO[] bonusItems)
        {
            var options = new JsonSerializerOptions { WriteIndented = true, };
            string jsonString = JsonSerializer.Serialize(bonusItems, options);
            System.IO.File.WriteAllText(@filename, jsonString);
        }
    }

    public static class BonusItemPOCOConvert
    {
        public static BonusItemPOCO[] Convert(EnhancementSet.BonusItem[] bonusItems)
        {
            BonusItemPOCO[] bonusItemPOCOs = new BonusItemPOCO[bonusItems.Length];
            for (int bonusItemIndex = 0; bonusItemIndex < bonusItemPOCOs.Length; bonusItemIndex++)
            {
                bonusItemPOCOs[bonusItemIndex] = Convert(bonusItems[bonusItemIndex]);
            }
            return bonusItemPOCOs;
        }

        public static BonusItemPOCO Convert(EnhancementSet.BonusItem bonusItem)
        {
            BonusItemPOCO bonusItemPOCO = new BonusItemPOCO();
            bonusItemPOCO.alt_string = bonusItem.AltString;
            bonusItemPOCO.names = bonusItem.Name;
            bonusItemPOCO.power_ids = Convert(bonusItem.Index);
            bonusItemPOCO.pv_mode = bonusItem.PvMode.ToString();
            bonusItemPOCO.slotted = bonusItem.Slotted;
            bonusItemPOCO.special = bonusItem.Special;

            return bonusItemPOCO;
        }

        public static string[] Convert(int[] indexes)
        {
            string[] results = new string[indexes.Length];
            for (int i = 0; i < indexes.Length; i++)
            {
                IPower power = DatabaseAPI.GetPowerByIndex(indexes[i]);
                results[i] = power.FullName;
            }
            return results;
        }
    }

    public class BonusItemPOCO
    {
        public string alt_string { get; set; }
        public string[] names { get; set; }
        public string[] power_ids { get; set; }
        public string pv_mode { get; set; }
        public int slotted { get; set; }
        public int special { get; set; }
    }

    public static class BonusItemPOCOTest
    {
        public static void TestNewCode()
        {
            TestWrite();
        }
        public static void TestWrite()
        {
            EnhancementSet enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(10);
            BonusItemPOCO bonusItemPOCO = BonusItemPOCOConvert.Convert(enhancementSet.Bonus[0]);
            BonusItemPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\bonus-item.json", bonusItemPOCO);
            BonusItemPOCO[] bonusItemPOCOs = BonusItemPOCOConvert.Convert(enhancementSet.Bonus);
            BonusItemPOCOJSONSerializer.Write("D:\\src\\dotnet\\Hero-Designer\\Output\\bonus-items.json", bonusItemPOCOs);
        }
    }
}

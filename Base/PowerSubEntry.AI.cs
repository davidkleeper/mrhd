﻿using System;

public partial class PowerSubEntry : IComparable
{
    public int CompareTo(object powerSubEntry)
    {
        PowerSubEntry pse = powerSubEntry as PowerSubEntry;
        if (null == pse) return 1;
        if (Powerset != pse.Powerset) return Powerset.CompareTo(pse.Powerset);
        if (Power != pse.Power) return Power.CompareTo(pse.Power);
        if (nIDPower != pse.nIDPower) return nIDPower.CompareTo(pse.nIDPower);
        if (StatInclude != pse.StatInclude) return StatInclude.CompareTo(pse.StatInclude);
        return 0;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AIMath
{
    class I9SlotMath
    {
        public const float RelativeLevelAdjustUp = 0.05f;
        public const float RelativeLevelAdjustDown = 0.1f;
        public static float CalcRelativeLevelMultiplier(Enums.eEnhRelative relativeLevel)
        {
            if (relativeLevel == Enums.eEnhRelative.None) return 0.0f; 
            int adjustedLevel = (int)(relativeLevel - 4);
            if (adjustedLevel >= 0) return (float)(1.0 + adjustedLevel * RelativeLevelAdjustUp);
            return (float)(1.0 + adjustedLevel * RelativeLevelAdjustDown);
        }

        public static float CalcMultiplier(bool isSuperior, Enums.eType type, Enums.eEnhGrade grade, Enums.eSchedule schedule, int ioLevel, Enums.eEnhRelative relativeLevel)
        {
            float relativeLevelMultiplier = CalcRelativeLevelMultiplier(relativeLevel);
            float scheduleMultiplier = DatabaseAPI.GetScheduleMultiplier(type, grade, schedule, ioLevel);
            float multiplier = scheduleMultiplier * relativeLevelMultiplier;
            if (isSuperior) multiplier *= 1.25f;
            return multiplier;
        }

        public static float CalcMultiplier(I9Slot slot)
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(slot.Enh);
            if (null == enhancement) return 0.0f;
            return CalcMultiplier(enhancement.Superior, enhancement.TypeID, slot.Grade, enhancement.Schedule, slot.IOLevel, slot.RelativeLevel);
        }

        public static float CalcDefense(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Defense, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcResistance(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Resistance, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcDamage(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Damage, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcAccuracy(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Accuracy, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcFly(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.SpeedFlying, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcJump(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.SpeedJumping, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcRun(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.SpeedRunning, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcConfuse(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Confused, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcHold(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Held, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcImmobilize(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Immobilized, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcSleep(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Sleep, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcStun(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Stunned, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcTerrorize(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Terrorized, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcKnockBack(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Mez, (int)Enums.eMez.Knockback, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcHeal(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Heal, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcRecovery(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Recovery, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcEnduranceDiscount(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.EnduranceDiscount, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcRechargeReduction(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.RechargeTime, -1, Enums.eBuffDebuff.BuffOnly);
        }

        public static float CalcSlow(I9Slot slot)
        {
            return CalcSlotEffect(slot, Enums.eEnhance.Slow, -1, Enums.eBuffDebuff.DeBuffOnly);
        }

        public static float CalcSlotEffect(I9Slot slot, Enums.eEnhance desiredEffect, int desiredSubEffect, Enums.eBuffDebuff buffOrDebuff)
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(slot.Enh);
            if (null == enhancement) return 0.0f;
            float multiplier = CalcMultiplier(slot);
            float result = 0.0f;
            foreach (Enums.sEffect sEffect in enhancement.Effect)
            {
                result += AIMath.SEffectMath.CalcSEffect(desiredEffect, desiredSubEffect, sEffect, multiplier, buffOrDebuff);
            }
            return result;
        }

        public static void TestNewCode()
        {
            TestCalcRelativeLevelMultiplier();
            TestCalcMultiplier();
            TestCalcSlotEffect();
            TestCalcDefense();
            TestCalcResistance();
            TestCalcDamage();
            TestCalcAccuracy();
            TestCalcFly();
            TestCalcJump();
            TestCalcRun();
            TestCalcConfuse();
            TestCalcHold();
            TestCalcImmobilize();
            TestCalcSleep();
            TestCalcStun();
            TestCalcTerrorize();
            TestCalcKnockBack();
            TestCalcHeal();
            TestCalcRecovery();
            TestCalcEnduranceDiscount();
            TestCalcRechargeReduction();
            TestCalcSlow();
        }

        public static void TestCalcRelativeLevelMultiplier()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.None;
            float i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            float answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.None);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.Even);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.MinusOne;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.MinusOne);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.MinusTwo;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.MinusTwo);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.MinusThree;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.MinusThree);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusFive;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.PlusFive);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusFour;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.PlusFour);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusThree;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.PlusThree);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusTwo;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.PlusTwo);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusOne;
            i9SlotAnswer = i9Slot.GetRelativeLevelMultiplier();
            answer = CalcRelativeLevelMultiplier(Enums.eEnhRelative.PlusOne);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.001f));
        }

        public static void TestCalcMultiplier()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.None;
            i9Slot.Grade = Enums.eEnhGrade.TrainingO;
            i9Slot.IOLevel = 0;
            float i9SlotAnswer = i9Slot.GetScheduleMult(Enums.eType.SpecialO, Enums.eSchedule.A);
            float answer = CalcMultiplier(false, Enums.eType.SpecialO, Enums.eEnhGrade.TrainingO, Enums.eSchedule.A, 10, Enums.eEnhRelative.None);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.PlusFive;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 0;
            i9SlotAnswer = i9Slot.GetScheduleMult(Enums.eType.SpecialO, Enums.eSchedule.A);
            answer = CalcMultiplier(false, Enums.eType.SpecialO, Enums.eEnhGrade.None, Enums.eSchedule.A, 20, Enums.eEnhRelative.PlusFive);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.MinusThree;
            i9Slot.Grade = Enums.eEnhGrade.SingleO;
            i9Slot.IOLevel = 0;
            i9SlotAnswer = i9Slot.GetScheduleMult(Enums.eType.Normal, Enums.eSchedule.A);
            answer = CalcMultiplier(false, Enums.eType.Normal, Enums.eEnhGrade.SingleO, Enums.eSchedule.A, 30, Enums.eEnhRelative.MinusThree);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = SpecialEnhancements.SuperiorAscendencyOfTheDominatorRechargeChanceForDamage;
            i9SlotAnswer = i9Slot.GetScheduleMult(Enums.eType.SetO, Enums.eSchedule.A);
            answer = CalcMultiplier(true, Enums.eType.SetO, Enums.eEnhGrade.None, Enums.eSchedule.A, 49, Enums.eEnhRelative.Even);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
        }

        public static void TestCalcSlotEffect()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = SpecialEnhancements.SuperiorAscendencyOfTheDominatorRechargeChanceForDamage;
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(SpecialEnhancements.SuperiorAscendencyOfTheDominatorRechargeChanceForDamage);
            float i9SlotAnswer = i9Slot.GetEnhancementEffect(Enums.eEnhance.RechargeTime, -1, 0);
            float answer = CalcSlotEffect(i9Slot, Enums.eEnhance.RechargeTime, -1, Enums.eBuffDebuff.DeBuffOnly);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(i9SlotAnswer, answer, 0.0001f));
        }

        public static void TestCalcDefense()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Defense;
            float answer = CalcDefense(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "CalcDefense is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Accuracy;
            answer = CalcDefense(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcDefense is incorrect!");
        }

        public static void TestCalcResistance()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Resistance;
            float answer = CalcResistance(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "CalcResistance is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Accuracy;
            answer = CalcResistance(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcResistance is incorrect!");
        }

        public static void TestCalcDamage()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Damage;
            float answer = CalcDamage(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcDamage is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Accuracy;
            answer = CalcDamage(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcDamage is incorrect!");
        }

        public static void TestCalcAccuracy()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Accuracy;
            float answer = CalcAccuracy(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcAccuracy is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcAccuracy(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcAccuracy is incorrect!");
        }

        public static void TestCalcFly()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Fly;
            float answer = CalcFly(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcFly is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcFly(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcFly is incorrect!");
        }

        public static void TestCalcJump()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Jump;
            float answer = CalcJump(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcJump is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcJump(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcJump is incorrect!");
        }

        public static void TestCalcRun()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Run;
            float answer = CalcRun(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcRun is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcRun(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcRun is incorrect!");
        }

        public static void TestCalcConfuse()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Confuse;
            float answer = CalcConfuse(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcConfuse is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcConfuse(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcConfuse is incorrect!");
        }

        public static void TestCalcHold()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Hold;
            float answer = CalcHold(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcHold is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcHold(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcHold is incorrect!");
        }

        public static void TestCalcImmobilize()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Immobilize;
            float answer = CalcImmobilize(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcImmobilize is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcImmobilize(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcImmobilize is incorrect!");
        }

        public static void TestCalcSleep()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Sleep;
            float answer = CalcSleep(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcSleep is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcSleep(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcSleep is incorrect!");
        }

        public static void TestCalcStun()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Stun;
            float answer = CalcStun(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcStun is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcStun(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcStun is incorrect!");
        }

        public static void TestCalcTerrorize()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Fear;
            float answer = CalcTerrorize(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcTerrorize is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcTerrorize(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcTerrorize is incorrect!");
        }

        public static void TestCalcKnockBack()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Knockback;
            float answer = CalcKnockBack(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.764f, answer, 0.0001f), "CalcKnockBack is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcKnockBack(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcKnockBack is incorrect!");
        }

        public static void TestCalcHeal()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Heal;
            float answer = CalcHeal(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcHeal is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcHeal(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcHeal is incorrect!");
        }

        public static void TestCalcRecovery()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.EnduranceModification;
            float answer = CalcRecovery(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcRecovery is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcRecovery(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcRecovery is incorrect!");
        }

        public static void TestCalcEnduranceDiscount()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.EnduranceReduction;
            float answer = CalcEnduranceDiscount(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcEnduranceDiscount is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcEnduranceDiscount(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcEnduranceDiscount is incorrect!");
        }

        public static void TestCalcRechargeReduction()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Recharge;
            float answer = CalcRechargeReduction(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcRechargeReduction is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcRechargeReduction(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcRechargeReduction is incorrect!");
        }

        public static void TestCalcSlow()
        {
            I9Slot i9Slot = new I9Slot();
            i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
            i9Slot.Grade = Enums.eEnhGrade.None;
            i9Slot.IOLevel = 49;
            i9Slot.Enh = (int)EnhancementType.Slow;
            float answer = CalcSlow(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "CalcSlow is incorrect!");

            i9Slot.Enh = (int)EnhancementType.Damage;
            answer = CalcSlow(i9Slot);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0f, answer, 0.0001f), "CalcSlow is incorrect!");
        }
    }
}

    
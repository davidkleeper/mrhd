﻿using Base.Data_Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMath
{
    public static class EffectMath
    {
        public static float CalcAllDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.None, archetype);
        }

        public static float CalcMeleeDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Melee, archetype);
        }

        public static float CalcRangedDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Ranged, archetype);
        }

        public static float CalcAoEDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.AoE, archetype);
        }

        public static float CalcSmashingDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Smashing, archetype);
        }

        public static float CalcLethalDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Lethal, archetype);
        }

        public static float CalcFireDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Fire, archetype);
        }

        public static float CalcColdDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Cold, archetype);
        }

        public static float CalcEnergyDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Energy, archetype);
        }

        public static float CalcNegativeEnergyDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Negative, archetype);
        }

        public static float CalcPsionicDefense(IEffect effect, Archetype archetype)
        {
            return CalcDefenseType(effect, Enums.eDamage.Psionic, archetype);
        }

        public static float CalcDefenseType(IEffect effect, Enums.eDamage damageType, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsDefenseType(effect, damageType, archetype)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcSmashingResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Smashing, archetype);
        }

        public static float CalcLethalResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Lethal, archetype);
        }

        public static float CalcFireResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Fire, archetype);
        }

        public static float CalcColdResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Cold, archetype);
        }

        public static float CalcEnergyResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Energy, archetype);
        }

        public static float CalcNegativeEnergyResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Negative, archetype);
        }

        public static float CalcPsionicResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Psionic, archetype);
        }

        public static float CalcToxicResistance(IEffect effect, Archetype archetype)
        {
            return CalcResistanceType(effect, Enums.eDamage.Toxic, archetype);
        }

        public static float CalcResistanceType(IEffect effect, Enums.eDamage damageType, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsResistanceType(effect, damageType, archetype)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcDamageBuff(IEffect effect, Enums.ePvX pvxMode, Archetype archetype)
        {
            if (effect.EffectType != Enums.eEffectType.DamageBuff) return 0.0f;
            if (effect.EffectClass == Enums.eEffectClass.Ignored) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (effect.SpecialCase != Enums.eSpecialCase.None) return 0.0f;
            if (effect.DamageType == Enums.eDamage.Special && effect.ToWho == Enums.eToWho.ToSelf) return 0.0f;

            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcAccuracy(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsAccuracy(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcFly(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsFly(effect, archetype)) return 0.0f;
            if (effect.EffectType != Enums.eEffectType.SpeedFlying) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcJump(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsJump(effect, archetype)) return 0.0f;
            if (effect.EffectType != Enums.eEffectType.SpeedJumping) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRun(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRun(effect, archetype)) return 0.0f;
            if (effect.EffectType != Enums.eEffectType.SpeedRunning) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcTeleport(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsTeleport(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcConfuse(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsConfuse(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcHold(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsHold(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcImmobilize(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsImmobilize(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcSleep(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsSleep(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcStun(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsStun(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcTerrorize(IEffect effect, Enums.ePvX pvxMode)
        {
            if (!AILogic.EffectLogic.IsTerrorize(effect)) return 0.0f;
            if (effect.PvMode != Enums.ePvX.Any && effect.PvMode != pvxMode) return 0.0f;
            if (!BaseUtils.IsKindaSortaEqual(effect.BaseProbability, 1f, 0.0001f)) return 0.0f;
            return effect.Duration;
        }

        public static float CalcConfuseResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsConfuseResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcHoldResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsHoldResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcImmobilizeResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsImmobilizeResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcSleepResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsSleepResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcStunResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsStunResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcTerrorizeResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsTerrorizeResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockBack(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockBack(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockDown(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockDown(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockUp(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockUp(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockBackResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockBackResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockDownResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockDownResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcKnockUpResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsKnockUpResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRepel(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRepel(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRepelResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRepelResistance(effect)) return 0.0f;
            if (effect.ToWho != Enums.eToWho.ToSelf && effect.ToWho != Enums.eToWho.All) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcHeal(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsHeal(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcHitPoints(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsHitPoints(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRegeneration(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRegeneration(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRecovery(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRecovery(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcEnduranceDiscount(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsEnduranceDiscount(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcEnduranceIncrease(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsEnduranceIncrease(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRechargeReduction(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRechargeReduction(effect, archetype)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcMovementSlow(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsMovementSlow(effect, archetype)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRechargeSlow(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRechargeSlow(effect, archetype)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcMovementSlowResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsMovementSlowResistance(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcRechargeSlowResistance(IEffect effect, Archetype archetype)
        {
            if (!AILogic.EffectLogic.IsRechargeSlowResistance(effect)) return 0.0f;
            return AIMath.EffectMath.CalcMag(effect, archetype);
        }

        public static float CalcProbability(IPower power, IEffect effect, float hastenBuff, float modifyEffects = 0.0f)
        {
            float probability = effect.BaseProbability;
            if (effect.ProcsPerMinute > 0.0 && probability < 0.01 && power != null)
            {
                float AoEModifier = (float)(power.AoEModifier * 0.75 + 0.25);
                float procsPerMinute = effect.ProcsPerMinute;
                float globalRecharge = (hastenBuff - 100) / 100;
                float recharge = power.BaseRechargeTime / (power.BaseRechargeTime / power.RechargeTime - globalRecharge);
                if (power.PowerType == Enums.ePowerType.Click)
                    probability = System.Math.Min(System.Math.Max((procsPerMinute * (recharge + power.CastTimeReal)) / (60.0f * AoEModifier), (float)(0.0500000007450581 + 0.0149999996647239 * effect.ProcsPerMinute)), 0.9f);
                else
                    probability = System.Math.Min(System.Math.Max((procsPerMinute * 10) / (60f * AoEModifier), (float)(0.0500000007450581 + 0.0149999996647239 * effect.ProcsPerMinute)), 0.9f);
            }
            probability += modifyEffects;
            probability = System.Math.Min(probability, 1.0f);
            probability = System.Math.Max(probability, 0.0f);
            return probability;
        }

        public static float CalcMag(IEffect effect, Archetype archetype)
        {
            float result = 0.0f;
            float modifier = GetModifier(effect, archetype);
            switch (effect.AttribType)
            {
                case Enums.eAttribType.Magnitude:
                    if (Math.Abs(effect.Math_Mag - 0.0f) > 0.01) return effect.Math_Mag;
                    float mag = effect.nMagnitude;
                    if (effect.EffectType == Enums.eEffectType.Damage) mag = -mag;
                    result = effect.Scale * mag * modifier;
                    break;
                case Enums.eAttribType.Duration:
                    if (Math.Abs(effect.Math_Mag - 0.0f) > 0.01) return effect.Math_Mag;
                    result = effect.nMagnitude;
                    if (effect.EffectType == Enums.eEffectType.Damage) result = -result;
                    break;
                case Enums.eAttribType.Expression:
                    result = effect.ParseMagnitudeExpression() * modifier;
                    if (effect.EffectType == Enums.eEffectType.Damage) result = -result;
                    break;
            }
            return result;
        }

        private const int MathLevelBase = 49;
        public static float GetModifier(IEffect iEffect, Archetype archetype)
        {
            int iClass = 0;
            int iLevel = MathLevelBase;
            var effPower = iEffect.GetPower();
            if (effPower == null)
            {
                if (iEffect.Enhancement == null)
                    return 1f;
            }
            else
                iClass = string.IsNullOrEmpty(effPower.ForcedClass) 
                    ? (iEffect.Absorbed_Class_nID <= -1 ? archetype.Idx : iEffect.Absorbed_Class_nID) 
                    : DatabaseAPI.NidFromUidClass(effPower.ForcedClass);
            return GetModifier(iClass, iEffect.nModifierTable, iLevel);
        }

        static float GetModifier(int iClass, int iTable, int iLevel)
        {
            float num;
            if (iClass < 0)
                num = 0.0f;
            else if (iTable < 0)
                num = 0.0f;
            else if (iLevel < 0)
                num = 0.0f;
            else if (iClass > DatabaseAPI.Database.Classes.Length - 1)
            {
                num = 0.0f;
            }
            else
            {
                iClass = DatabaseAPI.Database.Classes[iClass].Column;
                num = iClass >= 0 ? (iTable <= DatabaseAPI.Database.AttribMods.Modifier.Length - 1 ? (iLevel <= DatabaseAPI.Database.AttribMods.Modifier[iTable].Table.Length - 1 ? (iClass <= DatabaseAPI.Database.AttribMods.Modifier[iTable].Table[iLevel].Length - 1 ? DatabaseAPI.Database.AttribMods.Modifier[iTable].Table[iLevel][iClass] : 0.0f) : 0.0f) : 0.0f) : 0.0f;
            }
            return num;
        }

        public static void TestNewCode()
        {
            /*
            TestCalcAllDefense();
            TestCalcMeleeDefense();
            TestCalcRangedDefense();
            TestCalcAoEDefense();
            TestCalcSmashingDefense();
            TestCalcLethalDefense();
            TestCalcFireDefense();
            TestCalcColdDefense();
            TestCalcEnergyDefense();
            TestCalcNegativeEnergyDefense();
            TestCalcPsionicDefense();
            TestCalcSmashingResistance();
            TestCalcLethalResistance();
            TestCalcFireResistance();
            TestCalcColdResistance();
            TestCalcEnergyResistance();
            TestCalcNegativeEnergyResistance();
            TestCalcPsionicResistance();
            TestCalcToxicResistance();
            TestCalcDamage();
            TestCalcAccuracy();
            TestCalcFly();
            TestCalcJump();
            TestCalcRun();
            TestCalcTeleport();
            TestCalcConfuse();
            TestCalcHold();
            TestCalcImmobilize();
            TestCalcSleep();
            TestCalcStun();
            TestCalcTerrorize();
            TestCalcConfuseResistance();
            TestCalcHoldResistance();
            TestCalcImmobilizeResistance();
            TestCalcSleepResistance();
            TestCalcStunResistance();
            TestCalcTerrorizeResistance();
            TestCalcKnockBack();
            TestCalcKnockDown();
            TestCalcKnockUp();
            TestCalcKnockBackResistance();
            TestCalcKnockDownResistance();
            TestCalcKnockUpResistance();
            TestCalcRepel();
            TestCalcRepelResistance();
            TestCalcHeal();
            TestCalcHitPoints();
            TestCalcRegeneration();
            TestCalcRecovery();
            TestCalcEnduranceDiscount();
            TestCalcEnduranceIncrease();
            TestCalcRechargeReduction();
            TestCalcMovementSlow();
            TestCalcRechargeSlow();
            TestCalcMovementSlowResistance();
            TestCalcRechargeSlowResistance();
            TestCalcProbability();
            TestCalcMag();
            */
        }

        public static void TestCalcAllDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            float answer = CalcAllDefense(personalForceField.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.525f, 0.0001f), "CalcAllDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            answer = CalcAllDefense(battleAgility.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcAllDefense() is incorrect!");
        }

        public static void TestCalcMeleeDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            float answer = CalcMeleeDefense(deflection.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcMeleeDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            answer = CalcMeleeDefense(battleAgility.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcMeleeDefense() is incorrect!");
        }

        public static void TestCalcRangedDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            float answer = CalcRangedDefense(battleAgility.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcRangedDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            answer = CalcRangedDefense(deflection.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcRangedDefense() is incorrect!");
        }

        public static void TestCalcAoEDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            float answer = CalcAoEDefense(battleAgility.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcAoEDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            answer = CalcAoEDefense(deflection.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcAoEDefense() is incorrect!");
        }

        public static void TestCalcSmashingDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            float answer = CalcSmashingDefense(rockArmor.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcSmashingDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            answer = CalcSmashingDefense(fireShield.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcSmashingDefense() is incorrect!");
        }

        public static void TestCalcLethalDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            float answer = CalcLethalDefense(rockArmor.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcLethalDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            answer = CalcLethalDefense(fireShield.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcLethalDefense() is incorrect!");
        }

        public static void TestCalcFireDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            float answer = CalcFireDefense(wetIce.Effects[31], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "CalcFireDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            answer = CalcFireDefense(temperatureProtection.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcFireDefense() is incorrect!");
        }

        public static void TestCalcColdDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            float answer = CalcColdDefense(wetIce.Effects[32], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "CalcColdDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            answer = CalcColdDefense(temperatureProtection.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcColdDefense() is incorrect!");
        }

        public static void TestCalcEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            float answer = CalcEnergyDefense(crystalArmor.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            answer = CalcEnergyDefense(plasmaShield.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcEnergyDefense() is incorrect!");
        }

        public static void TestCalcNegativeEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            float answer = CalcNegativeEnergyDefense(crystalArmor.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcNegativeEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            answer = CalcNegativeEnergyDefense(plasmaShield.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcNegativeEnergyDefense() is incorrect!");
        }

        public static void TestCalcPsionicDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            float answer = CalcPsionicDefense(minerals.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.175f, 0.0001f), "CalcPsionicDefense() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            answer = CalcPsionicDefense(strengthOfWill.Effects[6], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcPsionicDefense() is incorrect!");
        }

        public static void TestCalcSmashingResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            float answer = CalcSmashingResistance(fireShield.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcSmashingResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            answer = CalcSmashingResistance(rockArmor.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcSmashingResistance() is incorrect!");
        }

        public static void TestCalcLethalResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            float answer = CalcLethalResistance(fireShield.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcLethalResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            answer = CalcLethalResistance(rockArmor.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcLethalResistance() is incorrect!");
        }

        public static void TestCalcFireResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            float answer = CalcFireResistance(temperatureProtection.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcFireResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            answer = CalcFireResistance(wetIce.Effects[31], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcFireResistance() is incorrect!");
        }

        public static void TestCalcColdResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            float answer = CalcColdResistance(temperatureProtection.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.07f, 0.0001f), "CalcColdResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            answer = CalcColdResistance(wetIce.Effects[32], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcColdResistance() is incorrect!");
        }

        public static void TestCalcEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            float answer = CalcEnergyResistance(plasmaShield.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            answer = CalcEnergyResistance(crystalArmor.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcEnergyResistance() is incorrect!");
        }

        public static void TestCalcNegativeEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            float answer = CalcNegativeEnergyResistance(plasmaShield.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcNegativeEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            answer = CalcNegativeEnergyResistance(crystalArmor.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcNegativeEnergyResistance() is incorrect!");
        }

        public static void TestCalcPsionicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            float answer = CalcPsionicResistance(strengthOfWill.Effects[6], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0875f, 0.0001f), "CalcPsionicResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            answer = CalcPsionicResistance(minerals.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcPsionicResistance() is incorrect!");
        }

        public static void TestCalcToxicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            float answer = CalcToxicResistance(earthsEnbrace.Effects[2], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.14f, 0.0001f), "CalcToxicResistance() is incorrect!");
        }

        public static void TestCalcDamage()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            float answer = CalcDamageBuff(mesmerize.Effects[1], Enums.ePvX.PvE, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 62.5615f, 0.0001f), "CalcDamage() is incorrect!");
        }

        public static void TestCalcAccuracy()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
            IPower focusedAccuracy = maceMastery.Powers[3];
            float answer = CalcAccuracy(focusedAccuracy.Effects[2], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.2f, 0.0001f), "CalcAccuracy() is incorrect!");
        }

        public static void TestCalcFly()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            float answer = CalcFly(fly.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.365f, 0.0001f), "CalcFly() is incorrect!");
        }

        public static void TestCalcJump()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            float answer = CalcJump(superJump.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 2.49f, 0.0001f), "CalcJump() is incorrect!");
        }

        public static void TestCalcRun()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            float answer = CalcRun(superSpeed.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 3.5f, 0.0001f), "CalcRun() is incorrect!");
        }

        public static void TestCalcTeleport()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            float answer = CalcTeleport(teleport.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.0f, 0.0001f), "CalcTeleport() is incorrect!");
        }

        public static void TestCalcConfuse()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            float answer = CalcConfuse(confuse.Effects[0], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 23.84f, 0.0001f), "CalcConfuse() is incorrect!");
        }

        public static void TestCalcHold()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower dominate = mindControl.Powers[2];
            float answer = CalcHold(dominate.Effects[0], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 14.304f, 0.0001f), "CalcHold() is incorrect!");
        }

        public static void TestCalcImmobilize()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            float answer = CalcImmobilize(ringOfFire.Effects[4], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 17.88f, 0.0001f), "CalcImmobilize() is incorrect!");
        }

        public static void TestCalcSleep()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            float answer = CalcSleep(mesmerize.Effects[0], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 35.76f, 0.0001f), "CalcSleep() is incorrect!");
        }

        public static void TestCalcStun()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower flashFire = fireControl.Powers[5];
            float answer = CalcStun(flashFire.Effects[3], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 9.536f, 0.0001f), "CalcStun() is incorrect!");
        }

        public static void TestCalcTerrorize()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower terrify = mindControl.Powers[7];
            float answer = CalcTerrorize(terrify.Effects[0], Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 22.35f, 0.0001f), "CalcTerrorize() is incorrect!");
        }

        public static void TestCalcConfuseResistance()
        {
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            float answer = CalcConfuseResistance(tactics.Effects[5], DatabaseAPI.GetArchetypeByName("Controller"));
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.12449992f, 0.0001f), "CalcConfuseResistance() is incorrect!");
        }

        public static void TestCalcHoldResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcHoldResistance(dispersionBubble.Effects[13], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcHoldResistance() is incorrect!");
        }

        public static void TestCalcImmobilizeResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcImmobilizeResistance(dispersionBubble.Effects[15], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcImmobilizeResistance() is incorrect!");
        }

        public static void TestCalcSleepResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            float answer = CalcSleepResistance(health.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4844f, 0.0001f), "CalcSleepResistance() is incorrect!");
        }

        public static void TestCalcStunResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcStunResistance(dispersionBubble.Effects[14], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcStunResistance() is incorrect!");
        }

        public static void TestCalcTerrorizeResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            float answer = CalcTerrorizeResistance(tactics.Effects[6], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.42385f, 0.0001f), "CalcTerrorizeResistance() is incorrect!");
        }

        public static void TestCalcKnockBack()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            float answer = CalcKnockBack(repulsionField.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 4.9851f, 0.0001f), "CalcKnockBack() is incorrect!");
        }

        public static void TestCalcKnockDown()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            float answer = CalcKnockDown(fissure.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.67f, 0.0001f), "CalcKnockDown() is incorrect!");
        }

        public static void TestCalcKnockUp()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            float answer = CalcKnockUp(levitate.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 9.9702f, 0.0001f), "CalcKnockUp() is incorrect!");
        }

        public static void TestCalcKnockBackResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockBackResistance(rooted.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockBackResistance() is incorrect!");
        }

        public static void TestCalcKnockDownResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockDownResistance(rooted.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockDownResistance() is incorrect!");
        }

        public static void TestCalcKnockUpResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockUpResistance(rooted.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockUpResistance() is incorrect!");
        }

        public static void TestCalcRepel()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower forceBubble = forceField.Powers[8];
            float answer = CalcRepel(forceBubble.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 10f, 0.0001f), "CalcRepel() is incorrect!");
        }

        public static void TestCalcRepelResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower activeDefense = shieldDefense.Powers[3];
            float answer = CalcRepelResistance(activeDefense.Effects[16], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcRepelResistance() is incorrect!");
        }

        public static void TestCalcHeal()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset painDomination = DatabaseAPI.GetPowersetByName("Pain Domination", Enums.ePowerSetType.Primary);
            IPower soothe = painDomination.Powers[1];
            float answer = CalcHeal(soothe.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 188.906174f, 0.0001f), "CalcHeal() is incorrect!");
        }

        public static void TestCalcHitPoints()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEmbrace = stoneArmor.Powers[2];
            float answer = CalcHitPoints(earthsEmbrace.Effects[1], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 240.9518f, 0.0001f), "CalcHitPoints() is incorrect!");
        }

        public static void TestCalcRegeneration()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            float answer = CalcRegeneration(health.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4f, 0.0001f), "CalcRegeneration() is incorrect!");
        }

        public static void TestCalcRecovery()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            float answer = CalcRecovery(stamina.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.25f, 0.0001f), "CalcRecovery() is incorrect!");
        }

        public static void TestCalcEnduranceDiscount()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            float answer = CalcEnduranceDiscount(conservePower.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.192f, 0.0001f), "CalcEnduranceDiscount() is incorrect!");
        }

        public static void TestCalcEnduranceIncrease()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2736);
            IPower superiorConditioning = energyMastery.Powers[0];
            float answer = CalcEnduranceIncrease(superiorConditioning.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 5f, 0.0001f), "CalcEnduranceIncrease() is incorrect!");
        }

        public static void TestCalcRechargeReduction()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower hasten = speed.Powers[1];
            float answer = CalcRechargeReduction(hasten.Effects[0], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7f, 0.0001f), "CalcRechargeReduction() is incorrect!");
        }

        public static void TestCalcMovementSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            float answer = CalcMovementSlow(timesJuncture.Effects[18], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.36f, 0.0001f), "CalcMovementSlow() is incorrect!");
        }

        public static void TestCalcRechargeSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            float answer = CalcRechargeSlow(webGrenade.Effects[2], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.5f, 0.0001f), "CalcRechargeSlow() is incorrect!");
        }

        public static void TestCalcMovementSlowResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            float answer = CalcMovementSlowResistance(speedBoost.Effects[5], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.75f, 0.0001f), "CalcMovementSlowResistance() is incorrect!");
        }

        public static void TestCalcRechargeSlowResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            float answer = CalcRechargeSlowResistance(speedBoost.Effects[4], archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.5f, 0.0001f), "CalcRechargeSlowResistance() is incorrect!");
        }

        public static void TestCalcProbability()
        {
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
            foreach (IEffect effect in mesmerize.Power.Effects)
            {
                float modifiyEffects = 0.0f;
                if (Base.Master_Classes.MidsContext.Character != null && !string.IsNullOrEmpty(effect.EffectId) && Base.Master_Classes.MidsContext.Character.ModifyEffects.ContainsKey(effect.EffectId))
                    modifiyEffects = Base.Master_Classes.MidsContext.Character.ModifyEffects[effect.EffectId];
                float originalAnswer = effect.Probability;
                float answer = AIMath.EffectMath.CalcProbability(mesmerize.Power, effect, Base.Master_Classes.MidsContext.Character.DisplayStats.BuffHaste(false), modifiyEffects);
                Debug.Assert(originalAnswer == answer, "CalcProbability() is incorrect!");
            }
        }

        public static void TestCalcMag()
        {
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
            foreach (IEffect effect in mesmerize.Power.Effects)
            {
                float originalAnswer = effect.Mag;
                float answer = AIMath.EffectMath.CalcMag(effect, Base.Master_Classes.MidsContext.Archetype);
                Debug.Assert(originalAnswer == answer, "CalcMag() is incorrect!");
            }
        }
    }
}

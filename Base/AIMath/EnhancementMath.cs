﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMath
{
    class EnhancementMath
    {
        public const float EDModifier1 = 0.8999999f;
        public const float EDModifier2 = 0.6999999f;
        public const float EDModifier3 = 0.1500000f;
        public static float CalcED(Enums.eSchedule schedule, float value)
        {
            if (Enums.eSchedule.None == schedule || Enums.eSchedule.Multiple == schedule) return 0.0f;
            float[] ed = new float[3];
            for (int index = 0; index <= 2; ++index)
                ed[index] = DatabaseAPI.Database.MultED[(int)schedule][index];
            if (value <= ed[0])
                return value;

            float ed1MinusEd0 = ed[1] - ed[0];
            float ed2MinusEd1 = ed[2] - ed[1];
            float[] edm = {
                ed[0],
                ed[0] + (ed1MinusEd0 * EDModifier1),
                ed[0] + (ed1MinusEd0 * EDModifier1) + (ed2MinusEd1 * EDModifier2)
            };
            float valueMinusEd0 = value - ed[0];
            float valueMinusEd1 = value - ed[1];
            float valueMinusEd2 = value - ed[2];
            if (value > ed[1])
            {
                if (value > ed[2]) 
                    return edm[2] + (valueMinusEd2 * EDModifier3);
                else
                    return edm[1] + (valueMinusEd1 * EDModifier2);
            }
            return edm[0] + (valueMinusEd0 * EDModifier1);
        }

        public static void TestNewCode()
        {
            TestCalcED();
        }

        public static void TestCalcED()
        {
            float originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.0f);
            float answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.1f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.1f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.2f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.2f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.3f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.3f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.4f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.4f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.5f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.5f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.6f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.6f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.7f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.7f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.8f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.8f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 0.9f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 0.9f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 1.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 1.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 1.5f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 1.5f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 2.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 2.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 4.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 4.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 6.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 6.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 8.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 8.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 10.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 10.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 20.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 20.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 30.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 30.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 40.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 40.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 50.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 50.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 75.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 75.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, 100.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, 100.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));

            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.1f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.1f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.2f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.2f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.3f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.3f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.4f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.4f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.5f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.5f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.6f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.6f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.7f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.7f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.8f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.8f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -0.9f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -0.9f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -1.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -1.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -1.5f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -1.5f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -2.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -2.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -4.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -4.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -6.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -6.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -8.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -8.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -10.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -10.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -20.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -20.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -30.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -30.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -40.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -40.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -50.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -50.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -75.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -75.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
            originalAnswer = Enhancement.ApplyED(Enums.eSchedule.A, -100.0f);
            answer = EnhancementMath.CalcED(Enums.eSchedule.A, -100.0f);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(originalAnswer, answer, 0.0001f));
        }
    }
}

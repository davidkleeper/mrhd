﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMath
{
    public static class SEffectMath
    {
        public static float CalcSEffect(Enums.eEnhance desiredEffect, int desiredSubEffect, Enums.sEffect effect, float multiplier, Enums.eBuffDebuff buffOrDebuff)
        {
            if (effect.Mode != Enums.eEffMode.Enhancement) return 0.0f;
            if (effect.Schedule == Enums.eSchedule.None) return 0.0f;
            if ((Enums.eEnhance)effect.Enhance.ID != desiredEffect) return 0.0f;
            if (desiredSubEffect >= 0 && desiredSubEffect != effect.Enhance.SubID) return 0.0f;
            if (Enums.eBuffDebuff.Any != buffOrDebuff && Enums.eBuffDebuff.Any != effect.BuffMode) 
            {
                if (effect.BuffMode != buffOrDebuff) return 0.0f;
            }

            float result = multiplier;
            if (System.Math.Abs(effect.Multiplier) > 0.01)
                result *= effect.Multiplier;
            return result;
        }
    }
}

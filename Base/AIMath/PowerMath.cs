﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMath
{
    class PowerMath
    {
        public static float CalcAllDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcAllDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcMeleeDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcMeleeDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRangedDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRangedDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcAoEDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcAoEDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcSmashingDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcSmashingDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcLethalDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcLethalDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcFireDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcFireDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcColdDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcColdDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcEnergyDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcEnergyDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcNegativeEnergyDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcNegativeEnergyDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcPsionicDefense(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcPsionicDefense(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcSmashingResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcSmashingResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcLethalResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcLethalResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcFireResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcFireResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcColdResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcColdResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcEnergyResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcEnergyResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcNegativeEnergyResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcNegativeEnergyResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcPsionicResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcPsionicResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcToxicResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcToxicResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcDamageBuff(IPower power, Enums.ePvX pvxMode, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            float result = 0.0f;
            var damageMath = Base.Master_Classes.MidsContext.Config.DamageMath;

            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcDamageBuff(effect, pvxMode, archetype));
            }
            if (0 < results.Count) result = results.Max();
            return result;
        }

        private static float FormatProcessedDamage(IPower power, float processedDamage, ConfigData.SDamageMath damMath)
        {
            float res = processedDamage;
            switch (damMath.ReturnValue)
            {
                case ConfigData.EDamageReturn.DPS:
                    if (power.PowerType == Enums.ePowerType.Toggle && power.ActivatePeriod > 0.0)
                    {
                        res /= power.ActivatePeriod;
                        break;
                    }
                    if (power.RechargeTime + (double)power.CastTime + power.InterruptTime > 0.0) res /= power.RechargeTime + power.CastTime + power.InterruptTime;
                    break;
                case ConfigData.EDamageReturn.DPA:
                    if (power.PowerType == Enums.ePowerType.Toggle && power.ActivatePeriod > 0.0)
                    {
                        res /= power.ActivatePeriod;
                        break;
                    }
                    if (power.CastTime > 0.0) res /= power.CastTime;
                    break;
            }
            return res;
        }

        public static float CalcAccuracy(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcAccuracy(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcFly(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcFly(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcJump(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcJump(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRun(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRun(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcTeleport(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcTeleport(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcConfuse(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcConfuse(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcHold(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcHold(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcImmobilize(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcImmobilize(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcSleep(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcSleep(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcStun(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcStun(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcTerrorize(IPower power, Enums.ePvX pvxMode)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcTerrorize(effect, pvxMode));
            }
            return results.Max();
        }

        public static float CalcConfuseResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcConfuseResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcHoldResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcHoldResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcImmobilizeResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcImmobilizeResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcSleepResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcSleepResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcStunResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcStunResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcTerrorizeResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcTerrorizeResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockBack(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockBack(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockDown(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockDown(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockUp(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockUp(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockBackResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockBackResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockDownResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockDownResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcKnockUpResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcKnockUpResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRepel(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRepel(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRepelResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRepelResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcHeal(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcHeal(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcHitPoints(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcHitPoints(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRegeneration(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRegeneration(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRecovery(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRecovery(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcEnduranceDiscount(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcEnduranceDiscount(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcEnduranceIncrease(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcEnduranceIncrease(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRechargeReduction(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRechargeReduction(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcMovementSlow(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcMovementSlow(effect, archetype));
            }
            return results.Min();
        }

        public static float CalcRechargeSlow(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRechargeSlow(effect, archetype));
            }
            return results.Min();
        }

        public static float CalcMovementSlowResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcMovementSlowResistance(effect, archetype));
            }
            return results.Max();
        }

        public static float CalcRechargeSlowResistance(IPower power, Base.Data_Classes.Archetype archetype)
        {
            List<float> results = new List<float>();
            foreach (IEffect effect in power.Effects)
            {
                results.Add(AIMath.EffectMath.CalcRechargeSlowResistance(effect, archetype));
            }
            return results.Max();
        }

        public static void TestNewCode()
        {
            /*
            TestCalcAllDefense();
            TestCalcMeleeDefense();
            TestCalcRangedDefense();
            TestCalcAoEDefense();
            TestCalcSmashingDefense();
            TestCalcLethalDefense();
            TestCalcFireDefense();
            TestCalcColdDefense();
            TestCalcEnergyDefense();
            TestCalcNegativeEnergyDefense();
            TestCalcPsionicDefense();
            TestCalcSmashingResistance();
            TestCalcLethalResistance();
            TestCalcFireResistance();
            TestCalcColdResistance();
            TestCalcEnergyResistance();
            TestCalcNegativeEnergyResistance();
            TestCalcPsionicResistance();
            TestCalcToxicResistance();
            TestCalcDamage();
            TestCalcAccuracy();
            TestCalcFly();
            TestCalcJump();
            TestCalcRun();
            TestCalcTeleport();
            TestCalcConfuse();
            TestCalcHold();
            TestCalcImmobilize();
            TestCalcSleep();
            TestCalcStun();
            TestCalcTerrorize();
            TestCalcConfuseResistance();
            TestCalcHoldResistance();
            TestCalcImmobilizeResistance();
            TestCalcSleepResistance();
            TestCalcStunResistance();
            TestCalcTerrorizeResistance();
            TestCalcKnockBack();
            TestCalcKnockDown();
            TestCalcKnockUp();
            TestCalcKnockBackResistance();
            TestCalcKnockDownResistance();
            TestCalcKnockUpResistance();
            TestCalcRepel();
            TestCalcRepelResistance();
            TestCalcHeal();
            TestCalcHitPoints();
            TestCalcRegeneration();
            TestCalcRecovery();
            TestCalcEnduranceDiscount();
            TestCalcEnduranceIncrease();
            TestCalcRechargeReduction();
            TestCalcMovementSlow();
            TestCalcRechargeSlow();
            TestCalcMovementSlowResistance();
            TestCalcRechargeSlowResistance();
            TestCalcProbability();
            TestCalcMag();
            */
        }

        public static void TestCalcAllDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
            IPower personalForceField = forceField.Powers[0];
            float answer = CalcAllDefense(personalForceField, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.525f, 0.0001f), "CalcAllDefense() is incorrect!");

            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            answer = CalcAllDefense(battleAgility, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcAllDefense() is incorrect!");
        }

        public static void TestCalcMeleeDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower deflection = shieldDefense.Powers[0];
            float answer = CalcMeleeDefense(deflection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcMeleeDefense() is incorrect!");

            IPower battleAgility = shieldDefense.Powers[1];
            answer = CalcMeleeDefense(battleAgility, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcMeleeDefense() is incorrect!");
        }

        public static void TestCalcRangedDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            float answer = CalcRangedDefense(battleAgility, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcRangedDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            answer = CalcRangedDefense(deflection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcRangedDefense() is incorrect!");
        }

        public static void TestCalcAoEDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower battleAgility = shieldDefense.Powers[1];
            float answer = CalcAoEDefense(battleAgility, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "CalcAoEDefense() is incorrect!");

            IPower deflection = shieldDefense.Powers[0];
            answer = CalcAoEDefense(deflection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcAoEDefense() is incorrect!");
        }

        public static void TestCalcSmashingDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            float answer = CalcSmashingDefense(rockArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcSmashingDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            answer = CalcSmashingDefense(fireShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcSmashingDefense() is incorrect!");
        }

        public static void TestCalcLethalDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            float answer = CalcLethalDefense(rockArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcLethalDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            answer = CalcLethalDefense(fireShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcLethalDefense() is incorrect!");
        }

        public static void TestCalcFireDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            float answer = CalcFireDefense(wetIce, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "CalcFireDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            answer = CalcFireDefense(temperatureProtection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcFireDefense() is incorrect!");
        }

        public static void TestCalcColdDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            float answer = CalcColdDefense(wetIce, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "CalcColdDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            answer = CalcColdDefense(temperatureProtection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcColdDefense() is incorrect!");
        }

        public static void TestCalcEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            float answer = CalcEnergyDefense(crystalArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            answer = CalcEnergyDefense(plasmaShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcEnergyDefense() is incorrect!");
        }

        public static void TestCalcNegativeEnergyDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            float answer = CalcNegativeEnergyDefense(crystalArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "CalcNegativeEnergyDefense() is incorrect!");

            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            answer = CalcNegativeEnergyDefense(plasmaShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcNegativeEnergyDefense() is incorrect!");
        }

        public static void TestCalcPsionicDefense()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            float answer = CalcPsionicDefense(minerals, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.175f, 0.0001f), "CalcPsionicDefense() is incorrect!");

            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            answer = CalcPsionicDefense(strengthOfWill, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcPsionicDefense() is incorrect!");
        }

        public static void TestCalcSmashingResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            float answer = CalcSmashingResistance(fireShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcSmashingResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            answer = CalcSmashingResistance(rockArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcSmashingResistance() is incorrect!");
        }

        public static void TestCalcLethalResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower fireShield = fieryAura.Powers[1];
            float answer = CalcLethalResistance(fireShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcLethalResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rockArmor = stoneArmor.Powers[0];
            answer = CalcLethalResistance(rockArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcLethalResistance() is incorrect!");
        }

        public static void TestCalcFireResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            float answer = CalcFireResistance(temperatureProtection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcFireResistance() is incorrect!");

            IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
            IPower wetIce = iceArmor.Powers[3];
            answer = CalcFireResistance(wetIce, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcFireResistance() is incorrect!");
        }

        public static void TestCalcColdResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower temperatureProtection = fieryAura.Powers[3];
            float answer = CalcColdResistance(temperatureProtection, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.07f, 0.0001f), "CalcColdResistance() is incorrect!");
        }

        public static void TestCalcEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            float answer = CalcEnergyResistance(plasmaShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            answer = CalcEnergyResistance(crystalArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcEnergyResistance() is incorrect!");
        }

        public static void TestCalcNegativeEnergyResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
            IPower plasmaShield = fieryAura.Powers[5];
            float answer = CalcNegativeEnergyResistance(plasmaShield, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "CalcNegativeEnergyResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower crystalArmor = stoneArmor.Powers[6];
            answer = CalcNegativeEnergyResistance(crystalArmor, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcNegativeEnergyResistance() is incorrect!");
        }

        public static void TestCalcPsionicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
            IPower strengthOfWill = willpower.Powers[8];
            float answer = CalcPsionicResistance(strengthOfWill, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0875f, 0.0001f), "CalcPsionicResistance() is incorrect!");

            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower minerals = stoneArmor.Powers[7];
            answer = CalcPsionicResistance(minerals, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0f, 0.0001f), "CalcPsionicResistance() is incorrect!");
        }

        public static void TestCalcToxicResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEnbrace = stoneArmor.Powers[2];
            float answer = CalcToxicResistance(earthsEnbrace, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.14f, 0.0001f), "CalcToxicResistance() is incorrect!");
        }

        public static void TestCalcDamage()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            float answer = CalcDamageBuff(mesmerize, Enums.ePvX.PvE, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 62.5615f, 0.0001f), "CalcDamage() is incorrect!");
        }

        public static void TestCalcAccuracy()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
            IPower focusedAccuracy = maceMastery.Powers[3];
            float answer = CalcAccuracy(focusedAccuracy, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.2f, 0.0001f), "CalcAccuracy() is incorrect!");
        }

        public static void TestCalcFly()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
            IPower fly = flight.Powers[1];
            float answer = CalcFly(fly, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.365f, 0.0001f), "CalcFly() is incorrect!");
        }

        public static void TestCalcJump()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
            IPower superJump = leaping.Powers[1];
            float answer = CalcJump(superJump, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 2.49f, 0.0001f), "CalcJump() is incorrect!");
        }

        public static void TestCalcRun()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower superSpeed = speed.Powers[2];
            float answer = CalcRun(superSpeed, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 3.5f, 0.0001f), "CalcRun() is incorrect!");
        }

        public static void TestCalcTeleport()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset teleportation = DatabaseAPI.GetPowersetByName("Teleportation", Enums.ePowerSetType.Pool);
            IPower teleport = teleportation.Powers[1];
            float answer = CalcTeleport(teleport, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.0f, 0.0001f), "CalcTeleport() is incorrect!");
        }

        public static void TestCalcConfuse()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower confuse = mindControl.Powers[3];
            float answer = CalcConfuse(confuse, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 23.84f, 0.0001f), "CalcConfuse() is incorrect!");
        }

        public static void TestCalcHold()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower dominate = mindControl.Powers[2];
            float answer = CalcHold(dominate, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 14.304f, 0.0001f), "CalcHold() is incorrect!");
        }

        public static void TestCalcImmobilize()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower ringOfFire = fireControl.Powers[2];
            float answer = CalcImmobilize(ringOfFire, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 17.88f, 0.0001f), "CalcImmobilize() is incorrect!");
        }

        public static void TestCalcSleep()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower mesmerize = mindControl.Powers[0];
            float answer = CalcSleep(mesmerize, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 35.76f, 0.0001f), "CalcSleep() is incorrect!");
        }

        public static void TestCalcStun()
        {
            IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
            IPower flashFire = fireControl.Powers[5];
            float answer = CalcStun(flashFire, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 9.536f, 0.0001f), "CalcStun() is incorrect!");
        }

        public static void TestCalcTerrorize()
        {
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower terrify = mindControl.Powers[7];
            float answer = CalcTerrorize(terrify, Enums.ePvX.PvE);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 22.35f, 0.0001f), "CalcTerrorize() is incorrect!");
        }

        public static void TestCalcConfuseResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            float answer = CalcConfuseResistance(tactics, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.78715f, 0.0001f), "CalcConfuseResistance() is incorrect!");
        }

        public static void TestCalcHoldResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcHoldResistance(dispersionBubble, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcHoldResistance() is incorrect!");
        }

        public static void TestCalcImmobilizeResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcImmobilizeResistance(dispersionBubble, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcImmobilizeResistance() is incorrect!");
        }

        public static void TestCalcSleepResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            float answer = CalcSleepResistance(health, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4844f, 0.0001f), "CalcSleepResistance() is incorrect!");
        }

        public static void TestCalcStunResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower dispersionBubble = forceField.Powers[5];
            float answer = CalcStunResistance(dispersionBubble, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7266f, 0.0001f), "CalcStunResistance() is incorrect!");
        }

        public static void TestCalcTerrorizeResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
            IPower tactics = leadership.Powers[2];
            float answer = CalcTerrorizeResistance(tactics, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.42385f, 0.0001f), "CalcTerrorizeResistance() is incorrect!");
        }

        public static void TestCalcKnockBack()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower repulsionField = forceField.Powers[6];
            float answer = CalcKnockBack(repulsionField, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 4.9851f, 0.0001f), "CalcKnockBack() is incorrect!");
        }

        public static void TestCalcKnockDown()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
            IPower fissure = stoneMastery.Powers[0];
            float answer = CalcKnockDown(fissure, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.67f, 0.0001f), "CalcKnockDown() is incorrect!");
        }

        public static void TestCalcKnockUp()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
            IPower levitate = mindControl.Powers[1];
            float answer = CalcKnockUp(levitate, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 9.9702f, 0.0001f), "CalcKnockUp() is incorrect!");
        }

        public static void TestCalcKnockBackResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockBackResistance(rooted, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockBackResistance() is incorrect!");
        }

        public static void TestCalcKnockDownResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockDownResistance(rooted, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockDownResistance() is incorrect!");
        }

        public static void TestCalcKnockUpResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower rooted = stoneArmor.Powers[4];
            float answer = CalcKnockUpResistance(rooted, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcKnockUpResistance() is incorrect!");
        }

        public static void TestCalcRepel()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Secondary);
            IPower forceBubble = forceField.Powers[8];
            float answer = CalcRepel(forceBubble, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 10f, 0.0001f), "CalcRepel() is incorrect!");
        }

        public static void TestCalcRepelResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
            IPower activeDefense = shieldDefense.Powers[3];
            float answer = CalcRepelResistance(activeDefense, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "CalcRepelResistance() is incorrect!");
        }

        public static void TestCalcHeal()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset painDomination = DatabaseAPI.GetPowersetByName("Pain Domination", Enums.ePowerSetType.Primary);
            IPower soothe = painDomination.Powers[1];
            float answer = CalcHeal(soothe, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 188.906174f, 0.0001f), "CalcHeal() is incorrect!");
        }

        public static void TestCalcHitPoints()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
            IPower earthsEmbrace = stoneArmor.Powers[2];
            float answer = CalcHitPoints(earthsEmbrace, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 240.9518f, 0.0001f), "CalcHitPoints() is incorrect!");
        }

        public static void TestCalcRegeneration()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower health = fitness.Powers[1];
            float answer = CalcRegeneration(health, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4f, 0.0001f), "CalcRegeneration() is incorrect!");
        }

        public static void TestCalcRecovery()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
            IPower stamina = fitness.Powers[3];
            float answer = CalcRecovery(stamina, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.25f, 0.0001f), "CalcRecovery() is incorrect!");
        }

        public static void TestCalcEnduranceDiscount()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
            IPower conservePower = energyMastery.Powers[0];
            float answer = CalcEnduranceDiscount(conservePower, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.192f, 0.0001f), "CalcEnduranceDiscount() is incorrect!");
        }

        public static void TestCalcEnduranceIncrease()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2736);
            IPower superiorConditioning = energyMastery.Powers[0];
            float answer = CalcEnduranceIncrease(superiorConditioning, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 5f, 0.0001f), "CalcEnduranceIncrease() is incorrect!");
        }

        public static void TestCalcRechargeReduction()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
            IPower hasten = speed.Powers[1];
            float answer = CalcRechargeReduction(hasten, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7f, 0.0001f), "CalcRechargeReduction() is incorrect!");
        }

        public static void TestCalcMovementSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
            IPower timesJuncture = timeManipulation.Powers[2];
            float answer = CalcMovementSlow(timesJuncture, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.36f, 0.0001f), "CalcMovementSlow() is incorrect!");
        }

        public static void TestCalcRechargeSlow()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
            IPower webGrenade = traps.Powers[0];
            float answer = CalcRechargeSlow(webGrenade, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.5f, 0.0001f), "CalcRechargeSlow() is incorrect!");
        }

        public static void TestCalcMovementSlowResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            float answer = CalcMovementSlowResistance(speedBoost, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.75f, 0.0001f), "CalcMovementSlowResistance() is incorrect!");
        }

        public static void TestCalcRechargeSlowResistance()
        {
            Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
            IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
            IPower speedBoost = kinetics.Powers[5];
            float answer = CalcRechargeSlowResistance(speedBoost, archetype);
            Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.5f, 0.0001f), "CalcRechargeSlowResistance() is incorrect!");
        }

        public static void TestCalcProbability()
        {
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
            foreach (IEffect effect in mesmerize.Power.Effects)
            {
                float modifiyEffects = 0.0f;
                if (Base.Master_Classes.MidsContext.Character != null && !string.IsNullOrEmpty(effect.EffectId) && Base.Master_Classes.MidsContext.Character.ModifyEffects.ContainsKey(effect.EffectId))
                    modifiyEffects = Base.Master_Classes.MidsContext.Character.ModifyEffects[effect.EffectId];
                float originalAnswer = effect.Probability;
                float answer = AIMath.EffectMath.CalcProbability(mesmerize.Power, effect, Base.Master_Classes.MidsContext.Character.DisplayStats.BuffHaste(false), modifiyEffects);
                Debug.Assert(originalAnswer == answer, "CalcProbability() is incorrect!");
            }
        }

        public static void TestCalcMag()
        {
            PowerEntry mesmerize = new PowerEntry(1, DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary).Powers[0]);
            foreach (IEffect effect in mesmerize.Power.Effects)
            {
                float originalAnswer = effect.Mag;
                float answer = AIMath.EffectMath.CalcMag(effect, Base.Master_Classes.MidsContext.Archetype);
                Debug.Assert(originalAnswer == answer, "CalcMag() is incorrect!");
            }
        }
    }
}

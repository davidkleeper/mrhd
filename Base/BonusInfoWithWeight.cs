﻿using System;
using System.Collections.Generic;

public class BonusInfoWithWeight : BonusInfo
{
    public float Weight { get; set; }

    public BonusInfoWithWeight()
    {
        Weight = 1.0f;
    }

    public BonusInfoWithWeight(string displayName, Enums.eEffectType effectType, Enums.eEffectType modifies, Enums.eDamage damageType, Enums.eMez mezType, float weight, Enums.eBuffMode buffMode = Enums.eBuffMode.Normal, bool includePvP = false)
    : base(displayName, effectType, modifies, damageType, mezType, buffMode, includePvP)
    {
        Weight = weight;
    }

    public BonusInfoWithWeight(Enums.eEffectType effectType)
    : base("", effectType, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Normal)
    {
        Weight = 1.0f;
    }

    public BonusInfoWithWeight(Enums.eEffectType effectType, Enums.eDamage damageType)
    : base("", effectType, Enums.eEffectType.None, damageType, Enums.eMez.None, Enums.eBuffMode.Normal)
    {
        Weight = 1.0f;
    }

    public BonusInfoWithWeight(BonusInfo bonusInfo, float weight)
    : base(bonusInfo.DisplayName, bonusInfo.EffectType, bonusInfo.Modifies, bonusInfo.DamageType, bonusInfo.MezType, bonusInfo.BuffMode, bonusInfo.IncludePvP)
    {
        Weight = weight;
    }

    new public int CompareTo(object obj)
    {
        BonusInfoWithWeight other = obj as BonusInfoWithWeight;
        if (other == null) throw new ArgumentException("Object is not a BonusInfoWithWeight");
        int compareResult = base.CompareTo(obj);
        if (0 != compareResult) return compareResult;
        compareResult = this.Weight.CompareTo(other.Weight);
        return compareResult;
    }

    new public object Clone()
    {
        BonusInfoWithWeight clone = new BonusInfoWithWeight(DisplayName, EffectType, Modifies, DamageType, MezType, Weight, BuffMode);
        return clone;
    }

    public static List<BonusInfoWithWeight> GetBonusInfoWithWeight(string bonus, float weight, bool includePvP = false, float cap = -1)
    {
        List<BonusInfo> bonusInfoList = BonusInfo.GetBonusInfo(bonus);
        List<BonusInfoWithWeight> bonusInfoWithWeightList = new List<BonusInfoWithWeight>();

        foreach (BonusInfo bonusInfo in bonusInfoList)
        {
            BonusInfoWithWeight bonusInfoWithWeight = new BonusInfoWithWeight(bonusInfo, weight);
            bonusInfoWithWeight.IncludePvP = includePvP;
            bonusInfoWithWeight.Cap = cap;
            bonusInfoWithWeightList.Add(bonusInfoWithWeight);
        }
        return bonusInfoWithWeightList;
    }
}

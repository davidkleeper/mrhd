﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public partial class EnhancementSet
{
    public static EnhancementSet GetHighestBonusEffect(HashSet<EnhancementSet> EnhancementSets, int slots, BonusInfo bonusInfo)
    {
        EnhancementSet highest = null;
        if (1 > slots || 6 < slots)
            return highest;
        float sum = 0.0f;

        foreach (EnhancementSet enhancementSet in EnhancementSets)
        {
            float setSum = enhancementSet.SumEnhancementSetBonusEffect(slots, bonusInfo);
            if (setSum < sum)
                continue;
            sum = setSum;
            highest = enhancementSet;
        }
        return highest;
    }

    public override string ToString()
    {
        return DisplayName;
    }

    public bool IsSuperior()
    {
        return DisplayName.StartsWith("Superior");
    }

    public bool HasSuperior()
    {
        return -1 != DatabaseAPI.GetEnhancementSetIndexByName("Superior " + DisplayName);
    }

    public EnhancementSet GetSuperiorVersion()
    {
        if (!HasSuperior()) return null;
        return DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Superior " + DisplayName));
    }

    public EnhancementSet GetNonSuperiorVersion()
    {
        if (!IsSuperior()) return null;
        string name = DisplayName.Remove(0, 9);
        return DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName(name));
    }

    public List<IPower> GetBonusPowers(Enums.ePvX pvxMode)
    {
        List<IPower> bonusPowers = new List<IPower>();
        foreach (EnhancementSet.BonusItem bonusItem in Bonus)
        {
            for (int bonusIndex = 0; bonusIndex < bonusItem.Index.Length; ++bonusIndex)
            {
                if (pvxMode == Enums.ePvX.PvP && bonusItem.PvMode != Enums.ePvX.PvP)
                    continue;
                if (pvxMode == Enums.ePvX.PvE && bonusItem.PvMode == Enums.ePvX.PvP)
                    continue;
                int powerIndex = bonusItem.Index[bonusIndex];
                IPower bonusPower = DatabaseAPI.GetPowerByIndex(powerIndex);
                if (null == bonusPower)
                    continue;
                bonusPowers.Add(bonusPower);
            }
        }
        return bonusPowers;
    }

    public float SumEnhancementSetBonusEffect(int slots, BonusInfo bonusInfo)
    {
        float sum = 0.0f;
        if (1 > slots || 6 < slots)
            return sum;
        List<IPower> bonusPowers = GetBonusPowers(Enums.ePvX.PvE);

        int slotCount = 1;
        foreach (IPower bonusPower in bonusPowers)
        {
            List<float> bonuses = new List<float>();
            for (int effectIndex = 0; effectIndex < bonusPower.Effects.Length; ++effectIndex)
            {
                IEffect effect = bonusPower.Effects[effectIndex];
                if (bonusInfo.Matches(effect))
                    bonuses.Add(effect.Scale);
            }
            if (0 < bonuses.Count) sum += bonuses.Max();
            slotCount++;
            if (slotCount >= slots)
                break;
        }
        if (false == bonusInfo.IncludePvP)
            return Math.Abs(sum);
        slotCount = 1;
        bonusPowers = GetBonusPowers(Enums.ePvX.PvP);
        foreach (IPower bonusPower in bonusPowers)
        {
            List<float> bonuses = new List<float>();
            for (int effectIndex = 0; effectIndex < bonusPower.Effects.Length; ++effectIndex)
            {
                IEffect effect = bonusPower.Effects[effectIndex];
                if (bonusInfo.Matches(effect))
                    bonuses.Add(effect.Scale);
            }
            if (0 < bonuses.Count) sum += bonuses.Max();
            slotCount++;
            if (slotCount >= slots)
                break;
        }
        return Math.Abs(sum);
    }

    public bool HasBonusEffect(int slots, BonusInfo bonusInfo, float scale = -1.0f)
    {
        List<IPower> bonusPowers = GetBonusPowers(Enums.ePvX.PvE);
        if (2 > slots || 6 < slots)
            return false;

        int slotCount = 1;
        foreach (IPower bonusPower in bonusPowers)
        {
            for (int effectIndex = 0; effectIndex < bonusPower.Effects.Length; ++effectIndex)
            {
                IEffect effect = bonusPower.Effects[effectIndex];
                if (!bonusInfo.Matches(effect))
                    continue;
                if (!BaseUtils.IsKindaSortaEqual(-1, scale, 0.01f) && !BaseUtils.IsKindaSortaEqual(effect.Scale, scale, 0.001f))
                    continue;
                return true;
            }
            slotCount++;
            if (slotCount >= slots)
                break;
        }
        if (false == bonusInfo.IncludePvP)
            return false;
        bonusPowers = GetBonusPowers(Enums.ePvX.PvP);
        slotCount = 1;
        foreach (IPower bonusPowerPvP in bonusPowers)
        {
            for (int effectIndex = 0; effectIndex < bonusPowerPvP.Effects.Length; ++effectIndex)
            {
                IEffect effect = bonusPowerPvP.Effects[effectIndex];
                if (!bonusInfo.Matches(effect))
                    continue;
                if (!BaseUtils.IsKindaSortaEqual(-1, scale, 0.01f) && !BaseUtils.IsKindaSortaEqual(effect.Scale, scale, 0.001f))
                    continue;
                return true;
            }
            slotCount++;
            if (slotCount >= slots)
                break;
        }
        return false;
    }


    public static EnhancementSet[] SortEnhancementSets(HashSet<EnhancementSet> sets, int slots, BonusInfoWithWeight bonusInfoWithWeight)
    {
        EnhancementSet[] enhancementSetArray = sets.ToArray();
        Array.Sort(enhancementSetArray, delegate (EnhancementSet set1, EnhancementSet set2) {
            float sum1 = set1.SumEnhancementSetBonusEffect(slots, bonusInfoWithWeight);
            float sum2 = set2.SumEnhancementSetBonusEffect(slots, bonusInfoWithWeight);
            return sum1.CompareTo(sum2);
        });
        Array.Reverse(enhancementSetArray, 0, enhancementSetArray.Length);
        return enhancementSetArray;
    }

    public static string[] GetList()
    {
        List<string> linesList = new List<string>();
        string headers = "Name, Set Type, Effect Type, Damage/Enhancement/Mez Type, Scale, Slots Required, Min Level, Max Level, PvP Bonus?";
        linesList.Add(headers);
        foreach (EnhancementSet enhancementSet in DatabaseAPI.Database.EnhancementSets)
        {
            List<IPower> bonusPowers = enhancementSet.GetBonusPowers(Enums.ePvX.PvE);
            int slotsRequired = 2;
            foreach (IPower bonusPower in bonusPowers)
            {
                for (int effectIndex = 0; effectIndex < bonusPower.Effects.Length; ++effectIndex)
                {
                    IEffect effect = bonusPower.Effects[effectIndex];
                    string DEMType = effect.DamageType.ToString();
                    if (effect.ETModifies != Enums.eEffectType.None)
                        DEMType = effect.ETModifies.ToString();
                    if (effect.MezType != Enums.eMez.None)
                        DEMType = effect.MezType.ToString();
                    string line = enhancementSet.DisplayName + ", " + enhancementSet.SetType + ", " + effect.EffectType + ", " + DEMType + ", " + effect.Scale + ", " + slotsRequired + ", " + (enhancementSet.LevelMin + 1) + ", " + (enhancementSet.LevelMax + 1);
                    line += ", False";
                    linesList.Add(line);
                }
                ++slotsRequired;
                if (6 < slotsRequired)
                    slotsRequired = 2;
            }
            bonusPowers = enhancementSet.GetBonusPowers(Enums.ePvX.PvP);
            slotsRequired = 2;
            foreach (IPower bonusPower in bonusPowers)
            {
                for (int effectIndex = 0; effectIndex < bonusPower.Effects.Length; ++effectIndex)
                {
                    IEffect effect = bonusPower.Effects[effectIndex];
                    string DEMType = effect.DamageType.ToString();
                    if (effect.ETModifies != Enums.eEffectType.None)
                        DEMType = effect.ETModifies.ToString();
                    if (effect.MezType != Enums.eMez.None)
                        DEMType = effect.MezType.ToString();
                    string line = enhancementSet.DisplayName + ", " + enhancementSet.SetType + ", " + effect.EffectType + ", " + DEMType + ", " + effect.Scale + ", " + slotsRequired + ", " + (enhancementSet.LevelMin + 1) + ", " + (enhancementSet.LevelMax + 1);
                    line += ", True";
                    linesList.Add(line);
                }
                ++slotsRequired;
                if (6 < slotsRequired)
                    slotsRequired = 2;
            }
        }
        string[] lines = linesList.ToArray();
        return lines;
    }

    public static void TestNewCode()
    {
        TestGetBonusPowers();
        TestHasBonusEffect();
        //TestSumBonusEffect();
        //TestGetHighestBonusEffect();
        TestSortEnhancementSets();
        TestSuperior();
    }

    public static void TestGetBonusPowers()
    {
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(200);
        EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
        List<IPower> bonusPowers = enhancementSet.GetBonusPowers(Enums.ePvX.PvE);

        Debug.Assert(bonusPowers.Count == 5, "bonusPowers.Count is incorrect!");
        IPower[] bonusPowersArray = bonusPowers.ToArray();

        foreach (IPower power in bonusPowersArray)
        {
            Debug.Print(power.DisplayName);
        }
        Debug.Assert(bonusPowersArray[0].DisplayName == "Moderate Improved Recovery Bonus", "bonusPowersArray[0].DisplayName is incorrect!");
        Debug.Assert(bonusPowersArray[1].DisplayName == "Moderate Fire, Cold and Mez Resistance", "bonusPowersArray[1].DisplayName is incorrect!");
        Debug.Assert(bonusPowersArray[2].DisplayName == "Large Improved Regeneration Bonus", "bonusPowersArray[2].DisplayName is incorrect!");
        Debug.Assert(bonusPowersArray[3].DisplayName == "Large Lethal, Smash and Mez Resistance", "bonusPowersArray[3].DisplayName is incorrect!");
        Debug.Assert(bonusPowersArray[4].DisplayName == "Huge Increased Ranged/Energy/Negative Energy Def Bonus", "bonusPowersArray[4].DisplayName is incorrect!");
    }

    public static void TestHasBonusEffect()
    {
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(200);
        EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
        bool hasEnergyDefenseBonus = enhancementSet.HasBonusEffect(6, BonusInfo.DefenseEnergy);
        Debug.Assert(hasEnergyDefenseBonus == true, "hasEnergyDefenseBonus is incorrect!");

        hasEnergyDefenseBonus = enhancementSet.HasBonusEffect(6, BonusInfo.DefenseEnergy, 0.0188f);
        Debug.Assert(hasEnergyDefenseBonus == true, "hasEnergyDefenseBonus is incorrect!");

        hasEnergyDefenseBonus = enhancementSet.HasBonusEffect(6, BonusInfo.DefenseEnergy, 0.05f);
        Debug.Assert(hasEnergyDefenseBonus == false, "hasEnergyDefenseBonus is incorrect!");

        bool hasEnergyResistanceBonus = enhancementSet.HasBonusEffect(6, BonusInfo.ResistanceEnergy);
        Debug.Assert(hasEnergyResistanceBonus == false, "hasEnergyResistanceBonus is incorrect!");

        bool hasDefenseBonus = enhancementSet.HasBonusEffect(6, BonusInfo.DefenseAll);
        Debug.Assert(hasDefenseBonus == true, "hasDefenseBonus is incorrect!");

        bool hasFireDefenseBonus = enhancementSet.HasBonusEffect(6, BonusInfo.DefenseFire);
        Debug.Assert(hasFireDefenseBonus == false, "hasFireDefenseBonus is incorrect!");

        bool hasFireResistanceBonus = enhancementSet.HasBonusEffect(6, BonusInfo.ResistanceFire);
        Debug.Assert(hasFireResistanceBonus == true, "hasFireResistanceBonus is incorrect!");

        hasEnergyDefenseBonus = enhancementSet.HasBonusEffect(5, BonusInfo.DefenseEnergy);
        Debug.Assert(hasEnergyDefenseBonus == false, "hasEnergyDefenseBonus is incorrect!");

        bool hasSmashingResistanceBonus = enhancementSet.HasBonusEffect(5, BonusInfo.ResistanceSmashing);
        Debug.Assert(hasSmashingResistanceBonus == true, "hasSmashingResistanceBonus is incorrect!");

        hasSmashingResistanceBonus = enhancementSet.HasBonusEffect(4, BonusInfo.ResistanceSmashing);
        Debug.Assert(hasSmashingResistanceBonus == false, "hasSmashingResistanceBonus is incorrect!");

        bool hasRegenerationBonus = enhancementSet.HasBonusEffect(4, BonusInfo.Regeneration);
        Debug.Assert(hasRegenerationBonus == true, "hasRegenerationBonus is incorrect!");

        hasRegenerationBonus = enhancementSet.HasBonusEffect(3, BonusInfo.Regeneration);
        Debug.Assert(hasRegenerationBonus == false, "hasRegenerationBonus is incorrect!");

        hasFireResistanceBonus = enhancementSet.HasBonusEffect(3, BonusInfo.ResistanceFire);
        Debug.Assert(hasFireResistanceBonus == true, "hasFireResistanceBonus is incorrect!");

        hasFireResistanceBonus = enhancementSet.HasBonusEffect(2, BonusInfo.ResistanceFire);
        Debug.Assert(hasFireResistanceBonus == false, "hasFireResistanceBonus is incorrect!");

        enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Shield Wall"));
        Debug.Assert(enhancementSet != null, "enhancementSet is incorrect!");

        hasRegenerationBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Regeneration);
        Debug.Assert(hasRegenerationBonus == true, "hasRegenerationBonus is incorrect!");
        hasRegenerationBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Regeneration, 0.1f);
        Debug.Assert(hasRegenerationBonus == true, "hasRegenerationBonus is incorrect!");
        hasRegenerationBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Regeneration, 0.01f);
        Debug.Assert(hasRegenerationBonus == false, "hasRegenerationBonus is incorrect!");
        hasRegenerationBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Regeneration.CloneWithPvP(), -1.0f);
        Debug.Assert(hasRegenerationBonus == true, "hasRegenerationBonus is incorrect!");

        bool hasRecoveryBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Recovery);
        Debug.Assert(hasRecoveryBonus == false, "hasRecoveryBonus is incorrect!");
        hasRecoveryBonus = enhancementSet.HasBonusEffect(6, BonusInfo.Recovery.CloneWithPvP(), -1.0f);
        Debug.Assert(hasRecoveryBonus == true, "hasRecoveryBonus is incorrect!");
    }

    public static void TestSumBonusEffect()
    {
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(200);
        EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
        float sum = enhancementSet.SumEnhancementSetBonusEffect(6, BonusInfo.DefenseAll);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.075f, sum, 0.0001f), "sum is incorrect!");
        sum = enhancementSet.SumEnhancementSetBonusEffect(5, BonusInfo.DefenseAll);
        Debug.Assert(sum == 0.0, "sum is incorrect!");
        sum = enhancementSet.SumEnhancementSetBonusEffect(6, BonusInfo.DefenseRanged);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.0375f, sum, 0.0001f), "sum is incorrect!");

        IPowerset powerset = DatabaseAPI.GetPowersetByName("Fire Blast", Enums.ePowerSetType.Primary);
        PowerEntry fireball = new PowerEntry(1, powerset.Powers[2]);
        fireball.Slot(new int[] { 26, 26, 26, 26, 26, 26 });
        EnhancementSet[] enhancementSets = fireball.Power.GetEnhancementSetsWithBonusEffect(3, BonusInfo.ResistanceFire).ToArray();
        EnhancementSet ragnarok = enhancementSets[1];
        EnhancementSet positronsBlast = enhancementSets[0];
        fireball.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[0]).GetLoadString(), ":");
        fireball.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[1]).GetLoadString(), ":");
        fireball.Slots[2].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[2]).GetLoadString(), ":");
        sum = new BonusInfo("", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Fire, Enums.eMez.None).SumBonuses(fireball);
        Debug.Assert(sum > 0.0224 && sum < 0.0226, "sum is incorrect!");
        fireball.Slots[0].LoadFromString(DatabaseAPI.GetEnhancementByIndex(ragnarok.Enhancements[0]).GetLoadString(), ":");
        fireball.Slots[1].LoadFromString(DatabaseAPI.GetEnhancementByIndex(ragnarok.Enhancements[1]).GetLoadString(), ":");
        fireball.Slots[2].LoadFromString(DatabaseAPI.GetEnhancementByIndex(ragnarok.Enhancements[2]).GetLoadString(), ":");
        sum = new BonusInfo("", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Fire, Enums.eMez.None).SumBonuses(fireball);
        Debug.Assert(sum > 0.059 && sum < 0.061, "sum is incorrect!");
        fireball.Slots[3].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[0]).GetLoadString(), ":");
        fireball.Slots[4].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[1]).GetLoadString(), ":");
        fireball.Slots[5].LoadFromString(DatabaseAPI.GetEnhancementByIndex(positronsBlast.Enhancements[2]).GetLoadString(), ":");
        sum = new BonusInfo("", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Fire, Enums.eMez.None).SumBonuses(fireball);
        Debug.Assert(sum > 0.0824 && sum < 0.0826, "sum is incorrect!");
    }

    public static void TestGetHighestBonusEffect()
    {
        IPower power = DatabaseAPI.GetPowerByIndex(0);
        HashSet<EnhancementSet> defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefenseAll);
        EnhancementSet highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 6, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None));
        Debug.Print("Highest Defense (6 slots)");
        Debug.Print(highestBonusEffect.DisplayName + " " + highestBonusEffect.SumEnhancementSetBonusEffect(6, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None)));
        Debug.Print(" ");
        Debug.Assert(highestBonusEffect.DisplayName == "Superior Winter's Bite", "highestBonusEffect.DisplayName is incorrect!");

        defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(5, BonusInfo.DefenseAll);
        highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 5, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None));
        Debug.Print("Highest Defense (5 slots)");
        Debug.Print(highestBonusEffect.DisplayName + " " + highestBonusEffect.SumEnhancementSetBonusEffect(5, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None)));
        Debug.Print(" ");
        Debug.Assert(highestBonusEffect.DisplayName == "Superior Winter's Bite", "highestBonusEffect.DisplayName is incorrect!");

        defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(4, BonusInfo.DefenseAll);
        highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 4, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None));
        Debug.Print("Highest Defense (4 slots)");
        Debug.Print(highestBonusEffect.DisplayName + " " + highestBonusEffect.SumEnhancementSetBonusEffect(4, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None)));
        Debug.Print(" ");
        Debug.Assert(highestBonusEffect.DisplayName == "Superior Spider's Bite", "highestBonusEffect.DisplayName is incorrect!");

        defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(3, BonusInfo.DefenseAll);
        highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 3, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None));
        Debug.Print("Highest Defense (3 slots)");
        Debug.Print(highestBonusEffect.DisplayName + " " + highestBonusEffect.SumEnhancementSetBonusEffect(3, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None)));
        Debug.Print(" ");
        Debug.Assert(highestBonusEffect.DisplayName == "Thunderstrike", "highestBonusEffect.DisplayName is incorrect!");

        defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(2, BonusInfo.DefenseAll);
        highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 2, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None));
        Debug.Print("Highest Defense (2 slots)");
        if (null != highestBonusEffect) Debug.Print(highestBonusEffect.DisplayName + " " + highestBonusEffect.SumEnhancementSetBonusEffect(2, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None)));
        Debug.Print(" ");

        defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefensePsionic);
        highestBonusEffect = GetHighestBonusEffect(defenseBonusSets, 6, new BonusInfo("", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Psionic, Enums.eMez.None));
        Debug.Print(" ");
        Debug.Print("Highest Psionic Defense");
        Debug.Print(highestBonusEffect.DisplayName);
        Debug.Assert(highestBonusEffect.DisplayName == "Apocalypse", "highestBonusEffect.DisplayName is incorrect!");
    }

    public static void TestSortEnhancementSets()
    {
        IPower power = DatabaseAPI.GetPowerByIndex(0);
        HashSet<EnhancementSet> defenseBonusSets = power.GetEnhancementSetsWithBonusEffect(6, BonusInfo.DefenseAll);
        EnhancementSet[] enhancementSets = SortEnhancementSets(defenseBonusSets, 6, new BonusInfoWithWeight(BonusInfo.DefenseAll, 1.0f));

        for (int index1 = 0; index1 < enhancementSets.Length - 2; index1++)
        {
            float sum1 = enhancementSets[index1].SumEnhancementSetBonusEffect(6, BonusInfo.DefenseAll);
            float sum2 = enhancementSets[index1 + 1].SumEnhancementSetBonusEffect(6, BonusInfo.DefenseAll);
            Debug.Assert(sum1 >= sum2, "SortEnhancementSets is incorrect!");
        }
    }

    public static void TestSuperior()
    {
        EnhancementSet enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Winter's Bite"));
        Debug.Assert(enhancementSet.IsSuperior() == false, "enhancementSet.IsSuperior() is incorrect!");
        Debug.Assert(enhancementSet.HasSuperior() == true, "enhancementSet.HasSuperior() is incorrect!");
        Debug.Assert(enhancementSet.GetSuperiorVersion().DisplayName == "Superior Winter's Bite", "enhancementSet.GetSuperiorVersion().DisplayName is incorrect!");
        Debug.Assert(enhancementSet.GetNonSuperiorVersion() == null, "enhancementSet.GetNonSuperiorVersion() is incorrect!");

        enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Superior Winter's Bite"));
        Debug.Assert(enhancementSet.IsSuperior() == true, "enhancementSet.IsSuperior() is incorrect!");
        Debug.Assert(enhancementSet.HasSuperior() == false, "enhancementSet.HasSuperior() is incorrect!");
        Debug.Assert(enhancementSet.GetSuperiorVersion() == null, "enhancementSet.GetSuperiorVersion() is incorrect!");
        Debug.Assert(enhancementSet.GetNonSuperiorVersion().DisplayName == "Winter's Bite", "enhancementSet.GetNonSuperiorVersion().DisplayName is incorrect!");
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum EnhancementType : int
{
    Accuracy = 26,
    Interrupt = 27,
    Confuse = 28,
    Damage = 29,
    Defense = 30,
    DefenseDebuff = 31,
    EnduranceModification = 32,
    EnduranceReduction = 33,
    Fear = 34,
    Fly = 35,
    Heal = 36,
    Hold = 37,
    Immobilize = 38,
    Intangible = 39,
    Jump = 40,
    Knockback = 41,
    Range = 42,
    Recharge = 43,
    Resistance = 44,
    Run = 45,
    Sleep = 46,
    Slow = 47,
    Stun = 48,
    Taunt = 49,
    ToHit = 50,
    ToHitDebuff = 51
}


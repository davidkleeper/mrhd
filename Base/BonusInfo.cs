﻿using Base.Data_Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

public class BonusInfo : IComparable, ICloneable
{
    public readonly static BonusInfo Accuracy = new BonusInfo("Accuracy", Enums.eEffectType.Enhancement, Enums.eEffectType.Accuracy, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Confuse = new BonusInfo("Confuse Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Confused);
    public readonly static BonusInfo DamageBuff = new BonusInfo("Damage", Enums.eEffectType.DamageBuff, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo DefenseAll = new BonusInfo("Defense (All)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo DefenseAoE = new BonusInfo("Defense (AoE)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.AoE, Enums.eMez.None);
    public readonly static BonusInfo DefenseCold = new BonusInfo("Defense (Cold)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Cold, Enums.eMez.None);
    public readonly static BonusInfo DefenseEnergy = new BonusInfo("Defense (Energy)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Energy, Enums.eMez.None);
    public readonly static BonusInfo DefenseFire = new BonusInfo("Defense (Fire)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Fire, Enums.eMez.None);
    public readonly static BonusInfo DefenseLethal = new BonusInfo("Defense (Lethal)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Lethal, Enums.eMez.None);
    public readonly static BonusInfo DefenseMelee = new BonusInfo("Defense (Melee)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Melee, Enums.eMez.None);
    public readonly static BonusInfo DefenseNegative = new BonusInfo("Defense (Negative)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Negative, Enums.eMez.None);
    public readonly static BonusInfo DefensePsionic = new BonusInfo("Defense (Psionic)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Psionic, Enums.eMez.None);
    public readonly static BonusInfo DefenseRanged = new BonusInfo("Defense (Ranged)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Ranged, Enums.eMez.None);
    public readonly static BonusInfo DefenseSmashing = new BonusInfo("Defense (Smashing)", Enums.eEffectType.Defense, Enums.eEffectType.None, Enums.eDamage.Smashing, Enums.eMez.None);
    public readonly static BonusInfo EnduranceDiscount = new BonusInfo("Endurance Discount", Enums.eEffectType.Enhancement, Enums.eEffectType.EnduranceDiscount, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Fly = new BonusInfo("Fly", Enums.eEffectType.SpeedFlying, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Heal = new BonusInfo("Heal", Enums.eEffectType.Enhancement, Enums.eEffectType.Heal, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Hold = new BonusInfo("Hold Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Held);
    public readonly static BonusInfo HitPoints = new BonusInfo("Hit Points", Enums.eEffectType.HitPoints, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Immobilize = new BonusInfo("Immobilize Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Immobilized);
    public readonly static BonusInfo Jump = new BonusInfo("Jump", Enums.eEffectType.SpeedJumping, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo KnockbackProtection = new BonusInfo("Knockback Protection", Enums.eEffectType.Mez, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Knockback);
    public readonly static BonusInfo Knockdown = new BonusInfo("Knockdown", Enums.eEffectType.AddBehavior, Enums.eEffectType.Null, Enums.eDamage.None, Enums.eMez.Knockback);
    public readonly static BonusInfo MaxEndurance = new BonusInfo("Maximum Endurance", Enums.eEffectType.Endurance, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo MezEnhance = new BonusInfo("Mez Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo MezResistance = new BonusInfo("Mez Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo PetDefense = new BonusInfo("Pet Defense", Enums.eEffectType.Defense, Enums.eEffectType.AddBehavior, Enums.eDamage.Unique1, Enums.eMez.None);
    public readonly static BonusInfo PetResistance = new BonusInfo("Pet Resistance", Enums.eEffectType.Resistance, Enums.eEffectType.AddBehavior, Enums.eDamage.Unique1, Enums.eMez.None);
    public readonly static BonusInfo Range = new BonusInfo("Range", Enums.eEffectType.Enhancement, Enums.eEffectType.Range, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo RechargeTime = new BonusInfo("Recharge Time", Enums.eEffectType.Enhancement, Enums.eEffectType.RechargeTime, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Recovery = new BonusInfo("Recovery", Enums.eEffectType.Recovery, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo ReduceDamage = new BonusInfo("Reduce Damage", Enums.eEffectType.AddBehavior, Enums.eEffectType.None, Enums.eDamage.Special, Enums.eMez.None);
    public readonly static BonusInfo Regeneration = new BonusInfo("Regeneration", Enums.eEffectType.Regeneration, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo RepelResistance = new BonusInfo("Repel Resistance", Enums.eEffectType.Mez, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Knockback);
    public readonly static BonusInfo ResistanceCold = new BonusInfo("Resistance (Cold)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Cold, Enums.eMez.None);
    public readonly static BonusInfo ResistanceEnergy = new BonusInfo("Resistance (Energy)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Energy, Enums.eMez.None);
    public readonly static BonusInfo ResistanceFire = new BonusInfo("Resistance (Fire)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Fire, Enums.eMez.None);
    public readonly static BonusInfo ResistanceLethal = new BonusInfo("Resistance (Lethal)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Lethal, Enums.eMez.None);
    public readonly static BonusInfo ResistanceNegative = new BonusInfo("Resistance (Negative)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Negative, Enums.eMez.None);
    public readonly static BonusInfo ResistancePsionic = new BonusInfo("Resistance (Psionic)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Psionic, Enums.eMez.None);
    public readonly static BonusInfo ResistanceSmashing = new BonusInfo("Resistance (Smashing)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Smashing, Enums.eMez.None);
    public readonly static BonusInfo ResistanceToxic = new BonusInfo("Resistance (Toxic)", Enums.eEffectType.Resistance, Enums.eEffectType.None, Enums.eDamage.Toxic, Enums.eMez.None);
    public readonly static BonusInfo ResistanceSlowMovement = new BonusInfo("Resistance Slow Movement", Enums.eEffectType.ResEffect, Enums.eEffectType.SpeedRunning, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo ResistanceSlowRecharge = new BonusInfo("Resistance Slow Recharge", Enums.eEffectType.ResEffect, Enums.eEffectType.RechargeTime, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Running = new BonusInfo("Running", Enums.eEffectType.SpeedRunning, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo Sleep = new BonusInfo("Sleep Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Sleep);
    public readonly static BonusInfo Slow = new BonusInfo("Slow", Enums.eEffectType.Enhancement, Enums.eEffectType.SpeedRunning, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Debuff);
    public readonly static BonusInfo Stun = new BonusInfo("Stun Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Stunned);
    public readonly static BonusInfo Terrorize = new BonusInfo("Terrorize Enhance", Enums.eEffectType.Enhancement, Enums.eEffectType.Mez, Enums.eDamage.None, Enums.eMez.Terrorized);

    public readonly static BonusInfo MaxFlySpeed = new BonusInfo("Max Fly Speed", Enums.eEffectType.Fly, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo SpeedFlying = new BonusInfo("Speed Flying", Enums.eEffectType.SpeedFlying, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo MaxJumpSpeed = new BonusInfo("Max Jump Speed", Enums.eEffectType.MaxJumpSpeed, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo SpeedJumping = new BonusInfo("Speed Jumping", Enums.eEffectType.SpeedJumping, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo JumpHeight = new BonusInfo("Jump Height", Enums.eEffectType.JumpHeight, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo SpeedRunning = new BonusInfo("Speed Running", Enums.eEffectType.SpeedRunning, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo ResistanceSlowSpeedFlying = new BonusInfo("Resistance Slow Speed Flying", Enums.eEffectType.ResEffect, Enums.eEffectType.SpeedFlying, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo ResistanceSlowSpeedRunning = new BonusInfo("Resistance Slow Speed Running", Enums.eEffectType.ResEffect, Enums.eEffectType.SpeedRunning, Enums.eDamage.None, Enums.eMez.None);
    public readonly static BonusInfo SlowSpeedFlying = new BonusInfo("Slow Speed Flying", Enums.eEffectType.Enhancement, Enums.eEffectType.SpeedFlying, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Debuff);
    public readonly static BonusInfo SlowSpeedJumping = new BonusInfo("Slow Speed Jumping", Enums.eEffectType.Enhancement, Enums.eEffectType.SpeedJumping, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Debuff);
    public readonly static BonusInfo SlowHeightJumping = new BonusInfo("Slow Height Jumping", Enums.eEffectType.Enhancement, Enums.eEffectType.JumpHeight, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Debuff);
    public readonly static BonusInfo SlowSpeedRunning = new BonusInfo("Slow Speed Running", Enums.eEffectType.Enhancement, Enums.eEffectType.SpeedRunning, Enums.eDamage.None, Enums.eMez.None, Enums.eBuffMode.Debuff);
    public readonly static BonusInfo ConfusedResistance = new BonusInfo("Confused Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Confused);
    public readonly static BonusInfo HoldResistance = new BonusInfo("Hold Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Held);
    public readonly static BonusInfo ImmobilizeResistance = new BonusInfo("Immobilize Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Immobilized);
    public readonly static BonusInfo SleepResistance = new BonusInfo("Sleep Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Sleep);
    public readonly static BonusInfo StunResistance = new BonusInfo("Stun Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Stunned);
    public readonly static BonusInfo TerrorizeResistance = new BonusInfo("Terrorize Resistance", Enums.eEffectType.MezResist, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Terrorized);
    public readonly static BonusInfo KnockupProtection = new BonusInfo("Knockup Protection", Enums.eEffectType.Mez, Enums.eEffectType.None, Enums.eDamage.None, Enums.eMez.Knockup);

    public static List<BonusInfo> GetBonusInfo(string bonus)
    {
        List<BonusInfo> bonusInfoList = new List<BonusInfo>();

        if (Accuracy.DisplayName == bonus)
            bonusInfoList.Add(Accuracy.Clone() as BonusInfo);
        else if (Confuse.DisplayName == bonus)
            bonusInfoList.Add(Confuse.Clone() as BonusInfo);
        else if (DamageBuff.DisplayName == bonus)
            bonusInfoList.Add(DamageBuff.Clone() as BonusInfo);
        else if (DefenseAll.DisplayName == bonus)
            bonusInfoList.Add(DefenseAll.Clone() as BonusInfo);
        else if (DefenseAoE.DisplayName == bonus)
            bonusInfoList.Add(DefenseAoE.Clone() as BonusInfo);
        else if (DefenseCold.DisplayName == bonus)
            bonusInfoList.Add(DefenseCold.Clone() as BonusInfo);
        else if (DefenseEnergy.DisplayName == bonus)
            bonusInfoList.Add(DefenseEnergy.Clone() as BonusInfo);
        else if (DefenseFire.DisplayName == bonus)
            bonusInfoList.Add(DefenseFire.Clone() as BonusInfo);
        else if (DefenseLethal.DisplayName == bonus)
            bonusInfoList.Add(DefenseLethal.Clone() as BonusInfo);
        else if (DefenseMelee.DisplayName == bonus)
            bonusInfoList.Add(DefenseMelee.Clone() as BonusInfo);
        else if (DefenseNegative.DisplayName == bonus)
            bonusInfoList.Add(DefenseNegative.Clone() as BonusInfo);
        else if (DefensePsionic.DisplayName == bonus)
            bonusInfoList.Add(DefensePsionic.Clone() as BonusInfo);
        else if (DefenseRanged.DisplayName == bonus)
            bonusInfoList.Add(DefenseRanged.Clone() as BonusInfo);
        else if (DefenseSmashing.DisplayName == bonus)
            bonusInfoList.Add(DefenseSmashing.Clone() as BonusInfo);
        else if (EnduranceDiscount.DisplayName == bonus)
            bonusInfoList.Add(EnduranceDiscount.Clone() as BonusInfo);
        else if (Fly.DisplayName == bonus)
            bonusInfoList.Add(Fly.Clone() as BonusInfo);
        else if (Heal.DisplayName == bonus)
            bonusInfoList.Add(Heal.Clone() as BonusInfo);
        else if (HitPoints.DisplayName == bonus)
            bonusInfoList.Add(HitPoints.Clone() as BonusInfo);
        else if (Hold.DisplayName == bonus)
            bonusInfoList.Add(Hold.Clone() as BonusInfo);
        else if (Immobilize.DisplayName == bonus)
            bonusInfoList.Add(Immobilize.Clone() as BonusInfo);
        else if (Jump.DisplayName == bonus)
            bonusInfoList.Add(Jump.Clone() as BonusInfo);
        else if (KnockbackProtection.DisplayName == bonus)
            bonusInfoList.Add(KnockbackProtection.Clone() as BonusInfo);
        else if (Knockdown.DisplayName == bonus)
            bonusInfoList.Add(Knockdown.Clone() as BonusInfo);
        else if (MaxEndurance.DisplayName == bonus)
            bonusInfoList.Add(MaxEndurance.Clone() as BonusInfo);
        else if (MezEnhance.DisplayName == bonus)
            bonusInfoList.Add(MezEnhance.Clone() as BonusInfo);
        else if (MezResistance.DisplayName == bonus)
            bonusInfoList.Add(MezResistance.Clone() as BonusInfo);
        else if (PetDefense.DisplayName == bonus)
            bonusInfoList.Add(PetDefense.Clone() as BonusInfo);
        else if (PetResistance.DisplayName == bonus)
            bonusInfoList.Add(PetResistance.Clone() as BonusInfo);
        else if (Range.DisplayName == bonus)
            bonusInfoList.Add(Range.Clone() as BonusInfo);
        else if (RechargeTime.DisplayName == bonus)
            bonusInfoList.Add(RechargeTime.Clone() as BonusInfo);
        else if (Recovery.DisplayName == bonus)
            bonusInfoList.Add(Recovery.Clone() as BonusInfo);
        else if (ReduceDamage.DisplayName == bonus)
            bonusInfoList.Add(ReduceDamage.Clone() as BonusInfo);
        else if (Regeneration.DisplayName == bonus)
            bonusInfoList.Add(Regeneration.Clone() as BonusInfo);
        else if (RepelResistance.DisplayName == bonus)
            bonusInfoList.Add(RepelResistance.Clone() as BonusInfo);
        else if (ResistanceCold.DisplayName == bonus)
            bonusInfoList.Add(ResistanceCold.Clone() as BonusInfo);
        else if (ResistanceEnergy.DisplayName == bonus)
            bonusInfoList.Add(ResistanceEnergy.Clone() as BonusInfo);
        else if (ResistanceFire.DisplayName == bonus)
            bonusInfoList.Add(ResistanceFire.Clone() as BonusInfo);
        else if (ResistanceLethal.DisplayName == bonus)
            bonusInfoList.Add(ResistanceLethal.Clone() as BonusInfo);
        else if (ResistanceNegative.DisplayName == bonus)
            bonusInfoList.Add(ResistanceNegative.Clone() as BonusInfo);
        else if (ResistancePsionic.DisplayName == bonus)
            bonusInfoList.Add(ResistancePsionic.Clone() as BonusInfo);
        else if (ResistanceSlowMovement.DisplayName == bonus)
            bonusInfoList.Add(ResistanceSlowMovement.Clone() as BonusInfo);
        else if (ResistanceSlowRecharge.DisplayName == bonus)
            bonusInfoList.Add(ResistanceSlowRecharge.Clone() as BonusInfo);
        else if (ResistanceSmashing.DisplayName == bonus)
            bonusInfoList.Add(ResistanceSmashing.Clone() as BonusInfo);
        else if (ResistanceToxic.DisplayName == bonus)
            bonusInfoList.Add(ResistanceToxic.Clone() as BonusInfo);
        else if (Running.DisplayName == bonus)
            bonusInfoList.Add(Running.Clone() as BonusInfo);
        else if (Sleep.DisplayName == bonus)
            bonusInfoList.Add(Sleep.Clone() as BonusInfo);
        else if (Slow.DisplayName == bonus)
            bonusInfoList.Add(Slow.Clone() as BonusInfo);
        else if (Stun.DisplayName == bonus)
            bonusInfoList.Add(Stun.Clone() as BonusInfo);
        else if (Terrorize.DisplayName == bonus)
            bonusInfoList.Add(Terrorize.Clone() as BonusInfo);

        return bonusInfoList;
    }

    public string DisplayName { get; set; }
    public Enums.eEffectType EffectType { get; set; }
    public Enums.eEffectType Modifies { get; set; }
    public Enums.eDamage DamageType { get; set; }
    public Enums.eMez MezType { get; set; }
    public Enums.eBuffMode BuffMode { get; set; }
    public bool IncludePvP { get; set; }
    public float Cap { get; set; }

    public BonusInfo()
    {
        DisplayName = "";
        EffectType = Enums.eEffectType.None;
        Modifies = Enums.eEffectType.None;
        DamageType = Enums.eDamage.None;
        MezType = Enums.eMez.None;
        BuffMode = Enums.eBuffMode.Normal;
        IncludePvP = false;
        Cap = -1;
    }

    public BonusInfo(string displayName, Enums.eEffectType effectType, Enums.eEffectType modifies, Enums.eDamage damageType, Enums.eMez mezType, Enums.eBuffMode buffMode = Enums.eBuffMode.Normal, bool includePvP = false)
    {
        DisplayName = displayName;
        EffectType = effectType;
        Modifies = modifies;
        DamageType = damageType;
        MezType = mezType;
        BuffMode = buffMode;
        IncludePvP = includePvP;
        Cap = -1;
    }

    public BonusInfo(Enums.eEffectType effectType)
    {
        DisplayName = "";
        EffectType = effectType;
        Modifies = Enums.eEffectType.None;
        DamageType = Enums.eDamage.None;
        MezType = Enums.eMez.None;
        BuffMode = Enums.eBuffMode.Normal;
        IncludePvP = false;
        Cap = -1;
    }

    public BonusInfo(Enums.eEffectType effectType, Enums.eDamage damageType)
    {
        DisplayName = "";
        EffectType = effectType;
        Modifies = Enums.eEffectType.None;
        DamageType = damageType;
        MezType = Enums.eMez.None;
        BuffMode = Enums.eBuffMode.Normal;
        IncludePvP = false;
        Cap = -1;
    }

    public override string ToString()
    {
        return DisplayName;
    }

    public int CompareTo(object obj)
    {
        if (obj == null) return 1;
        BonusInfo other = obj as BonusInfo;
        if (other == null) throw new ArgumentException("Object is not a BonusInfo");
        int compareResult = this.DisplayName.CompareTo(other.DisplayName);
        if (0 != compareResult) return compareResult;
        compareResult = this.EffectType.CompareTo(other.EffectType);
        if (0 != compareResult) return compareResult;
        compareResult = this.Modifies.CompareTo(other.Modifies);
        if (0 != compareResult) return compareResult;
        compareResult = this.DamageType.CompareTo(other.DamageType);
        if (0 != compareResult) return compareResult;
        compareResult = this.MezType.CompareTo(other.MezType);
        if (0 != compareResult) return compareResult;
        compareResult = this.BuffMode.CompareTo(other.BuffMode);
        if (0 != compareResult) return compareResult;
        compareResult = this.IncludePvP.CompareTo(other.IncludePvP);
        if (0 != compareResult) return compareResult;
        compareResult = this.Cap.CompareTo(other.Cap);
        return compareResult;
    }

    public object Clone()
    {
        BonusInfo clone = new BonusInfo(DisplayName, EffectType, Modifies, DamageType, MezType, BuffMode);
        clone.IncludePvP = IncludePvP;
        clone.Cap = Cap;
        return clone;
    }

    public BonusInfo CloneWithPvP(bool pvp = true)
    {
        BonusInfo cloneWithPvP = Clone() as BonusInfo;
        cloneWithPvP.IncludePvP = pvp;
        return cloneWithPvP;
    }

    public bool BelowCap(float value)
    {
        if (-1 == Cap) return true;
        return value < Cap;
    }

    public bool Matches(IEffect effect)
    {
        if (effect.EffectType != EffectType)
            return false;
        if (effect.EffectType == Enums.eEffectType.Enhancement && EffectType == Enums.eEffectType.Enhancement && Modifies == Enums.eEffectType.Mez && MezType == Enums.eMez.None)
        {
            return effect.MezType == Enums.eMez.Afraid
                || effect.MezType == Enums.eMez.Confused
                || effect.MezType == Enums.eMez.Held
                || effect.MezType == Enums.eMez.Immobilized
                || effect.MezType == Enums.eMez.Sleep
                || effect.MezType == Enums.eMez.Stunned
                || effect.MezType == Enums.eMez.Terrorized;
        }
        if (effect.EffectType == Enums.eEffectType.MezResist && EffectType == Enums.eEffectType.MezResist && Modifies == Enums.eEffectType.Mez && MezType == Enums.eMez.None)
        {
            return effect.MezType == Enums.eMez.Afraid
                || effect.MezType == Enums.eMez.Confused
                || effect.MezType == Enums.eMez.Held
                || effect.MezType == Enums.eMez.Immobilized
                || effect.MezType == Enums.eMez.Sleep
                || effect.MezType == Enums.eMez.Stunned
                || effect.MezType == Enums.eMez.Terrorized;
        }
        if (effect.EffectType == Enums.eEffectType.Enhancement)
            return effect.ETModifies == Modifies && effect.MezType == MezType;
        if (Enums.eDamage.None == DamageType && Enums.eMez.None == MezType && Enums.eBuffMode.Normal == BuffMode)
            return true;
        if (Enums.eDamage.None == DamageType && Enums.eMez.None == MezType && (int)effect.buffMode == (int)BuffMode)
            return true;
        else if (Enums.eDamage.None != DamageType && effect.DamageType == DamageType)
            return true;
        else if (Enums.eMez.None != MezType && effect.MezType == MezType)
            return true;
        return false;
    }

    public EnhancementType[] GetRelatedEnhancementTypes()
    {
        List<EnhancementType> enhancementTypes = new List<EnhancementType>();

        if (0 == this.DisplayName.CompareTo(Accuracy.DisplayName)) enhancementTypes.Add(EnhancementType.Accuracy);
        else if (0 == this.DisplayName.CompareTo(Confuse.DisplayName)) enhancementTypes.Add(EnhancementType.Confuse);
        else if (0 == this.DisplayName.CompareTo(DamageBuff.DisplayName)) enhancementTypes.Add(EnhancementType.Damage);
        else if (0 == this.DisplayName.CompareTo(DefenseAll.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseAoE.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseCold.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseEnergy.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseFire.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseMelee.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseNegative.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefensePsionic.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseRanged.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName)) enhancementTypes.Add(EnhancementType.Defense);
        else if (0 == this.DisplayName.CompareTo(EnduranceDiscount.DisplayName)) enhancementTypes.Add(EnhancementType.EnduranceReduction);
        else if (0 == this.DisplayName.CompareTo(Fly.DisplayName)) enhancementTypes.Add(EnhancementType.Fly);
        else if (0 == this.DisplayName.CompareTo(Heal.DisplayName)) enhancementTypes.Add(EnhancementType.Heal);
        else if (0 == this.DisplayName.CompareTo(Hold.DisplayName)) enhancementTypes.Add(EnhancementType.Hold);
        else if (0 == this.DisplayName.CompareTo(Immobilize.DisplayName)) enhancementTypes.Add(EnhancementType.Immobilize);
        else if (0 == this.DisplayName.CompareTo(Jump.DisplayName)) enhancementTypes.Add(EnhancementType.Jump);
        else if (0 == this.DisplayName.CompareTo(MezEnhance.DisplayName))
        {
            enhancementTypes.Add(EnhancementType.Confuse);
            enhancementTypes.Add(EnhancementType.Hold);
            enhancementTypes.Add(EnhancementType.Immobilize);
            enhancementTypes.Add(EnhancementType.Sleep);
            enhancementTypes.Add(EnhancementType.Stun);
            enhancementTypes.Add(EnhancementType.Fear);
        }
        else if (0 == this.DisplayName.CompareTo(Range.DisplayName)) enhancementTypes.Add(EnhancementType.Range);
        else if (0 == this.DisplayName.CompareTo(RechargeTime.DisplayName)) enhancementTypes.Add(EnhancementType.Recharge);
        else if (0 == this.DisplayName.CompareTo(Recovery.DisplayName)) enhancementTypes.Add(EnhancementType.EnduranceModification);
        else if (0 == this.DisplayName.CompareTo(Regeneration.DisplayName)) enhancementTypes.Add(EnhancementType.Heal);
        else if (0 == this.DisplayName.CompareTo(ResistanceCold.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceEnergy.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceFire.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceLethal.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceNegative.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceSmashing.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(ResistanceToxic.DisplayName)) enhancementTypes.Add(EnhancementType.Resistance);
        else if (0 == this.DisplayName.CompareTo(Running.DisplayName)) enhancementTypes.Add(EnhancementType.Run);
        else if (0 == this.DisplayName.CompareTo(Sleep.DisplayName)) enhancementTypes.Add(EnhancementType.Sleep);
        else if (0 == this.DisplayName.CompareTo(Stun.DisplayName)) enhancementTypes.Add(EnhancementType.Stun);
        else if (0 == this.DisplayName.CompareTo(Terrorize.DisplayName)) enhancementTypes.Add(EnhancementType.Fear);
        return enhancementTypes.ToArray();
    }

    public Enums.sTwinID[] GetRelatedEffectAndSubEffects()
    {
        List<Enums.sTwinID> effectAndSubEffects = new List<Enums.sTwinID>();
        Func<int, int, Enums.sTwinID> Build = (effectID, subEffectID) =>
        {
            Enums.sTwinID effectAndSubEffect = new Enums.sTwinID();
            effectAndSubEffect.ID = (int)effectID;
            effectAndSubEffect.SubID = subEffectID;
            return effectAndSubEffect;
        };

        if (0 == this.DisplayName.CompareTo(Accuracy.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Accuracy, -1));
        else if (0 == this.DisplayName.CompareTo(Confuse.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Confused));
        else if (0 == this.DisplayName.CompareTo(DamageBuff.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Damage, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseAll.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseAoE.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseCold.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseEnergy.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseFire.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseLethal.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseMelee.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseNegative.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefensePsionic.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseRanged.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Defense, -1));
        else if (0 == this.DisplayName.CompareTo(EnduranceDiscount.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.EnduranceDiscount, -1));
        else if (0 == this.DisplayName.CompareTo(Fly.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.SpeedFlying, -1));
        else if (0 == this.DisplayName.CompareTo(Heal.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Heal, -1));
        else if (0 == this.DisplayName.CompareTo(Hold.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Held));
        else if (0 == this.DisplayName.CompareTo(Immobilize.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Immobilized));
        else if (0 == this.DisplayName.CompareTo(Jump.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.SpeedJumping, -1));
        else if (0 == this.DisplayName.CompareTo(MezEnhance.DisplayName))
        {
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Confused));
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Held));
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Immobilized));
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Sleep));
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Stunned));
            effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Terrorized));
        }
        else if (0 == this.DisplayName.CompareTo(Range.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Range, -1));
        else if (0 == this.DisplayName.CompareTo(RechargeTime.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.RechargeTime, -1));
        else if (0 == this.DisplayName.CompareTo(Recovery.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Endurance, -1));
        else if (0 == this.DisplayName.CompareTo(Regeneration.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Heal, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceCold.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceEnergy.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceFire.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceLethal.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceNegative.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceSmashing.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(ResistanceToxic.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.Resistance, -1));
        else if (0 == this.DisplayName.CompareTo(Running.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEnhance.SpeedRunning, -1));
        else if (0 == this.DisplayName.CompareTo(Sleep.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Sleep));
        else if (0 == this.DisplayName.CompareTo(Stun.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Stunned));
        else if (0 == this.DisplayName.CompareTo(Terrorize.DisplayName)) effectAndSubEffects.Add(Build((int)Enums.eEffectType.Mez, (int)Enums.eMez.Terrorized));
        return effectAndSubEffects.ToArray();
    }

    public int[] GetRelatedSpecialIOIndexes()
    {
        List<int> specialIOIndexes = new List<int>();

        if (0 == this.DisplayName.CompareTo(Accuracy.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.AnalyzeWeaknessChanceForToHit);
            specialIOIndexes.Add(SpecialEnhancements.KismetPlusAccuracy);
            specialIOIndexes.Add(SpecialEnhancements.SiphonInsightChanceForToHit);
        }
        else if (0 == this.DisplayName.CompareTo(Confuse.DisplayName)) specialIOIndexes.Add(SpecialEnhancements.CoercivePersuasionContagiousConfusion);
        else if (0 == this.DisplayName.CompareTo(DamageBuff.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.AnnihilationChanceForResDebuff);
            specialIOIndexes.Add(SpecialEnhancements.ApocalypseChanceOfDamageNegative);
            specialIOIndexes.Add(SpecialEnhancements.ArmageddonChanceForFireDamage);
            specialIOIndexes.Add(SpecialEnhancements.AscendencyOfTheDominatorRechargeChanceForDamage);
            specialIOIndexes.Add(SpecialEnhancements.AssassinsMarkRechargeTimeRchgBuildUp);
            specialIOIndexes.Add(SpecialEnhancements.BlastersWrathRechargeChanceForFireDamage);
            specialIOIndexes.Add(SpecialEnhancements.BrutesFuryRechargeFuryBonus);
            specialIOIndexes.Add(SpecialEnhancements.CloudSensesChanceForNegativeEnergyDamage);
            specialIOIndexes.Add(SpecialEnhancements.CriticalStrikesRechargeTime50CritProc);
            specialIOIndexes.Add(SpecialEnhancements.DecimationChanceOfBuildUp);
            specialIOIndexes.Add(SpecialEnhancements.DominatingGraspRechargeTimeFieryOrb);
            specialIOIndexes.Add(SpecialEnhancements.EradicationChanceForEnergyDamage);
            specialIOIndexes.Add(SpecialEnhancements.FuryOfTheGladiatorChanceForResDebuff);
            specialIOIndexes.Add(SpecialEnhancements.GaussiansSynchronizedFireControlChanceForBuildUp);
            specialIOIndexes.Add(SpecialEnhancements.GhostWidowsEmbraceChanceOfDamagePsionic);
            specialIOIndexes.Add(SpecialEnhancements.GladiatorsNetChanceOfDamageLethal);
            specialIOIndexes.Add(SpecialEnhancements.GladiatorsJavelinChanceOfDamageToxic);
            specialIOIndexes.Add(SpecialEnhancements.GladiatorsStrikeChanceForSmashingDamage);
            specialIOIndexes.Add(SpecialEnhancements.GlimpseOfTheAbyssChanceOfDamagePsionic);
            specialIOIndexes.Add(SpecialEnhancements.HecatombChanceOfDamageNegative);
            specialIOIndexes.Add(SpecialEnhancements.ImpededSwiftnessChanceOfDamageSmashing);
            specialIOIndexes.Add(SpecialEnhancements.JavelinVolleyChanceOfDamageLethal);
            specialIOIndexes.Add(SpecialEnhancements.KheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.MakosBiteChanceOfDamageLethal);
            specialIOIndexes.Add(SpecialEnhancements.MalaisesIllusionsChanceOfDamagePsionic);
            specialIOIndexes.Add(SpecialEnhancements.MaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage);
            specialIOIndexes.Add(SpecialEnhancements.NeuronicShutdownChanceOfDamagePsionic);
            specialIOIndexes.Add(SpecialEnhancements.ObliterationChanceForSmashingDamage);
            specialIOIndexes.Add(SpecialEnhancements.OpportunityStrikesRechargeTimeChanceForOpportunity);
            specialIOIndexes.Add(SpecialEnhancements.OverpoweringPresenceRechargeTimeEnergyFont);
            specialIOIndexes.Add(SpecialEnhancements.PerfectZingerChanceForPsiDamage);
            specialIOIndexes.Add(SpecialEnhancements.PositronsBlastChanceOfDamageEnergy);
            specialIOIndexes.Add(SpecialEnhancements.SciroccosDervishChanceOfDamageLethal);
            specialIOIndexes.Add(SpecialEnhancements.ScrappersStrikeRechargeCriticalHitBonus);
            specialIOIndexes.Add(SpecialEnhancements.ShieldBreakerChanceForLethalDamage);
            specialIOIndexes.Add(SpecialEnhancements.SoulboundAllegianceChanceForBuildUp);
            specialIOIndexes.Add(SpecialEnhancements.SpidersBiteRechargeTimeGlobalToxic);
            specialIOIndexes.Add(SpecialEnhancements.StalkersGuileRechargeChanceToHide);
            specialIOIndexes.Add(SpecialEnhancements.StingOfTheManticoreChanceOfDamageToxic);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorAscendencyOfTheDominatorRechargeChanceForDamage);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorAssassinsMarkRechargeTimeRchgBuildUp);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorBlastersWrathRechargeChanceForFireDamage);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorBrutesFuryRechargeFuryBonus);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorCriticalStrikesRechargeTime50CritProc);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorDominatingGraspRechargeTimeFieryOrb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorKheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorMaliceOfTheCorruptorRechargeChanceForNegativeEnergyDamage);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorOpportunityStrikesRechargeTimeChanceForOpportunity);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorOverpoweringPresenceRechargeTimeEnergyFont);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorScourgingBlastRechargeTimePBAoEEnd);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorScrappersStrikeRechargeCriticalHitBonus);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorSpidersBiteRechargeTimeGlobalToxic);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorStalkersGuileRechargeChanceToHide);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorWillOfTheControllerRechargeChanceForPsionicDamage);
            specialIOIndexes.Add(SpecialEnhancements.TouchOfDeathChanceOfDamageNegative);
            specialIOIndexes.Add(SpecialEnhancements.TouchOfLadyGreyChanceForNegativeDamage);
            specialIOIndexes.Add(SpecialEnhancements.TouchOfTheNictusChanceForNegativeEnergyDamage);
            specialIOIndexes.Add(SpecialEnhancements.TrapOfTheHunterChanceOfDamageLethal);
            specialIOIndexes.Add(SpecialEnhancements.UnbreakableConstraintChanceForSmashingDamage);
            specialIOIndexes.Add(SpecialEnhancements.WillOfTheControllerRechargeChanceForPsionicDamage);
        }
        else if (0 == this.DisplayName.CompareTo(DefenseAll.DisplayName)
        || (0 == this.DisplayName.CompareTo(DefenseAoE.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseCold.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseEnergy.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseFire.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseLethal.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseMelee.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseNegative.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefensePsionic.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseRanged.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName))
        || (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName)))
        {
            specialIOIndexes.Add(SpecialEnhancements.GladiatorsArmorTPProtection3DefAll);
            specialIOIndexes.Add(SpecialEnhancements.SteadfastProtectionPlusDefense);
        }
        else if (0 == this.DisplayName.CompareTo(Heal.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.CallOfTheSandmanChanceOfHealSelf);
            specialIOIndexes.Add(SpecialEnhancements.DefendersBastionRechargeChanceForMinorPBAoEHeal);
            specialIOIndexes.Add(SpecialEnhancements.EntombRechargeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.EntropicChaosChanceOfHealSelf);
            specialIOIndexes.Add(SpecialEnhancements.EssenceTransferRechargeTimeGlobalHeal);
            specialIOIndexes.Add(SpecialEnhancements.GauntletedFistRechargeTimeAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.PanaceaHitPointsEndurance);
            specialIOIndexes.Add(SpecialEnhancements.PreventiveMedicineChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SentinelsWardRechargeTimeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorDefendersBastionRechargeChanceForMinorPBAoEHeal);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorEntombRechargeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorEssenceTransferRechargeTimeGlobalHeal);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorGauntletedFistRechargeTimeAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorSentinelsWardRechargeTimeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorVigilantAssaultRechargeTimePBAoEAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.VigilantAssaultRechargeTimePBAoEAbsorb);
        }
        else if (0 == this.DisplayName.CompareTo(HitPoints.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.EntombRechargeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.PreventiveMedicineChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.KheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.GauntletedFistRechargeTimeAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SentinelsWardRechargeTimeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorEntombRechargeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorGauntletedFistRechargeTimeAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorKheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorSentinelsWardRechargeTimeChanceForAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorVigilantAssaultRechargeTimePBAoEAbsorb);
            specialIOIndexes.Add(SpecialEnhancements.UnbreakableGuardMaxHP);
            specialIOIndexes.Add(SpecialEnhancements.VigilantAssaultRechargeTimePBAoEAbsorb);
        }
        else if (0 == this.DisplayName.CompareTo(Hold.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.BlisteringColdChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.DevastationChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.GravitationalAnchorChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.LockdownChanceForPlus2MagHold);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorBlisteringColdChanceForHold);
        }
        else if (0 == this.DisplayName.CompareTo(Immobilize.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.FrozenBlastRechargeChanceForImmobilize);
            specialIOIndexes.Add(SpecialEnhancements.RazzleDazzleChanceOfImmobilize);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorFrozenBlastRechargeChanceForImmobilize);
        }
        else if (0 == this.DisplayName.CompareTo(KnockbackProtection.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.BlessingOfTheZephyrKnockbackReduction);
            specialIOIndexes.Add(SpecialEnhancements.KarmaKnockbackProtection);
            specialIOIndexes.Add(SpecialEnhancements.SteadfastProtectionKnockbackProtection);
        }
        else if (0 == this.DisplayName.CompareTo(Knockdown.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.AvalancheRechargeChanceForKnockdown);
            specialIOIndexes.Add(SpecialEnhancements.KineticCombatKnockdownBonus);
            specialIOIndexes.Add(SpecialEnhancements.OverwhelmingForceDamageChanceForKnockdownKnockbackToKnockdown);
            specialIOIndexes.Add(SpecialEnhancements.RagnarokChanceForKnockdown);
            specialIOIndexes.Add(SpecialEnhancements.StupefyChanceOfKnockback);
            specialIOIndexes.Add(SpecialEnhancements.SuddenAccelerationKnockbackToKnockdown);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorAvalancheRechargeChanceForKnockdown);
        }
        else if (0 == this.DisplayName.CompareTo(MezEnhance.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.BlisteringColdChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.DevastationChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.DominionOfArachnosRechargeChanceForMinusDmgAndTerrorize);
            specialIOIndexes.Add(SpecialEnhancements.FrozenBlastRechargeChanceForImmobilize);
            specialIOIndexes.Add(SpecialEnhancements.GravitationalAnchorChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.LockdownChanceForPlus2MagHold);
            specialIOIndexes.Add(SpecialEnhancements.OverpoweringPresenceRechargeTimeEnergyFont);
            specialIOIndexes.Add(SpecialEnhancements.PoundingSlugfestDisorientBonus);
            specialIOIndexes.Add(SpecialEnhancements.RazzleDazzleChanceOfImmobilize);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorBlisteringColdChanceForHold);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorDominionOfArachnosRechargeChanceForMinusDmgAndTerrorize);
            specialIOIndexes.Add(SpecialEnhancements.UnspeakableTerrorDisorientBonus);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorFrozenBlastRechargeChanceForImmobilize);
        }
        else if (0 == this.DisplayName.CompareTo(MezResistance.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.AegisPsionicStatusResistance);
            specialIOIndexes.Add(SpecialEnhancements.DefiantBarrageRechargeTimeStatus);
            specialIOIndexes.Add(SpecialEnhancements.ExecutionersContractDisorientBonus);
            specialIOIndexes.Add(SpecialEnhancements.ImperviousSkinStatusResistance);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorDefiantBarrageRechargeTimeStatus);
        }
        else if (0 == this.DisplayName.CompareTo(PetDefense.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.CallToArmsDefenseBonusAuraForPets);
            specialIOIndexes.Add(SpecialEnhancements.CommandOfTheMastermindRechargePetAoEDefenseAura);
            specialIOIndexes.Add(SpecialEnhancements.EdictOfTheMasterDefenseBonus);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorCommandOfTheMastermindRechargePetAoEDefenseAura);
        }
        else if (0 == this.DisplayName.CompareTo(PetResistance.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.ExpedientReinforcementResistBonusAuraForPets);
            specialIOIndexes.Add(SpecialEnhancements.MarkOfSupremacyEndurancePetResistRegen);
            specialIOIndexes.Add(SpecialEnhancements.SovereignRightResistBonus);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorMarkOfSupremacyEndurancePetResistRegen);
        }
        else if (0 == this.DisplayName.CompareTo(RechargeTime.DisplayName))
        {

            specialIOIndexes.Add(SpecialEnhancements.ForceFeedbackChanceForRecharge);
            specialIOIndexes.Add(SpecialEnhancements.LuckOfTheGamblerGlobalRecharge);
        }
        else if (0 == this.DisplayName.CompareTo(Recovery.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.MiracleRecovery);
            specialIOIndexes.Add(SpecialEnhancements.NuminasConvalesenceRegenerationRecovery);
            specialIOIndexes.Add(SpecialEnhancements.PanaceaHitPointsEndurance);
            specialIOIndexes.Add(SpecialEnhancements.PerformanceShifterChanceForPlusEndurance);
            specialIOIndexes.Add(SpecialEnhancements.ScourgingBlastRechargeTimePBAoEEnd);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorScourgingBlastRechargeTimePBAoEEnd);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorUnrelentingFuryRechargeTimeRegenEnd);
            specialIOIndexes.Add(SpecialEnhancements.TheftOfEssenceChanceForEndurance);
            specialIOIndexes.Add(SpecialEnhancements.UnrelentingFuryRechargeTimeRegenEnd);
        }
        else if (0 == this.DisplayName.CompareTo(ReduceDamage.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.AbsoluteAmazementChanceForToHitDebuff);
            specialIOIndexes.Add(SpecialEnhancements.DominionOfArachnosRechargeChanceForMinusDmgAndTerrorize);
            specialIOIndexes.Add(SpecialEnhancements.FortunataHypnosisChanceForPlacate);
            specialIOIndexes.Add(SpecialEnhancements.TempestChanceOfEndDrain);
        }
        else if (0 == this.DisplayName.CompareTo(Regeneration.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.NuminasConvalesenceRegenerationRecovery);
            specialIOIndexes.Add(SpecialEnhancements.PanaceaHitPointsEndurance);
            specialIOIndexes.Add(SpecialEnhancements.RegenerativeTissueRegeneration);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorUnrelentingFuryRechargeTimeRegenEnd);
            specialIOIndexes.Add(SpecialEnhancements.UnrelentingFuryRechargeTimeRegenEnd);
        }
        else if (0 == this.DisplayName.CompareTo(ResistanceCold.DisplayName)
        || (0 == this.DisplayName.CompareTo(ResistanceEnergy.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistanceFire.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistanceLethal.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistanceNegative.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistanceSmashing.DisplayName))
        || (0 == this.DisplayName.CompareTo(ResistanceToxic.DisplayName)))
        {
            specialIOIndexes.Add(SpecialEnhancements.KheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.MightOfTheTankerRechargeChanceForResAll);
            specialIOIndexes.Add(SpecialEnhancements.ReactiveDefensesScalingResistDamage);
            specialIOIndexes.Add(SpecialEnhancements.ShieldWallResTeleportation5ResAll);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorKheldiansGraceRechargeFormEmpowerment);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorMightOfTheTankerRechargeChanceForResAll);
            if (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName))
            {
                specialIOIndexes.Add(SpecialEnhancements.AegisPsionicStatusResistance);
                specialIOIndexes.Add(SpecialEnhancements.ImperviumArmorResPsi);
            }
        }
        else if (0 == this.DisplayName.CompareTo(ResistanceSlowMovement.DisplayName)) { }
        else if (0 == this.DisplayName.CompareTo(ResistanceSlowRecharge.DisplayName)) { }
        else if (0 == this.DisplayName.CompareTo(Running.DisplayName)) specialIOIndexes.Add(SpecialEnhancements.GiftOfTheAncientsRunSpeed);
        else if (0 == this.DisplayName.CompareTo(Slow.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.BasilisksGazeChanceForRechargeSlow);
            specialIOIndexes.Add(SpecialEnhancements.DarkWatchersDespairChanceForRechargeSlow);
            specialIOIndexes.Add(SpecialEnhancements.InducedComaChanceOfMinusRecharge);
            specialIOIndexes.Add(SpecialEnhancements.PacingOfTheTurtleChanceOfMinusRecharge);
            specialIOIndexes.Add(SpecialEnhancements.SuperiorWintersBiteRechargeChanceForMinusSpeedAndMinusRecharge);
            specialIOIndexes.Add(SpecialEnhancements.WintersBiteRechargeChanceForMinusSpeedAndMinusRecharge);
        }
        else if (0 == this.DisplayName.CompareTo(Stun.DisplayName))
        {
            specialIOIndexes.Add(SpecialEnhancements.ExecutionersContractDisorientBonus);
            specialIOIndexes.Add(SpecialEnhancements.PoundingSlugfestDisorientBonus);
            specialIOIndexes.Add(SpecialEnhancements.UnspeakableTerrorDisorientBonus);
        }
        return specialIOIndexes.ToArray();
    }

    public bool CanEnhance(PowerEntry powerEntry)
    {
        int[] specialIOInexes = GetRelatedSpecialIOIndexes();
        foreach (int specialIOIndex in specialIOInexes)
        {
            IEnhancement specialIO = DatabaseAPI.GetEnhancementByIndex(specialIOIndex);
            if (powerEntry.CanAddEnhancement(specialIO))
                return true;
        }
        HashSet<EnhancementSet> enhancementSets = powerEntry.Power.GetEnhancementSetsWithBonusEffect(powerEntry.SlotCount, this);
        return 0 != enhancementSets.Count;
    }

    public float SumPowerEffects(IPower power, Archetype archetype)
    {
        Enums.ePvX pvx = IncludePvP ? Enums.ePvX.PvP : Enums.ePvX.PvE;
        if (0 == this.DisplayName.CompareTo(Accuracy.DisplayName)) return AIMath.PowerMath.CalcAccuracy(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Confuse.DisplayName)) return AIMath.PowerMath.CalcConfuse(power, pvx);
        else if (0 == this.DisplayName.CompareTo(DamageBuff.DisplayName)) return AIMath.PowerMath.CalcDamageBuff(power, pvx, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseAll.DisplayName)) return AIMath.PowerMath.CalcAllDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseAoE.DisplayName)) return AIMath.PowerMath.CalcAoEDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseCold.DisplayName)) return AIMath.PowerMath.CalcColdDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseEnergy.DisplayName)) return AIMath.PowerMath.CalcEnergyDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseFire.DisplayName)) return AIMath.PowerMath.CalcFireDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseLethal.DisplayName)) return AIMath.PowerMath.CalcLethalDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseMelee.DisplayName)) return AIMath.PowerMath.CalcMeleeDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseNegative.DisplayName)) return AIMath.PowerMath.CalcNegativeEnergyDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefensePsionic.DisplayName)) return AIMath.PowerMath.CalcPsionicDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseRanged.DisplayName)) return AIMath.PowerMath.CalcRangedDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName)) return AIMath.PowerMath.CalcSmashingDefense(power, archetype);
        else if (0 == this.DisplayName.CompareTo(EnduranceDiscount.DisplayName)) return AIMath.PowerMath.CalcEnduranceDiscount(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Fly.DisplayName)) return AIMath.PowerMath.CalcFly(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Heal.DisplayName)) return AIMath.PowerMath.CalcHeal(power, archetype);
        else if (0 == this.DisplayName.CompareTo(HitPoints.DisplayName)) return AIMath.PowerMath.CalcHitPoints(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Hold.DisplayName)) return AIMath.PowerMath.CalcHold(power, pvx);
        else if (0 == this.DisplayName.CompareTo(Immobilize.DisplayName)) return AIMath.PowerMath.CalcImmobilize(power, pvx);
        else if (0 == this.DisplayName.CompareTo(Jump.DisplayName)) return AIMath.PowerMath.CalcJump(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Knockdown.DisplayName)) return AIMath.PowerMath.CalcKnockDown(power, archetype);
        else if (0 == this.DisplayName.CompareTo(KnockbackProtection.DisplayName))
        {
            List<float> totals = new List<float>();
            totals.Add(AIMath.PowerMath.CalcKnockBackResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcKnockDownResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcKnockUpResistance(power, archetype));
            return totals.Max();
        }
        else if (0 == this.DisplayName.CompareTo(MaxEndurance.DisplayName)) return AIMath.PowerMath.CalcEnduranceIncrease(power, archetype);
        else if (0 == this.DisplayName.CompareTo(MezEnhance.DisplayName))
        {
            List<float> totals = new List<float>();
            totals.Add(AIMath.PowerMath.CalcConfuse(power, pvx));
            totals.Add(AIMath.PowerMath.CalcHold(power, pvx));
            totals.Add(AIMath.PowerMath.CalcImmobilize(power, pvx));
            totals.Add(AIMath.PowerMath.CalcSleep(power, pvx));
            totals.Add(AIMath.PowerMath.CalcStun(power, pvx));
            totals.Add(AIMath.PowerMath.CalcTerrorize(power, pvx));
            return totals.Max();
        }
        else if (0 == this.DisplayName.CompareTo(MezResistance.DisplayName))
        {
            List<float> totals = new List<float>();
            totals.Add(AIMath.PowerMath.CalcConfuseResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcHoldResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcImmobilizeResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcSleepResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcStunResistance(power, archetype));
            totals.Add(AIMath.PowerMath.CalcTerrorizeResistance(power, archetype));
            return totals.Max();
        }
        else if (0 == this.DisplayName.CompareTo(RechargeTime.DisplayName)) return AIMath.PowerMath.CalcRechargeReduction(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Recovery.DisplayName)) return AIMath.PowerMath.CalcRecovery(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Regeneration.DisplayName)) return AIMath.PowerMath.CalcRegeneration(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceCold.DisplayName)) return AIMath.PowerMath.CalcColdResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceEnergy.DisplayName)) return AIMath.PowerMath.CalcEnergyResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceFire.DisplayName)) return AIMath.PowerMath.CalcFireResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceLethal.DisplayName)) return AIMath.PowerMath.CalcLethalResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceNegative.DisplayName)) return AIMath.PowerMath.CalcNegativeEnergyResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName)) return AIMath.PowerMath.CalcPsionicResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceSmashing.DisplayName)) return AIMath.PowerMath.CalcSmashingResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceToxic.DisplayName)) return AIMath.PowerMath.CalcToxicResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceSlowMovement.DisplayName)) return AIMath.PowerMath.CalcMovementSlowResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(ResistanceSlowRecharge.DisplayName)) return AIMath.PowerMath.CalcRechargeSlowResistance(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Running.DisplayName)) return AIMath.PowerMath.CalcRun(power, archetype);
        else if (0 == this.DisplayName.CompareTo(Sleep.DisplayName)) return AIMath.PowerMath.CalcSleep(power, pvx);
        else if (0 == this.DisplayName.CompareTo(Slow.DisplayName))
        {
            List<float> totals = new List<float>();
            totals.Add(AIMath.PowerMath.CalcMovementSlow(power, archetype));
            totals.Add(AIMath.PowerMath.CalcRechargeSlow(power, archetype));
            return totals.Min();
        }
        else if (0 == this.DisplayName.CompareTo(Stun.DisplayName)) return AIMath.PowerMath.CalcStun(power, pvx);
        else if (0 == this.DisplayName.CompareTo(Terrorize.DisplayName)) return AIMath.PowerMath.CalcTerrorize(power, pvx);
        return 0.0f;
    }

    public float SumEnhancementEffects(I9Slot slot)
    {
        List<EnhancementType> enhancementTypes = new List<EnhancementType>();
        if (0 == this.DisplayName.CompareTo(Accuracy.DisplayName)) return AIMath.I9SlotMath.CalcAccuracy(slot);
        else if (0 == this.DisplayName.CompareTo(Confuse.DisplayName)) return AIMath.I9SlotMath.CalcConfuse(slot);
        else if (0 == this.DisplayName.CompareTo(DamageBuff.DisplayName)) return AIMath.I9SlotMath.CalcDamage(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseAll.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseAoE.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseCold.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseEnergy.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseFire.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseLethal.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseMelee.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseNegative.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefensePsionic.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseRanged.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(DefenseSmashing.DisplayName)) return AIMath.I9SlotMath.CalcDefense(slot);
        else if (0 == this.DisplayName.CompareTo(EnduranceDiscount.DisplayName)) return AIMath.I9SlotMath.CalcEnduranceDiscount(slot);
        else if (0 == this.DisplayName.CompareTo(Fly.DisplayName)) return AIMath.I9SlotMath.CalcFly(slot);
        else if (0 == this.DisplayName.CompareTo(Heal.DisplayName)) return AIMath.I9SlotMath.CalcHeal(slot);
        else if (0 == this.DisplayName.CompareTo(Hold.DisplayName)) return AIMath.I9SlotMath.CalcHold(slot);
        else if (0 == this.DisplayName.CompareTo(Immobilize.DisplayName)) return AIMath.I9SlotMath.CalcImmobilize(slot);
        else if (0 == this.DisplayName.CompareTo(Jump.DisplayName)) return AIMath.I9SlotMath.CalcJump(slot);
        else if (0 == this.DisplayName.CompareTo(MezEnhance.DisplayName))
        {
            List<float> totals = new List<float>();
            totals.Add(AIMath.I9SlotMath.CalcConfuse(slot));
            totals.Add(AIMath.I9SlotMath.CalcHold(slot));
            totals.Add(AIMath.I9SlotMath.CalcImmobilize(slot));
            totals.Add(AIMath.I9SlotMath.CalcSleep(slot));
            totals.Add(AIMath.I9SlotMath.CalcStun(slot));
            totals.Add(AIMath.I9SlotMath.CalcTerrorize(slot));
            return totals.Max();
        }
        else if (0 == this.DisplayName.CompareTo(RechargeTime.DisplayName)) return AIMath.I9SlotMath.CalcRechargeReduction(slot);
        else if (0 == this.DisplayName.CompareTo(Recovery.DisplayName)) return AIMath.I9SlotMath.CalcRecovery(slot);
        else if (0 == this.DisplayName.CompareTo(Regeneration.DisplayName)) return AIMath.I9SlotMath.CalcHeal(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceCold.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceEnergy.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceFire.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceLethal.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceNegative.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistancePsionic.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceSmashing.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(ResistanceToxic.DisplayName)) return AIMath.I9SlotMath.CalcResistance(slot);
        else if (0 == this.DisplayName.CompareTo(Running.DisplayName)) return AIMath.I9SlotMath.CalcRun(slot);
        else if (0 == this.DisplayName.CompareTo(Sleep.DisplayName)) return AIMath.I9SlotMath.CalcSleep(slot);
        else if (0 == this.DisplayName.CompareTo(Slow.DisplayName)) return AIMath.I9SlotMath.CalcSlow(slot);
        else if (0 == this.DisplayName.CompareTo(Stun.DisplayName)) return AIMath.I9SlotMath.CalcStun(slot);
        else if (0 == this.DisplayName.CompareTo(Terrorize.DisplayName)) return AIMath.I9SlotMath.CalcTerrorize(slot);
        return 0.0f;
    }

    public float SumEnhancementEffects(PowerEntry powerEntry)
    {
        float total = 0.0f;
        foreach (SlotEntry slot in powerEntry.Slots)
        {
            total += SumEnhancementEffects(slot.Enhancement);
        }
        return total;
    }

    public float SumSpecialEnhancements(PowerEntry powerEntry)
    {
        float total = 0.0f;
        int[] enhacementIndexes = GetRelatedSpecialIOIndexes();
        foreach (int enhancementIndex in enhacementIndexes)
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementIndex);
            if (!powerEntry.HasEnhancement(enhancement)) continue;
            total += SpecialEnhancements.GetSpecialBonus(this, enhancement);
        }
        return total;
    }

    public float SumBonuses(PowerEntry powerEntry)
    {
        float sum = 0.0f;
        Dictionary<string, List<IEnhancement>> enhancementGroups = powerEntry.GetEnhancementsGroupedBySet();

        foreach (KeyValuePair<string, List<IEnhancement>> kvp in enhancementGroups)
        {
            if ("None" == kvp.Key)
                continue;
            int enhancementsetIndex = DatabaseAPI.GetEnhancementSetIndexByName(kvp.Key);
            if (-1 == enhancementsetIndex)
                continue;
            EnhancementSet enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(enhancementsetIndex);
            if (null == enhancementSet)
                continue;
            int enhancementCount = kvp.Value.Count;
            sum += enhancementSet.SumEnhancementSetBonusEffect(enhancementCount, this);
        }
        return sum;
    }

    public float Sum(PowerEntry powerEntry, Archetype archetype)
    {
        float sumPowerEffect = SumPowerEffects(powerEntry.Power, archetype);
        return sumPowerEffect
            + (sumPowerEffect * SumEnhancementEffects(powerEntry))
            + SumSpecialEnhancements(powerEntry)
            + SumBonuses(powerEntry);
    }

    public static void TestNewCode()
    {
        TestMatches();
        //TestSumPowerEffects();
        //TestSumEnhancementEffects();
    }

    public static void TestMatches()
    {
        EnhancementSet luckOfTheGambler = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Luck of the Gambler"));
        List<IPower> luckOfTheGamblerBonuses = luckOfTheGambler.GetBonusPowers(Enums.ePvX.PvE);
        IPower accuracyBonus = luckOfTheGamblerBonuses[2];
        IEffect accuracyEffect = accuracyBonus.Effects[0];
        IPower hitPointsBonus = luckOfTheGamblerBonuses[1];
        IEffect hitPointsEffect = hitPointsBonus.Effects[0];
        Debug.Assert(BonusInfo.Accuracy.Matches(accuracyEffect), "BonusInfo.Accuracy.Matches(accuracyEffect) is incorrect!");
        Debug.Assert(BonusInfo.DefenseAll.Matches(accuracyEffect) == false, "BonusInfo.DefenseAll.Matches(accuracyEffect) is incorrect!");
        Debug.Assert(BonusInfo.HitPoints.Matches(hitPointsEffect), "BonusInfo.HitPoints.Matches(hitPointsEffect) is incorrect!");
        Debug.Assert(BonusInfo.DefenseAll.Matches(hitPointsEffect) == false, "BonusInfo.DefenseAll.Matches(hitPointsEffect) is incorrect!");

        EnhancementSet giftOfTheAncients = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Gift of the Ancients"));
        List<IPower> giftOfTheAncientsBonuses = giftOfTheAncients.GetBonusPowers(Enums.ePvX.PvE);
        IPower maxEndBonus = giftOfTheAncientsBonuses[2];
        IEffect maxEndEffect = maxEndBonus.Effects[0];
        IPower mezResistanceBonus = giftOfTheAncientsBonuses[1];
        IEffect holdResistanceEffect = mezResistanceBonus.Effects[3];
        Debug.Assert(BonusInfo.MaxEndurance.Matches(maxEndEffect), "BonusInfo.MaxEndurance.Matches(maxEndEffect) is incorrect!");
        Debug.Assert(BonusInfo.HoldResistance.Matches(holdResistanceEffect), "BonusInfo.HoldResistance.Matches(holdResistanceEffect) is incorrect!");

        EnhancementSet absoluteAmazement = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Absolute Amazement"));
        List<IPower> absoluteAmazementBonuses = absoluteAmazement.GetBonusPowers(Enums.ePvX.PvE);
        IPower rehargeTimeBonus = absoluteAmazementBonuses[3];
        IEffect rechargeTimeEffect = rehargeTimeBonus.Effects[0];
        Debug.Assert(BonusInfo.RechargeTime.Matches(rechargeTimeEffect), "BonusInfo.RechargeTime.Matches(rechargeTimeEffect) is incorrect!");

        EnhancementSet shieldWall = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Shield Wall"));
        List<IPower> shieldWallBonuses = shieldWall.GetBonusPowers(Enums.ePvX.PvE);
        IPower damageBonus = shieldWallBonuses[6];
        IEffect damageBonusEffect = damageBonus.Effects[0];
        Debug.Assert(BonusInfo.DamageBuff.Matches(damageBonusEffect), "BonusInfo.Damage.Matches(damageBonusEffect) is incorrect!");

        EnhancementSet unbreakableGuard = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Unbreakable Guard"));
        List<IPower> unbreakableGuardBonuses = unbreakableGuard.GetBonusPowers(Enums.ePvX.PvE);
        IPower enduranceDiscountBonus = unbreakableGuardBonuses[0];
        IEffect enduranceDiscountEffect = enduranceDiscountBonus.Effects[0];
        Debug.Assert(BonusInfo.EnduranceDiscount.Matches(enduranceDiscountEffect), "BonusInfo.EnduranceDiscount.Matches(enduranceDiscountEffect) is incorrect!");

        EnhancementSet overpoweringPresence = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Overpowering Presence"));
        List<IPower> overpoweringPresenceBonuses = overpoweringPresence.GetBonusPowers(Enums.ePvX.PvE);
        IPower immobilizeBonus = overpoweringPresenceBonuses[0];
        IPower sleepBonus = overpoweringPresenceBonuses[1];
        IPower terrorizedBonus = overpoweringPresenceBonuses[2];
        IPower confuseBonus = overpoweringPresenceBonuses[3];
        IPower holdBonus = overpoweringPresenceBonuses[4];
        IPower stunBonus = overpoweringPresenceBonuses[5];
        IEffect immobilizeEffect = immobilizeBonus.Effects[0];
        IEffect sleepEffect = sleepBonus.Effects[0];
        IEffect terrorizeEffect = terrorizedBonus.Effects[0];
        IEffect confuseEffect = confuseBonus.Effects[0];
        IEffect holdEffect = holdBonus.Effects[0];
        IEffect stunEffect = stunBonus.Effects[0];
        Debug.Assert(BonusInfo.Immobilize.Matches(immobilizeEffect), "BonusInfo.Immobilize.Matches(immobilizeEffect) is incorrect!");
        Debug.Assert(BonusInfo.Sleep.Matches(sleepEffect), "BonusInfo.Sleep.Matches(sleepEffect) is incorrect!");
        Debug.Assert(BonusInfo.Terrorize.Matches(terrorizeEffect), "BonusInfo.Terrorize.Matches(terrorizeEffect) is incorrect!");
        Debug.Assert(BonusInfo.Confuse.Matches(confuseEffect), "BonusInfo.Confuse.Matches(confuseEffect) is incorrect!");
        Debug.Assert(BonusInfo.Hold.Matches(holdEffect), "BonusInfo.Hold.Matches(holdEffect) is incorrect!");
        Debug.Assert(BonusInfo.Stun.Matches(stunEffect), "BonusInfo.Stun.Matches(stunEffect) is incorrect!");

        EnhancementSet coercivePersuasion = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Coercive Persuasion "));
        List<IPower> coercivePersuasionPresenceBonuses = coercivePersuasion.GetBonusPowers(Enums.ePvX.PvE);
        confuseBonus = coercivePersuasionPresenceBonuses[1];
        confuseEffect = confuseBonus.Effects[0];
        Debug.Assert(BonusInfo.Confuse.Matches(confuseEffect), "BonusInfo.Confuse.Matches(confuseEffect) is incorrect!");

        EnhancementSet performanceShifter = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Performance Shifter"));
        List<IPower> performanceShifterBonuses = performanceShifter.GetBonusPowers(Enums.ePvX.PvE);
        IPower movementBonus = performanceShifterBonuses[0];
        IEffect speedJumpingEffect = movementBonus.Effects[0];
        IEffect JumpHeightEffect = movementBonus.Effects[1];
        IEffect speedFlyingEffect = movementBonus.Effects[2];
        IEffect speedRunningEffect = movementBonus.Effects[3];
        Debug.Assert(BonusInfo.Jump.Matches(speedJumpingEffect), "BonusInfo.Jump.Matches(speedJumpingEffect) is incorrect!");
        Debug.Assert(BonusInfo.JumpHeight.Matches(JumpHeightEffect), "BonusInfo.JumpHeight.Matches(JumpHeightEffect) is incorrect!");
        Debug.Assert(BonusInfo.Fly.Matches(speedFlyingEffect), "BonusInfo.Fly.Matches(speedFlyingEffect) is incorrect!");
        Debug.Assert(BonusInfo.SpeedRunning.Matches(speedRunningEffect), "BonusInfo.SpeedRunning.Matches(speedRunningEffect) is incorrect!");

        EnhancementSet overwhelmingForce = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Overwhelming Force"));
        List<IPower> overwhelmingForceBonuses = overwhelmingForce.GetBonusPowers(Enums.ePvX.PvE);
        IPower knockbackProtectionBonus = overwhelmingForceBonuses[4];
        IEffect knockbackProtectionEffect = knockbackProtectionBonus.Effects[0];
        IEffect knockupProtectionEffect = knockbackProtectionBonus.Effects[1];
        Debug.Assert(BonusInfo.KnockbackProtection.Matches(knockbackProtectionEffect), "BonusInfo.KnockbackProtection.Matches(knockbackProtectionEffect) is incorrect!");
        Debug.Assert(BonusInfo.KnockupProtection.Matches(knockupProtectionEffect), "BonusInfo.KnockupProtection.Matches(knockupProtectionEffect) is incorrect!");

        EnhancementSet wintersBite = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Winter's Bite"));
        List<IPower> wintersBiteBonuses = wintersBite.GetBonusPowers(Enums.ePvX.PvE);
        IPower slowResistanceBonus = wintersBiteBonuses[1];
        IEffect slowRunResistanceEffect = slowResistanceBonus.Effects[0];
        IEffect slowFlyResistanceEffect = slowResistanceBonus.Effects[2];
        IEffect slowRechargeResistanceEffect = slowResistanceBonus.Effects[1];
        Debug.Assert(BonusInfo.ResistanceSlowRecharge.Matches(slowRechargeResistanceEffect), "BonusInfo.ResistanceSlowRecharge.Matches(slowRechargeResistanceEffect) is incorrect!");
        Debug.Assert(BonusInfo.ResistanceSlowSpeedRunning.Matches(slowRunResistanceEffect), "BonusInfo.ResistanceSlowSpeedRunning.Matches(slowRunResistanceEffect) is incorrect!");
        Debug.Assert(BonusInfo.ResistanceSlowSpeedFlying.Matches(slowFlyResistanceEffect), "BonusInfo.ResistanceSlowSpeedFlying.Matches(slowFlyResistanceEffect) is incorrect!");

        EnhancementSet pacingOfTheTurtle = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Pacing of the Turtle"));
        List<IPower> pacingOfTheTurtleBonuses = pacingOfTheTurtle.GetBonusPowers(Enums.ePvX.PvE);
        IPower slowBonus = pacingOfTheTurtleBonuses[1];
        IEffect slowSpeedJumpingEffect = slowBonus.Effects[0];
        IEffect slowHeightJumpingEffect = slowBonus.Effects[1];
        IEffect slowSpeedFlyingEffect = slowBonus.Effects[2];
        IEffect slowSpeedRunningEffect = slowBonus.Effects[3];
        Debug.Assert(BonusInfo.SlowSpeedJumping.Matches(slowSpeedJumpingEffect), "BonusInfo.SlowSpeedJumping.Matches(slowSpeedJumpingEffect) is incorrect!");
        Debug.Assert(BonusInfo.SlowHeightJumping.Matches(slowHeightJumpingEffect), "BonusInfo.SlowHeightJumping.Matches(slowHeightJumpingEffect) is incorrect!");
        Debug.Assert(BonusInfo.SlowSpeedFlying.Matches(slowSpeedFlyingEffect), "BonusInfo.SlowSpeedFlying.Matches(slowSpeedFlyingEffect) is incorrect!");
        Debug.Assert(BonusInfo.SlowSpeedRunning.Matches(slowSpeedRunningEffect), "BonusInfo.SlowSpeedRunning.Matches(slowSpeedRunningEffect) is incorrect!");
        Debug.Assert(BonusInfo.Slow.Matches(slowSpeedRunningEffect), "BonusInfo.SlowMovement.Matches(slowSpeedRunningEffect) is incorrect!");
    }

    public static void TestSumPowerEffects()
    {
        Base.Data_Classes.Archetype archetype = DatabaseAPI.GetArchetypeByName("Blaster");
        IPowerset forceField = DatabaseAPI.GetPowersetByName("Force Field", Enums.ePowerSetType.Primary);
        IPower personalForceField = forceField.Powers[0];
        float answer = DefenseAll.SumPowerEffects(personalForceField, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.525f, 0.0001f), "DefenseAll is incorrect!");

        IPowerset shieldDefense = DatabaseAPI.GetPowersetByName("Shield Defense", Enums.ePowerSetType.Primary);
        IPower deflection = shieldDefense.Powers[0];
        answer = DefenseMelee.SumPowerEffects(deflection, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "DefenseMelee is incorrect!");

        IPower battleAgility = shieldDefense.Powers[1];
        answer = DefenseRanged.SumPowerEffects(battleAgility, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "DefenseRanged is incorrect!");

        answer = DefenseAoE.SumPowerEffects(battleAgility, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.105f, 0.0001f), "DefenseAoE is incorrect!");

        IPowerset stoneArmor = DatabaseAPI.GetPowersetByName("Stone Armor", Enums.ePowerSetType.Primary);
        IPower rockArmor = stoneArmor.Powers[0];
        answer = DefenseSmashing.SumPowerEffects(rockArmor, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "DefenseSmashing is incorrect!");

        answer = DefenseLethal.SumPowerEffects(rockArmor, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "DefenseLethal is incorrect!");

        IPowerset iceArmor = DatabaseAPI.GetPowersetByName("Ice Armor", Enums.ePowerSetType.Primary);
        IPower wetIce = iceArmor.Powers[3];
        answer = DefenseFire.SumPowerEffects(wetIce, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "DefenseFire is incorrect!");

        answer = DefenseCold.SumPowerEffects(wetIce, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.007f, 0.0001f), "DefenseCold is incorrect!");

        IPower crystalArmor = stoneArmor.Powers[6];
        answer = DefenseEnergy.SumPowerEffects(crystalArmor, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "DefenseEnergy is incorrect!");

        answer = DefenseNegative.SumPowerEffects(crystalArmor, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.112f, 0.0001f), "DefenseNegative is incorrect!");

        IPower minerals = stoneArmor.Powers[7];
        answer = DefensePsionic.SumPowerEffects(minerals, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.175f, 0.0001f), "DefensePsionic is incorrect!");

        IPowerset fieryAura = DatabaseAPI.GetPowersetByName("Fiery Aura", Enums.ePowerSetType.Primary);
        IPower fireShield = fieryAura.Powers[1];
        answer = ResistanceSmashing.SumPowerEffects(fireShield, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "ResistanceSmashing is incorrect!");

        answer = ResistanceLethal.SumPowerEffects(fireShield, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "ResistanceLethal is incorrect!");

        IPower temperatureProtection = fieryAura.Powers[3];
        answer = ResistanceFire.SumPowerEffects(temperatureProtection, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "ResistanceFire is incorrect!");

        answer = ResistanceCold.SumPowerEffects(temperatureProtection, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.07f, 0.0001f), "ResistanceCold is incorrect!");

        IPower plasmaShield = fieryAura.Powers[5];
        answer = ResistanceEnergy.SumPowerEffects(plasmaShield, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "ResistanceEnergy is incorrect!");

        answer = ResistanceNegative.SumPowerEffects(plasmaShield, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.210f, 0.0001f), "ResistanceNegative is incorrect!");

        IPowerset willpower = DatabaseAPI.GetPowersetByName("Willpower", Enums.ePowerSetType.Primary);
        IPower strengthOfWill = willpower.Powers[8];
        answer = ResistancePsionic.SumPowerEffects(strengthOfWill, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.0875f, 0.0001f), "ResistancePsionic is incorrect!");

        IPower earthsEmbrace = stoneArmor.Powers[2];
        answer = ResistanceToxic.SumPowerEffects(earthsEmbrace, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.14f, 0.0001f), "ResistanceToxic is incorrect!");

        IPowerset mindControl = DatabaseAPI.GetPowersetByName("Mind Control", Enums.ePowerSetType.Primary);
        IPower mesmerize = mindControl.Powers[0];
        answer = DamageBuff.SumPowerEffects(mesmerize, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 62.5615f, 0.0001f), "Damages is incorrect!");

        IPowerset maceMastery = DatabaseAPI.GetPowersetByIndex(2765);
        IPower focusedAccuracy = maceMastery.Powers[3];
        answer = Accuracy.SumPowerEffects(focusedAccuracy, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.2f, 0.0001f), "Accuracys is incorrect!");

        IPowerset flight = DatabaseAPI.GetPowersetByName("Flight", Enums.ePowerSetType.Pool);
        IPower fly = flight.Powers[1];
        answer = Fly.SumPowerEffects(fly, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.365f, 0.0001f), "Fly is incorrect!");

        IPowerset leaping = DatabaseAPI.GetPowersetByName("Leaping", Enums.ePowerSetType.Pool);
        IPower superJump = leaping.Powers[1];
        answer = Jump.SumPowerEffects(superJump, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 2.49f, 0.0001f), "Jump is incorrect!");

        IPowerset speed = DatabaseAPI.GetPowersetByName("Speed", Enums.ePowerSetType.Pool);
        IPower superSpeed = speed.Powers[2];
        answer = Running.SumPowerEffects(superSpeed, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 3.5f, 0.0001f), "Running is incorrect!");

        IPower confuse = mindControl.Powers[3];
        answer = Confuse.SumPowerEffects(confuse, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 23.84f, 0.0001f), "Confuse is incorrect!");

        answer = MezEnhance.SumPowerEffects(confuse, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 23.84f, 0.0001f), "MezEnhance is incorrect!");

        IPower dominate = mindControl.Powers[2];
        answer = Hold.SumPowerEffects(dominate, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 14.304f, 0.0001f), "Hold is incorrect!");

        IPowerset fireControl = DatabaseAPI.GetPowersetByName("Fire Control", Enums.ePowerSetType.Primary);
        IPower ringOfFire = fireControl.Powers[2];
        answer = Immobilize.SumPowerEffects(ringOfFire, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 17.88f, 0.0001f), "Immobilize is incorrect!");

        answer = Sleep.SumPowerEffects(mesmerize, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 35.76f, 0.0001f), "Sleep is incorrect!");

        IPower flashFire = fireControl.Powers[5];
        answer = Stun.SumPowerEffects(flashFire, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 11.92f, 0.0001f), "Stun is incorrect!");

        IPower terrify = mindControl.Powers[7];
        answer = Terrorize.SumPowerEffects(terrify, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 17.88f, 0.0001f), "Terrorize is incorrect!");

        IPowerset stoneMastery = DatabaseAPI.GetPowersetByName("Stone Mastery", Enums.ePowerSetType.Ancillary);
        IPower fissure = stoneMastery.Powers[0];
        answer = Knockdown.SumPowerEffects(fissure, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.67f, 0.0001f), "Knockdown is incorrect!");

        IPowerset leadership = DatabaseAPI.GetPowersetByName("Leadership", Enums.ePowerSetType.Pool);
        IPower tactics = leadership.Powers[2];
        answer = MezResistance.SumPowerEffects(tactics, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.78715f, 0.0001f), "v is incorrect!");

        IPower dispersionBubble = forceField.Powers[5];
        answer = MezResistance.SumPowerEffects(dispersionBubble, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4844f, 0.0001f), "MezResistance is incorrect!");

        IPowerset fitness = DatabaseAPI.GetPowersetByName("Inherent Fitness", Enums.ePowerSetType.Inherent);
        IPower health = fitness.Powers[1];
        answer = MezResistance.SumPowerEffects(health, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4844f, 0.0001f), "MezResistance is incorrect!");

        IPower rooted = stoneArmor.Powers[4];
        answer = KnockbackProtection.SumPowerEffects(rooted, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 100f, 0.0001f), "KnockbackProtection is incorrect!");

        IPowerset painDomination = DatabaseAPI.GetPowersetByName("Pain Domination", Enums.ePowerSetType.Primary);
        IPower soothe = painDomination.Powers[1];
        answer = Heal.SumPowerEffects(soothe, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 188.906174f, 0.0001f), "Heal is incorrect!");

        answer = HitPoints.SumPowerEffects(earthsEmbrace, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 240.9518f, 0.0001f), "HitPoints is incorrect!");

        answer = Regeneration.SumPowerEffects(health, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.4f, 0.0001f), "Regeneration is incorrect!");

        IPower stamina = fitness.Powers[3];
        answer = Recovery.SumPowerEffects(stamina, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.25f, 0.0001f), "Recovery is incorrect!");

        IPowerset energyMastery = DatabaseAPI.GetPowersetByIndex(2735);
        IPower conservePower = energyMastery.Powers[0];
        answer = EnduranceDiscount.SumPowerEffects(conservePower, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.192f, 0.0001f), "EnduranceDiscount is incorrect!");

        IPowerset energyMastery2 = DatabaseAPI.GetPowersetByIndex(2736);
        IPower superiorConditioning = energyMastery2.Powers[0];
        answer = MaxEndurance.SumPowerEffects(superiorConditioning, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 5f, 0.0001f), "MaxEndurance is incorrect!");

        IPower hasten = speed.Powers[1];
        answer = RechargeTime.SumPowerEffects(hasten, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.7f, 0.0001f), "RechargeTime is incorrect!");

        IPowerset timeManipulation = DatabaseAPI.GetPowersetByName("Time Manipulation", Enums.ePowerSetType.Primary);
        IPower timesJuncture = timeManipulation.Powers[2];
        answer = Slow.SumPowerEffects(timesJuncture, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.36f, 0.0001f), "Slow is incorrect!");

        IPowerset traps = DatabaseAPI.GetPowersetByName("Traps", Enums.ePowerSetType.Primary);
        IPower webGrenade = traps.Powers[0];
        answer = Slow.SumPowerEffects(webGrenade, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, -0.5f, 0.0001f), "Slow is incorrect!");

        IPowerset kinetics = DatabaseAPI.GetPowersetByName("Kinetics", Enums.ePowerSetType.Primary);
        IPower speedBoost = kinetics.Powers[5];
        answer = ResistanceSlowMovement.SumPowerEffects(speedBoost, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 1.75f, 0.0001f), "ResistanceSlowMovement() is incorrect!");

        answer = ResistanceSlowRecharge.SumPowerEffects(speedBoost, archetype);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(answer, 0.5f, 0.0001f), "ResistanceSlowRecharge is incorrect!");
    }

    public static void TestSumEnhancementEffects()
    {
        I9Slot i9Slot = new I9Slot();
        i9Slot.RelativeLevel = Enums.eEnhRelative.Even;
        i9Slot.Grade = Enums.eEnhGrade.None;
        i9Slot.IOLevel = 49;
        i9Slot.Enh = (int)EnhancementType.Defense;
        float answer = DefenseAll.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseAll is incorrect!");
        answer = DefenseSmashing.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseSmashing is incorrect!");
        answer = DefenseLethal.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseLethal is incorrect!");
        answer = DefenseEnergy.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseEnergy is incorrect!");
        answer = DefenseNegative.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseNegative is incorrect!");
        answer = DefenseFire.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseFire is incorrect!");
        answer = DefenseCold.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseCold is incorrect!");
        answer = DefensePsionic.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefensePsionic is incorrect!");
        answer = DefenseAoE.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseAoE is incorrect!");
        answer = DefenseRanged.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseRanged is incorrect!");
        answer = DefenseMelee.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "DefenseMelee is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Resistance;
        answer = ResistanceCold.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceCold is incorrect!");
        answer = ResistanceFire.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceFire is incorrect!");
        answer = ResistanceEnergy.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceEnergy is incorrect!");
        answer = ResistanceNegative.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceNegative is incorrect!");
        answer = ResistanceSmashing.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceSmashing is incorrect!");
        answer = ResistanceLethal.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceLethal is incorrect!");
        answer = ResistancePsionic.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistancePsionic is incorrect!");
        answer = ResistanceToxic.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.255f, answer, 0.0001f), "ResistanceToxic is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Fly;
        answer = Fly.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Fly is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Jump;
        answer = Jump.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Jump is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Run;
        answer = Running.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Running is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Confuse;
        answer = Confuse.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Confuse is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Hold;
        answer = Hold.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Hold is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Immobilize;
        answer = Immobilize.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Immobilize is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Sleep;
        answer = Sleep.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Sleep is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Stun;
        answer = Stun.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Stun is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Fear;
        answer = Terrorize.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Terrorize is incorrect!");
        answer = MezEnhance.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "MezEnhance is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Recharge;
        answer = RechargeTime.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "RechargeTime is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Slow;
        answer = Slow.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Slow is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Heal;
        answer = Heal.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Heal is incorrect!");
        i9Slot.Enh = (int)EnhancementType.EnduranceModification;
        answer = Recovery.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Recovery is incorrect!");
        i9Slot.Enh = (int)EnhancementType.EnduranceReduction;
        answer = EnduranceDiscount.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "EnduranceDiscount is incorrect!");

        i9Slot.Enh = (int)EnhancementType.Accuracy;
        answer = Accuracy.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Accuracy is incorrect!");
        i9Slot.Enh = (int)EnhancementType.Damage;
        answer = DamageBuff.SumEnhancementEffects(i9Slot);
        Debug.Assert(BaseUtils.IsKindaSortaEqual(0.424f, answer, 0.0001f), "Damage is incorrect!");
    }
}

﻿using System.Diagnostics;

public class BaseUtils
{
    public static bool IsKindaSortaEqual(float value1, float value2, float tolerance)
    {
        float guard1 = value2 + tolerance;
        float guard2 = value2 - tolerance;
        float high = System.Math.Max(guard1, guard2);
        float low = System.Math.Min(guard1, guard2);
        return value1 <= high && value1 >= low;
    }

    public static void TestNewCode()
    {
        TestIsKindaSortaEqual();
    }

    public static void TestIsKindaSortaEqual()
    {
        float a = 1.1f;
        float b = 1.0f;
        Debug.Assert(BaseUtils.IsKindaSortaEqual(a, b, 0.1f) == true, "isKindaSortaEqual is incorrect!");
        Debug.Assert(BaseUtils.IsKindaSortaEqual(a, b, 0.01f) == false, "isKindaSortaEqual is incorrect!");
    }
}
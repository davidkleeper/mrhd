﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public partial class Enhancement : IEnhancement
{
    public IEnhancement GetSuperiorVersion()
    {
        EnhancementSet enhancementSet = GetEnhancementSet();
        if (null == enhancementSet) return null;
        if (!enhancementSet.HasSuperior()) return null;
        EnhancementSet superiorEnhancementSet = enhancementSet.GetSuperiorVersion();
        if (null == superiorEnhancementSet) return null;
        for (int enhancementIndex = 0; enhancementIndex < superiorEnhancementSet.Enhancements.Length; enhancementIndex++)
        {
            IEnhancement checkEnhancement = DatabaseAPI.GetEnhancementByIndex(superiorEnhancementSet.Enhancements[enhancementIndex]);
            if (null != checkEnhancement && checkEnhancement.Name == Name)
                return checkEnhancement;
        }
        return null;
    }

    public IEnhancement GetNonSuperiorVersion()
    {
        EnhancementSet enhancementSet = GetEnhancementSet();
        if (null == enhancementSet) return null;
        if (!enhancementSet.IsSuperior()) return null;
        EnhancementSet nonSuperiorEnhancementSet = enhancementSet.GetNonSuperiorVersion();
        if (null == nonSuperiorEnhancementSet) return null;
        for (int enhancementIndex = 0; enhancementIndex < nonSuperiorEnhancementSet.Enhancements.Length; enhancementIndex++)
        {
            IEnhancement checkEnhancement = DatabaseAPI.GetEnhancementByIndex(nonSuperiorEnhancementSet.Enhancements[enhancementIndex]);
            if (null != checkEnhancement && checkEnhancement.Name == Name)
                return checkEnhancement;
        }
        return null;
    }

    public EnhancementType[] GetEnhancementTypes()
    {
        List<EnhancementType> result = new List<EnhancementType>();
        foreach (Enums.sEffect effect in Effect)
        {
            EnhancementType[] enhancementTypes = EnumUtils.Convert(effect.Enhance, effect.BuffMode);
            result.AddRange(enhancementTypes);
        }
        return result.ToArray();
    }

    public string GetLoadString(int level = 49, string delimiter = ":")
    {
        // Set DisplayName(Can be empty):Enhancement Short Name:Type(enum eType):Fallback:Relative Level:Grade(enum eEnhGrade):IO Level
        EnhancementSet set = GetEnhancementSet();
        string loadString = ((null == set) ? "" : set.DisplayName) + delimiter;
        loadString += ShortName + delimiter;
        loadString += (int)TypeID + delimiter;
        loadString += "-1" + delimiter;
        loadString += "4" + delimiter;
        loadString += "0" + delimiter;
        loadString += level.ToString();
        return loadString;
    }

    public EnhancementSet GetEnhancementSet()
    {
        foreach (EnhancementSet enhancementSet in DatabaseAPI.Database.EnhancementSets)
        {
            if (enhancementSet.Uid == UIDSet)
                return enhancementSet;
        }
        return null;
    }

    public bool HasEffect(Enums.eEnhance effectType)
    {
        foreach (Enums.sEffect effect in Effect)
        {
            if (effect.Enhance.ID == (int)effectType)
                return true;
        }
        return false;
    }

    // Call this after all database info has loaded, such as at the end of LoadEnhancementDb()
    public static void TestNewCode()
    {
        TestGetEnhancementTypes();
        TestGetLoadSring();
        TestGetEnhancementSet();
        TestHasEffect();
        TestSuperior();
    }

    public static void TestGetEnhancementTypes()
    {
        for (int enhancementId = (int)EnhancementType.Accuracy; enhancementId <= (int)EnhancementType.ToHitDebuff; enhancementId++)
        {
            IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementId);
            EnhancementType[] types = enhancement.GetEnhancementTypes();
            EnhancementType type = types[0];
            Debug.Assert(types.Any(item => (int)item == enhancementId), "GetEnhancementTypes " + enhancementId + " is incorrect!");
        }
    }

    public static void TestGetLoadSring()
    {
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(200);
        string loadString = enhancement.GetLoadString();
        Debug.Assert(loadString == "Executioner's Contract:Stun%:4:-1:4:0:49", "loadString is incorrect!");

        enhancement = DatabaseAPI.GetEnhancementByIndex(26);
        loadString = enhancement.GetLoadString();
        Debug.Assert(loadString == ":Acc:2:-1:4:0:49", "loadString is incorrect!");

        IPowerset powerset = DatabaseAPI.GetPowersetByName("Fire Blast", Enums.ePowerSetType.Primary);
        PowerEntry fireball = new PowerEntry(1, powerset.Powers[2]);
        string acc = DatabaseAPI.GetEnhancementByIndex(26).GetLoadString();
        string dam = DatabaseAPI.GetEnhancementByIndex(29).GetLoadString();
        string rech = DatabaseAPI.GetEnhancementByIndex(43).GetLoadString();
        fireball.Slot(new int[] { 26, 26, 26, 26, 26, 26 });
        fireball.Slots[0].LoadFromString(acc, ":");
        fireball.Slots[1].LoadFromString(dam, ":");
        fireball.Slots[2].LoadFromString(dam, ":");
        fireball.Slots[3].LoadFromString(dam, ":");
        fireball.Slots[4].LoadFromString(rech, ":");
        fireball.Slots[5].LoadFromString(rech, ":");
        Debug.Assert(fireball.Slots[0].Enhancement.Enh == 26, "toon.Level2.Slots[0].Enhancement.Enh is incorrect!");
        Debug.Assert(fireball.Slots[1].Enhancement.Enh == 29, "toon.Level2.Slots[1].Enhancement.Enh is incorrect!");
        Debug.Assert(fireball.Slots[2].Enhancement.Enh == 29, "toon.Level2.Slots[2].Enhancement.Enh is incorrect!");
        Debug.Assert(fireball.Slots[3].Enhancement.Enh == 29, "toon.Level2.Slots[3].Enhancement.Enh is incorrect!");
        Debug.Assert(fireball.Slots[4].Enhancement.Enh == 43, "toon.Level2.Slots[4].Enhancement.Enh is incorrect!");
        Debug.Assert(fireball.Slots[5].Enhancement.Enh == 43, "toon.Level2.Slots[5].Enhancement.Enh is incorrect!");
    }

    public static void TestGetEnhancementSet()
    {
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(0);
        EnhancementSet enhancementSet = enhancement.GetEnhancementSet();
        Debug.Assert(enhancementSet == null, "enhancementSet is not null!");

        enhancement = DatabaseAPI.GetEnhancementByIndex(200);
        enhancementSet = enhancement.GetEnhancementSet();
        Debug.Assert(enhancementSet != null, "enhancementSet is null!");
        Debug.Assert(enhancementSet.DisplayName == "Executioner's Contract", "enhancementSet.DisplayName is incorrect!");
    }

    public static void TestHasEffect()
    {
        IEnhancement acc = DatabaseAPI.GetEnhancementByIndex(26);
        IEnhancement dam = DatabaseAPI.GetEnhancementByIndex(29);
        IEnhancement rech = DatabaseAPI.GetEnhancementByIndex(43);
        IEnhancement crushingImpactAccDamEnd = DatabaseAPI.GetEnhancementByIndex(100);
        Debug.Assert(acc.HasEffect(Enums.eEnhance.Accuracy) == true, "HasEffect(acc, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(acc.HasEffect(Enums.eEnhance.Damage) == false, "HasEffect(acc, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(dam.HasEffect(Enums.eEnhance.Damage) == true, "HasEffect(dam, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(dam.HasEffect(Enums.eEnhance.Accuracy) == false, "HasEffect(dam, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(rech.HasEffect(Enums.eEnhance.RechargeTime) == true, "HasEffect(rech, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(rech.HasEffect(Enums.eEnhance.Accuracy) == false, "HasEffect(rech, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(crushingImpactAccDamEnd.HasEffect(Enums.eEnhance.Accuracy) == true, "HasEffect(crushingImpactAccDamEnd, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(crushingImpactAccDamEnd.HasEffect(Enums.eEnhance.Damage) == true, "HasEffect(crushingImpactAccDamEnd, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(crushingImpactAccDamEnd.HasEffect(Enums.eEnhance.EnduranceDiscount) == true, "HasEffect(crushingImpactAccDamEnd, Enums.eEffectType.Accuracy) is incorrect!");
        Debug.Assert(crushingImpactAccDamEnd.HasEffect(Enums.eEnhance.RechargeTime) == false, "HasEffect(crushingImpactAccDamEnd, Enums.eEffectType.Accuracy) is incorrect!");
    }

    public static void TestSuperior()
    {
        EnhancementSet enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Winter's Bite"));
        IEnhancement enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementSet.Enhancements[0]);
        Debug.Assert(enhancement.GetSuperiorVersion().Name == enhancement.Name, "enhancement.GetSuperiorVersion().Name is incorrect!");
        Debug.Assert(enhancement.GetNonSuperiorVersion() == null, "enhancement.GetNonSuperiorVersion() is incorrect!");
        Debug.Assert(enhancement.GetSuperiorVersion().UIDSet != enhancement.UIDSet, "enhancement.GetSuperiorVersion().UIDSet != enhancement.UIDSet is incorrect!");

        enhancementSet = DatabaseAPI.GetEnhancementSetByIndex(DatabaseAPI.GetEnhancementSetIndexByName("Superior Winter's Bite"));
        enhancement = DatabaseAPI.GetEnhancementByIndex(enhancementSet.Enhancements[0]);
        Debug.Assert(enhancement.GetSuperiorVersion() == null, "enhancement.GetSuperiorVersion() is incorrect!");
        Debug.Assert(enhancement.GetNonSuperiorVersion().Name == enhancement.Name, "enhancement.GetNonSuperiorVersion().Name is incorrect!");
        Debug.Assert(enhancement.GetNonSuperiorVersion().UIDSet != enhancement.UIDSet, "enhancement.GetNonSuperiorVersion().UIDSet != enhancement.UIDSet is incorrect!");
    }
}
